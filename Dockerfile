
FROM openjdk:8
RUN apt-get update && apt-get install -y curl && apt install -y maven
ARG BUILD_TIMESTAMP=time
RUN mkdir BUILDSPACE
WORKDIR BUILDSPACE
COPY . .
RUN mvn clean install -DskipTests=true

# Module: Configurator

RUN echo "*********************************** Building Module: Configurator **********************************************"
ENV CONFIGURATOR_URL="https://artifactory.getlend.in/artifactory/lendin-artifacts/product-team/lendin-core/stable/""${BUILD_TIMESTAMP}""/configurator-0.0.1-SNAPSHOT.war"
RUN echo $CONFIGURATOR_URL
RUN curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ./modules/configurator/target/configurator-0.0.1-SNAPSHOT.war ""${CONFIGURATOR_URL}"" --insecure
RUN curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ./modules/configurator/target/configurator-0.0.1-SNAPSHOT.war "https://artifactory.getlend.in/artifactory/lendin-artifacts/product-team/lendin-core/stable/latest/configurator-0.0.1-SNAPSHOT.war" --insecure

# Module: wf-implementation
RUN echo "*********************************** Building Module: WF-IMPLEMENTATION **********************************************"
ENV WF_IMPLEMENTATION="https://artifactory.getlend.in/artifactory/lendin-artifacts/product-team/lendin-core/stable/""${BUILD_TIMESTAMP}""/wf-implementation-0.0.1-SNAPSHOT.war"
RUN echo $WF_IMPLEMENTATION
RUN curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ./modules/wf-implementation/target/wf-implementation-0.0.1-SNAPSHOT.war ""${WF_IMPLEMENTATION}"" --insecure
RUN curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ./modules/wf-implementation/target/wf-implementation-0.0.1-SNAPSHOT.war "https://artifactory.getlend.in/artifactory/lendin-artifacts/product-team/lendin-core/stable/wf-implementation-SNAPSHOT.war" --insecure


