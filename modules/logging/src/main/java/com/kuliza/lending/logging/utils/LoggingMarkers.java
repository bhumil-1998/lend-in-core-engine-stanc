package com.kuliza.lending.logging.utils;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/** @author manjunathch */
public class LoggingMarkers {
  public static final Marker JSON = MarkerFactory.getMarker("JSON-MASK");
}

