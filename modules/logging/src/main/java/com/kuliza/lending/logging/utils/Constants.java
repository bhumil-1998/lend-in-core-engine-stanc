package com.kuliza.lending.logging.utils;

import java.util.HashMap;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;

public class Constants {
	public static final String JSON_LOGGING_MASK_KEYS = "private|creditCard|aadhaar|password|pan|panNumber|cvv|email|mobileNumber|phoneNumber|otp|";
	public static final String JSON_PATTERN = "\"(" + "%s" + ")\" ?: ?\"([^\"]+)\"";
	private static final String LOG_REQUEST_HEADER_NAMES = "accept,accept-encoding";
	public static final String JSON_REPLACEMENT_REGEX = "\"$1\": \"******\"";
	public static final String BASE64_REPLACEMENT_REGEX = "******";
	public static final String BASE64_REGEX = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$";
	public static String getLogRequestHeaderNames() {
		return LOG_REQUEST_HEADER_NAMES;
	}
	public static final String DEFAULT_RESULT_STRING = "Result is empty or byteArray or non serializable class such as HttpResponse, so not logged";
	public static final String LOGGER_POINTCUT_VALUE = "com.kuliza.lending.logging.logaspects.LogPointcuts.logAnnotationPointcut()";
}
