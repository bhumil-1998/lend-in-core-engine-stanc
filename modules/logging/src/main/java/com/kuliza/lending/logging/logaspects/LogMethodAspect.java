package com.kuliza.lending.logging.logaspects;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.logging.utils.Constants;
import com.kuliza.lending.logging.utils.LogType;
import javax.servlet.http.HttpServletResponse;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogMethodAspect {

  @Value("${MethodParameterNamesToBeSkipped}")
  private String methodParametersToBeSkipped;

  @Around(Constants.LOGGER_POINTCUT_VALUE)
  public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
    final long start = System.currentTimeMillis();
    final Object proceed = joinPoint.proceed();
    final long executionTime = System.currentTimeMillis() - start;
    String str = joinPoint.getSignature() + " executed in " + executionTime + "ms";
    LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.EXITING_METHOD, str,
        methodParametersToBeSkipped);
    return proceed;
  }

  @Before(Constants.LOGGER_POINTCUT_VALUE)
  public void logMethodArguments(JoinPoint joinPoint) throws Throwable {
    LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.ENTERING_METHOD, null,
        methodParametersToBeSkipped);
  }

  @AfterReturning(value = Constants.LOGGER_POINTCUT_VALUE , returning = "result")
  public void logMethodResult(JoinPoint joinPoint, Object result) throws Throwable {
    ObjectMapper objectMapper = new ObjectMapper();
    String finalResponse = Constants.DEFAULT_RESULT_STRING;
		if(result!=null && !(result instanceof com.mashape.unirest.http.HttpResponse)
				&& !(result instanceof HttpServletResponse) && !(result instanceof byte[])) {
			finalResponse = objectMapper.writeValueAsString(result);
		}
    LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.EXITING_METHOD, finalResponse,
        methodParametersToBeSkipped);
  }

  @AfterThrowing(value = Constants.LOGGER_POINTCUT_VALUE, throwing = "exception")
  public void logMethodException(JoinPoint joinPoint, Throwable exception) throws Throwable {
    LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.EXCEPTION, exception,
        methodParametersToBeSkipped);
  }
}
