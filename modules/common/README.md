***
# About Common module

*    This module contains all the generic methods.
*    Generic methods means the methods that work irrespective of the module (for example : convert json object to excel sheet).
*    All the methods written in this module are static.
*    Dependency of this module is present in all modules(pom.xml) except logging module.
*    All these methods are written in CommonHelperFunctions.java in common module.
*    There is a file HttpConnection.java in connection package which is used for establishing http connection.
*    UnirestConnection.java is used for calling one microservice from another.
