package com.kuliza.lending.common;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommonUtilsTest {
    @Test
    public void correctEmailValidationTest(){
        Assert.assertEquals(CommonHelperFunctions.validateEmailString(CommonTestConstants.validEmailAddress), Boolean.TRUE);
    }

    @Test
    public void incorrectEmailValidationTest(){
        Assert.assertEquals(CommonHelperFunctions.validateEmailString(CommonTestConstants.invalidEmailAddress), Boolean.FALSE);
    }

    @Test
    public void correctStringValidationTest(){
        Assert.assertEquals(CommonHelperFunctions.validateEmailString(CommonTestConstants.validString), Boolean.TRUE);
    }

    @Test
    public void incorrectStringValidationTest(){
        Assert.assertEquals(CommonHelperFunctions.validateEmailString(CommonTestConstants.invalidString), Boolean.FALSE);
    }
}