package com.kuliza.lending.common;

public interface CommonTestConstants {
    String validEmailAddress = "info@getlend.in";
    String invalidEmailAddress = "getlend.in";
    String validString = "getlend.in";
    String invalidString = null;
}
