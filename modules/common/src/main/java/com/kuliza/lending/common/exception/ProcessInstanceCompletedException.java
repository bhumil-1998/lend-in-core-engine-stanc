package com.kuliza.lending.common.exception;

public class ProcessInstanceCompletedException extends Exception {

	public ProcessInstanceCompletedException(String processInstanceId) {
		super("This process instance : " + processInstanceId
				+ " has ended. Please use history services to query any data.");
	}

}
