package com.kuliza.lending.common.connection;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;

public class UnirestConnection {

    private static final Logger logger = LoggerFactory.getLogger(UnirestConnection.class);
    public static HttpResponse sendPOST(Map<String, Object> request, Map<String, String> headers, String url){
        HttpResponse httpResponse = null;
        try{
             httpResponse = Unirest.post( url).
                    headers(headers).
                    body(CommonHelperFunctions.toJson(request)).asString();
        } catch (Exception e){
            logger.warn("==========sendPOST==========exception : " + e);
        }

        return httpResponse;
    }

    public static HttpResponse sendDELETE(Map<String, Object> request, Map<String, String> headers, String url){

        HttpResponse httpResponse = null;
        try {
            httpResponse = Unirest.delete(url).headers(headers).
                    body(CommonHelperFunctions.toJson(request)).asString();
        } catch (Exception e){
            logger.warn("==========sendDELETE==========exception : " + e);
        }

        return httpResponse;
    }


    public static HttpResponse sendGET(Map<String, String> headers, String url){

        HttpResponse httpResponse = null;
        try {
            httpResponse = Unirest.get(url).headers(headers).asString();
        } catch (Exception e){
            logger.warn("==========sendGET==========exception : " + e);
        }

        return httpResponse;
    }


    public static HttpResponse sendPOST(JSONArray request, Map<String, String> headers, String url){

        HttpResponse httpResponse = null;
        try {
            httpResponse = Unirest.post(url).
                    headers(headers).body(request).asString();
        } catch (Exception e){
            logger.warn("==========sendPOST==========exception : " + e);
        }

        return httpResponse;
    }

    public static HttpResponse sendPOST(JSONObject jsonObject, Map<String, String> headers, String url){

        HttpResponse httpResponse = null;
        try {
            httpResponse = Unirest.post(url).
                    headers(headers).body(jsonObject).asString();
        } catch (Exception e){
            logger.warn("==========sendPOST==========exception : " + e);
        }

        return httpResponse;
    }

    public static HttpResponse sendPUT(Map<String, Object> request, Map<String, String> headers, String url){

        HttpResponse httpResponse = null;
        try {
            httpResponse = Unirest.put(url).
                    headers(headers).
                    body(CommonHelperFunctions.toJson(request)).asString();
        } catch (Exception e){
            logger.warn("==========sendPUT==========exception : " + e);
        }

        return httpResponse;
    }

    public static HttpResponse sendPUT(JSONArray request, Map<String, String> headers, String url){

        HttpResponse httpResponse = null;
        try {
            httpResponse = Unirest.put(url).
                    headers(headers).body(request).asString();
        } catch (Exception e){
            logger.warn("==========sendPUT==========exception : " + e);
        }

        return httpResponse;
    }


}