package com.kuliza.lending.common.filter;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Component
public class RequestUUIDAssignmentFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Just Empty
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		MDC.put("requestId", UUID.randomUUID().toString());
		chain.doFilter(request, response);
		MDC.remove("requestId");
	}

	@Override
	public void destroy() {
		// Just Empty
	}

}
