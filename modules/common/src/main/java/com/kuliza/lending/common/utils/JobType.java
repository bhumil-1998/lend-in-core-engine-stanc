package com.kuliza.lending.common.utils;

public enum JobType {

	INBOUND_API, SERVICE_TASK, OUTBOUND_API

}
