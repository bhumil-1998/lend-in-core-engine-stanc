package com.kuliza.lending.common.exceptionhandling;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import org.springframework.util.StringUtils;

public class EntityNotFoundException extends Exception {

  private static Object searchParamsMap;
  private static final Map<String, String> MAP = toMap(String.class, String.class, searchParamsMap);
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public EntityNotFoundException(Class<?> clazz, String... searchParamsMap) {
    super(
        EntityNotFoundException.generateMessage(clazz.getSimpleName(), extracted(searchParamsMap)));
  }

  private static Map<String, String> extracted(String... searchParamsMap) {
    return MAP;
  }

  private static String generateMessage(String entity, Map<String, String> searchParams) {
    return StringUtils.capitalize(entity) + " was not found for parameters " + searchParams;
  }

  private static <K, V> Map<K, V> toMap(Class<K> keyType, Class<V> valueType, Object... entries) {
    if (entries.length % 2 == 1) {
      throw new IllegalArgumentException("Invalid entries");
    }
    return IntStream.range(0, entries.length / 2)
        .map(i -> i * 2)
        .collect(
            HashMap::new,
            (m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])),
            Map::putAll);
  }
}

