package com.kuliza.lending.backoffice.exceptions;

public class RoleNotAvailableException extends RuntimeException {

	public RoleNotAvailableException() {
		super();
	}

	public RoleNotAvailableException(String message) {
		super(message);
	}
}
