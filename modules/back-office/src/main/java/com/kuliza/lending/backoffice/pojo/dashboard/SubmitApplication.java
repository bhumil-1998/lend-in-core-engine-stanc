package com.kuliza.lending.backoffice.pojo.dashboard;

import javax.validation.constraints.NotNull;

public class SubmitApplication {

	@NotNull(message = "outcome cannot be null.")
	private String outcome;

	@NotNull(message = "comment cannot be null.")
	private String comment;

	public SubmitApplication() {
		super();
	}

	public SubmitApplication(
			@NotNull(message = "outcome cannot be null.") String outcome,
			String comment) {
		super();
		this.outcome = outcome;
		this.comment = comment;
	}

	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
