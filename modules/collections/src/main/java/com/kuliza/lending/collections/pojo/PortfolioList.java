package com.kuliza.lending.collections.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.kuliza.lending.collections.utils.CollectionConstants;

import java.util.List;

/*
    * This is the model for accepting input for the creation/updation of a decision table
    * The user can create a new decision table(version 1), update the current version or add a new version of
      the existing decision table.
*/
public class PortfolioList {

	@NotNull(message = "Portfolios Cannot be null")
	@Size(min = CollectionConstants.LIST_MIN_SIZE)
	List<Portfolio> portfolios;

	public PortfolioList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PortfolioList(List<Portfolio> portfolios) {
		super();
		this.portfolios = portfolios;
	}

	public List<Portfolio> getPortfolios() {
		return portfolios;
	}

	public void setPortfolios(List<Portfolio> portfolios) {
		this.portfolios = portfolios;
	}


}
