package com.kuliza.lending.collections.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.collections.pojo.Portfolio;
import com.kuliza.lending.collections.pojo.PortfolioList;
import com.kuliza.lending.collections.services.CollectionEngineService;
import com.kuliza.lending.collections.utils.CollectionConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(CollectionConstants.COLLECTION_ENGINE_URL)
public class CollectionEngineController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CollectionEngineController.class);

	@Autowired
	private CollectionEngineService collectionEngineService;

	@Autowired
	private ObjectMapper objectMapper;

	@PostMapping(CollectionConstants.ONBOARD_PORTFOLIO_URL)
	public ResponseEntity<Object> onboardPortfolio(@Valid @RequestBody Portfolio request) throws Exception {
		return CommonHelperFunctions
				.buildResponseEntity(collectionEngineService.onboardPortfolio(request));

	}
	
	@PostMapping(CollectionConstants.ONBOARD_BULK_PORTFOLIO_URL)
	public ResponseEntity<Object> onboardBulkPortfolio(@Valid @RequestBody PortfolioList portfolioList) throws Exception {
		for (Portfolio portfolio : portfolioList.getPortfolios()) {
			collectionEngineService.onboardPortfolio(portfolio);
		}
		return CommonHelperFunctions
				.buildResponseEntity(new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE));

	}

}
