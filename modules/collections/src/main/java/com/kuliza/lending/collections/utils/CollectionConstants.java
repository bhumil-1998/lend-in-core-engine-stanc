package com.kuliza.lending.collections.utils;


import java.util.HashMap;
import java.util.Map;

public class CollectionConstants {
    private CollectionConstants() {
    	
        throw new UnsupportedOperationException();
    }

    // Controller URLs
    public static final String DECISION_TABLE_URL = "/decision-table/";
    public static final String CREATE_DECISION_TABLE_URL = "create";
    public static final String DEPLOY_DECISION_TABLE_URL = "deploy";
    public static final String FETCH_DECISION_TABLE_URL = "fetch";
    public static final String DELETE_DECISION_TABLE_URL = "delete";
    
    
    public static final String COLLECTION_ENGINE_URL = "/lending/collection/engine/";
    public static final String ONBOARD_PORTFOLIO_URL = "onboard-portfolio";
    public static final String ONBOARD_BULK_PORTFOLIO_URL = "onboard-bulk-portfolios";
    // POJO Constants
    
    public static final int AMOUNT_MIN_VALUE = 0;
    public static final int LIST_MIN_SIZE = 1;
    
    public static final Map<String, String> aggregationConfig;
	static {
		aggregationConfig = new HashMap<String, String>();
		aggregationConfig.put("disbursedAmount", "sum");
		aggregationConfig.put("dueAmount", "sum");
		aggregationConfig.put("currentEmi", "sum");
		aggregationConfig.put("outstandingAmount", "sum");
		aggregationConfig.put("dpd", "max");
	}
    
	public static final String COLLECTION_CMMN_CASE_NAME = "collections";
	public static final String COLLECTION_PORTAL_TYPE = "collection";
    
}
