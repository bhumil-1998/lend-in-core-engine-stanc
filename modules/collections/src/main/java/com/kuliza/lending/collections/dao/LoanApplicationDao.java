package com.kuliza.lending.collections.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.collections.model.LoanApplicationModel;
import com.kuliza.lending.collections.model.PortfolioModel;

@Repository
public interface LoanApplicationDao extends CrudRepository<LoanApplicationModel, Long> {

	public LoanApplicationModel findById(long id);

	public LoanApplicationModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public LoanApplicationModel findByLoanApplicationIdAndPortfolioModelAndIsDeleted(String loanApplicatonId, PortfolioModel portfolioModel, boolean isDeleted);

}
