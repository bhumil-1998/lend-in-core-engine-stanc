***
# About Authorization module 

*    When a user login, the user is authenticated. Once the user is logged in, after that all the requests are authorized. This module is all about that. For authentication and authorization we are using spring security and keycloak.
*    IAM is an open source software product to allow single sign-on with Identity Management and Access Management aimed at modern applications and services. 
    
**Authorization modules operations includes below tasks:**

*    User authentication
*    Issuance and validation of Access Token and Refresh Token
*    User session management
*    Creation/Management of users
    
**Prerequisites for this module is to have IAM/Keycloak running.**

*    IAM/keycloak properties in application.properties file.
    
**Give the values of these properties according to your IAM/Keycloak configuration**  

        keycloak.host=http://35.200.333.222:8009/    
        keycloak.client-id=lending    
        keycloak.client-secret=c927f002-fd9e-42p1-b374-7f9541358b44
        keycloak.realm=kuliza_realm
        keycloak.admin-username=<your keycloak username>
        keycloak.admin-email=<your keycloak email>
        keycloak.admin-password=<your keycloak password>
        keycloak.allowed-origins=*

        ############### max number of Active sessions and offline sessions #############

        keycloak.offline-sessions=1
        keycloak.active-sessions=1

