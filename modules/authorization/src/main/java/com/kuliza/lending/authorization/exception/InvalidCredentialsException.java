package com.kuliza.lending.authorization.exception;

public class InvalidCredentialsException extends RuntimeException {

  private static final long serialVersionUID = 7169518961042958739L;

  public InvalidCredentialsException(String message) {
    super(message);
  }

}
