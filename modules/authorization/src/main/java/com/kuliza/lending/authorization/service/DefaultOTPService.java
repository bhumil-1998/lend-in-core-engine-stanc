package com.kuliza.lending.authorization.service;

import com.kuliza.lending.authorization.config.OTPConfig;
import com.kuliza.lending.authorization.config.WfImplConfig;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DefaultOTPService {

  @Autowired
  private OTPService otpService;

  @Autowired
  private OTPServiceWithCache otpServiceWithCache;

  @Autowired
  private OTPConfig otpConfig;

  @Autowired
  private WfImplConfig wfImplConfig;

  private static final Logger logger = LoggerFactory.getLogger(DefaultOTPService.class);

  protected ApiResponse sendOTP(String mobileNumber, String otp) {
    return null;
  }

  private ValidationResponse generateOTP(HttpServletRequest request,
      UserPasswordGenerationForm userForm) {
    if(wfImplConfig.getGenerateOTP()){
      if (otpConfig.getUseCacheForOTP()) {
        return otpServiceWithCache.otpGenerate(request, userForm);
      } else {
        return otpService.otpGenerate(request, userForm);
      }
    }
    return new ValidationResponse(true, AuthenticationConstants.OTP_SKIP);
  }

  public ApiResponse generateAndSendPassword(HttpServletRequest request,
      UserPasswordGenerationForm userForm) {
    return null;
  }

  @LogMethodDetails
  public ValidationResponse validateUser(HttpServletRequest request,
      UserValidationinputForm validationForm) {
    if (otpConfig.getUseCacheForOTP()) {
      return otpServiceWithCache.checkBlockedOrValidateOTP(request, validationForm);
    }
    return otpService.checkBlockedOrValidateOTP(request, validationForm);
  }

  @LogMethodDetails
  public ApiResponse generateUserPassword(HttpServletRequest request,
      UserPasswordGenerationForm userForm) throws Exception {
    ValidationResponse response = this.generateOTP(request, userForm);
    if (response != null && response.isConducive()) {
      return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
    }
    return new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase(),
        response != null ? response.getMessage() : null);
  }
}
