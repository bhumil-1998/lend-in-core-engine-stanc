package com.kuliza.lending.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.common.utils.Constants.DeviceType;

public class UserMPINInputForm {

	@NotNull(message = "MPIN cannot be null")
	@Size(min = 4, max = 6)
	@Pattern(regexp = "[0-9]+")
	String mpin;
	
	@NotNull(message = "Device Id cannot be null")
	@NotEmpty(message = "Device Id cannot be empty")
	String deviceId;
	
	@NotNull(message = "Device type cannot be null")
	DeviceType deviceType;
	
	public UserMPINInputForm() {
		super();
	}

	public UserMPINInputForm(String mpin, String deviceId,
			DeviceType deviceType) {
		super();
		this.mpin = mpin;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

}
