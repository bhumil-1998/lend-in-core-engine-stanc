package com.kuliza.lending.authorization.constants;

public interface Constants {

  // Sample API Parameters
  String GROUP_ID_EXAMPLE = "a2db397c-3e93-4681-be30-69291d1e9315";
  String ENCODING_SUCCESS_MESSAGE = "Journey users passwords encoded successfully";
  String RecoverPasswordOTPMailContent = "<html><body><table><tr><span style='font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif;font-size:16px;line-height:32px;color:#3b5998'> Hi {0},<br> We received a request to reset your account password. Enter the following password reset code: <b>{1}</b></span>';</tr><tr><td width='100px' align='left' valign='middle' style='font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif;font-size:19px;line-height:32px;color:#3b5998'><h3>-Lend.In</h3></td></tr></table></body></html>";
  String RecoverPasswordOTPMailSubject = "{0} is your account recovery code";
  String AuthCodeFormat = "{0}_{1}";
  String EmailOTPGeneratedSuccessMessage = "OTP has been sent successfully";
  String UserPasswordChanged = "Your password has been reset successfully";
  int otpCountdown = 30;
}
