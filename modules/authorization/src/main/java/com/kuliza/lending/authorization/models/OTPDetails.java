package com.kuliza.lending.authorization.models;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;

@Entity
@Table(name = "otp_details")
@ConditionalOnExpression("${otp.useCacheForOTP:false}")
public class OTPDetails extends AbstractOTPDetails {
//	Please add your fields here if you want to extend otp details table.

  public OTPDetails() {
    super();
  }

  public OTPDetails(String otpHash, String userIdentifier, String ipAddress,
      String secretKey, long otpGenerationTime, String userStageTag) {
    super(otpHash, userIdentifier, ipAddress, secretKey, otpGenerationTime, userStageTag);
  }

  public OTPDetails(String userIdentifier, String ipAddress, String secretKey,
      long otpGenerationTime, String userStageTag) {
    super(userIdentifier, ipAddress, secretKey, otpGenerationTime, userStageTag);
  }
}
