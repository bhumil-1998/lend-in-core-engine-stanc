package com.kuliza.lending.authorization.utils;

public class AuthenticationConstants {

  // OTP Constants
  public static final String OTP_SKIP = "OTP Skip";
  public static final String OTP_GENERATION_TIME_KEY = "otpGenerationTime";
  public static final long ONE_MINUTE_TO_MILLIS = 60000;
  public static final String OTP_INVALID = "Invalid OTP for %s!";
  public static final String USER_BLOCKED = "Blocked for %s minutes.";
  public static final String USER_WRONG_DETAILS = "User entered wrong details.";
  public static final String OTP_VALIDATE_SKIP_SUCCESS = "Skipping OTP validation based on environment flag for : %s";
  public static final String OTP_VALIDATE_SUCCESS = "OTP is Valid";
  public static final String OTP_NEW_OTP_GENERATE_MESSAGE = "User needs to generate otp again";
  public static final String OTP_GENERATE_SKIP_SUCCESS = "Skipping OTP generation based on environment flag for : %s";
  public static final String OTP_TIME_DETAILS = "User Block Time: %s, User OTP Threshold Time: %s";
  public static final String OTP_DEFAULT_FORMAT = "MMddyy";

  public static final String USER_NOT_FOUND = "User with this token not found.";
  public static final String USER_DEVICE_NOT_REGISTERED = "User Mpin Not Set.";
  public static final String MPIN_SET_SUCCESS_MESSAGE = "MPIN Set Successfully.";
  public static final String MPIN_VALIDATION_SUCCESS_MESSAGE = "MPIN Validation Successfull.";
  public static final String INVALID_MPIN_ERROR_MESSAGE = "Invalid MPIN.";

  public enum UserIdTypes{
    email, mobile;
  }

}
