package com.kuliza.lending.pojo;

import org.hibernate.validator.constraints.NotEmpty;

public class RecoverPasswordOtpValidateApiView {

    @NotEmpty
    private String email;

    @NotEmpty
    private String otp;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public RecoverPasswordOtpValidateApiView(String email, String otp) {
        this.email = email;
        this.otp = otp;
    }

    public RecoverPasswordOtpValidateApiView() {
    }
}



