package com.kuliza.lending.authorization.service;


import static com.kuliza.lending.authorization.utils.OTPUtil.checkOptionalOTP;

import com.kuliza.lending.authorization.config.OTPConfig;
import com.kuliza.lending.authorization.config.WfImplConfig;
import com.kuliza.lending.authorization.config.keycloak.Secret;
import com.kuliza.lending.authorization.config.keycloak.TOTPManager;

import com.kuliza.lending.authorization.dao.OTPDetailsDao;
import com.kuliza.lending.authorization.models.OTPDetails;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.authorization.utils.OTPUtil;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class OTPService {

  private TOTPManager manager;
  private OTPDetailsDao otpDetailsDao;
  private OTPConfig otpConfig;
  private WfImplConfig wfImplConfig;
  private Integer otpLength;


  private static final Logger logger = LoggerFactory.getLogger(OTPService.class);

  @Autowired
  public OTPService(final OTPDetailsDao otpDetailsDao, final OTPConfig otpConfig,
      final WfImplConfig wfImplConfig,
      @Value("${otp.otpLength}") final Integer otpLength,
      @Value("${otp.otpExpiryTimeInMinutes}") final Integer otpExpiryTime) {
    super();
    this.otpDetailsDao = otpDetailsDao;
    this.otpConfig = otpConfig;
    this.wfImplConfig = wfImplConfig;
    this.otpLength = otpLength;
    this.manager = new TOTPManager(otpLength, otpExpiryTime * 60);
  }

  /**
   * Generates the OTP and sends it to user
   *
   * @return BaseValidationResponse
   */
  protected ValidationResponse otpGenerate(HttpServletRequest request,
      UserPasswordGenerationForm userForm) {
    printDetails();
    String mobileNumber = userForm.getUsername();
    String userStageTag = userForm.getUserStageTag();
    logger.info("OTP generate: " + mobileNumber);
    long currentTime = new Date().getTime();
    String deviceIdentifier = OTPUtil.getDeviceIdentifier(request, userForm.getDeviceId());
    OTPDetails otpDetails = otpDetailsDao
        .findTopByUserIdentifierAndUserStageTagAndIsDeletedOrderByIdDesc(mobileNumber, userStageTag,
            false);
    long userBlockTime = otpDetails != null && otpDetails.getUserBlockTime() != null ? otpDetails
        .getUserBlockTime().getTime() :
        Long.MAX_VALUE;
    long otpThresholdTime = otpDetails != null ? otpDetails.getOtpGenerationTime() + (
        otpConfig.getOtpThresholdInMinutes() * AuthenticationConstants.ONE_MINUTE_TO_MILLIS)
        : Long.MAX_VALUE;
    if (otpDetails == null || (otpDetails != null && ((userBlockTime < currentTime) || (
        otpDetails.getOtpGenerateCount() < otpConfig.getMaxOtpGenerateAttempts())))) {
      long otpTime = System.currentTimeMillis();
      byte[] secret = Secret.generate();
      String otp = manager.generate(secret);
      String key = Secret.toHex(secret);
      logger.info(key);
      logger.info(
          String.format(AuthenticationConstants.OTP_TIME_DETAILS, userBlockTime, otpThresholdTime));
      if (otpDetails == null || (userBlockTime < currentTime) || (userBlockTime == Long.MAX_VALUE
          && otpThresholdTime < currentTime)) {
        // New row for otp
        if (otpDetails != null && ((userBlockTime < currentTime) || (userBlockTime == Long.MAX_VALUE
            && otpThresholdTime < currentTime))) {
          // As cool down time or block time is passed, mark previous record deleted
          otpDetails.setIsDeleted(true);
          otpDetailsDao.save(otpDetails);
        }
        OTPDetails newOTPDetails = new OTPDetails(mobileNumber, deviceIdentifier, key, otpTime,
            userStageTag);
        otpDetailsDao.save(newOTPDetails);
        return new ValidationResponse(true, otp);
      } else {
        // Resend OTP
        checkForBlockedUser(otpDetails, otpConfig.getMaxOtpGenerateAttempts(), false);
        long otpExpiryTime = otpDetails.getOtpGenerationTime() +
            (otpConfig.getOtpExpiryTimeInMinutes() * AuthenticationConstants.ONE_MINUTE_TO_MILLIS);
        if (otpExpiryTime > currentTime) {
          // Previous otp not expired yet
          otp = manager.generate(Secret.fromHex(otpDetails.getSecretKey()),
              otpDetails.getOtpGenerationTime());
          return new ValidationResponse(true, otp);
        } else {
          // Resend new otp
          otpDetails.setSecretKey(key);
          otpDetails.setOtpGenerationTime(otpTime);
          otpDetailsDao.save(otpDetails);
          return new ValidationResponse(true, otp);
        }
      }
    } else {
      String userBlockedMessage = String.format(AuthenticationConstants.USER_BLOCKED,
          otpConfig.getUserBlockTimeInMinutes());
      logger.info(userBlockedMessage);
      return new ValidationResponse(false, userBlockedMessage);
    }
  }

  /**
   * Checks for otp validation
   *
   * @param request: User Request
   * @param userValidationinputForm: user Input details for validationg
   * @return ApiResponse
   */
  @LogMethodDetails
  public ValidationResponse checkBlockedOrValidateOTP(HttpServletRequest request,
      UserValidationinputForm userValidationinputForm) {
    String mobileNumber = userValidationinputForm.getUsername();
    String userStageTag = userValidationinputForm.getUserStageTag();
    String otp = userValidationinputForm.getOtp();
    long currentTime = new Date().getTime();
    String deviceIdentifier = OTPUtil
        .getDeviceIdentifier(request, userValidationinputForm.getDeviceId());
    OTPDetails otpDetails = otpDetailsDao
        .findTopByUserIdentifierAndDeviceIdentifierAndUserStageTagAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
            mobileNumber, deviceIdentifier, userStageTag, false, false, false);
    long userBlockTime = otpDetails != null && otpDetails.getUserBlockTime() != null ? otpDetails
        .getUserBlockTime().getTime() :
        Long.MAX_VALUE;
    if (otpDetails == null || (otpDetails != null && (userBlockTime < currentTime))) {
      logger.info("OTP details are empty with all checks for mobile: " + mobileNumber);
      OTPDetails userLatestOTPDetails = otpDetailsDao

          .findTopByUserIdentifierAndDeviceIdentifierAndUserStageTagAndIsDeletedOrderByIdDesc(
              mobileNumber, deviceIdentifier, userStageTag, false);
      if (userLatestOTPDetails == null) {
        return new ValidationResponse(false, AuthenticationConstants.USER_WRONG_DETAILS);
      } else {
        if (userLatestOTPDetails.getUserBlockTime().getTime() >= currentTime) {
          return new ValidationResponse(false,
              String.format(AuthenticationConstants.USER_BLOCKED,
                  otpConfig.getUserBlockTimeInMinutes()));
        } else {
          return new ValidationResponse(false,
              AuthenticationConstants.OTP_NEW_OTP_GENERATE_MESSAGE);
        }
      }
    } else {
      // Sent OTP is valid as per time
      checkForBlockedUser(otpDetails,
          otpConfig.getMaxOtpValidateAttempts(), true);
      if (otpValidate(otp, otpDetails.getSecretKey(), otpDetails.getOtpGenerationTime())) {
        otpDetails.setIsDeleted(true);
        otpDetailsDao.save(otpDetails);
        return new ValidationResponse(true, AuthenticationConstants.OTP_VALIDATE_SUCCESS);
      } else {
        return new ValidationResponse(false,
            String.format(AuthenticationConstants.OTP_INVALID, mobileNumber));
      }
    }
  }

  /**
   * Validates the otp submitted by user
   *
   * @return boolean flag
   */
  private Boolean otpValidate(String otp, String secret, long otpGenerationTime) {
    if (wfImplConfig.getValidateDefaultOTP() && checkOptionalOTP(otp)) {
      return true;
    }
    byte[] v = Secret.fromHex(secret);
    return manager.validate(v, otp);
  }

  /**
   * This method checks number of requests from user and decides whether current user should be
   * blocked or not
   */
  private void checkForBlockedUser(OTPDetails otpDetails, int maxAttempts,
      boolean isValidatingOTP) {
    if (isValidatingOTP) {
      int count = otpDetails.getOtpValidateCount();
      otpDetails.setOtpValidateCount(++count);
      if (count >= maxAttempts) {
        otpDetails.setBlockedOnValidate(true);
        otpDetails.setBlockedOnGenerate(true);
        otpDetails.setOtpGenerateCount(otpConfig.getMaxOtpGenerateAttempts());
        otpDetails.setUserBlockTime(otpConfig.getUserBlockTimeInMinutes());
      }
    } else {
      int count = otpDetails.getOtpGenerateCount();
      otpDetails.setOtpGenerateCount(++count);
      if (count >= maxAttempts) {
        otpDetails.setBlockedOnGenerate(true);
        otpDetails.setBlockedOnValidate(true);
        otpDetails.setOtpValidateCount(otpConfig.getMaxOtpValidateAttempts());
        otpDetails.setUserBlockTime(otpConfig.getUserBlockTimeInMinutes());
      }
    }
    otpDetailsDao.save(otpDetails);
  }


  private void printDetails(){
    logger.info("OTP Expiry Time" + otpConfig.getOtpExpiryTimeInMinutes());
  }

}
