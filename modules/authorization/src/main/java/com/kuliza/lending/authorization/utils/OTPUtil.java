package com.kuliza.lending.authorization.utils;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

public class OTPUtil {

  public static String getClientIP(HttpServletRequest request) {
    String xfHeader = request.getHeader("X-Forwarded-For");
    if (xfHeader == null) {
      return request.getRemoteAddr();
    }
    return xfHeader.split(",")[0];
  }

  public static boolean checkOptionalOTP(String otp){
    Date today = new Date();
    String optionalOTP = CommonHelperFunctions.getDateStringValue(today, AuthenticationConstants.OTP_DEFAULT_FORMAT);
    return otp.equals(optionalOTP);
  }

  public static String getDeviceIdentifier(HttpServletRequest request,
      String deviceId) {
    return deviceId != null ? deviceId : getClientIP(request);
  }
}
