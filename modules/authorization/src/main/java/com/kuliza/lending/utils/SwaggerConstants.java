package com.kuliza.lending.utils;

public class SwaggerConstants {
	
	public static final String AC_SIGN_UP = "Sign up";
	public static final String AC_SIGN_UP_RESPONSE = "";
	
	public static final String AC_LOGIN = "Login with the valid credentials";
	public static final String AC_LOGIN_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"access_token\": \"sqa2KGZtmKcthN5fanuD4OwMg\",\n" + 
			"        \"refresh_token\": \"LKNAeBZ1H5cDvDm77UqgJ_zTd44--SVPHvCyVxa6g5de-\",\n" + 
			"        \"role\": [\n" + 
			"            \"realm-admin\",\n" + 
			"            \"ce_admin\"\n" + 
			"        ],\n" + 
			"        \"refresh_expires_in\": 1317600,\n" + 
			"        \"groups\": [\n" + 
			"            {\n" + 
			"                \"access\": {\n" + 
			"                    \"view\": true,\n" + 
			"                    \"manage\": true,\n" + 
			"                    \"manageMembership\": true\n" + 
			"                },\n" + 
			"                \"attributes\": {},\n" + 
			"                \"clientRoles\": {},\n" + 
			"                \"id\": \"a2db397c-3e93-4681-be30-69291d1e9315\",\n" + 
			"                \"name\": \"collections\",\n" + 
			"                \"path\": \"/collections\",\n" + 
			"                \"realmRoles\": [\n" + 
			"                    \"collections_admin\"\n" + 
			"                ],\n" + 
			"                \"subGroups\": []\n" + 
			"            },\n" + 
			"            {\n" + 
			"                \"access\": {\n" + 
			"                    \"view\": true,\n" + 
			"                    \"manage\": true,\n" + 
			"                    \"manageMembership\": true\n" + 
			"                },\n" + 
			"                \"attributes\": {},\n" + 
			"                \"clientRoles\": {},\n" + 
			"                \"id\": \"b6eeea91-7329-402b-b188-e4b627866b65\",\n" + 
			"                \"name\": \"Test1Level2\",\n" + 
			"                \"path\": \"/TestGroupLevel1/Test1Level2\",\n" + 
			"                \"realmRoles\": [\n" + 
			"                    \"TestGroupLevelRole2\"\n" + 
			"                ],\n" + 
			"                \"subGroups\": []\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"token_type\": \"bearer\",\n" + 
			"        \"expires_in\": 1317600,\n" + 
			"        \"username\": \"admin@kuliza.com\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_ALL_USERS = "To get the list of all users";
	public static final String AC_ALL_USERS_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        \"a@a.in\",\n" + 
			"        \"abcadmin@kuliza.com\",\n" + 
			"        \"abhinav.agarwal@kuliza.com\",\n" + 
			"        \"actuator\",\n" + 
			"        \"admin-1@kuliza.com\",\n" + 
			"        \"admin1@kuliza.com\",\n" + 
			"        \"admin@kuliza.com\",\n" + 
			"        \"adminel@kuliza.com\",\n" + 
			"        \"adminlms@kuliza.com\",\n" + 
			"        \"admin_test1@kuliza.com\",\n" + 
			"        \"admin_test@kuliza.com\",\n" + 
			"        \"agent@sub-k.com\",\n" + 
			"        \"aman\",\n" + 
			"        \"crmccsgp@kuliza.com\",\n" + 
			"        \"croffel@kuliza.com\",\n" + 
			"        \"dealer1@gmail.com\",\n" + 
			"        \"deepak.srinivasan@kuliza.com\",\n" + 
			"        \"default_credit_user@kuliza.com\",\n" + 
			"        \"dev99@kuliza.com\",\n" + 
			"        \"devashish.j@fi.com\",\n" + 
			"        \"devmanager99@kuliza.com\",\n" + 
			"        \"documentmanagerbl@kuliza.com\",\n" + 
			"        \"earlycollections@lend.in\",\n" + 
			"        \"eating@gmail.com\"\n" + 
			"    ],\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_LOGOUT = "To logout the user";
	public static final String AC_LOGOUT_RESPONSE = "{\n" + 
			"    \"data\": \"Logged Out\",\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_REFRESH_TOKEN = "To refresh token";
	public static final String AC_REFRESH_TOKEN_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"access_token\": \"jx93JM1NVsKiVhLyFdgcCGIPagf53GZKvKXzgvBYkqqfdwg1XFfD7goLamJSZVwSjImeVD_6V8xTggsUt5w\",\n" + 
			"        \"refresh_token\": \"Oy5xD5u1ereOexu2Bn-RZvBIxWm2jlDE8EmW-h-ZjCNJmhf8NSjI7RekgOKOBozUxLxn0C5RkojED2OqKTzMMp0XqwDEiURfg\",\n" + 
			"        \"role\": [\n" + 
			"            \"realm-admin\",\n" + 
			"            \"ce_admin\"\n" + 
			"        ],\n" + 
			"        \"refresh_expires_in\": 1315752,\n" + 
			"        \"token_type\": \"bearer\",\n" + 
			"        \"expires_in\": 1315752,\n" + 
			"        \"username\": \"admin@kuliza.com\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_SESSION_LOGOUT = "Logout from all sessions";
	public static final String AC_SESSION_LOGOUT_RESPONSE = "{\n" + 
			"	\"data\":\"cmmnuser@kuliza.com\",\n" + 
			"	\"status\":200,\n" + 
			"	\"message\":\"success\"\n" + 
			"}";
	
	public static final String AC_ADD_ROLE = "To add new role";
	public static final String AC_ADD_ROLE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"Business\": \"Created Successfully\"\n" + 
			"    },\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_ADD_USER_ROLES = "Add user roles";
	public static final String AC_ADD_USER_ROLES_RESPONSE = "{\n" + 
			"    \"data\": \"Roles ::[Business] successfully assigned  to user\",\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_GET_GROUPS = "To get the groups";
	public static final String AC_GET_GROUPS_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        \"AJSJHDAJD\",\n" + 
			"        \"AJSJHDAJaaD\",\n" + 
			"        \"ASDDDD\",\n" + 
			"        \"Acc Test Admin\",\n" + 
			"        \"Accion Demo\",\n" + 
			"        \"Boori\",\n" + 
			"        \"Business Loan\",\n" + 
			"        \"CA\",\n" + 
			"        \"CAADMIN_TEST\",\n" + 
			"        \"CM\",\n" + 
			"        \"CMMN CA\",\n" + 
			"        \"CmmnBMPoc\",\n" + 
			"        \"Credit Analyst POC\",\n" + 
			"        \"Developer BA\",\n" + 
			"        \"KHATRA\",\n" + 
			"        \"Kuliza Portal Admin\",\n" + 
			"        \"Lenders of Sub-K\",\n" + 
			"        \"Marketing\",\n" + 
			"        \"Marketing Admin\",\n" + 
			"        \"NAVDDDD\",\n" + 
			"        \"salesVP\",\n" + 
			"        \"test1\",\n" + 
			"        \"testAdmin\",\n" + 
			"        \"testUser\",\n" + 
			"        \"testUser1\",\n" + 
			"        \"training analyst\",\n" + 
			"        \"users\"\n" + 
			"    ],\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_LIST_OF_GROUPS = "List of groups associated with user";
	public static final String AC_LIST_OF_GROUPS_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"groups\": [\n" + 
			"            {\n" + 
			"                \"access\": {\n" + 
			"                    \"view\": true,\n" + 
			"                    \"manage\": true,\n" + 
			"                    \"manageMembership\": true\n" + 
			"                },\n" + 
			"                \"attributes\": {},\n" + 
			"                \"clientRoles\": {},\n" + 
			"                \"id\": \"a2db397c-3e93-4681-be30-69291d1e9315\",\n" + 
			"                \"name\": \"collections\",\n" + 
			"                \"path\": \"/collections\",\n" + 
			"                \"realmRoles\": [\n" + 
			"                    \"collections_admin\"\n" + 
			"                ],\n" + 
			"                \"subGroups\": []\n" + 
			"            },\n" + 
			"            {\n" + 
			"                \"access\": {\n" + 
			"                    \"view\": true,\n" + 
			"                    \"manage\": true,\n" + 
			"                    \"manageMembership\": true\n" + 
			"                },\n" + 
			"                \"attributes\": {},\n" + 
			"                \"clientRoles\": {},\n" + 
			"                \"id\": \"b6eeea91-7329-402b-b188-e4b627866b65\",\n" + 
			"                \"name\": \"Test1Level2\",\n" + 
			"                \"path\": \"/TestGroupLevel1/Test1Level2\",\n" + 
			"                \"realmRoles\": [\n" + 
			"                    \"TestGroupLevelRole2\"\n" + 
			"                ],\n" + 
			"                \"subGroups\": []\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"username\": \"admin@kuliza.com\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_GROUP_DETAILS = "Group details";
	public static final String AC_GROUP_DETAILS_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"access\": {\n" + 
			"            \"view\": true,\n" + 
			"            \"manage\": true,\n" + 
			"            \"manageMembership\": true\n" + 
			"        },\n" + 
			"        \"attributes\": {},\n" + 
			"        \"clientRoles\": {},\n" + 
			"        \"id\": \"a2db397c-3e93-4681-be30-69291d1e9315\",\n" + 
			"        \"name\": \"collections\",\n" + 
			"        \"path\": \"/collections\",\n" + 
			"        \"realmRoles\": [\n" + 
			"            \"collections_admin\"\n" + 
			"        ],\n" + 
			"        \"subGroups\": []\n" + 
			"    },\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_LIST_GROUP = "List members associated with Group";
	public static final String AC_LIST_GROUP_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"access\": null,\n" + 
			"            \"applicationRoles\": null,\n" + 
			"            \"attributes\": {\n" + 
			"                \"user_id\": [\n" + 
			"                    \"2\"\n" + 
			"                ]\n" + 
			"            },\n" + 
			"            \"clientConsents\": null,\n" + 
			"            \"clientRoles\": null,\n" + 
			"            \"createdTimestamp\": 1540479594892,\n" + 
			"            \"credentials\": null,\n" + 
			"            \"disableableCredentialTypes\": [\n" + 
			"                \"password\"\n" + 
			"            ],\n" + 
			"            \"email\": \"admin@kuliza.com\",\n" + 
			"            \"emailVerified\": false,\n" + 
			"            \"enabled\": true,\n" + 
			"            \"federatedIdentities\": null,\n" + 
			"            \"federationLink\": null,\n" + 
			"            \"firstName\": null,\n" + 
			"            \"groups\": null,\n" + 
			"            \"id\": \"ae6d423e-6679-437e-af76-6862deed1d5d\",\n" + 
			"            \"lastName\": null,\n" + 
			"            \"notBefore\": 1564056559,\n" + 
			"            \"origin\": null,\n" + 
			"            \"realmRoles\": null,\n" + 
			"            \"requiredActions\": [],\n" + 
			"            \"self\": null,\n" + 
			"            \"serviceAccountClientId\": null,\n" + 
			"            \"socialLinks\": null,\n" + 
			"            \"totp\": false,\n" + 
			"            \"username\": \"admin@kuliza.com\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"access\": null,\n" + 
			"            \"applicationRoles\": null,\n" + 
			"            \"attributes\": {\n" + 
			"                \"parent\": [\n" + 
			"                    \"123\"\n" + 
			"                ],\n" + 
			"                \"parent2\": [\n" + 
			"                    \"[123,123]\"\n" + 
			"                ]\n" + 
			"            },\n" + 
			"            \"clientConsents\": null,\n" + 
			"            \"clientRoles\": null,\n" + 
			"            \"createdTimestamp\": 1554048206101,\n" + 
			"            \"credentials\": null,\n" + 
			"            \"disableableCredentialTypes\": [\n" + 
			"                \"password\"\n" + 
			"            ],\n" + 
			"            \"email\": \"test101@kuliza.com\",\n" + 
			"            \"emailVerified\": true,\n" + 
			"            \"enabled\": true,\n" + 
			"            \"federatedIdentities\": null,\n" + 
			"            \"federationLink\": null,\n" + 
			"            \"firstName\": \"Test\",\n" + 
			"            \"groups\": null,\n" + 
			"            \"id\": \"b62cfab0-294a-4767-9116-9ffd4ffea92d\",\n" + 
			"            \"lastName\": \"101\",\n" + 
			"            \"notBefore\": 0,\n" + 
			"            \"origin\": null,\n" + 
			"            \"realmRoles\": null,\n" + 
			"            \"requiredActions\": [],\n" + 
			"            \"self\": null,\n" + 
			"            \"serviceAccountClientId\": null,\n" + 
			"            \"socialLinks\": null,\n" + 
			"            \"totp\": false,\n" + 
			"            \"username\": \"test101\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_ALL_GROUP_MEMBERS = "List members associated with Group and it's subgroups";
	public static final String AC_ALL_GROUP_MEMBERS_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"access\": {\n" + 
			"                \"view\": true,\n" + 
			"                \"manage\": true,\n" + 
			"                \"manageMembership\": true\n" + 
			"            },\n" + 
			"            \"attributes\": {},\n" + 
			"            \"clientRoles\": {},\n" + 
			"            \"id\": \"a2db397c-3e93-4681-be30-69291d1e9315\",\n" + 
			"            \"memberList\": [\n" + 
			"                {\n" + 
			"                    \"access\": null,\n" + 
			"                    \"applicationRoles\": null,\n" + 
			"                    \"attributes\": {\n" + 
			"                        \"user_id\": [\n" + 
			"                            \"2\"\n" + 
			"                        ]\n" + 
			"                    },\n" + 
			"                    \"clientConsents\": null,\n" + 
			"                    \"clientRoles\": null,\n" + 
			"                    \"createdTimestamp\": 1540479594892,\n" + 
			"                    \"credentials\": null,\n" + 
			"                    \"disableableCredentialTypes\": [\n" + 
			"                        \"password\"\n" + 
			"                    ],\n" + 
			"                    \"email\": \"admin@kuliza.com\",\n" + 
			"                    \"emailVerified\": false,\n" + 
			"                    \"enabled\": true,\n" + 
			"                    \"federatedIdentities\": null,\n" + 
			"                    \"federationLink\": null,\n" + 
			"                    \"firstName\": null,\n" + 
			"                    \"groups\": null,\n" + 
			"                    \"id\": \"ae6d423e-6679-437e-af76-6862deed1d5d\",\n" + 
			"                    \"lastName\": null,\n" + 
			"                    \"notBefore\": 1564056559,\n" + 
			"                    \"origin\": null,\n" + 
			"                    \"realmRoles\": null,\n" + 
			"                    \"requiredActions\": [],\n" + 
			"                    \"self\": null,\n" + 
			"                    \"serviceAccountClientId\": null,\n" + 
			"                    \"socialLinks\": null,\n" + 
			"                    \"totp\": false,\n" + 
			"                    \"username\": \"admin@kuliza.com\"\n" + 
			"                },\n" + 
			"                {\n" + 
			"                    \"access\": null,\n" + 
			"                    \"applicationRoles\": null,\n" + 
			"                    \"attributes\": {\n" + 
			"                        \"parent\": [\n" + 
			"                            \"123\"\n" + 
			"                        ],\n" + 
			"                        \"parent2\": [\n" + 
			"                            \"[123,123]\"\n" + 
			"                        ]\n" + 
			"                    },\n" + 
			"                    \"clientConsents\": null,\n" + 
			"                    \"clientRoles\": null,\n" + 
			"                    \"createdTimestamp\": 1554048206101,\n" + 
			"                    \"credentials\": null,\n" + 
			"                    \"disableableCredentialTypes\": [\n" + 
			"                        \"password\"\n" + 
			"                    ],\n" + 
			"                    \"email\": \"test101@kuliza.com\",\n" + 
			"                    \"emailVerified\": true,\n" + 
			"                    \"enabled\": true,\n" + 
			"                    \"federatedIdentities\": null,\n" + 
			"                    \"federationLink\": null,\n" + 
			"                    \"firstName\": \"Test\",\n" + 
			"                    \"groups\": null,\n" + 
			"                    \"id\": \"b62cfab0-294a-4767-9116-9ffd4ffea92d\",\n" + 
			"                    \"lastName\": \"101\",\n" + 
			"                    \"notBefore\": 0,\n" + 
			"                    \"origin\": null,\n" + 
			"                    \"realmRoles\": null,\n" + 
			"                    \"requiredActions\": [],\n" + 
			"                    \"self\": null,\n" + 
			"                    \"serviceAccountClientId\": null,\n" + 
			"                    \"socialLinks\": null,\n" + 
			"                    \"totp\": false,\n" + 
			"                    \"username\": \"test101\"\n" + 
			"                }\n" + 
			"            ],\n" + 
			"            \"name\": \"collections\",\n" + 
			"            \"path\": \"/collections\",\n" + 
			"            \"realmRoles\": [\n" + 
			"                \"collections_admin\"\n" + 
			"            ],\n" + 
			"            \"subGroups\": []\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	
	public static final String AC_SUB_GROUPS_WITH_GROUP = "List sub groups associated with Group with member list";
	public static final String AC_SUB_GROUPS_WITH_GROUP_RESPONSE = "{\n" + 
			"	    \"data\": [\n" + 
			"	        {\n" + 
			"	            \"access\": {\n" + 
			"	                \"view\": true,\n" + 
			"	                \"manage\": true,\n" + 
			"	                \"manageMembership\": true\n" + 
			"	            },\n" + 
			"	            \"attributes\": {},\n" + 
			"	            \"clientRoles\": {},\n" + 
			"	            \"id\": \"a2db397c-3e93-4681-be30-69291d1e9315\",\n" + 
			"	            \"memberList\": [\n" + 
			"	                {\n" + 
			"	                    \"access\": null,\n" + 
			"	                    \"applicationRoles\": null,\n" + 
			"	                    \"attributes\": {\n" + 
			"	                        \"user_id\": [\n" + 
			"	                            \"2\"\n" + 
			"	                        ]\n" + 
			"	                    },\n" + 
			"	                    \"clientConsents\": null,\n" + 
			"	                    \"clientRoles\": null,\n" + 
			"	                    \"createdTimestamp\": 1540479594892,\n" + 
			"	                    \"credentials\": null,\n" + 
			"	                    \"disableableCredentialTypes\": [\n" + 
			"	                        \"password\"\n" + 
			"	                    ],\n" + 
			"	                    \"email\": \"admin@kuliza.com\",\n" + 
			"	                    \"emailVerified\": false,\n" + 
			"	                    \"enabled\": true,\n" + 
			"	                    \"federatedIdentities\": null,\n" + 
			"	                    \"federationLink\": null,\n" + 
			"	                    \"firstName\": null,\n" + 
			"	                    \"groups\": null,\n" + 
			"	                    \"id\": \"ae6d423e-6679-437e-af76-6862deed1d5d\",\n" + 
			"	                    \"lastName\": null,\n" + 
			"	                    \"notBefore\": 1564056559,\n" + 
			"	                    \"origin\": null,\n" + 
			"	                    \"realmRoles\": null,\n" + 
			"	                    \"requiredActions\": [],\n" + 
			"	                    \"self\": null,\n" + 
			"	                    \"serviceAccountClientId\": null,\n" + 
			"	                    \"socialLinks\": null,\n" + 
			"	                    \"totp\": false,\n" + 
			"	                    \"username\": \"admin@kuliza.com\"\n" + 
			"	                },\n" + 
			"	                {\n" + 
			"	                    \"access\": null,\n" + 
			"	                    \"applicationRoles\": null,\n" + 
			"	                    \"attributes\": {\n" + 
			"	                        \"parent\": [\n" + 
			"	                            \"123\"\n" + 
			"	                        ],\n" + 
			"	                        \"parent2\": [\n" + 
			"	                            \"[123,123]\"\n" + 
			"	                        ]\n" + 
			"	                    },\n" + 
			"	                    \"clientConsents\": null,\n" + 
			"	                    \"clientRoles\": null,\n" + 
			"	                    \"createdTimestamp\": 1554048206101,\n" + 
			"	                    \"credentials\": null,\n" + 
			"	                    \"disableableCredentialTypes\": [\n" + 
			"	                        \"password\"\n" + 
			"	                    ],\n" + 
			"	                    \"email\": \"test101@kuliza.com\",\n" + 
			"	                    \"emailVerified\": true,\n" + 
			"	                    \"enabled\": true,\n" + 
			"	                    \"federatedIdentities\": null,\n" + 
			"	                    \"federationLink\": null,\n" + 
			"	                    \"firstName\": \"Test\",\n" + 
			"	                    \"groups\": null,\n" + 
			"	                    \"id\": \"b62cfab0-294a-4767-9116-9ffd4ffea92d\",\n" + 
			"	                    \"lastName\": \"101\",\n" + 
			"	                    \"notBefore\": 0,\n" + 
			"	                    \"origin\": null,\n" + 
			"	                    \"realmRoles\": null,\n" + 
			"	                    \"requiredActions\": [],\n" + 
			"	                    \"self\": null,\n" + 
			"	                    \"serviceAccountClientId\": null,\n" + 
			"	                    \"socialLinks\": null,\n" + 
			"	                    \"totp\": false,\n" + 
			"	                    \"username\": \"test101\"\n" + 
			"	                }\n" + 
			"	            ],\n" + 
			"	            \"name\": \"collections\",\n" + 
			"	            \"path\": \"/collections\",\n" + 
			"	            \"realmRoles\": [\n" + 
			"	                \"collections_admin\"\n" + 
			"	            ],\n" + 
			"	            \"subGroups\": []\n" + 
			"	        }\n" + 
			"	    ],\n" + 
			"	    \"message\": \"OK\",\n" + 
			"	    \"status\": 200\n" + 
			"	}";
	
	public static final String AC_PARENT_GROUP = "Parent Group for a SubGroup";
	public static final String AC_PARENT_GROUP_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"access\": null,\n" + 
			"        \"attributes\": {},\n" + 
			"        \"clientRoles\": {},\n" + 
			"        \"id\": \"61e8e458-013d-43b4-9695-582e84bfc187\",\n" + 
			"        \"name\": \"Acc Test 1\",\n" + 
			"        \"path\": \"/Acc Test Admin/Acc Test 1\",\n" + 
			"        \"realmRoles\": [\n" + 
			"            \"sanTest1\"\n" + 
			"        ],\n" + 
			"        \"subGroups\": [\n" + 
			"            {\n" + 
			"                \"access\": null,\n" + 
			"                \"attributes\": {},\n" + 
			"                \"clientRoles\": {},\n" + 
			"                \"id\": \"e92e2764-8e41-4a90-8855-ea66c2c8aabb\",\n" + 
			"                \"name\": \"Acc Test 2\",\n" + 
			"                \"path\": \"/Acc Test Admin/Acc Test 1/Acc Test 2\",\n" + 
			"                \"realmRoles\": [\n" + 
			"                    \"sanTest2\"\n" + 
			"                ],\n" + 
			"                \"subGroups\": []\n" + 
			"            }\n" + 
			"        ]\n" + 
			"    },\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_PARENT_SUBGROUP = "Parent Group Members for a SubGroup";
	public static final String AC_PARENT_SUBGROUP_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"access\": null,\n" + 
			"            \"applicationRoles\": null,\n" + 
			"            \"attributes\": null,\n" + 
			"            \"clientConsents\": null,\n" + 
			"            \"clientRoles\": null,\n" + 
			"            \"createdTimestamp\": 1564392103839,\n" + 
			"            \"credentials\": null,\n" + 
			"            \"disableableCredentialTypes\": [\n" + 
			"                \"password\"\n" + 
			"            ],\n" + 
			"            \"email\": \"santest1@acc.com\",\n" + 
			"            \"emailVerified\": false,\n" + 
			"            \"enabled\": true,\n" + 
			"            \"federatedIdentities\": null,\n" + 
			"            \"federationLink\": null,\n" + 
			"            \"firstName\": null,\n" + 
			"            \"groups\": null,\n" + 
			"            \"id\": \"8c8553b0-6b8e-466e-9f0e-ff132778b892\",\n" + 
			"            \"lastName\": null,\n" + 
			"            \"notBefore\": 0,\n" + 
			"            \"origin\": null,\n" + 
			"            \"realmRoles\": null,\n" + 
			"            \"requiredActions\": [],\n" + 
			"            \"self\": null,\n" + 
			"            \"serviceAccountClientId\": null,\n" + 
			"            \"socialLinks\": null,\n" + 
			"            \"totp\": false,\n" + 
			"            \"username\": \"santest1@acc.com\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_ENCODE_PASSWORD = "Encode journey users passworD";
	public static final String AC_ENCODE_PASSWORD_RESPONSE = "";
	
	public static final String AC_NEW_GROUP = "Create new group";
	public static final String AC_NEW_GROUP_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"group1group2\": \"Created Successfully\"\n" + 
			"    },\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
}
