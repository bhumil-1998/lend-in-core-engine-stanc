package com.kuliza.lending.authorization.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WfImplConfig {
	
	@Value("${wfImpl.user.disableValidation}")
	boolean disableValidation;
	
	@Value("${wfImpl.mpin.deviceIndependent}")
	boolean deviceIndependentMPIN;
	
    @Value("${otp.generateOTP}")
    private Boolean generateOTP;

    @Value("${otp.validateDefaultOTP}")
    private Boolean validateDefaultOTP;
    
    public Boolean getGenerateOTP() {
        return generateOTP;
    }

    public void setGenerateOTP(Boolean generateOTP) {
        this.generateOTP = generateOTP;
    }

    public Boolean getValidateDefaultOTP() {
        return validateDefaultOTP;
    }

    public void setValidateDefaultOTP(Boolean validateDefaultOTP) {
        this.validateDefaultOTP = validateDefaultOTP;
    }

	public boolean isDisableValidation() {
		return disableValidation;
	}

	public void setDisableValidation(boolean disableValidation) {
		this.disableValidation = disableValidation;
	}

	public boolean isDeviceIndependentMPIN() {
		return deviceIndependentMPIN;
	}

	public void setDeviceIndependentMPIN(boolean deviceIndependentMPIN) {
		this.deviceIndependentMPIN = deviceIndependentMPIN;
	}

}
