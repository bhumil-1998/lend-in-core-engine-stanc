package com.kuliza.lending.pojo;

import org.hibernate.validator.constraints.NotEmpty;

public class RecoverPasswordResetApiView {

    @NotEmpty
    private String email;

    @NotEmpty
    private String authCode;

    @NotEmpty
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RecoverPasswordResetApiView(String email, String authCode, String password) {
        this.email = email;
        this.authCode = authCode;
        this.password = password;
    }

    public RecoverPasswordResetApiView() {
    }
}



