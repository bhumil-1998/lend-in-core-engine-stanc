package com.kuliza.lending.authorization.service;

import com.kuliza.lending.authorization.config.OTPConfig;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.config.keycloak.TOTPManager;
import com.kuliza.lending.authorization.constants.Constants;
import com.kuliza.lending.authorization.exception.ErrorMessages;
import com.kuliza.lending.authorization.exception.UserNotFoundException;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import com.kuliza.lending.utils.AuthUtils;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.text.MessageFormat;
import java.time.LocalTime;
import java.util.*;

@Service
public class RecoverPasswordManager {

    private final static Logger logger = LoggerFactory.getLogger(RecoverPasswordManager.class);

    @Autowired
    KeyCloakService keyCloakService;

    @Autowired
    OTPService otpService;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private OTPConfig otpConfig;

    /**
     * Initates a password recovery activity
     * @param encodedEmail: user email in encoded format
     * @return if user with email exists then generates and sends OTP to email.
     */
    public ApiResponse initiateRecoverPassword(String encodedEmail){
        String email = AuthUtils.decodePayloadAttribute(encodedEmail);
        if(!CommonHelperFunctions.validateEmailString(email)){
            return new ApiResponse(HttpStatus.BAD_REQUEST, ErrorMessages.INVALID_EMAIL_ADDRESS.getErrorMessage());
        }
        int otpExpiryTime = Constants.otpCountdown;
        UserRepresentation user;
        try {
            user = keyCloakService.findUserByEmail(email);
            if(user==null)
                throw new UserNotFoundException(ErrorMessages.USER_NOT_EXIST.getErrorMessage());
        } catch (Exception ex){
            return new ApiResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
        }

        UserPasswordGenerationForm userForm =
                new UserPasswordGenerationForm(email, AuthenticationConstants.UserIdTypes.email, email, null);
        ValidationResponse response = otpService.otpGenerate(null, userForm);

        if (!response.isConducive()) {
            if (response.getMessage().toLowerCase().contains("blocked")) {
                Integer blockedDuration = otpConfig.getUserBlockTimeInMinutes();
                return new ApiResponse(HttpStatus.BAD_REQUEST, MessageFormat.format(ErrorMessages.USER_BLOCKED.getErrorMessage(), blockedDuration.toString()));
            }
        }
        if (response != null && response.isConducive()) {

            String body =  MessageFormat.format(Constants.RecoverPasswordOTPMailContent, user.getFirstName(),response.getMessage());

            String subject = MessageFormat.format(Constants.RecoverPasswordOTPMailSubject, response.getMessage());
            if(initiateNotificationEmail(user.getEmail(), subject, body)) {
                Map<String, Integer> responseMap = new HashMap<>();
                responseMap.put("timeout", otpExpiryTime);
                return new ApiResponse(HttpStatus.OK, Constants.EmailOTPGeneratedSuccessMessage, responseMap);
            }
            else return new ApiResponse(HttpStatus.BAD_REQUEST,ErrorMessages.ERROR_SENDING_OTP.getErrorMessage());
        }

        return new ApiResponse(HttpStatus.BAD_REQUEST,ErrorMessages.ERROR_INITIATING_RECOVER_PASSWORD.getErrorMessage());
    }

    /**
     * Validate recoverPassword Otp.
     * @param encodedEmail: user email in the encoded format
     * @param encodedOtp: otp required for user validation
     * @return if OTP is valid then sends success with AuthCode which requires for next API.
     */
    public ApiResponse validateRecoverPasswordOtp(String encodedEmail, String encodedOtp){

        String email = AuthUtils.decodePayloadAttribute(encodedEmail);
        String otp = AuthUtils.decodePayloadAttribute(encodedOtp);

        if(!CommonHelperFunctions.validateEmailString(email) || !CommonHelperFunctions.validateString(otp))
            return new ApiResponse(HttpStatus.BAD_REQUEST, ErrorMessages.INVALID_PAYLOAD_ATTRIBUTE.getErrorMessage());

        UserValidationinputForm validationForm = new UserValidationinputForm();
        validationForm.setUsername(email);
        validationForm.setOtp(otp);
        validationForm.setIdType(AuthenticationConstants.UserIdTypes.email);
        validationForm.setDeviceId(email);
        validationForm.setUserStageTag(null);
        ValidationResponse response = otpService.checkBlockedOrValidateOTP(null, validationForm);

        if (!response.isConducive()) {
            if (response.getMessage().toLowerCase().contains("blocked")) {
                Integer blockedDuration = otpConfig.getUserBlockTimeInMinutes();
                return new ApiResponse(HttpStatus.BAD_REQUEST, MessageFormat.format(ErrorMessages.USER_BLOCKED.getErrorMessage(), blockedDuration.toString()));
            } else {
                return new ApiResponse(HttpStatus.BAD_REQUEST, ErrorMessages.INVALID_OTP.getErrorMessage(), ErrorMessages.INVALID_OTP.getErrorMessage());
            }
        }

        return new ApiResponse(HttpStatus.OK, response.getMessage(), generateAuthCode(email));
    }

    /**
     * Send the email notification.
     * @param toAddress: recipient email address
     * @param subject: Subject line
     * @param body: email body
     * @return True if email notification is triggered else False
     */
    private Boolean initiateNotificationEmail(String toAddress, String subject, String body){
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(msg, true);
            helper.setTo(toAddress);
            helper.setSubject(subject);
            helper.setText(body, true);
            javaMailSender.send(msg);
            return Boolean.TRUE;
        } catch (MessagingException e) {
            logger.error(ErrorMessages.ERROR_SENDING_OTP.getErrorMessage(), e);
            return Boolean.FALSE;
        } catch (Exception e) {
            logger.error(ErrorMessages.ERROR_SENDING_OTP.getErrorMessage(), e);
            return Boolean.FALSE;
        }

    }

    public ApiResponse resetUserPassword(String encodedEmail, String encodedPassword, String authorizationCode){
        String email = AuthUtils.decodePayloadAttribute(encodedEmail);
        String password = AuthUtils.decodePayloadAttribute(encodedPassword);

        if(validateAuthorizationCode(email, authorizationCode))
        {
            try {
                UserRepresentation user;
                user = keyCloakService.findUserByEmail(email);
                if(user==null)
                    throw new UserNotFoundException(ErrorMessages.USER_NOT_EXIST.getErrorMessage());
                if(keyCloakService.resetUserCredentialsByIAMId(user.getId(), password))
                    return new ApiResponse(HttpStatus.OK, Constants.UserPasswordChanged);
                else
                    return new ApiResponse(HttpStatus.BAD_REQUEST, ErrorMessages.ERROR_IN_PASSWORD_RESET.getErrorMessage());
            } catch (Exception ex){
                return new ApiResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
            }
        }
        return new ApiResponse(HttpStatus.MOVED_PERMANENTLY, ErrorMessages.INVALID_AUTH_CODE.getErrorMessage());
    }

    /**
     * validates auth code
     * @param email
     * @param authorizationCode: received auth code in the API
     * @return validates and returns True if email and authCode are compatible else False.
     */
    private Boolean validateAuthorizationCode(String email, String authorizationCode){
        int numberOfIterations = otpConfig.getOtpExpiryTimeInMinutes();
        numberOfIterations = numberOfIterations == 0 ? 3 : numberOfIterations;
        int minuteOfTheDay = getMinuteOfTheDay();
        for(int i=minuteOfTheDay-numberOfIterations;i<=minuteOfTheDay+1;i++) {
            if (validateAuthCode(generateAuthCode(email, i), authorizationCode))
                return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * Validate two auth Codes are equals or not
     * @param authCodeParam1
     * @param authCodeParam2
     * @return True if both are equals else False
     */
    private Boolean validateAuthCode(String authCodeParam1, String authCodeParam2){
        return authCodeParam1.equals(authCodeParam2);
    }

    /**
     * Get minutes of the days starting from 0 hour.
     * @return mintues of the day in the int format
     */
    private int getMinuteOfTheDay(){
        return LocalTime.now().toSecondOfDay()/60;
    }

    /**
     * Generate Auth Code
     * @param username
     * @param supportingNumber
     * @return String with encoded using username and supporting number
     */
    private String generateAuthCode(String username, int supportingNumber){
        return AuthUtils.encodePayloadAttribute(AuthUtils.textEncoding(MessageFormat.format(Constants.AuthCodeFormat, username, supportingNumber)));
    }

    /**
     * Generate Auth Code
     * @param username
     * @return String with encoded using username and getMinuteOfTheDay()
     */
    private String generateAuthCode(String username){
        return generateAuthCode(username, getMinuteOfTheDay());
    }
}
