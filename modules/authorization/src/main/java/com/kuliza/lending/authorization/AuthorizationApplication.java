package com.kuliza.lending.authorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.kuliza")
@EnableJpaRepositories(basePackages = "com.kuliza")
@EntityScan(basePackages = "com.kuliza")
@PropertySources({
		@PropertySource("classpath:lend-in-modules.properties")
})
public class AuthorizationApplication{

	public static void main(String[] args) {
		SpringApplication.run(AuthorizationApplication.class);
	}
}
