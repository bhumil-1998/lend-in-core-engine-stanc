package com.kuliza.lending.authorization.controller;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.constants.Constants;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.authorization.service.RecoverPasswordManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.pojo.UserDetails;
import com.kuliza.lending.pojo.UserLoginDetails;
import com.kuliza.lending.pojo.UserRoles;
import com.kuliza.lending.pojo.UserToken;
import com.kuliza.lending.utils.SwaggerConstants;
import com.kuliza.lending.pojo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;

import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/****
 * 
 * @author Naveen Dhalaria
 *
 */
@RestController
@RequestMapping("/auth")
@Api(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AuthorizationController {

  @Autowired
  KeyCloakManager keyCloakManager;

  @Autowired
  KeyCloakService keyCloakService;

  @Autowired
  RecoverPasswordManager recoverPasswordManager;

  /**
   * This method is to register a User
   * 
   * @return UserRepresentation is Successfully created.
   */
  @ApiOperation(value = SwaggerConstants.AC_SIGN_UP)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_SIGN_UP_RESPONSE, code = 200)})
  @PostMapping(value = "/signup")
  public ApiResponse signup(@Valid @RequestBody UserDetails user) {
    return keyCloakManager.signup(user.getFirstname(), user.getLastname(), user.getUsername(),
        user.getEmail(), user.getPassword(), user.getRoles(), user.getAttributes(), false);
  }

  /**
   * This method is used to login using email and password
   * 
   * @param user - details for user
   * @return json response with user token and other details
   */
  
  @ApiOperation(value = SwaggerConstants.AC_LOGIN)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_LOGIN_RESPONSE, code = 200)})
  @PostMapping(value = "/login")
  public ApiResponse login(@Valid @RequestBody UserLoginDetails user) {
    return keyCloakManager.loginWithEmailAndPassword(user.getEmail(), user.getPassword(), user.isEncoded());
  }

  /**
   * This method will take refresh token and return access token and other user details if token is
   * vaild
   * 
   * @param refresh_token
   * @return json response with token details
   */
  @ApiOperation(value = SwaggerConstants.AC_REFRESH_TOKEN)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_REFRESH_TOKEN_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @PostMapping(value = "/refresh-token")
  public ApiResponse refresh(@RequestBody UserToken token) {
    return keyCloakManager.getAccessTokenByRefreshToken(token.getRefresh_token());
  }

  /**
   * This method is used for logout user from current active session
   * 
   * @param token
   * @param request
   * @return Success or fail response
   */
  @ApiOperation(value = SwaggerConstants.AC_LOGOUT)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_LOGOUT_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @PostMapping(value = "/logout")
  public ApiResponse logout(@RequestBody UserToken token, HttpServletRequest request) {
    return keyCloakManager.logout(token.getRefresh_token());
  }

  /**
   * to get all the registered users
   * 
   * @return usernames
   */
  @ApiOperation(value = SwaggerConstants.AC_ALL_USERS)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_ALL_USERS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/all-users")
  public ApiResponse allUsersList() {
    List<String> allUsers = keyCloakService.allUsers();
    return new ApiResponse(HttpStatus.OK, "OK", allUsers);
  }

  /***
   * This end Point is for clear all the Active Sessions for a user associated with user while
   * creating them
   * 
   * @param request
   * @return
   */
  @ApiOperation(value = SwaggerConstants.AC_SESSION_LOGOUT)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_SESSION_LOGOUT_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @PostMapping("/sessions/logout")
  public ApiResponse allSessionLogout(HttpServletRequest request,
      @RequestBody Map<String, String> user) {

    UserRepresentation rep = keyCloakService.findUserByEmail(user.get("email"));
    String userID = null;
    if (rep != null) {
    	userID = rep.getId();
    }
    keyCloakService.logoutUserFromAllSessions(userID);
    return new ApiResponse(HttpStatus.OK, "OK", "cleared all Sessions for User " + user.get("email"));
  }

  /***
   * This end Point is for creating client level roles these roles can be associated with user while
   * creating them
   * 
   * @param UserRoles
   * @return a Map for roles with status "Created" or "Existing" role
   */
  @ApiOperation(value = SwaggerConstants.AC_ADD_ROLE)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_ADD_ROLE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @PostMapping("/roles/add-new-role")
  public ApiResponse createNewRole(@RequestBody UserRoles roles, HttpServletRequest request) {

    Map<String, String> rolesMap = keyCloakManager.createNewRole(roles.getRoles());
    return new ApiResponse(HttpStatus.OK, "OK", rolesMap);
  }

  /***
   * This end Point is for adding client level roles to the user
   * 
   * @param UserRoles
   * @return a Map for roles with status "Created" or "Existing" role
   */
  @ApiOperation(value = SwaggerConstants.AC_ADD_USER_ROLES)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_ADD_USER_ROLES_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @PostMapping("/users/roles/add-user-roles")
  public ApiResponse adduserToRole(@RequestBody UserRoles user) {

    return new ApiResponse(HttpStatus.OK, "OK",
        keyCloakManager.addUserToRole(user.getEmail(), user.getRoles()));

  }

  /***
   * This end Point is for creating new Groups in Realm
   * 
   * @param requestBody
   * @return a Map for roles with status "Created" or "Existing" Group
   */
  @ApiOperation(value = SwaggerConstants.AC_NEW_GROUP)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_NEW_GROUP_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @PostMapping("/groups/create-new-group")
  public ApiResponse createNewGroups(@RequestBody Map<String, List<String>> requestBody) {

    return new ApiResponse(HttpStatus.OK, "OK",
        keyCloakManager.createGroups(requestBody.get("groups")));
  }

  /***
   * This end Point is for Listing all Groups in Realm
   * 
   * @param requestBody
   * @return a Map for roles with status "Created" or "Existing" Group
   */
  @ApiOperation(value = SwaggerConstants.AC_GET_GROUPS)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_GET_GROUPS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/groups/all-groups")
  public ApiResponse createNewGroups() {

    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.AllGroups());
  }

  /***
   * Return Groups associated with the user.
   * 
   * @param request
   * @return list of groups with which user is associated.
   */
  @ApiOperation(value = SwaggerConstants.AC_LIST_OF_GROUPS)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_LIST_OF_GROUPS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/user/groups-with-username")
  public ApiResponse userGroups(ServletRequest request) {
    // Retrieving userName(email) from request object
    String email = (String) request.getAttribute("userName");
    // TODO: modify request object to return userId, and retrieve information based on that.
    // Retrieving user details based on` userName
    UserRepresentation userRepresentation = this.keyCloakService.findUserByEmail(email);
    return keyCloakService.userGroupsWithUserNAme(userRepresentation.getId(), email);
  }

  /***
   * Group Details API
   * 
   * @param id : Group ID.
   * @return group details
   */
  @ApiOperation(value = SwaggerConstants.AC_GROUP_DETAILS)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_GROUP_DETAILS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/group/details")
  public ApiResponse groupDetails(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.groupDetails(id));

  }

  /***
   * Group Members Listing API
   * 
   * @param id(Group ID)
   * @return list of all members associated with Group
   */
  @ApiOperation(value = SwaggerConstants.AC_LIST_GROUP)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_LIST_GROUP_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/group/members")
  public ApiResponse groupMembers(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.groupMembers(id));

  }


  /***
   * Group+SubGroup Members Listing API
   * 
   * @param id(Group ID)
   * @return list of all members associated with Group and it's SubGroups
   */
  @ApiOperation(value = SwaggerConstants.AC_ALL_GROUP_MEMBERS)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_ALL_GROUP_MEMBERS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/group/members/all")
  public ApiResponse nestedGroupMembers(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.subGroupDetailsWithMembers(id));

  }

  /***
   * Group+SubGroup Members Listing API
   *
   * @param id(Group ID)
   * @return list of all members associated with Group and it's SubGroups
   */
  @ApiOperation(value = SwaggerConstants.AC_SUB_GROUPS_WITH_GROUP)
 	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_SUB_GROUPS_WITH_GROUP_RESPONSE, code = 200)})
 	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
 			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/group/details/sub")
  public ApiResponse subGroupDetailsWithMembers(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.subGroupDetailsWithMembers(id));

  }

  /***
   * Retrieve Parent Group
   * 
   * @param id(Group ID)
   * @return Parent Group if any.
   */
  @ApiOperation(value = SwaggerConstants.AC_PARENT_GROUP)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_PARENT_GROUP_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/group/parent")
  public ApiResponse parentGroup(@RequestParam @ApiParam(value = "SubGroup ID",
      example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.parentGroup(id));

  }

  /***
   * Parent Groups Members Listing API
   * 
   * @param id(Group ID)
   * @return list of all members associated with Parent Group
   */
  @ApiOperation(value = SwaggerConstants.AC_PARENT_SUBGROUP)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_PARENT_SUBGROUP_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/group/parent/members")
  public ApiResponse parentGroupMembers(@RequestParam @ApiParam(value = "SubGroup ID",
      example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.parentGroupMembers(id));

  }

  // TODO: Delete below code once all implementations are using this branch as base branch.
  /***
   * Password hashing for all journey users.
   *
   * @return Success if all users hashing is completed else Error.
   */
  @ApiOperation(value = SwaggerConstants.AC_ENCODE_PASSWORD)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.AC_ENCODE_PASSWORD_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
  @GetMapping("/encode-user-passwords")
  public ApiResponse encodeJourneyUsersPasswords() {
    keyCloakService.hashPasswordForAllJourneyUsers();
    return new ApiResponse(HttpStatus.OK, "OK", Constants.ENCODING_SUCCESS_MESSAGE);
  }

  /**
   * Initiate Recover Password Activity
   * @param requestBody
   * @return ApiResponse
   */
  @PostMapping("/recover/initiate")
  @ApiOperation("Initiate recover password activity")
  public ApiResponse initiateRecoverPassword(@RequestBody @Valid RecoverPasswordInitiateApiView requestBody) {
    return recoverPasswordManager.initiateRecoverPassword(requestBody.getEmail());
  }


  /**
   * Recover Validate OTP
   * @param requestBody
   * @return ApiResponse
   */
  @PostMapping("/recover/validate")
  @ApiOperation("Validate OTP")
  public ApiResponse validateRecoverPasswordOtp(@RequestBody @Valid RecoverPasswordOtpValidateApiView requestBody) {
    return recoverPasswordManager.validateRecoverPasswordOtp(requestBody.getEmail(), requestBody.getOtp());
  }


  /**
   * Reset user password
   * @param requestBody
   * @return ApiResponse
   */
  @PostMapping("/recover/reset")
  @ApiOperation("Reset new password")
  public ApiResponse resetUserPasswordOtp(@RequestBody @Valid RecoverPasswordResetApiView requestBody) {
    return recoverPasswordManager.resetUserPassword(requestBody.getEmail(), requestBody.getPassword(), requestBody.getAuthCode());
  }
}
