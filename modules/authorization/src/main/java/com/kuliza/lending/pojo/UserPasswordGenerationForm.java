package com.kuliza.lending.pojo;

import com.kuliza.lending.authorization.utils.AuthenticationConstants.UserIdTypes;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class UserPasswordGenerationForm {

	@NotNull(message = "UserName cannot be null")
	@NotEmpty(message = "UserName cannot be empty")
	String username;
	
	@NotNull(message = "Id type cannot be null")
	@Enumerated(EnumType.STRING)
	UserIdTypes idType;

	String deviceId;

	String userStageTag;
	
	public UserPasswordGenerationForm() {
		super();
	}

	public UserPasswordGenerationForm(String username, UserIdTypes idType, String deviceId, String userStageTag) {
		super();
		this.username = username;
		this.idType = idType;
		this.deviceId = deviceId;
		this.userStageTag = userStageTag;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserIdTypes getIdType() {
		return idType;
	}

	public void setIdType(UserIdTypes idType) {
		this.idType = idType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getUserStageTag() {
		return userStageTag;
	}

	public void setUserStageTag(String userStageTag) {
		this.userStageTag = userStageTag;
	}
}
