package com.kuliza.lending.authorization.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.common.utils.Constants.DeviceType;

@Entity
@Table(name = "wf_user_device_register")
public class WfUserDeviceRegister extends AbstractWfUserDeviceRegister {
	@Column
	private String iamUsername;

	public WfUserDeviceRegister() {
		super();
	}

	public WfUserDeviceRegister(String iamUsername, DeviceType deviceType,
			String deviceId, String mpin) {
		super(deviceType, deviceId, mpin);
		this.iamUsername = iamUsername;
	}
	
	public WfUserDeviceRegister(String iamUsername, DeviceType deviceType,
			String deviceId) {
		super(deviceType, deviceId);
		this.iamUsername = iamUsername;
	}

	public String getWfUser() {
		return iamUsername;
	}

	public void setWfUser(String iamUsername) {
		this.iamUsername = iamUsername;
	}

	// Add more fields to this table if required in the format given below

	// @Column(nullable = true)
	// private String destinationType;
	//
	// public WfUserDeviceRegister() {
	// super();
	// this.setIsDeleted(false);
	// }
	//
	// public WfUserDeviceRegister(String sourceId, String sourceType,
	// String sourceName, String destinationType) {
	// super();
	// this.setSourceId(sourceId);
	// this.setSourceType(sourceType);
	// this.setSourceName(sourceName);
	// this.destinationType = destinationType;
	// }
	//
	// public String getDestinationType() {
	// return destinationType;
	// }
	//
	// public void setDestinationType(String destinationType) {
	// this.destinationType = destinationType;
	// }

}
