package com.kuliza.lending.authorization.service;

import com.kuliza.lending.authorization.dao.WfUserDeviceRegDao;
import com.kuliza.lending.authorization.models.WfUserDeviceRegister;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.pojo.UserMPINInputForm;
import com.kuliza.lending.utils.AuthUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class MPinService {

  @Autowired
  WfUserDeviceRegDao wfUserDeviceRegDao;

  /**
   * Service to Set MPIN.
   *
   * @param iamUsername
   * @param mpinForm
   * @return ApiResponse
   */

  @LogMethodDetails(userId = "username")
  public ApiResponse setMpin(String iamUsername, UserMPINInputForm mpinForm) {
    List<WfUserDeviceRegister> wfUserDeviceObjs = null;
    WfUserDeviceRegister wfUserDeviceObj = null;
    wfUserDeviceObjs = wfUserDeviceRegDao.findByIamUsernameAndDeviceIdAndDeviceTypeAndIsDeleted(
        iamUsername, mpinForm.getDeviceId(), mpinForm.getDeviceType(), false);
    if (wfUserDeviceObjs != null && !wfUserDeviceObjs.isEmpty()) {
      wfUserDeviceObj = wfUserDeviceObjs.get(0);
    } else {
      wfUserDeviceObj = new WfUserDeviceRegister(iamUsername, mpinForm.getDeviceType(),
          mpinForm.getDeviceId());
    }
    wfUserDeviceObj.setMpin(AuthUtils.textEncoding(mpinForm.getMpin()));
    wfUserDeviceRegDao.save(wfUserDeviceObj);
    return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
        AuthenticationConstants.MPIN_SET_SUCCESS_MESSAGE);
  }

  /**
   * Service to Validate MPIN
   */
  @LogMethodDetails(userId = "username")
  public ApiResponse validateMpin(String iamUsername, UserMPINInputForm mpinForm) {
    List<WfUserDeviceRegister> wfUserDeviceObjs = null;
    WfUserDeviceRegister wfUserDeviceObj = null;
    wfUserDeviceObjs = wfUserDeviceRegDao
        .findByIamUsernameAndDeviceIdAndDeviceTypeAndIsDeletedAndMpinNotNull(
            iamUsername, mpinForm.getDeviceId(), mpinForm.getDeviceType(), false);
    if (wfUserDeviceObjs != null && !wfUserDeviceObjs.isEmpty()) {
      wfUserDeviceObj = wfUserDeviceObjs.get(0);
      if (wfUserDeviceObj.getMpin().equals(AuthUtils.textEncoding(mpinForm.getMpin()))) {
        return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
            AuthenticationConstants.MPIN_VALIDATION_SUCCESS_MESSAGE);
      }
    } else {
      return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
          AuthenticationConstants.USER_DEVICE_NOT_REGISTERED);
    }
    return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
        AuthenticationConstants.INVALID_MPIN_ERROR_MESSAGE);
  }

}
