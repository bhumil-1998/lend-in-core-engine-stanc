package com.kuliza.lending.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kuliza.lending.authorization.utils.AuthenticationConstants.UserIdTypes;
import com.kuliza.lending.common.utils.Constants.DeviceType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class UserValidationinputForm {

	@NotNull(message = "UserName cannot be null")
	@NotEmpty(message = "UserName cannot be empty")
	String username;

	@NotNull(message = "OTP cannot be null")
	@Size(min = 4, max = 6)
	@Pattern(regexp = "[0-9]+")
	String otp;

	@NotNull(message = "Id type cannot be null")
	@Enumerated(EnumType.STRING)
	UserIdTypes idType;

	String deviceId;

	DeviceType deviceType;

	String userStageTag;

	public UserValidationinputForm() {
		super();
	}

	public UserValidationinputForm(String username, String otp, UserIdTypes idType,
			String deviceId, DeviceType deviceType, String userStageTag) {
		super();
		this.username = username;
		this.otp = otp;
		this.idType = idType;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.userStageTag = userStageTag;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getOtp() {
		return otp;
	}

	@JsonProperty
	public void setOtp(String otp) {
		this.otp = otp;
	}

	public UserIdTypes getIdType() {
		return idType;
	}

	public void setIdType(UserIdTypes idType) {
		this.idType = idType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getUserStageTag() {
		return userStageTag;
	}

	public void setUserStageTag(String userStageTag) {
		this.userStageTag = userStageTag;
	}

	@Override
	public String toString() {
		return "UserValidationinputForm{" +
				"username='" + username + '\'' +
				", idType='" + idType + '\'' +
				", deviceId='" + deviceId + '\'' +
				", deviceType='" + deviceType + '\'' +
				", userStageTag=" + userStageTag +
				'}';
	}
}
