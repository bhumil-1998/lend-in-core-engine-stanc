package com.kuliza.lending.authorization.service;

import static com.kuliza.lending.authorization.utils.OTPUtil.checkOptionalOTP;

import com.kuliza.lending.authorization.config.OTPConfig;
import com.kuliza.lending.authorization.config.WfImplConfig;
import com.kuliza.lending.authorization.config.keycloak.Secret;
import com.kuliza.lending.authorization.config.keycloak.TOTPManager;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.authorization.utils.OTPUtil;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;


@Service
public class OTPServiceWithCache {

  private OTPConfig otpConfig;

  private WfImplConfig wfImplConfig;

  private TOTPManager manager;

  private LoadingCache<String, Integer> otpGenerationCache;
  private LoadingCache<String, Integer> otpValidationCache;
  private LoadingCache<String, String> otpSecretCache;

  private static final Logger logger = LoggerFactory.getLogger(OTPServiceWithCache.class);

  /**
   * Constructor instantiates the cache where we keep track of number of times user tried to
   * generate otp
   */
  @Autowired
  public OTPServiceWithCache(
      @Value("${otp.OTPThresholdInMinutes}") final Integer otpThresholdInMinutes,
      @Value("${otp.otpExpiryTimeInMinutes}") final Integer otpExpiryTimeInMinutes,
      @Value("${otp.otpLength}") final Integer otpLength,
      final OTPConfig otpConfig, final WfImplConfig wfImplConfig) {
    super();

    otpGenerationCache = getOTPCache(otpThresholdInMinutes);

    // Keeps track of count of wrong submission of otp
    otpValidationCache = getOTPCache(otpThresholdInMinutes);

    // Saves otp secret of user, used in validation of otp
    otpSecretCache = getOTPSecretCache(otpExpiryTimeInMinutes);

    manager = new TOTPManager(otpLength, otpExpiryTimeInMinutes * 60);
    this.otpConfig = otpConfig;
    this.wfImplConfig = wfImplConfig;
  }

  public static LoadingCache<String, String> getOTPSecretCache(int otpExpiryTimeInMinutes){
    return CacheBuilder.newBuilder()
        .expireAfterWrite(otpExpiryTimeInMinutes, TimeUnit.MINUTES)
        .build(new CacheLoader<String, String>() {
          public String load(String key) {
            return "";
          }
        });
  }

  public static LoadingCache<String, Integer> getOTPCache(int otpThresholdInMinutes){
    return CacheBuilder.newBuilder()
        .expireAfterWrite(otpThresholdInMinutes, TimeUnit.MINUTES)
        .build(new CacheLoader<String, Integer>() {
          public Integer load(String key) {
            return 0;
          }
        });
  }

  /**
   * Generates the OTP and sends it to user
   *
   * @return ApiResponse
   */
  public ValidationResponse otpGenerate(HttpServletRequest request,
      UserPasswordGenerationForm userForm) {
    String mobileNumber = userForm.getUsername();
    logger.info("OTP generate: " + mobileNumber);
    ValidationResponse apiResponse = null;
    String deviceIdentifier = OTPUtil.getDeviceIdentifier(request, userForm.getDeviceId());
    apiResponse = checkForBlockedUser(otpGenerationCache, mobileNumber,
        otpConfig.getMaxOtpGenerateAttempts(), deviceIdentifier, userForm.getUserStageTag());
    if (apiResponse == null) {
      byte[] secret = Secret.generate();
      Long otpGenerationTime = System.currentTimeMillis();
      String otp = manager.generate(secret, otpGenerationTime);
      String key = Secret.toHex(secret);
      logger.info(key);
      otpSecretCache
          .put(deviceIdentifier + "@" + mobileNumber + "@" + userForm.getUserStageTag(), key);
      otpSecretCache
          .put(deviceIdentifier + "@" + mobileNumber + "@" + userForm.getUserStageTag()
                  + AuthenticationConstants.OTP_GENERATION_TIME_KEY
              , CommonHelperFunctions.getStringValue(otpGenerationTime));
      return new ValidationResponse(true, otp);
    }
    return apiResponse;
  }

  /**
   * Checks for otp validation
   *
   * @param request: User Request
   * @param validationForm: user submitted details of validation
   * @return ApiResponse
   */
  @LogMethodDetails
  public ValidationResponse checkBlockedOrValidateOTP(HttpServletRequest request,
      UserValidationinputForm validationForm) {
    String secret = null;
    Long otpGenerationTime = null;
    String mobileNumber = validationForm.getUsername();
    String otp = validationForm.getOtp();
    String deviceIdentifier = OTPUtil.getDeviceIdentifier(request, validationForm.getDeviceId());
    try {
      secret = otpSecretCache
          .get(deviceIdentifier + "@" + mobileNumber + "@" + validationForm.getUserStageTag());
      otpGenerationTime = Long.parseLong(otpSecretCache
          .get(deviceIdentifier + "@" + mobileNumber + "@" + validationForm.getUserStageTag()
              + AuthenticationConstants.OTP_GENERATION_TIME_KEY));
    } catch (NumberFormatException | ExecutionException e) {
      logger.warn(CommonHelperFunctions.getStackTrace(e));
      return new ValidationResponse(false,
          HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
    ValidationResponse apiResponse = this.checkForBlockedUser(otpValidationCache, mobileNumber,
        otpConfig.getMaxOtpValidateAttempts(), deviceIdentifier, validationForm.getUserStageTag());
    if (apiResponse == null) {
      if (this.otpValidate(otp, secret, otpGenerationTime)) {
        otpValidationCache.invalidate(
            deviceIdentifier + "#" + mobileNumber + "#" + validationForm.getUserStageTag());
        otpSecretCache.invalidate(
            deviceIdentifier + "@" + mobileNumber + "@" + validationForm.getUserStageTag());
        otpSecretCache.invalidate(
            deviceIdentifier + "@" + mobileNumber + "@" + validationForm.getUserStageTag()
                + AuthenticationConstants.OTP_GENERATION_TIME_KEY);
        return new ValidationResponse(true, AuthenticationConstants.OTP_VALIDATE_SUCCESS);
      } else {
        return new ValidationResponse(false,
            String.format(AuthenticationConstants.OTP_INVALID, mobileNumber));
      }
    }
    return new ValidationResponse(false,
        String.format(AuthenticationConstants.USER_BLOCKED, otpConfig.getUserBlockTimeInMinutes()));
  }

  /**
   * Validates the otp submitted by user
   *
   * @return boolean flag
   */
  private Boolean otpValidate(String otp, String secret, long otpGenerationTime) {
    if (wfImplConfig.getValidateDefaultOTP() && checkOptionalOTP(otp)) {
      return true;
    }
    byte[] v = Secret.fromHex(secret);
    return manager.validate(v, otp, otpGenerationTime);
  }

  /**
   * This method checks number of requests from user and decides whether current user should be
   * blocked or not
   */
  @LogMethodDetails
  public ValidationResponse checkForBlockedUser(LoadingCache<String, Integer> cache,
      String user, int maxAttempts, String deviceIdentifier, String userStageTag) {
    ValidationResponse response = null;
    int count = 0;
    try {
      count = cache.get(deviceIdentifier + "#" + user + "#" + userStageTag);
    } catch (ExecutionException e) {
      count = 0;
    }
    if (count >= maxAttempts) {
      response = new ValidationResponse(false,
          String
              .format(AuthenticationConstants.USER_BLOCKED, otpConfig.getUserBlockTimeInMinutes()));
    } else {
      count++;
      cache.put(deviceIdentifier + "#" + user + "#" + userStageTag, count);
    }
    return response;
  }
}
