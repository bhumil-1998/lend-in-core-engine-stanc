package com.kuliza.lending.pojo;

public class ValidationResponse {
  /**
   * This pojo represents a response structure from a method
   * To be used when we want to:
   *      send message string to be returned in error case/success
   *      reply back to callee with a flag if the work can be presumed positively or otherwise
   * */
  private boolean isConducive;

  private String message;

  public boolean isConducive() {
    return isConducive;
  }

  public void setConducive(boolean conducive) {
    isConducive = conducive;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ValidationResponse() {
    super();
  }

  public ValidationResponse(boolean isConducive, String message) {
    super();
    this.isConducive = isConducive;
    this.message = message;
  }
}
