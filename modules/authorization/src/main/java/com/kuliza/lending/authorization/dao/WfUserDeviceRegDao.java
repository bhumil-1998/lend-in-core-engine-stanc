package com.kuliza.lending.authorization.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.authorization.models.WfUserDeviceRegister;

@Repository
public interface WfUserDeviceRegDao extends CrudRepository<WfUserDeviceRegister, Long> {

	public WfUserDeviceRegister findById(long id);

	public WfUserDeviceRegister findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<WfUserDeviceRegister> findByIamUsernameAndDeviceIdAndDeviceTypeAndIsDeleted(String iamUsername,String deviceId, DeviceType deviceType,
			boolean isDeleted);
	
	public List<WfUserDeviceRegister> findByIamUsernameAndIsDeleted(String iamUsername,
			boolean isDeleted);
	
	public List<WfUserDeviceRegister> findByIamUsernameAndDeviceIdAndDeviceTypeAndIsDeletedAndMpinNotNull(String iamUsername,String deviceId, DeviceType deviceType,
			boolean isDeleted);
	
	public List<WfUserDeviceRegister> findByIamUsernameAndIsDeletedAndMpinNotNull(String iamUsername, boolean isDeleted);
}
