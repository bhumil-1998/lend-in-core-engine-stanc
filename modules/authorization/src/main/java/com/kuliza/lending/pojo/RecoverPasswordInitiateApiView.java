package com.kuliza.lending.pojo;

import org.hibernate.validator.constraints.NotEmpty;

public class RecoverPasswordInitiateApiView {

    @NotEmpty
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RecoverPasswordInitiateApiView(String email) {
        this.email = email;
    }

    public RecoverPasswordInitiateApiView() {
    }
}



