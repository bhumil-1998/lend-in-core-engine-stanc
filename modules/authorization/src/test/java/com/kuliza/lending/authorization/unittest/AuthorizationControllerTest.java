package com.kuliza.lending.authorization.unittest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.authorization.TestUtils.TestConstants;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.constants.Constants;
import com.kuliza.lending.authorization.controller.AuthorizationController;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.authorization.service.RecoverPasswordManager;
import com.kuliza.lending.authorization.utils.UnitTestConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.pojo.RecoverPasswordInitiateApiView;
import com.kuliza.lending.pojo.RecoverPasswordOtpValidateApiView;
import com.kuliza.lending.pojo.RecoverPasswordResetApiView;
import com.kuliza.lending.pojo.UserLoginDetails;
import com.kuliza.lending.utils.AuthUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(AuthorizationController.class)
public class AuthorizationControllerTest {
	@Mock
	KeyCloakManager keyCloakManager;

	@Mock
	KeyCloakService keyCloakService;

	@InjectMocks
	AuthorizationController authorizationController;

	@Mock
	RecoverPasswordManager recoverPasswordManager;

	@Test
	public void loginTest() {
		String loginRequestBody = UnitTestConstants.userLoginDetailsJSONString;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			UserLoginDetails userDetails = objectMapper.readValue(loginRequestBody, UserLoginDetails.class);
			Mockito.when(keyCloakManager.loginWithEmailAndPassword(userDetails.getEmail(), userDetails.getPassword(), false))
					.thenReturn(new ApiResponse(HttpStatus.OK, ""));
			Assert.assertEquals(200, authorizationController.login(userDetails).getStatus(), 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recoverPasswordInitTest(){
		try
		{
			String testEmail = TestConstants.VALID_EMAIL_ADDRESS;
			String encodedEmailString = AuthUtils.encodePayloadAttribute(testEmail);
			RecoverPasswordInitiateApiView recoverPasswordInitiateApiView = new RecoverPasswordInitiateApiView(encodedEmailString);
			ApiResponse validResponse = new ApiResponse(HttpStatus.OK, Constants.EmailOTPGeneratedSuccessMessage);
			Mockito.doReturn(validResponse).when(recoverPasswordManager).initiateRecoverPassword(recoverPasswordInitiateApiView.getEmail());
			ApiResponse actualResponse = authorizationController.initiateRecoverPassword(recoverPasswordInitiateApiView);
			Assert.assertEquals(200, actualResponse.getStatus());
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}


	@Test
	public void recoverPasswordValidateTest(){
		try
		{
			String encodedEmailString = AuthUtils.encodePayloadAttribute(TestConstants.VALID_EMAIL_ADDRESS);
			String encodedOtpString = AuthUtils.encodePayloadAttribute(TestConstants.OTP);
			RecoverPasswordOtpValidateApiView recoverPasswordOtpValidateApiView = new RecoverPasswordOtpValidateApiView(encodedEmailString, encodedOtpString);
			ApiResponse validResponse = new ApiResponse(HttpStatus.OK, Constants.EmailOTPGeneratedSuccessMessage);
			Mockito.doReturn(validResponse).when(recoverPasswordManager).validateRecoverPasswordOtp(recoverPasswordOtpValidateApiView.getEmail(), recoverPasswordOtpValidateApiView.getOtp());
			ApiResponse actualResponse = authorizationController.validateRecoverPasswordOtp(recoverPasswordOtpValidateApiView);
			Assert.assertEquals(200, actualResponse.getStatus());
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

	@Test
	public void recoverPasswordResetTest(){
		try
		{
			String encodedEmailString = AuthUtils.encodePayloadAttribute(TestConstants.VALID_EMAIL_ADDRESS);
			String encodedPasswordString = AuthUtils.encodePayloadAttribute(TestConstants.PASSWORD);
			String authCodeString = AuthUtils.encodePayloadAttribute(TestConstants.AUTH_CODE);
			RecoverPasswordResetApiView recoverPasswordResetApiView = new RecoverPasswordResetApiView(encodedEmailString, encodedPasswordString, authCodeString);
			ApiResponse validResponse = new ApiResponse(HttpStatus.OK, Constants.EmailOTPGeneratedSuccessMessage);
			Mockito.doReturn(validResponse).when(recoverPasswordManager).resetUserPassword(recoverPasswordResetApiView.getEmail(), recoverPasswordResetApiView.getPassword(), recoverPasswordResetApiView.getAuthCode());
			ApiResponse actualResponse = authorizationController.resetUserPasswordOtp(recoverPasswordResetApiView);
			Assert.assertEquals(200, actualResponse.getStatus());
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

}
