package com.kuliza.lending.authorization.defaultOtp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;

import com.google.common.cache.LoadingCache;
import com.kuliza.lending.authorization.config.OTPConfig;
import com.kuliza.lending.authorization.config.WfImplConfig;
import com.kuliza.lending.authorization.service.OTPServiceWithCache;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.authorization.utils.AuthenticationConstants.UserIdTypes;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.authorization.TestUtils.TestConstants;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@PrepareForTest({com.kuliza.lending.authorization.service.OTPServiceWithCache.class,
    com.google.common.cache.LoadingCache.class, com.google.common.cache.CacheLoader.class,
    com.google.common.cache.CacheBuilder.class})
public class OtpServiceWithCacheTest {
  private OTPConfig otpConfig;

  private WfImplConfig wfImplConfig;

  HttpServletRequest request;

  OTPServiceWithCache otpServiceWithCache = mock(OTPServiceWithCache.class);

  OTPServiceWithCache otpServiceWithCacheSpy;

  @Mock
  LoadingCache<String, Integer> otpcache;

  @Mock
  LoadingCache<String, String> otpcacheString;

  private UserPasswordGenerationForm userPasswordGenerationForm;
  private ValidationResponse validationResponseSuccess;
  private UserValidationinputForm validationForm;
  private int maxOTPAttempts;
  private String secretKey;
  private ValidationResponse nullResponse;
  private ValidationResponse userBlockResponse;
  private int otpThresholdInMinutes;
  private int otpExpiryTime;
  private String deviceId;
  private String username;
  private String otp;

  @Before
  public void init(){
    this.deviceId = TestConstants.USER_DEVICEID;
    this.username = TestConstants.USER_IDENTIFIER;
    this.otp = getDefaultOTP();
    this.userPasswordGenerationForm = new UserPasswordGenerationForm(username, UserIdTypes.mobile,
        deviceId, TestConstants.USER_STAGE_TAG);
    this.validationResponseSuccess = new ValidationResponse(true, TestConstants.SUCCESS_MESSAGE);
    this.validationForm = new UserValidationinputForm(username, otp, UserIdTypes.mobile, deviceId,
        DeviceType.android, TestConstants.USER_STAGE_TAG);
    this.maxOTPAttempts = TestConstants.USER_OTP_GENERATE_COUNT;
    this.nullResponse = null;
    this.secretKey = TestConstants.SECRET_KEY;
    this.otpExpiryTime = TestConstants.OTP_EXPIRY_time;
    this.otpThresholdInMinutes = TestConstants.USER_OTP_THRESHOLD_MINUTE;
    this.otpConfig = mock(OTPConfig.class);
    this.wfImplConfig = mock(WfImplConfig.class);
    this.userBlockResponse = new ValidationResponse(false, String.format(
        AuthenticationConstants.USER_BLOCKED, otpConfig.getUserBlockTimeInMinutes()));
    this.request = mock(HttpServletRequest.class);
    PowerMockito.mockStatic(OTPServiceWithCache.class);
    when(OTPServiceWithCache.getOTPCache(anyInt())).thenReturn(otpcache);
    when(OTPServiceWithCache.getOTPSecretCache(anyInt())).thenReturn(otpcacheString);
    this.otpServiceWithCacheSpy = spy(new OTPServiceWithCache(this.otpThresholdInMinutes,
        this.otpExpiryTime, 6, otpConfig, wfImplConfig));
    otpcacheString.put(deviceId+"@"+username, secretKey);
    otpcacheString.put(deviceId+"@"+username+TestConstants.OTP_GENERATION_TIME_KEY,
        String.valueOf(System.currentTimeMillis()));
    otpcache.put(deviceId+"#"+username, 0);
  }

  public String getDefaultOTP() {
    Date today = new Date();
    return CommonHelperFunctions.getDateStringValue(today, AuthenticationConstants.OTP_DEFAULT_FORMAT);
  }

  @Test
  public void checkForBlockedUserTest() throws Exception{
    when(otpcache.get(any())).thenReturn(1);
    ValidationResponse response = otpServiceWithCacheSpy
        .checkForBlockedUser(otpcache, userPasswordGenerationForm.getUsername(),
            maxOTPAttempts, userPasswordGenerationForm.getDeviceId(), TestConstants.USER_STAGE_TAG);
    assertNull(response);
  }

  @Test
  public void checkForBlockedUserBlockTest() throws Exception{
    when(otpcache.get(any())).thenReturn(5);
    ValidationResponse response = otpServiceWithCacheSpy
        .checkForBlockedUser(otpcache, userPasswordGenerationForm.getUsername(),
            maxOTPAttempts, userPasswordGenerationForm.getDeviceId(), TestConstants.USER_STAGE_TAG);
    assertFalse(response.isConducive());
  }

  @Test
  public void checkForBlockedUserExceptionTest() throws Exception{
    doThrow(ExecutionException.class).when(otpcache).get(anyString());
    ValidationResponse response = otpServiceWithCacheSpy
        .checkForBlockedUser(otpcache, userPasswordGenerationForm.getUsername(),
            maxOTPAttempts, userPasswordGenerationForm.getDeviceId(), TestConstants.USER_STAGE_TAG);
    assertNull(response);
  }

  @Test
  public void otpGenerateTest() throws Exception{
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(maxOTPAttempts);
    Mockito.doReturn(this.nullResponse).when(otpServiceWithCacheSpy).checkForBlockedUser(any(LoadingCache.class),
        any(String.class), any(Integer.class), any(String.class), any(String.class));
    when(otpcache.get(any())).thenReturn(1);
    ValidationResponse response = otpServiceWithCacheSpy.otpGenerate(request, userPasswordGenerationForm);
    assertTrue(response.isConducive());
  }

  @Test
  public void otpGenerateBlockUserTest() throws Exception{
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(maxOTPAttempts);
    when(otpcache.get(any())).thenReturn(1);
    Mockito.doReturn(
        new ValidationResponse(false, String.format(AuthenticationConstants.USER_BLOCKED,
            otpConfig.getUserBlockTimeInMinutes())))
        .when(otpServiceWithCacheSpy).checkForBlockedUser(
        any(LoadingCache.class), any(String.class), any(Integer.class), any(String.class), any(String.class));
    ValidationResponse response = otpServiceWithCacheSpy
        .otpGenerate(request, userPasswordGenerationForm);
    assertFalse(response.isConducive());
  }

  @Test
  public void checkBlockedOrValidateOTPWrongUserTest() throws Exception{
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(maxOTPAttempts);
    ValidationResponse r = new ValidationResponse(false, TestConstants.SOMETHING_WENT_WRONG);
    Mockito
        .doReturn(r)
        .when(otpServiceWithCacheSpy)
        .checkForBlockedUser(any(LoadingCache.class),
        any(String.class), any(Integer.class), any(String.class), any(String.class));
    ValidationResponse response  = otpServiceWithCacheSpy.checkBlockedOrValidateOTP(request, validationForm);
    assertFalse(response.isConducive());
  }

  @Test
  public void otpDefaultValidateTest() throws Exception {
    when(wfImplConfig.getValidateDefaultOTP()).thenReturn(true);
    assertTrue(Whitebox.invokeMethod(otpServiceWithCacheSpy, TestConstants.OTP_VALIDATE, otp, secretKey,
        System.currentTimeMillis()));
  }

  @Test
  public void checkBlockedOrValidateOTPTest() throws Exception{
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(maxOTPAttempts);
    ValidationResponse r = new ValidationResponse(true, TestConstants.SUCCESS_MESSAGE);
    doReturn(null).when(otpServiceWithCacheSpy)
        .checkForBlockedUser(any(LoadingCache.class),
            any(String.class), any(Integer.class), any(String.class), any(String.class));
    doReturn(secretKey).when(otpcacheString).get(deviceId + "@" + username + "@" + TestConstants.USER_STAGE_TAG);
    doReturn(String.valueOf(System.currentTimeMillis()))
        .when(otpcacheString)
        .get(deviceId + "@" + username + "@" + TestConstants.USER_STAGE_TAG + AuthenticationConstants.OTP_GENERATION_TIME_KEY);
    doReturn(0)
        .when(otpcache).get(deviceId + "#" + username + "#" + TestConstants.USER_STAGE_TAG);
    PowerMockito.doReturn(true).
        when(otpServiceWithCacheSpy, TestConstants.OTP_VALIDATE, any(String.class),
            any(String.class), any(Long.class));
    ValidationResponse response = otpServiceWithCacheSpy
        .checkBlockedOrValidateOTP(request, validationForm);
    assertTrue(response.isConducive());
  }

  @Test
  public void checkBlockedOrValidateOTPUserBlockTest() throws Exception{
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(maxOTPAttempts);
    ValidationResponse r = new ValidationResponse(true, TestConstants.SUCCESS_MESSAGE);
    doReturn(r).when(otpServiceWithCacheSpy)
        .checkForBlockedUser(any(LoadingCache.class),
            any(String.class), any(Integer.class), any(String.class), any(String.class));
    doReturn(secretKey).when(otpcacheString).get(deviceId+"@"+username);
    doReturn(String.valueOf(System.currentTimeMillis()))
        .when(otpcacheString).get(deviceId+"@"+username+ AuthenticationConstants.OTP_GENERATION_TIME_KEY);
    doReturn(0)
        .when(otpcache).get(deviceId+"#"+username);
    PowerMockito.doReturn(true).
        when(otpServiceWithCacheSpy, TestConstants.OTP_VALIDATE, any(String.class), any(String.class), any(Long.class));
    ValidationResponse response  = otpServiceWithCacheSpy.checkBlockedOrValidateOTP(request, validationForm);
    assertFalse(response.isConducive());
  }

  @Test
  public void checkBlockedOrValidateOTPInvalidTest() throws Exception{
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(maxOTPAttempts);
    doReturn(null).when(otpServiceWithCacheSpy)
        .checkForBlockedUser(any(LoadingCache.class),
            any(String.class), any(Integer.class), any(String.class), any(String.class));
    doReturn(secretKey).when(otpcacheString).get(deviceId+"@"+username);
    doReturn(String.valueOf(System.currentTimeMillis()))
        .when(otpcacheString).get(deviceId+"@"+username+ AuthenticationConstants.OTP_GENERATION_TIME_KEY);
    doReturn(0)
        .when(otpcache).get(deviceId+"#"+username);
    PowerMockito.doReturn(false).
        when(otpServiceWithCacheSpy, TestConstants.OTP_VALIDATE, any(String.class), any(String.class), any(Long.class));
    ValidationResponse response  = otpServiceWithCacheSpy.checkBlockedOrValidateOTP(request, validationForm);
    assertFalse(response.isConducive());
  }

}
