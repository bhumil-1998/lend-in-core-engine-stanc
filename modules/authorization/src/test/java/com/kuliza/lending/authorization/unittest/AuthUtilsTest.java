package com.kuliza.lending.authorization.unittest;

import com.kuliza.lending.authorization.utils.UnitTestConstants;
import com.kuliza.lending.utils.AuthUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthUtilsTest {

	@Test
	public void textEncodingTest() {
        Assert.assertEquals(UnitTestConstants.encodedPasswordKey, AuthUtils.textEncoding(UnitTestConstants.passwordKey));
	}
}
