package com.kuliza.lending.authorization.defaultOtp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.spy;

import com.kuliza.lending.authorization.config.OTPConfig;
import com.kuliza.lending.authorization.config.WfImplConfig;
import com.kuliza.lending.authorization.dao.OTPDetailsDao;
import com.kuliza.lending.authorization.models.OTPDetails;
import com.kuliza.lending.authorization.service.OTPService;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.authorization.utils.AuthenticationConstants.UserIdTypes;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.authorization.TestUtils.TestConstants;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.crypto.*"})
@PrepareForTest(com.kuliza.lending.authorization.service.OTPService.class)
public class OTPServiceTest {

  HttpServletRequest request;

  OTPDetailsDao otpDetailsDao;

  private OTPConfig otpConfig;

  private WfImplConfig wfImplConfig;

  private OTPService otpService = mock(OTPService.class);

  private int otpLength;
  private UserPasswordGenerationForm userPasswordGenerationForm;
  private OTPDetails otpDetails;
  private int maxOTPGenerateCount;
  private int otpThresholdInMinutes;
  private int otpExpiryTimeInMinutes;
  private int userBlockTimeInMinutes;
  private int userBlockCount;
  private UserValidationinputForm validationForm;
  private ValidationResponse validationResponseSuccess;
  private int maxOTPValidateCount;
  private String secretKey;
  private OTPService otpServiceSpyMock;
  private String otp;

  @Before
  public void setUp() throws Exception {
    this.otpLength = TestConstants.OTP_LENGTH;
    this.otp = getDefaultOTP();
    this.secretKey = TestConstants.SECRET_KEY;
    this.validationResponseSuccess = new ValidationResponse(true, TestConstants.SUCCESS_MESSAGE);
    this.userPasswordGenerationForm = new UserPasswordGenerationForm(TestConstants.USER_IDENTIFIER,
    		UserIdTypes.mobile, TestConstants.USER_DEVICEID, TestConstants.USER_STAGE_TAG);
    this.validationForm = new UserValidationinputForm(TestConstants.USER_IDENTIFIER, otp,
    		UserIdTypes.mobile, TestConstants.USER_DEVICEID, DeviceType.android, TestConstants.USER_STAGE_TAG);
    this.otpDetails = new OTPDetails(TestConstants.USER_IDENTIFIER, TestConstants.USER_IPADDRESS,
        secretKey, System.currentTimeMillis(), TestConstants.USER_STAGE_TAG);
    this.maxOTPGenerateCount = TestConstants.USER_OTP_GENERATE_COUNT;
    this.maxOTPValidateCount = TestConstants.USER_OTP_VALIDATE_COUNT;
    this.otpThresholdInMinutes = TestConstants.USER_OTP_THRESHOLD_MINUTE;
    this.otpExpiryTimeInMinutes = TestConstants.OTP_EXPIRY_time;
    this.userBlockTimeInMinutes =TestConstants.USER_BLOCK_TIME;
    this.userBlockCount = TestConstants.USER_BLOCK_COUNT;
    this.otpDetailsDao = mock(OTPDetailsDao.class);
    this.otpConfig = mock(OTPConfig.class);
    this.wfImplConfig = mock(WfImplConfig.class);
    this.request = mock(HttpServletRequest.class);
    this.otpServiceSpyMock = spy(new OTPService(otpDetailsDao, otpConfig, wfImplConfig, otpLength,
        otpExpiryTimeInMinutes));
    PowerMockito.whenNew(OTPService.class).withArguments(any(OTPDetailsDao.class),
        any(OTPConfig.class), any(WfImplConfig.class), any(Integer.class), any(Integer.class))
        .thenReturn(otpService);
  }

  public String getDefaultOTP() {
    Date today = new Date();
    return CommonHelperFunctions.getDateStringValue(today, AuthenticationConstants.OTP_DEFAULT_FORMAT);
  }

  @Test
  public void otpGenerateSuccessTest() throws Exception {
    when(otpDetailsDao
        .findTopByUserIdentifierAndIsDeletedOrderByIdDesc(
            this.userPasswordGenerationForm.getUsername(), false))
        .thenReturn(null);
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(this.maxOTPGenerateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    when(otpConfig.getOtpExpiryTimeInMinutes()).thenReturn(this.otpExpiryTimeInMinutes);
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = Whitebox.invokeMethod(otpServiceSpyMock, TestConstants.OTP_SERVICE_GENERATE, request,
        this.userPasswordGenerationForm);
    assertTrue(response.isConducive());
  }

  @Test
  public void otpNewGenerateSuccessTest() throws Exception {
    otpDetails.setOtpGenerationTime(otpDetails.getOtpGenerationTime()-otpThresholdInMinutes*60000-100);
    when(otpDetailsDao
        .findTopByUserIdentifierAndIsDeletedOrderByIdDesc(
            this.userPasswordGenerationForm.getUsername(), false))
        .thenReturn(otpDetails);
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(this.maxOTPGenerateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    when(otpConfig.getOtpExpiryTimeInMinutes()).thenReturn(this.otpExpiryTimeInMinutes);
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = Whitebox.invokeMethod(otpServiceSpyMock, TestConstants.OTP_SERVICE_GENERATE, request,
        this.userPasswordGenerationForm);
    assertTrue(response.isConducive());
    otpDetails.setOtpGenerationTime(System.currentTimeMillis());
  }

  @Test
  public void otpGenerateUserBlockTest() throws Exception {
    this.otpDetails.setOtpGenerateCount(userBlockCount);
    when(otpDetailsDao
        .findTopByUserIdentifierAndUserStageTagAndIsDeletedOrderByIdDesc(any(String.class),
            any(String.class), eq(false)))
        .thenReturn(this.otpDetails);
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(this.maxOTPGenerateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    when(otpConfig.getOtpExpiryTimeInMinutes()).thenReturn(this.otpExpiryTimeInMinutes);
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = Whitebox.invokeMethod(otpServiceSpyMock, TestConstants.OTP_SERVICE_GENERATE, request,
        this.userPasswordGenerationForm);
    assertFalse(response.isConducive());
    this.otpDetails.setOtpGenerateCount(1);
  }

  // Resend otp
  @Test
  public void otpGenerateResendTest() throws Exception {
    when(otpDetailsDao
        .findTopByUserIdentifierAndIsDeletedOrderByIdDesc(any(String.class), eq(false)))
        .thenReturn(this.otpDetails);
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(this.maxOTPGenerateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    when(otpConfig.getOtpExpiryTimeInMinutes()).thenReturn(this.otpExpiryTimeInMinutes);
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = Whitebox.invokeMethod(otpServiceSpyMock, TestConstants.OTP_SERVICE_GENERATE, request,
        this.userPasswordGenerationForm);
    assertTrue(response.isConducive());
    otpDetails.setOtpGenerationTime(1);
  }

  @Test
  public void otpGenerateNewResendTest() throws Exception {
    when(otpDetailsDao
        .findTopByUserIdentifierAndIsDeletedOrderByIdDesc(any(String.class), eq(false)))
        .thenReturn(this.otpDetails);
    when(otpConfig.getMaxOtpGenerateAttempts()).thenReturn(this.maxOTPGenerateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    when(otpConfig.getOtpExpiryTimeInMinutes()).thenReturn(0);
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = Whitebox.invokeMethod(otpServiceSpyMock, TestConstants.OTP_SERVICE_GENERATE, request,
        this.userPasswordGenerationForm);
    assertTrue(response.isConducive());
    otpDetails.setOtpGenerationTime(1);
  }

  @Test
  public void validateOTPUserNullTest() throws Exception {
    when(otpDetailsDao
        .findTopByUserIdentifierAndDeviceIdentifierAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
            any(String.class), any(String.class), eq(false), eq(false), eq(false)))
        .thenReturn(null);
    when(otpDetailsDao
        .findTopByUserIdentifierAndDeviceIdentifierAndIsDeletedOrderByIdDesc(
            this.userPasswordGenerationForm.getUsername(),
            this.userPasswordGenerationForm.getDeviceId(), false))
        .thenReturn(null);
    ValidationResponse response = otpServiceSpyMock
        .checkBlockedOrValidateOTP(request, this.validationForm);
    assertFalse(response.isConducive());
  }

  @Test
  public void checkForBlockedUserTest() throws Exception {
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    otpDetails.setOtpValidateCount(userBlockCount);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    Whitebox.invokeMethod(otpServiceSpyMock, TestConstants.CHECK_FOR_BLOCKED_USER,
        otpDetails, maxOTPValidateCount, true);
    assertTrue(otpDetails.isBlockedOnValidate());
    otpDetails.setOtpValidateCount(1);
  }

  @Test
  public void checkForBlockedUserGenerateTest() throws Exception {
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    otpDetails.setOtpGenerateCount(userBlockCount);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    Whitebox.invokeMethod(otpServiceSpyMock, TestConstants.CHECK_FOR_BLOCKED_USER,
        otpDetails, maxOTPValidateCount, false);
    assertTrue(otpDetails.isBlockedOnGenerate());
    otpDetails.setOtpGenerateCount(1);
  }

  @Test
  public void validateOTPUserBlockTest() throws Exception {
    when(otpDetailsDao
        .findTopByUserIdentifierAndDeviceIdentifierAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
            any(String.class), any(String.class), eq(false), eq(false), eq(false)))
        .thenReturn(null);
    when(otpDetailsDao
        .findTopByUserIdentifierAndDeviceIdentifierAndIsDeletedOrderByIdDesc(
            this.userPasswordGenerationForm.getUsername(),
            this.userPasswordGenerationForm.getDeviceId(), false))
        .thenReturn(this.otpDetails);
    when(otpConfig.getUserBlockTimeInMinutes()).thenReturn(this.userBlockTimeInMinutes);
    ValidationResponse response = otpServiceSpyMock
        .checkBlockedOrValidateOTP(request, this.validationForm);
    assertFalse(response.isConducive());
  }

  @Test
  public void validateDefaultOTPSuccessTest() throws Exception {
    when(otpDetailsDao
        .findTopByUserIdentifierAndDeviceIdentifierAndUserStageTagAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
            any(String.class), any(String.class), any(String.class), eq(false), eq(false),
            eq(false)))
        .thenReturn(this.otpDetails);
    when(otpConfig.getMaxOtpValidateAttempts()).thenReturn(this.maxOTPValidateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    when(wfImplConfig.getValidateDefaultOTP()).thenReturn(true);
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = otpServiceSpyMock
        .checkBlockedOrValidateOTP(request, this.validationForm);
    assertTrue(response.isConducive());
  }

  @Test
  public void validateOTPSuccessTest() throws Exception {
    doReturn(this.otpDetails).when(otpDetailsDao)
        .findTopByUserIdentifierAndDeviceIdentifierAndUserStageTagAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
            any(String.class), any(String.class), any(String.class), eq(false), eq(false), eq(false));
    when(otpConfig.getMaxOtpValidateAttempts()).thenReturn(this.maxOTPValidateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    doReturn(false).when(wfImplConfig).getValidateDefaultOTP();
    PowerMockito.doReturn(true).
        when(otpServiceSpyMock, TestConstants.OTP_VALIDATE,
            any(String.class), any(String.class), any(Long.class));
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = otpServiceSpyMock
        .checkBlockedOrValidateOTP(request, this.validationForm);
    assertTrue(response.isConducive());
  }

  @Test
  public void validateOTPInvalidTest() throws Exception {
    doReturn(this.otpDetails).when(otpDetailsDao)
        .findTopByUserIdentifierAndDeviceIdentifierAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
            any(String.class), any(String.class), eq(false), eq(false), eq(false));
    when(otpConfig.getMaxOtpValidateAttempts()).thenReturn(this.maxOTPValidateCount);
    when(otpConfig.getOtpThresholdInMinutes()).thenReturn(this.otpThresholdInMinutes);
    doReturn(false).when(wfImplConfig).getValidateDefaultOTP();
    PowerMockito.doReturn(false).
        when(otpServiceSpyMock, TestConstants.OTP_VALIDATE,
            any(String.class), any(String.class), any(Long.class));
    when(otpDetailsDao.save(any(OTPDetails.class))).thenReturn(this.otpDetails);
    ValidationResponse response = otpServiceSpyMock
        .checkBlockedOrValidateOTP(request, this.validationForm);
    assertFalse(response.isConducive());
  }

}
