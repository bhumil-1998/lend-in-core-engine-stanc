package com.kuliza.lending.authorization.TestUtils;

public class TestConstants {
  public static final String USER_IDENTIFIER = "99999999";
  public static final String USER_DEVICEID = "123";
  public static final String USER_STAGE_TAG = "login";
  public static final String USER_IDTYPE = "mobile";
  public static final String SUCCESS_MESSAGE = "OTP Generated successfully";
  public static final String SECRET_KEY = "1795bc1a1d96d66edde352cbc1fe3e848b059cf0";
  public static final Integer USER_BLOCK_COUNT = 6;
  public static final Integer USER_OTP_GENERATE_COUNT = 5;
  public static final Integer USER_OTP_VALIDATE_COUNT = 5;
  public static final Integer USER_OTP_THRESHOLD_MINUTE = 5;
  public static final Integer USER_BLOCK_TIME = 30;
  public static final Integer OTP_EXPIRY_time = 1;
  public static final Integer OTP_LENGTH = 6;
  public static final String USER_IPADDRESS = "123.123.123.123";
  public static final String OTP = "123456";
  public static final String SOMETHING_WENT_WRONG = "Something went wrong";
  public static final String GENERATE_OTP = "generateOTP";
  public static final String VALIDATE_USER = "validateUser";
  public static final String VALIDATE_AND_REGISTER = "validateAndRegisterUser";
  public static final String CHECK_FOR_BLOCKED_USER = "checkForBlockedUser";
  public static final String OTP_VALIDATE = "otpValidate";
  public static final String OTP_SERVICE_GENERATE = "otpGenerate";
  public static final String OTP_GENERATION_TIME_KEY = "otpGenerationTime";
  public static final String VALID_EMAIL_ADDRESS = "info@getlend.in";
  public static final String AUTH_CODE = "12345";
  public static final String PASSWORD = "lendin";



}
