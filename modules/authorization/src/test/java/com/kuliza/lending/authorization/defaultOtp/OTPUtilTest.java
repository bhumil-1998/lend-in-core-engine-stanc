package com.kuliza.lending.authorization.defaultOtp;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.authorization.utils.OTPUtil;
import javax.servlet.http.HttpServletRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = AuthenticationConstants.class)
@WebAppConfiguration
public class OTPUtilTest {
  @Mock
  HttpServletRequest request;

  @Test
  public void getClientIPTest(){
    assertNull(OTPUtil.getClientIP(request));
  }

  @Test
  public void getClientIPNotNullTest(){
    when(request.getHeader(any(String.class))).thenReturn("");
    when(request.getRemoteAddr()).thenReturn("");
    assertEquals("", OTPUtil.getClientIP(request));
  }

  @Test
  public void checkOptionalOTPWrongTest(){
    assertFalse(OTPUtil.checkOptionalOTP("123456"));
  }

  @Test
  public void getDeviceIdentifierTest(){
    String deviceId = "1234";
    assertEquals(deviceId, OTPUtil.getDeviceIdentifier(request, deviceId));
  }

  @Test
  public void getDeviceIdentifierNullTest(){
    assertNull(null, OTPUtil.getDeviceIdentifier(request, null));
  }

}
