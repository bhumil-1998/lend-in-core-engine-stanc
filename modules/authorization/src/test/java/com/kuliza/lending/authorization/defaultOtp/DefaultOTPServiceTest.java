package com.kuliza.lending.authorization.defaultOtp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;

import com.kuliza.lending.authorization.AuthorizationApplication;
import com.kuliza.lending.authorization.config.OTPConfig;
import com.kuliza.lending.authorization.config.WfImplConfig;
import com.kuliza.lending.authorization.service.DefaultOTPService;
import com.kuliza.lending.authorization.service.OTPService;
import com.kuliza.lending.authorization.service.OTPServiceWithCache;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.authorization.utils.AuthenticationConstants.UserIdTypes;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.authorization.TestUtils.TestConstants;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import java.lang.reflect.Method;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = AuthorizationApplication.class)
@WebAppConfiguration
public class DefaultOTPServiceTest {

  @Mock
  private OTPService otpService;

  @Mock
  private OTPServiceWithCache otpServiceWithCache;

  @Mock
  private OTPConfig otpConfig;

  @Mock
  private WfImplConfig wfImplConfig;

  @Mock
  HttpServletRequest request;

  @InjectMocks
  DefaultOTPService defaultOTPService;

  private UserPasswordGenerationForm userPasswordGenerationForm;
  private ValidationResponse validationResponseSuccess;
  private ValidationResponse validationResponseFailure;
  private UserValidationinputForm validationForm;
  private ApiResponse apiResponse;

  @Before
  public void init() {
    this.userPasswordGenerationForm = new UserPasswordGenerationForm(TestConstants.USER_IDENTIFIER,
        UserIdTypes.mobile, TestConstants.USER_DEVICEID, TestConstants.USER_STAGE_TAG);
    this.validationResponseSuccess = new ValidationResponse(true, TestConstants.SUCCESS_MESSAGE);
    this.validationForm = new UserValidationinputForm(TestConstants.USER_IDENTIFIER,
        TestConstants.OTP,
        UserIdTypes.mobile, TestConstants.USER_DEVICEID, DeviceType.android,
        TestConstants.USER_STAGE_TAG);
    this.validationResponseFailure = new ValidationResponse(false,
        TestConstants.SOMETHING_WENT_WRONG);
    this.apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
  }

  public String getDefaultOTP() {
    Date today = new Date();
    return CommonHelperFunctions.getDateStringValue(today, AuthenticationConstants.OTP_DEFAULT_FORMAT);
  }

  @Test
  public void generateOTPTest() throws Exception {
    when(wfImplConfig.getGenerateOTP()).thenReturn(true);
    when(otpConfig.getUseCacheForOTP()).thenReturn(true);
    when(otpServiceWithCache.otpGenerate(request, userPasswordGenerationForm))
        .thenReturn(this.validationResponseSuccess);
    Method method = DefaultOTPService.class
        .getDeclaredMethod(TestConstants.GENERATE_OTP, HttpServletRequest.class,
            UserPasswordGenerationForm.class);
    method.setAccessible(true);
    ValidationResponse actualResponse = (ValidationResponse) method
        .invoke(defaultOTPService, request, userPasswordGenerationForm);
    assertTrue(actualResponse.isConducive());
  }

  @Test
  public void generateOTPDBTest() throws Exception {
    when(wfImplConfig.getGenerateOTP()).thenReturn(true);
    when(otpConfig.getUseCacheForOTP()).thenReturn(false);
    when(ReflectionTestUtils.invokeMethod(otpService, TestConstants.OTP_SERVICE_GENERATE, request,
        userPasswordGenerationForm)).thenReturn(this.validationResponseSuccess);
    Method method = DefaultOTPService.class
        .getDeclaredMethod(TestConstants.GENERATE_OTP, HttpServletRequest.class,
            UserPasswordGenerationForm.class);
    method.setAccessible(true);
    ValidationResponse actualResponse = (ValidationResponse) method
        .invoke(defaultOTPService, request, userPasswordGenerationForm);
    assertTrue(actualResponse.isConducive());
  }

  @Test
  public void generateUserPasswordTest() throws Exception {
    when(otpServiceWithCache.otpGenerate(request, userPasswordGenerationForm))
        .thenReturn(this.validationResponseSuccess);
    ApiResponse response = defaultOTPService
        .generateUserPassword(request, userPasswordGenerationForm);
    assertEquals(Constants.SUCCESS_CODE, response.getStatus());
  }

  @Test
  public void generateUserPasswordFailureTest() throws Exception {
    when(wfImplConfig.getGenerateOTP()).thenReturn(true);
    when(otpConfig.getUseCacheForOTP()).thenReturn(true);
    doReturn(validationResponseFailure).when(otpServiceWithCache)
        .otpGenerate(request, userPasswordGenerationForm);
    ApiResponse response = defaultOTPService
        .generateUserPassword(request, userPasswordGenerationForm);
    assertEquals(Constants.FAILURE_CODE, response.getStatus());
  }

  @Test
  public void validateUserTest() throws Exception {
    when(otpConfig.getUseCacheForOTP()).thenReturn(true);
    when(wfImplConfig.getValidateDefaultOTP()).thenReturn(true);
    when(otpServiceWithCache.checkBlockedOrValidateOTP(request, this.validationForm))
        .thenReturn(this.validationResponseSuccess);
    Method method = DefaultOTPService.class
        .getDeclaredMethod(TestConstants.VALIDATE_USER, HttpServletRequest.class, UserValidationinputForm.class);
    method.setAccessible(true);
    ValidationResponse response = (ValidationResponse) method
        .invoke(defaultOTPService, request, this.validationForm);
    assertTrue(response.isConducive());
  }

  @Test
  public void validateUserDBTest() throws Exception {
    when(otpConfig.getUseCacheForOTP()).thenReturn(false);
    when(wfImplConfig.getValidateDefaultOTP()).thenReturn(true);
    when(otpService.checkBlockedOrValidateOTP(request, this.validationForm))
        .thenReturn(this.validationResponseSuccess);
    Method method = DefaultOTPService.class
        .getDeclaredMethod(TestConstants.VALIDATE_USER, HttpServletRequest.class, UserValidationinputForm.class);
    method.setAccessible(true);
    ValidationResponse response = (ValidationResponse) method
        .invoke(defaultOTPService, request, this.validationForm);
    assertTrue(response.isConducive());
  }

}
