package com.kuliza.lending.notifications.pojo;


public class NotificationProviderResponseData {

	Integer statusCode;
	
	String statusMessage;

	String refIdIB;
	
	String dataToSave;

	public NotificationProviderResponseData() {
		super();
	}

	public NotificationProviderResponseData(Integer statusCode, String statusMessage, String refIdIB,
			String dataToSave) {
		super();
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.refIdIB = refIdIB;
		this.dataToSave = dataToSave;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getRefIdIB() {
		return refIdIB;
	}

	public void setRefIdIB(String refIdIB) {
		this.refIdIB = refIdIB;
	}

	public String getDataToSave() {
		return dataToSave;
	}

	public void setDataToSave(String dataToSave) {
		this.dataToSave = dataToSave;
	}

}
