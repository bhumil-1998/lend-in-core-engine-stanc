package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;
import com.kuliza.lending.notifications.utils.NotificationConstants.VariableTypes;

public class VariablesRequestData {

	Long id;
	
	@NotNull(message = "Variable Key cannot be null.")
	@NotEmpty(message = "Variable key cannot be empty.")
	@Size(max = 255)
	String variableKey;

	@NotNull(message = "Variable name cannot be null.")
	@NotEmpty(message = "Variable name cannot be empty.")
	@Size(max = 255)
	String variableLabel;

	@Size(max = 255)
	String description;

	@NotNull(message = "System name cannot be null.")
	Systems system;

	@NotNull(message = "Variable Types cannot be null.")
	VariableTypes variableType;

	public VariablesRequestData() {
		super();
	}

	public VariablesRequestData(Long id, String variableKey, String variableLabel, String description, Systems system,
			VariableTypes variableType) {
		super();
		this.id = id;
		this.variableKey = variableKey;
		this.variableLabel = variableLabel;
		this.description = description;
		this.system = system;
		this.variableType = variableType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVariableKey() {
		return variableKey;
	}

	public void setVariableKey(String variableKey) {
		this.variableKey = variableKey;
	}

	public String getVariableLabel() {
		return variableLabel;
	}

	public void setVariableLabel(String variableLabel) {
		this.variableLabel = variableLabel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Systems getSystem() {
		return system;
	}

	public void setSystem(Systems system) {
		this.system = system;
	}

	public VariableTypes getVariableType() {
		return variableType;
	}

	public void setVariableType(VariableTypes variableType) {
		this.variableType = variableType;
	}
}
