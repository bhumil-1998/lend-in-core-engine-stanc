package com.kuliza.lending.notifications.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.models.NotificationRangesModel;
import com.kuliza.lending.notifications.models.NotificationVariablesModel;

@Transactional
public interface NotificationRangesDao extends CrudRepository<NotificationRangesModel, Long> {

	public NotificationRangesModel findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<NotificationRangesModel> findByNotificationModuleAndIsDeleted(
			NotificationModuleModel notificationModuleModel, boolean isDeleted);

	public NotificationRangesModel findByNotificationModuleAndNotificationVariablesModelAndIsDeleted(
			NotificationModuleModel notificationModuleModel, NotificationVariablesModel notificationVariablesModel,
			boolean isDeleted);
}
