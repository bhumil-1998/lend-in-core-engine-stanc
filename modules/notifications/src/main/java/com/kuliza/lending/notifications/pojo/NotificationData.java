package com.kuliza.lending.notifications.pojo;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class NotificationData {

	@NotNull(message = "Identifier cannot be null.")
	@NotEmpty(message = "Identifier cannot be empty.")
	String identifier;

	@NotNull(message = "Decision Key cannot be null.")
	@NotEmpty(message = "Decision Key cannot be empty.")
	String decisionKey;
	
	@NotNull(message = "Varables cannot be null.")
	Map<String, Object> variables;

	public NotificationData() {
		super();
	}

	public NotificationData(String identifier, String decisionKey,
			Map<String, Object> variables) {
		super();
		this.identifier = identifier;
		this.decisionKey = decisionKey;
		this.variables = variables;
	}
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDecisionKey() {
		return decisionKey;
	}

	public void setDecisionKey(String decisionKey) {
		this.decisionKey = decisionKey;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
	}

	@Override
	public String toString() {
		return "Notification Data Set: { identifier = " + identifier + ", decision key = " + decisionKey;
	}
	
}
