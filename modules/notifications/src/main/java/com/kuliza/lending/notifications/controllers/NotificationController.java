package com.kuliza.lending.notifications.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.notifications.pojo.EmailRequestData;
import com.kuliza.lending.notifications.pojo.SystemNotificationRequestData;
import com.kuliza.lending.notifications.pojo.TriggerNotificationRequestData;
import com.kuliza.lending.notifications.services.EmailServiceTask;
import com.kuliza.lending.notifications.services.NotificationService;
import com.kuliza.lending.notifications.services.SMSServiceTask;
import com.kuliza.lending.notifications.utils.NotificationConstants;

import com.kuliza.lending.notifications.utils.SwaggerConstants;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(NotificationConstants.NOTIFICATION_CONTROLLER_ENDPOINT)
public class NotificationController {

	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private SMSServiceTask smsServiceTask;
	
	@Autowired
	private EmailServiceTask emailServiceTask;

	@ApiOperation(value = SwaggerConstants.NC_CREATE_NOTIIFICATION )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NC_CREATE_NOTIIFICATION_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PostMapping(NotificationConstants.CREATE_NOTIFICATION_API_ENDPOINT)
	public ResponseEntity<Object> createNotification(@Valid @RequestBody SystemNotificationRequestData request) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationService.createNotification(request));
	}
	
	@ApiOperation(value = SwaggerConstants.NC_SEND_NOTIIFICATION )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NC_SEND_NOTIIFICATION_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PostMapping(NotificationConstants.SEND_NOTIFICATION_API_ENDPOINT)
	public ResponseEntity<Object> sendNotificaion(@Valid @RequestBody String request) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationService.sendNotification());
	}
	
	@ApiOperation(value = SwaggerConstants.NC_TRIGGER_SMS )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NC_TRIGGER_SMS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PostMapping(NotificationConstants.TRIGGER_SMS_API_ENDPOINT)
	public ResponseEntity<Object> triggerSMS(@Valid @RequestBody TriggerNotificationRequestData triggerRequest) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(smsServiceTask.sendSyncViaValueFirst(triggerRequest.getSourceType(),
				triggerRequest.getSourceId(), triggerRequest.getTemplateId()));
	}
	
	@ApiOperation(value = SwaggerConstants.NC_TRIGGER_EMAIL )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NC_TRIGGER_EMAIL_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PostMapping(NotificationConstants.TRIGGER_EMAIL_API_ENDPOINT)
	public ResponseEntity<Object> triggerEmail(@Valid @RequestBody TriggerNotificationRequestData triggerRequest) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(emailServiceTask.sendSyncViaGoogleSMTP(triggerRequest.getSourceType(),
			triggerRequest.getSourceId(), triggerRequest.getTemplateId()));
	}
	
	@ApiOperation(value = SwaggerConstants.NC_SEND_EMAIL )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NC_SEND_EMAIL_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)	
	@PostMapping(NotificationConstants.SEND_EMAIL_API_ENDPOINT)
	public ResponseEntity<Object> sendEmail(@Valid @RequestBody EmailRequestData emailObject) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(
			notificationService.sendEmail(emailObject.getTo(), emailObject.getSubject(), emailObject.getBody(), emailObject.getDocIds()));

	}

}
