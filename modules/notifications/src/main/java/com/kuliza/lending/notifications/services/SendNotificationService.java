package com.kuliza.lending.notifications.services;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.notifications.integrations.EmailSMTPIntegration;
import com.kuliza.lending.notifications.integrations.SMSValueFirstIntegration;
import com.kuliza.lending.notifications.models.NotificationModel;

@Service
public class SendNotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendNotificationService.class);

	@Autowired
	SMSValueFirstIntegration smsValueFirstIntegration;
	
	@Autowired
    private EmailSMTPIntegration emailSMTPIntegration;

	public ApiResponse sendSms(NotificationModel notificationObject) {
		// TODO Auto-generated method stub
		ResponseEntity<String> response = null;
		try {
			Map<String, String> payload = smsValueFirstIntegration.buildRequestPayload(notificationObject);
			LOGGER.info(CommonHelperFunctions.getStringValue(payload));
			response = smsValueFirstIntegration.hitIB(payload, notificationObject.getTemplateModel().getNotificationChannelModel());
			smsValueFirstIntegration.parseResponse(response, notificationObject);
			LOGGER.info(CommonHelperFunctions.getStringValue(response));
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, CommonHelperFunctions.jsonStringToMap(response.getBody()));
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, e.getMessage());
		}
	}
	
	public ApiResponse sendEmail(NotificationModel notificationObject) {
		// TODO Auto-generated method stub
		ResponseEntity<String> response = null;
		try {
			Map<String, String> payload = emailSMTPIntegration.buildRequestPayload(notificationObject);
			LOGGER.info(CommonHelperFunctions.getStringValue(payload));
			response = emailSMTPIntegration.hitIB(payload, notificationObject.getTemplateModel().getNotificationChannelModel());
			emailSMTPIntegration.parseResponse(response, notificationObject);
			LOGGER.info(CommonHelperFunctions.getStringValue(response));
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, CommonHelperFunctions.jsonStringToMap(response.getBody()));
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, e.getMessage());
		}
	}
}
