package com.kuliza.lending.notifications.kafka;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.TemplateModel;
import com.kuliza.lending.notifications.services.SendNotificationService;

@EnableKafka
@Component
@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA")
public class NotificationReceiverKafka {

	@Autowired
	NotificationDao notificationDao;

	@Autowired
	SendNotificationService sendNotificationService;	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationReceiverKafka.class);

	@KafkaListener(topics = "sms", groupId = "group-id")
	public void hitSMS(final String uuid) throws Exception {
		LOGGER.info("Hit SMS for notification id: {}", uuid);
		try {
			NotificationModel notificationObject = notificationDao.findById(uuid);
			TemplateModel templateObject = notificationObject.getTemplateModel();
			if (templateObject != null) {
				sendNotificationService.sendSms(notificationObject);
			} else {
				LOGGER.info("Template not found.");
			}
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		}
	}
	
	@KafkaListener(topics = "email", groupId = "group-id")
	public void hitEmail(final String uuid) throws Exception {
		LOGGER.info("Hit SMS for notification id: {}", uuid);
		try {
			NotificationModel notificationObject = notificationDao.findById(uuid);
			TemplateModel templateObject = notificationObject.getTemplateModel();
			if (templateObject != null) {
				sendNotificationService.sendEmail(notificationObject);
			} else {
				LOGGER.info("Template not found.");
			}
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		}
	}
	
}
