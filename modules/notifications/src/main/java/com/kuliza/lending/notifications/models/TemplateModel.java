package com.kuliza.lending.notifications.models;

import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.notifications.pojo.ConfigTemplateRequestData;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

@Entity
@Table(name = "config_template")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class TemplateModel extends BaseModel {

	@Column(nullable = false)
	private String name;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "channelId", nullable = false)
	private NotificationChannelModel notificationChannelModel;
	
	@Column
	private String fromAddress;

	@Column(nullable = false)
	private String toAddress;

	@Type(type = "json")
	@Column(columnDefinition = "json")
	private Map<String, Object> toMetadata;
	
	@Column
	private String subject;
	
	@Column(columnDefinition="LONGTEXT")
	private String body;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = ListConverter.class)
    private List<String> variables;
	
	public TemplateModel() {
		super();
		this.setIsDeleted(false);
	}

	public TemplateModel(String name, NotificationChannelModel notificationChannelModel, String fromAddress, String toAddress,
			Map<String, Object> toMetadata, String subject, String body, List<String> variables) {
		super();
		this.name = name;
		this.notificationChannelModel = notificationChannelModel;
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;
		this.toMetadata = toMetadata;
		this.subject = subject;
		this.body = body;
		this.variables = variables;
		this.setIsDeleted(false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NotificationChannelModel getNotificationChannelModel() {
		return notificationChannelModel;
	}

	public void setNotificationChannelModel(NotificationChannelModel notificationChannelModel) {
		this.notificationChannelModel = notificationChannelModel;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public Map<String, Object> getToMetadata() {
		return toMetadata;
	}

	public void setToMetadata(Map<String, Object> toMetadata) {
		this.toMetadata = toMetadata;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}
	
	public void setTemplateModel(ConfigTemplateRequestData configTemplateRequestData, NotificationChannelModel channelModel) {
		this.name = configTemplateRequestData.getName();
		this.notificationChannelModel = channelModel;
		this.fromAddress = configTemplateRequestData.getFromAddress();
		this.toAddress = configTemplateRequestData.getToAddress();
		this.toMetadata = configTemplateRequestData.getToMetadata();
		this.subject = configTemplateRequestData.getSubject();
		this.body = configTemplateRequestData.getBody();
		this.variables = configTemplateRequestData.getVariables();
	}
	
}
