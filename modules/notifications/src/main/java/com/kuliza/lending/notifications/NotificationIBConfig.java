package com.kuliza.lending.notifications;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "notificationIB")
public class NotificationIBConfig {
	String protocol;
	String host;
	Integer port;
	String subURL;
	String hash;
	
	@Value("${notification.ib.sms.valueFirst.username}")
	String valueFirstUsername;
	
	@Value("${notification.ib.sms.valueFirst.password}")
	String valueFirstPassword;
	
	@Value("${notification.ib.sms.valueFirst.from}")
	String valueFirstFrom;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getSubURL() {
		return subURL;
	}

	public void setSubURL(String subURL) {
		this.subURL = subURL;
	}

	public String getValueFirstUsername() {
		return valueFirstUsername;
	}

	public void setValueFirstUsername(String valueFirstUsername) {
		this.valueFirstUsername = valueFirstUsername;
	}

	public String getValueFirstPassword() {
		return valueFirstPassword;
	}

	public void setValueFirstPassword(String valueFirstPassword) {
		this.valueFirstPassword = valueFirstPassword;
	}

	public String getValueFirstFrom() {
		return valueFirstFrom;
	}

	public void setValueFirstFrom(String valueFirstFrom) {
		this.valueFirstFrom = valueFirstFrom;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
