package com.kuliza.lending.notifications.jms;

import java.util.HashMap;
import java.util.Map;

import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.engine.RuntimeService;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.decision_table.services.DecisionTableService;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.utils.NotificationConstants;

@Service
// @ConditionalOnExpression("${broker.name.rabbit:true}")
public class ScheduleProducerJMS {

	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	CmmnRuntimeService cmmnRuntimeService;
	
	@Autowired
	RuntimeService runtimeService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleProducerJMS.class);

	public Boolean createNotificationData(String identifier, String decisionKey, SourceType sourceType)
			throws JsonProcessingException, JSONException {
		try {
			Map<String, Object> variables = new HashMap<>();
			if (sourceType.equals(SourceType.cmmn)) {
				variables = cmmnRuntimeService.getVariables(identifier);
			} else {
				variables = runtimeService.getVariables(identifier);
			}
			LOGGER.info("In create notification data received");
			NotificationData notificationData = new NotificationData(identifier, decisionKey, variables);
			jmsTemplate.convertAndSend(NotificationConstants.JMS_NOTIFICATION_ROUTING_KEY, notificationData);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
