package com.kuliza.lending.notifications.services;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.utils.NotificationConstants;

@Service("EmailServiceTask")
public class EmailServiceTask {

	private static final Logger logger = LoggerFactory.getLogger(EmailServiceTask.class);

	@Autowired
	SendNotificationService sendNotificationService;
	
	@Autowired
	CreateNotificationService createNotificationService;

	@Autowired
	KafkaTemplate<String, String> kafkaTemplate;
	
	public ApiResponse sendSyncViaGoogleSMTP(SourceType sourceType, String sourceId, Long templateId) {
		try {
			NotificationModel notificationObject = createNotificationService.createNotificationObject(sourceId,
					templateId, sourceType, true);
			return sendNotificationService.sendEmail(notificationObject);
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, e.getMessage());
		}
	}
	
	public ApiResponse sendAsycViaGoogleSMTP(SourceType sourceType, String sourceId, Long templateId, Map<String, Object> variables) {
		try {
			NotificationModel notificationObject = createNotificationService.createNotificationObject(sourceId,
					templateId, sourceType, null);
			kafkaTemplate.send(NotificationConstants.EMAIL_TOPIC,
					notificationObject.getId());
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Email Triggered Successfully.");
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}
}
