package com.kuliza.lending.notifications.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "config_service_provider")
public class ServiceProviderModel extends BaseModel {

	@Column(nullable = false, unique = true)
	private String name;

	@Column
	private String fromAddress;

	@Column(nullable = false)
	private String ibEndpoint;

	public ServiceProviderModel() {
		super();
		this.setIsDeleted(false);
	}

	public ServiceProviderModel(String name, String fromAddress, String ibEndpoint) {
		super();
		this.name = name;
		this.fromAddress = fromAddress;
		this.ibEndpoint = ibEndpoint;
		this.setIsDeleted(false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	@JsonIgnore
	public String getIbEndpoint() {
		return ibEndpoint;
	}

	public void setIbEndpoint(String ibEndpoint) {
		this.ibEndpoint = ibEndpoint;
	}

}
