package com.kuliza.lending.notifications.services;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.dao.NotificationHistoryDao;
import com.kuliza.lending.notifications.dao.TemplateDao;
import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationHistoryModel;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.TemplateModel;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;


@Service
public class CreateNotificationService {
	
	@Autowired
	TemplateDao templateDao;

	@Autowired
	CmmnRuntimeService cmmnRuntimeService;
	

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	NotificationDao notificationDao;
	
	@Autowired
	NotificationHistoryDao notificationHistoryDao;

	@Transactional
	public void createNotificationObjects(ChannelTypes channelType, NotificationData notificationData, Map<String, Object> notificationConfig) {
		TemplateModel templateModel = templateDao.findByIdAndIsDeleted(
				CommonHelperFunctions.getLongValue(
						notificationConfig.get(
								NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(0)))
				, false);
		NotificationChannelModel notificationChannelModel = templateModel.getNotificationChannelModel();

		List<Date> timeFrames = getFrames(notificationChannelModel,
				CommonHelperFunctions.getIntegerValue(notificationConfig
						.get(NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(1))));

		Map<String, Object> caseVariables = notificationData.getVariables();

		
		List<String> templateVariableKeys = templateModel.getVariables();
		caseVariables = templateVariableKeys.stream().filter(caseVariables::containsKey)
				.collect(Collectors.toMap(Function.identity(), caseVariables::get));
		for (Date timeFrame : timeFrames) {
			NotificationModel notificationObject = new NotificationModel();
			notificationObject.setSourceId(notificationData.getIdentifier());
			notificationObject.setSendTime(timeFrame);
			notificationObject.setSendDate(new Date());
			notificationObject.setTemplateModel(templateModel);
			notificationObject.setUserAttributes(caseVariables);
			notificationObject.setIsSync(false);
			notificationObject = notificationDao.save(notificationObject);
			NotificationHistoryModel notificationHistoryModel = new NotificationHistoryModel(notificationObject);
			notificationHistoryDao.save(notificationHistoryModel);	
		}
	}

	private List<Date> getFrames(NotificationChannelModel notificationChannelModel, Integer frequency) {
		List<Date> dateFrames = new ArrayList<>();
		Calendar startTime = Calendar.getInstance();
		startTime.setTime(notificationChannelModel.getStartTime());
		Calendar endTime = Calendar.getInstance();
		endTime.setTime(notificationChannelModel.getEndTime());
		if (endTime.compareTo(startTime) > 0) {
			long hoursBetween = ChronoUnit.HOURS.between(startTime.toInstant(), endTime.toInstant());
			long duration = hoursBetween / frequency;
			dateFrames.add(startTime.getTime());
			for (int i = 1; i < frequency; i++) {
				startTime.add(Calendar.HOUR_OF_DAY, (int) duration);
				dateFrames.add(startTime.getTime());
			}
		}
		return dateFrames;
	}
	
	@Transactional
	public NotificationModel createNotificationObject(String sourceId, Long templateId, SourceType sourceType, Boolean isSync) throws Exception {
		NotificationModel notificationObject = new NotificationModel();
		Date today = new Date();
		Map<String, Object> variables = new HashMap<>();
		if (sourceType.equals(SourceType.cmmn)) {
			variables = cmmnRuntimeService.getVariables(sourceId);
		} else {
			variables = runtimeService.getVariables(sourceId);
		}
		TemplateModel templateObject = templateDao.findByIdAndIsDeleted(templateId, false);
		variables = templateObject.getVariables().stream().filter(variables::containsKey)
				.collect(Collectors.toMap(Function.identity(), variables::get));
		notificationObject.setSourceId(sourceId);
		notificationObject.setSendTime(today);
		notificationObject.setSendDate(today);
		notificationObject.setTemplateModel(templateObject);
		notificationObject.setUserAttributes(variables);
		notificationObject.setIsSync(CommonHelperFunctions.getBooleanValue(isSync));
		notificationObject = notificationDao.save(notificationObject);
		NotificationHistoryModel notificationHistoryModel = new NotificationHistoryModel(notificationObject);
		notificationHistoryDao.save(notificationHistoryModel);	
		return notificationObject;
	}
}
