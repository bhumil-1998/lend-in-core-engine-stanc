package com.kuliza.lending.notifications.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationVariablesModel;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;

@Transactional
public interface NotificationVariablesDao extends CrudRepository<NotificationVariablesModel, Long> {

	public NotificationVariablesModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<NotificationVariablesModel> findBySystemAndIsDeleted(Systems system, boolean isDeleted);
	
	public NotificationVariablesModel findByVariableKeyAndSystemAndIsDeleted(String variableKey, Systems system, boolean isDeleted);

}
