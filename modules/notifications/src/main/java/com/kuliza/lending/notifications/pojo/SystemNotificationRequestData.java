package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.kuliza.lending.common.utils.Constants.SourceType;

import java.util.List;

public class SystemNotificationRequestData {

	@NotNull(message = "Identifier List cannot be null")
	@Size(min = 1)
	List<String> identifierList;

	@NotNull(message = "Module Id cannot be null")
	public long moduleId;

	@NotNull(message = "Source type cannot be null")
	SourceType sourceType;

	public SystemNotificationRequestData() {
		super();
	}

	public SystemNotificationRequestData(List<String> identifierList, long moduleId, SourceType sourceType) {
		super();
		this.identifierList = identifierList;
		this.moduleId = moduleId;
		this.sourceType = sourceType;
	}

	public List<String> getIdentifierList() {
		return identifierList;
	}

	public void setIdentifierList(List<String> identifierList) {
		this.identifierList = identifierList;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

}
