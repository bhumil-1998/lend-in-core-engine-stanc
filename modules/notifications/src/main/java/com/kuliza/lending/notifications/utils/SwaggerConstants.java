package com.kuliza.lending.notifications.utils;

public class SwaggerConstants {
	public static final String NC_CREATE_NOTIFICATION = "To create notification";
	public static final String NC_CREATE_NOTIFICATION_RESPONSE = "";
	
	public static final String NCC_CREATE_MODULE = "To create module";
	public static final String NCC_CREATE_MODULE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"assignee\": \"admin@kuliza.com\",\n" + 
			"        \"createdTime\": \"20 Jan 2020\",\n" + 
			"        \"decisionTableKey\": null,\n" + 
			"        \"description\": \"DMN for post-due notification\",\n" + 
			"        \"id\": 44,\n" + 
			"        \"modifiedTime\": \"20 Jan 2020\",\n" + 
			"        \"name\": \"Post-Due Notifications\",\n" + 
			"        \"status\": \"CREATED\",\n" + 
			"        \"step\": 1,\n" + 
			"        \"system\": \"COLLECTIONS\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_GET_MODULE = "To get modules";
	public static final String NCC_GET_MODULE_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"assignee\": \"admin@kuliza.com\",\n" + 
			"            \"description\": \"\",\n" + 
			"            \"groupName\": \"OrderTesting\",\n" + 
			"            \"id\": 35,\n" + 
			"            \"modified\": \"09 Dec 2019\",\n" + 
			"            \"status\": \"CREATED\",\n" + 
			"            \"step\": 1,\n" + 
			"            \"system\": \"LMS\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"assignee\": \"admin@kuliza.com\",\n" + 
			"            \"description\": \"La la laaa..la la..\",\n" + 
			"            \"groupName\": \"Full Testing 9 Dec\",\n" + 
			"            \"id\": 36,\n" + 
			"            \"modified\": \"17 Dec 2019\",\n" + 
			"            \"status\": \"CREATED\",\n" + 
			"            \"step\": 1,\n" + 
			"            \"system\": \"COLLATERAL\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"assignee\": \"admin@kuliza.com\",\n" + 
			"            \"description\": \"\",\n" + 
			"            \"groupName\": \"testing version 2\",\n" + 
			"            \"id\": 37,\n" + 
			"            \"modified\": \"17 Jan 2020\",\n" + 
			"            \"status\": \"CREATED\",\n" + 
			"            \"step\": 3,\n" + 
			"            \"system\": \"LMS\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"assignee\": \"admin@kuliza.com\",\n" + 
			"            \"description\": \"\",\n" + 
			"            \"groupName\": \"testing version 3\",\n" + 
			"            \"id\": 38,\n" + 
			"            \"modified\": \"17 Jan 2020\",\n" + 
			"            \"status\": \"CREATED\",\n" + 
			"            \"step\": 3,\n" + 
			"            \"system\": \"LMS\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"assignee\": \"admin@kuliza.com\",\n" + 
			"            \"description\": \"for checking flow\",\n" + 
			"            \"groupName\": \"testing30dec\",\n" + 
			"            \"id\": 40,\n" + 
			"            \"modified\": \"14 Jan 2020\",\n" + 
			"            \"status\": \"CREATED\",\n" + 
			"            \"step\": 1,\n" + 
			"            \"system\": \"BACKOFFICE\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"assignee\": \"admin@kuliza.com\",\n" + 
			"            \"description\": \"testing of newly deployed tasks & bugs  sfnson s ngvonfi nefibneifbneibnfibn ifbni nfn eifbnifbnefibnfibnfbndfb ndfib nfibnfibnfibn fibn fibn fibnfibn bnffnfnfindfibn fib fdkb dfjkb b ibeib fib fibebefief bifbdf bjfbejf bejfb efbeb eifb eifb eifb f bf bef\",\n" + 
			"            \"groupName\": \"Testing10Jan v ssf[ sobhsfoin osnv isvsi vbsi si sis is silb slib slisb lsbsl bs kslks skslkskl ssskjl  sosn os osb so bo bo b o o   dfbd wg wgwg . gw gg wgwg \",\n" + 
			"            \"id\": 43,\n" + 
			"            \"modified\": \"14 Jan 2020\",\n" + 
			"            \"status\": \"CREATED\",\n" + 
			"            \"step\": 1,\n" + 
			"            \"system\": \"COLLATERAL\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_DELETE_MODULE = "To delete module";
	public static final String NCC_DELETE_MODULE_RESPONSE = "{\n" + 
			"    \"data\": \"Module deleted succesfully.\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_CONFIG_SYSTEM = "To get systems";
	public static final String NCC_CONFIG_SYSTEM_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        \"LOS\",\n" + 
			"        \"LMS\",\n" + 
			"        \"BACKOFFICE\",\n" + 
			"        \"COLLECTIONS\",\n" + 
			"        \"COLLATERAL\"\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_VARIABLE_TYPES = "To get variables types";
	public static final String NCC_VARIABLE_TYPES_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        \"NUMBER\",\n" + 
			"        \"STRING\",\n" + 
			"        \"DATE\",\n" + 
			"        \"BOOLEAN\"\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_ADD_SERVICE_PROVIDER = "To add service provider";
	public static final String NCC_ADD_SERVICE_PROVIDER_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"id\": 6,\n" + 
			"        \"name\": \"SES Emaill\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_GET_SERVICE_PROVIDERS = "To get service providers";
	public static final String NCC_GET_SERVICE_PROVIDERS_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"id\": 1,\n" + 
			"            \"name\": \"Value first SMS\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"id\": 2,\n" + 
			"            \"name\": \"SMTP Email\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"id\": 3,\n" + 
			"            \"name\": \"Gupshup SMS\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"id\": 4,\n" + 
			"            \"name\": \"SES Email\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"id\": 6,\n" + 
			"            \"name\": \"SES Emaill\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_DELETE_SERVICE_PROVIDERS = "To delete service provider";
	public static final String NCC_DELETE_SERVICE_PROVIDERS_RESPONSE = "{\n" + 
			"	\"data\":\"Service Provider deleted succesfully.\",\n" + 
			"	\"status\":200,\n" + 
			"	\"message\":\"success\"\n" + 
			"}";
	
	public static final String NCC_EDIT_NOTIFICATION = "To edit notification";
	public static final String NCC_EDIT_NOTIFICATION_RESPONSE = "{\n" + 
			"    \"data\": \"Module not found\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_CONFIG_STEPPER_POST = "This is used to create or update channels, variables, ranges etc (depends on the stepper value, these are used to define rules. The response mentioned below is for ranges";
	public static final String NCC_CONFIG_STEPPER_POST_RESPONSE = "{\n" + 
			"	\"data\":\"Ranges added successfully\",\n" + 
			"	\"status\":200,\n" + 
			"	\"message\":\"success\"\n" + 
			"}";
	
	public static final String NCC_CONFIG_STEPPER_GET = "This is used to get channels, variables, ranges etc  (depends on the stepper value), the response given below is for global variables";
	public static final String NCC_CONFIG_STEPPER_GET_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"description\": \"Pincode of the Main Applicant's primary Address\",\n" + 
			"            \"id\": 29,\n" + 
			"            \"key\": \"pincode\",\n" + 
			"            \"label\": \"Pin Code\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"STRING\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"Outstanding Loan Amount\",\n" + 
			"            \"id\": 30,\n" + 
			"            \"key\": \"outstandingamount\",\n" + 
			"            \"label\": \"Outstanding Amount\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"NUMBER\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"Default Count\",\n" + 
			"            \"id\": 31,\n" + 
			"            \"key\": \"defaultcount\",\n" + 
			"            \"label\": \"No of Defaults\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"NUMBER\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"Collateral Present\",\n" + 
			"            \"id\": 33,\n" + 
			"            \"key\": \"collateralpresent\",\n" + 
			"            \"label\": \"Collateral Present\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"BOOLEAN\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"Borrower VIP or not\",\n" + 
			"            \"id\": 34,\n" + 
			"            \"key\": \"vipstatus\",\n" + 
			"            \"label\": \"VIP\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"BOOLEAN\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"PTP Count\",\n" + 
			"            \"id\": 35,\n" + 
			"            \"key\": \"promisetopaycount\",\n" + 
			"            \"label\": \"Promise to Pay Count\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"NUMBER\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"dpd\",\n" + 
			"            \"id\": 44,\n" + 
			"            \"key\": \"dpd\",\n" + 
			"            \"label\": \"DPD\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"NUMBER\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"product\",\n" + 
			"            \"id\": 45,\n" + 
			"            \"key\": \"loanp\",\n" + 
			"            \"label\": \"loan Product\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"STRING\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"amount\",\n" + 
			"            \"id\": 46,\n" + 
			"            \"key\": \"dueamt\",\n" + 
			"            \"label\": \"Due Amount\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"NUMBER\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"\",\n" + 
			"            \"id\": 47,\n" + 
			"            \"key\": \"test1\",\n" + 
			"            \"label\": \"test1\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"NUMBER\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"7gu\",\n" + 
			"            \"id\": 49,\n" + 
			"            \"key\": \"test51\",\n" + 
			"            \"label\": \"test51\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"NUMBER\"\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"description\": \"osfjbos\",\n" + 
			"            \"id\": 54,\n" + 
			"            \"key\": \"date\",\n" + 
			"            \"label\": \"date\",\n" + 
			"            \"system\": \"COLLECTIONS\",\n" + 
			"            \"variableType\": \"DATE\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_CONFIG_STEPPER_DELETE = "To delete channel, variable, range etc. The response mentioned below is for global variables.";
	public static final String NCC_CONFIG_STEPPER_DELETE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"channelType\": \"EMAIL\",\n" + 
			"        \"endTime\": \"07:30:55\",\n" + 
			"        \"id\": 36,\n" + 
			"        \"notificationModule\": {\n" + 
			"            \"assignee\": \"admin@kuliza.com\",\n" + 
			"            \"createdTime\": \"14 Nov 2019\",\n" + 
			"            \"decisionTableKey\": null,\n" + 
			"            \"description\": \"bla bla bla\",\n" + 
			"            \"id\": 31,\n" + 
			"            \"modifiedTime\": \"07 Jan 2020\",\n" + 
			"            \"name\": \"testingFlow\",\n" + 
			"            \"status\": \"CREATED\",\n" + 
			"            \"step\": 1,\n" + 
			"            \"system\": \"LMS\"\n" + 
			"        },\n" + 
			"        \"serviceProviders\": [\n" + 
			"            {\n" + 
			"                \"providerId\": 1,\n" + 
			"                \"retries\": 1\n" + 
			"            },\n" + 
			"            {\n" + 
			"                \"providerId\": 0,\n" + 
			"                \"retries\": 0\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"startTime\": \"00:30:55\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NCC_CONFIG_STEPPER_PUT = "To update channel, variable, range etc";
	public static final String NCC_CONFIG_STEPPER_PUT_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"assignee\": \"admin@kuliza.com\",\n" + 
			"        \"createdTime\": \"09 Dec 2019\",\n" + 
			"        \"decisionTableKey\": null,\n" + 
			"        \"description\": \"La la laaa..la la..\",\n" + 
			"        \"id\": 36,\n" + 
			"        \"modifiedTime\": \"17 Dec 2019\",\n" + 
			"        \"name\": \"Full Testing 9 Dec\",\n" + 
			"        \"status\": \"CREATED\",\n" + 
			"        \"step\": 1,\n" + 
			"        \"system\": \"COLLATERAL\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NC_CREATE_NOTIIFICATION = "To create notification";
	public static final String NC_CREATE_NOTIIFICATION_RESPONSE = "{\n" + 
			"    \"data\": null,\n" + 
			"    \"message\": \"Success\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NC_SEND_NOTIIFICATION = "To send notification";
	public static final String NC_SEND_NOTIIFICATION_RESPONSE = "{\n" + 
			"    \"data\": 19800000,\n" + 
			"    \"message\": \"Send Notification @ \",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NC_DUMMY = "To create dummy end point";
	public static final String NC_DUMMY_RESPONSE = "{\n" + 
			"    \"data\": null,\n" + 
			"    \"message\": \"Success\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NC_TRIGGER_SMS = "Triggering sms end point";
	public static final String NC_TRIGGER_SMS_RESPONSE = "";
	
	public static final String NC_TRIGGER_EMAIL = "Triggering email end point";
	public static final String NC_TRIGGER_EMAIL_RESPONSE = "{\n" + 
			"    \"requestId\": \"d76a388c-8687-45d0-a1c4-29ce6fc062f2\",\n" + 
			"    \"sp#statusCode\": \"200\",\n" + 
			"    \"sp#statusMessage\": \"Email sent successfully.\",\n" + 
			"    \"sp#dataToSave\": \"Email sent successfully.\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String NC_SEND_EMAIL = "Send email";
	public static final String NC_SEND_EMAIL_RESPONSE = "{\n" + 
			"	\"data\":\"Email sent successfully.\",\n" + 
			"	\"message\":\"success\",\n" + 
			"	\"status\":200\n" + 
			"}";
}
