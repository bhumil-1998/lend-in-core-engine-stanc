package com.kuliza.lending.notifications.utils;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;

public class NotificationHelperFunction extends CommonHelperFunctions {
	
	private static final Logger logger = LoggerFactory.getLogger(CommonHelperFunctions.class);
	
	public static String substituteVariabes(Map<String, Object> variables, String str) {
		StrSubstitutor sub = new StrSubstitutor(variables, "{{", "}}");
		return sub.replace(str);
	}
	
	public static String getIBEndpoint(String protocol, String host, Integer port, String subURL) {
		return protocol + "://" + host + ":" + port + subURL;
	}
	
	public static String getDMSEndpoint(String protocol, String host, Integer port, String subURL) {
		return protocol + "://" + host + ":" + port + subURL;
	}
	
	
	public static Object getPOJOFromJsonString(String jsonString, Class<?> clazz) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(jsonString, clazz);
		} catch (JsonParseException e) {
			logger.error(getStackTrace(e));
			return null;
		} catch (JsonMappingException e) {
			logger.error(getStackTrace(e));
			return null;
		} catch (IOException e) {
			logger.error(getStackTrace(e));
			return null;
		}
	}
}
