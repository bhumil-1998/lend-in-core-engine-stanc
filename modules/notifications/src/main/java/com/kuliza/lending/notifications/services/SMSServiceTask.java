package com.kuliza.lending.notifications.services;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.utils.NotificationConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service("SMSServiceTask")
public class SMSServiceTask {

	private static final Logger logger = LoggerFactory.getLogger(SMSServiceTask.class);

	@Autowired
	SendNotificationService sendNotificationService;
	
	@Autowired
	CreateNotificationService createNotificationService;

	@Autowired
	KafkaTemplate<String, String> kafkaTemplate;

	public ApiResponse sendSyncViaValueFirst(SourceType sourceType, String sourceId, Long templateId) {
		try {
			NotificationModel notificationObject = createNotificationService.createNotificationObject(sourceId,
					templateId, sourceType, true);
			return sendNotificationService.sendSms(notificationObject);
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, e.getMessage());
		}
	}
	
	public ApiResponse sendAsyncViaValueFirst(SourceType sourceType, String sourceId, Long templateId) {
		try {
			NotificationModel notificationObject = createNotificationService.createNotificationObject(sourceId,
					templateId, sourceType, true);
			kafkaTemplate.send(NotificationConstants.SMS_TOPIC, notificationObject.getId());
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "SMS Triggered Successfully.");
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}
}
