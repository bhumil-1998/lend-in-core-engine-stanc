package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.common.utils.Constants.SourceType;

public class TriggerNotificationRequestData {

	@NotNull(message = "Source Identifier List cannot be null")
	@NotEmpty(message = "Source Identifier List cannot be empty")
	String sourceId;

	@NotNull(message = "Module Id cannot be null")
	public Long templateId;

	@NotNull(message = "Source type cannot be null")
	SourceType sourceType;

	public TriggerNotificationRequestData() {
		super();
	}

	public TriggerNotificationRequestData(String sourceId, Long templateId, SourceType sourceType) {
		super();
		this.sourceId = sourceId;
		this.templateId = templateId;
		this.sourceType = sourceType;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}
}
