package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;

public class ConfigModuleRequestData {

	@NotNull(message = "Group name cannot be null.")
	@NotEmpty(message = "Group name cannot be empty.")
	@Size(max = 255)
	String groupName;

	@Size(max = 255)
	String description;

	Long id;

	@NotNull(message = "System name cannot be null.")
	Systems system;
	
	String assignee;
	
	String modified;
	
	ModuleStatus status;

	Integer step;

	public ConfigModuleRequestData() {
		super();
	}

	public ConfigModuleRequestData(NotificationModuleModel moduleObject) {
		this.groupName = moduleObject.getName();
		this.description = moduleObject.getDescription();
		this.id = moduleObject.getId();
		this.system = moduleObject.getSystem();
		this.assignee = moduleObject.getAssignee();
		this.modified = (moduleObject.getModified() != null ? 
				CommonHelperFunctions.getDateInFormat(moduleObject.getModified(), NotificationConstants.MODULE_DATE_FORMAT) : "");
		this.status = moduleObject.getStatus();
		this.step = moduleObject.getStep();
	}
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Systems getSystem() {
		return system;
	}

	public void setSystem(Systems system) {
		this.system = system;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public ModuleStatus getStatus() {
		return status;
	}

	public void setStatus(ModuleStatus status) {
		this.status = status;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

}
