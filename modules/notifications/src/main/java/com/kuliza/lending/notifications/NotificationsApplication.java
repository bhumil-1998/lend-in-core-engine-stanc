package com.kuliza.lending.notifications;

import static com.kuliza.lending.common.utils.Constants.JASYPT_ENCRYPTOR_PASSWORD;

import javax.sql.DataSource;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

import liquibase.integration.spring.SpringLiquibase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kuliza" })
@EnableJms
@EnableAutoConfiguration
@EnableScheduling
@EnableRetry
@EnableEncryptableProperties
@EnableConfigurationProperties(LiquibaseProperties.class)
@PropertySources({
	@PropertySource("classpath:lend-in-modules.properties")
})
public class NotificationsApplication extends SpringBootServletInitializer {
	
	@Autowired
	private LiquibaseProperties properties;
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(NotificationsApplication.class);
	}

	public static void main(String[] args) {
		System.setProperty("jasypt.encryptor.password", JASYPT_ENCRYPTOR_PASSWORD);
		SpringApplication.run(NotificationsApplication.class);
	}
	
	@Bean
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog(this.properties.getChangeLog());
		liquibase.setContexts(this.properties.getContexts());
		liquibase.setDefaultSchema(this.properties.getDefaultSchema());
		liquibase.setDropFirst(this.properties.isDropFirst());
		liquibase.setShouldRun(this.properties.isEnabled());
		liquibase.setLabels(this.properties.getLabels());
		liquibase.setChangeLogParameters(this.properties.getParameters());
		liquibase.setRollbackFile(this.properties.getRollbackFile());
		liquibase.setDatabaseChangeLogLockTable("LOS_DATABASECHANGELOGLOCK");
		liquibase.setDatabaseChangeLogTable("LOS_DATABASECHANGELOG");
		return liquibase;
	}

}
