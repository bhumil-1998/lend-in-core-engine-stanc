package com.kuliza.lending.notifications.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.notifications.pojo.ConfigChannelListData;
import com.kuliza.lending.notifications.pojo.ConfigModuleRequestData;
import com.kuliza.lending.notifications.pojo.ConfigServiceProviderInputData;
import com.kuliza.lending.notifications.pojo.ConfigTemplateRequestData;
import com.kuliza.lending.notifications.pojo.DraftOrPublishNotificationTable;
import com.kuliza.lending.notifications.pojo.NotificationTableRequestData;
import com.kuliza.lending.notifications.pojo.RangesListRequestData;
import com.kuliza.lending.notifications.pojo.VariableListRequestData;
import com.kuliza.lending.notifications.services.NotificationConfigService;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;
import com.kuliza.lending.notifications.utils.SwaggerConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(NotificationConstants.NOTIFICATION_CONFIG_CONTROLLER_ENDPOINT)
public class NotificationConfigController {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private NotificationConfigService notificationConfigService;

	@ApiOperation(value = SwaggerConstants.NCC_CREATE_MODULE )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_CREATE_MODULE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
	@PostMapping(NotificationConstants.CONFIG_MODULES_API_ENDPOINT)
	public ResponseEntity<Object> addModule(Principal principal,
			@Valid @RequestBody ConfigModuleRequestData configModuleRequestData) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.createOrUpdatModule(principal.getName(), configModuleRequestData));
	}

	@ApiOperation(value = SwaggerConstants.NCC_GET_MODULE )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_GET_MODULE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@GetMapping(NotificationConstants.CONFIG_MODULES_API_ENDPOINT)
	public ResponseEntity<Object> getModule(Principal principal) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getModule(principal.getName()));
	}

	@ApiOperation(value = SwaggerConstants.NCC_DELETE_MODULE )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_DELETE_MODULE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@DeleteMapping(NotificationConstants.CONFIG_MODULES_API_ENDPOINT)
	public ResponseEntity<Object> deleteModule(Principal principal, @RequestParam(required = true, value = "id") long id)
			throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.deleteModule(principal.getName(), id));
	}

	@ApiOperation(value = SwaggerConstants.NCC_CONFIG_SYSTEM )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_CONFIG_SYSTEM_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@GetMapping(NotificationConstants.CONFIG_SYSTEM_LIST_API_ENDPOINT)
	public ResponseEntity<Object> getSystems() throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getSystems());
	}
	
	@ApiOperation(value = SwaggerConstants.NCC_VARIABLE_TYPES )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_VARIABLE_TYPES_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@GetMapping(NotificationConstants.CONFIG_VARIABLE_TYPES_LIST_API_ENDPOINT)
	public ResponseEntity<Object> getVariableTypes() throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getVariableTypes());
	}

	@ApiOperation(value = SwaggerConstants.NCC_ADD_SERVICE_PROVIDER )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_ADD_SERVICE_PROVIDER_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PostMapping(NotificationConstants.CONFIG_SERVICE_PROVIDERS_API_ENDPOINT)
	public ResponseEntity<Object> addServiceProvider(@Valid @RequestBody ConfigServiceProviderInputData serviceProviderData)
			throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.createOrUpdateServiceProviders(serviceProviderData));
	}

	@ApiOperation(value = SwaggerConstants.NCC_GET_SERVICE_PROVIDERS )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_GET_SERVICE_PROVIDERS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@GetMapping(NotificationConstants.CONFIG_SERVICE_PROVIDERS_API_ENDPOINT)
	public ResponseEntity<Object> getServiceProviders() throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getServiceProviders());
	}

	@ApiOperation(value = SwaggerConstants.NCC_DELETE_SERVICE_PROVIDERS )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_DELETE_SERVICE_PROVIDERS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@DeleteMapping(NotificationConstants.CONFIG_SERVICE_PROVIDERS_API_ENDPOINT)
	public ResponseEntity<Object> deleteServiceProvider(@RequestParam(required = true, value = "id") long id) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.deleteServiceProvider(id));
	}

	@ApiOperation(value = SwaggerConstants.NCC_EDIT_NOTIFICATION )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_EDIT_NOTIFICATION_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PutMapping(NotificationConstants.CONFIG_EDIT_MODULES_API_ENDPOINT)
	public ResponseEntity<Object> editNotificationConfig(Principal principal,
			@RequestParam(required = true, value = "id") Long moduleId) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.editNotificationConfig(principal.getName(), moduleId));
	}

	@ApiOperation(value = SwaggerConstants.NCC_CONFIG_STEPPER_POST )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_CONFIG_STEPPER_POST_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PostMapping(NotificationConstants.CONFIG_STEPPER_API_ENDPOINT)
	public ResponseEntity<Object> configStepperPost(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@Valid @RequestBody String request) throws Exception {
		switch (stepper) {
		case 1:
			ConfigChannelListData channels = objectMapper.readValue(request, ConfigChannelListData.class);
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.createOrUpdateChannels(principal.getName(), channels));
		case 2:
			VariableListRequestData variables = objectMapper.readValue(request, VariableListRequestData.class);
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.createOrUpdateVariables(principal.getName(), variables));
		case 3:
			RangesListRequestData rangeList = objectMapper.readValue(request, RangesListRequestData.class);
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.createOrUpdateRanges(principal.getName(), rangeList));
		case 4:
			NotificationTableRequestData notificationtableRequest = objectMapper.readValue(request,
					NotificationTableRequestData.class);
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.createOrUpdateNotificationTable(principal.getName(),
					notificationtableRequest));
		case 5:
			DraftOrPublishNotificationTable draftOrPublishNotificationTable = objectMapper.readValue(request,
					DraftOrPublishNotificationTable.class);
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.draftOrPublishNotificationTable(principal.getName(),
					draftOrPublishNotificationTable));
		default:
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed."));
		}
	}

	@ApiOperation(value = SwaggerConstants.NCC_CONFIG_STEPPER_GET )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_CONFIG_STEPPER_GET_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@GetMapping(NotificationConstants.CONFIG_STEPPER_API_ENDPOINT)
	public ResponseEntity<Object> configStepperGet(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@RequestParam(required = false, value = "id") Long id,
			@RequestParam(required = false, value = "system") Systems system) throws Exception {
		switch (stepper) {
		case 1:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getChannels(principal.getName(), id));
		case 2:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getGlobalVariables(system));
		case 3:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getVariableRanges(principal.getName(), id));
		case 4:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getNotificationTableLogic(principal.getName(), id));
		case 5:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getNotificationTableLogic(principal.getName(), id));
		default:
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed."));
		}
	}

	@ApiOperation(value = SwaggerConstants.NCC_CONFIG_STEPPER_DELETE )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_CONFIG_STEPPER_DELETE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@DeleteMapping(NotificationConstants.CONFIG_STEPPER_API_ENDPOINT)
	public ResponseEntity<Object> configStepperDelete(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@RequestParam(required = true, value = "id") Long id) throws Exception {
		switch (stepper) {
		case 1:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.deleteChannel(principal.getName(), id));
		case 2:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.deleteGlobalVariables(id));
		case 3:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.deleteVariableRange(principal.getName(), id));
		case 4:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.deleteNotificationTable(principal.getName(), id));
		default:
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed."));
		}
	}

	@ApiOperation(value = SwaggerConstants.NCC_CONFIG_STEPPER_PUT )
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.NCC_CONFIG_STEPPER_PUT_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@PutMapping(NotificationConstants.CONFIG_BACK_STEPPER_API_ENDPOINT)
	public ResponseEntity<Object> configStepperGet(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@RequestParam(required = false, value = "id") Long id) throws Exception {
		switch (stepper) {
		case 2:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.decrementStep(principal.getName(), id, stepper));
		case 3:
			notificationConfigService.deleteAllVariablesRanges(principal.getName(), id);
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.decrementStep(principal.getName(), id, stepper));
		case 4:
			notificationConfigService.deleteNotificationTable(principal.getName(), id);
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.decrementStep(principal.getName(), id, stepper));
		case 5:
			return CommonHelperFunctions.buildResponseEntity(notificationConfigService.decrementStep(principal.getName(), id, stepper));
		default:
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed."));
		}
	}
	
	@PostMapping(NotificationConstants.CONFIG_TEMPLATE_API_ENDPOINT)
	public ResponseEntity<Object> addOrUpdateTemplate(Principal principal,
			@Valid @RequestBody ConfigTemplateRequestData configTemplateRequestData) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.createOrUpdateTemplate(principal.getName(), configTemplateRequestData));
	}

	@GetMapping(NotificationConstants.CONFIG_TEMPLATE_API_ENDPOINT)
	public ResponseEntity<Object> getTemplate(Principal principal, @RequestParam(required = true, value = "channelId") Long channelId) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.getTemplates(principal.getName(), channelId));
	}

	@DeleteMapping(NotificationConstants.CONFIG_TEMPLATE_API_ENDPOINT)
	public ResponseEntity<Object> deleteTemplate(Principal principal, @RequestParam(required = true, value = "id") long templateId)
			throws Exception {
		return CommonHelperFunctions.buildResponseEntity(notificationConfigService.deleteTemplate(principal.getName(), templateId));
	}

}
