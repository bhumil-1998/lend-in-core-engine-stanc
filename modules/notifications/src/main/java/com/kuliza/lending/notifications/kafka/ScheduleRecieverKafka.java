package com.kuliza.lending.notifications.kafka;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.decision_table.services.DecisionTableService;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.services.CreateNotificationService;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

@EnableKafka
@Component
@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA")
public class ScheduleRecieverKafka  {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private CreateNotificationService service;
	
	@Autowired
	private DecisionTableService dmnService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleRecieverKafka.class);

	@KafkaListener(topics = NotificationConstants.NOTIFICATION_SCHEDULE_TOPIC, groupId = "group-id")
	public void notificationReceiver(final String notificationDataString) throws Exception {
		LOGGER.info("Notification Receiver Data: {}", notificationDataString);
		try {
			NotificationData notificationData = objectMapper.readValue(notificationDataString, NotificationData.class);
//			Map<String, Object> notificationConfig = new HashMap<String, Object>();
//			notificationConfig.put("emailTemplate", 2);
//			notificationConfig.put("emailFrequency", 1);
			Map<String, Object> notificationConfig = dmnService.executeDecisionTable(notificationData.getDecisionKey(), notificationData.getVariables());
			LOGGER.info("Notification Template: " + notificationData.toString());
			if (notificationConfig.containsKey("smsTemplate")) {
				service.createNotificationObjects(ChannelTypes.SMS, notificationData, notificationConfig);
			}
			if (notificationConfig.containsKey("emailTemplate")) {
				service.createNotificationObjects(ChannelTypes.EMAIL, notificationData, notificationConfig);
			}
			if (notificationConfig.containsKey("pushTemplate")) {
				service.createNotificationObjects(ChannelTypes.PUSH, notificationData, notificationConfig);
			}

		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		}
	}

}