package com.kuliza.lending.notifications.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.engine_common.configs.DmsConfig;

import com.kuliza.lending.engine_common.services.EngineCommonService;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.dao.NotificationModuleDao;
import com.kuliza.lending.notifications.dao.TemplateDao;
import com.kuliza.lending.notifications.integrations.SMSValueFirstIntegration;
import com.kuliza.lending.notifications.jms.ScheduleProducerJMS;
import com.kuliza.lending.notifications.jms.NotificationProducerJMS;
import com.kuliza.lending.notifications.kafka.NotificationProducerKafka;
import com.kuliza.lending.notifications.kafka.ScheduleProducerKafka;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.models.TemplateModel;
import com.kuliza.lending.notifications.pojo.SystemNotificationRequestData;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
public class NotificationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);
	
	@Autowired
	private NotificationProducerKafka messageProducerKafka;

	@Autowired
	private ScheduleProducerKafka scheduleProducerKafka;

	@Autowired
	private NotificationModuleDao notificationModuleDao;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private NotificationDao notificationDao;
	
	@Autowired
    private EngineCommonService engineCommonService;
	

	private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);
	
	public ApiResponse sendEmail(String toAddress, String subject, String body, String docs)
			throws IOException, UnirestException {
		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		 
		try {
            // Close the file
			helper = new MimeMessageHelper(msg, true);
			List<File> attachments = new ArrayList<File>();
			if (docs != null && !"".equals(docs)) {
				for (String doc : docs.split(",")) {
					Map<String, Object> responseFromDMS = engineCommonService.downloadFileInfo(doc.trim());
					File file = new File("/tmp/" + doc.trim() + "_" + new Date().getTime() + "."
							+ CommonHelperFunctions.getStringValue(responseFromDMS.get("fileName")).split("\\.")[1]);
					try (OutputStream os = new FileOutputStream(file)){
						os.write((byte[]) responseFromDMS.get("bytes"));
					} catch (Exception e) {
						return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
					}
					helper.addAttachment(CommonHelperFunctions.getStringValue(responseFromDMS.get("fileName")), file);
					attachments.add(file);
				}
			}
            helper.setTo(toAddress);
			helper.setSubject(subject);
			helper.setText(body, true);
			javaMailSender.send(msg);
			for (File file : attachments) {
				file.delete();
			}
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Email sent successfully.");
		} catch (MessagingException e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}
	
	public ApiResponse createNotification(SystemNotificationRequestData systemRequest) throws Exception {
		try {
			
			NotificationModuleModel notificationModule = notificationModuleDao.findByIdAndStatusAndIsDeleted(systemRequest.getModuleId(),
					ModuleStatus.PUBLISHED, false);
			if (notificationModule != null) {
				if (systemRequest.getIdentifierList() != null && !systemRequest.getIdentifierList().isEmpty()) {
					for (String identifier : systemRequest.getIdentifierList()) {
						if (!scheduleProducerKafka.createNotificationData(identifier, notificationModule.getDecisionTableKey(), 
								systemRequest.getSourceType())) {
							return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
									"Error in making Notification Objects.");
						}
					}
				} else {
					return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, "No identifier list found.");
				}
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"Notification module not found for the given id.");
			}
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Notification Scheduled Succesfully.");
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}


	public ApiResponse sendNotification() throws Exception {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
			Calendar currentTime = Calendar.getInstance();
			Date sendTime = simpleDateFormat.parse(CommonHelperFunctions.getStringValue(new Integer(currentTime.get(Calendar.HOUR_OF_DAY) - 1)) + ":00:00");
			List<NotificationModel> notificationObjects = notificationDao.findByIsSyncAndStatus(false, null);
			for (NotificationModel notificationObject : notificationObjects) {
				messageProducerKafka.sendNotification(notificationObject);
			}
			return new ApiResponse(HttpStatus.OK, "Send Notification @ ", sendTime.getTime());
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.OK, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}

}
