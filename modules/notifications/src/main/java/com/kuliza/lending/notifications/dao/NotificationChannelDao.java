package com.kuliza.lending.notifications.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

@Transactional
public interface NotificationChannelDao extends CrudRepository<NotificationChannelModel, Long> {

	public NotificationChannelModel findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<NotificationChannelModel> findByNotificationModuleAndIsDeleted(
			NotificationModuleModel notificationModuleModel, boolean isDeleted);

	public NotificationChannelModel findByNotificationModuleAndChannelTypeAndIsDeleted(
			NotificationModuleModel notificationModuleModel, ChannelTypes channelTypes, boolean isDeleted);

}
