package com.kuliza.lending.notifications.jms;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.decision_table.services.DecisionTableService;
import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.services.CreateNotificationService;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

@Component
@EnableJms
// @ConditionalOnExpression("${broker.name.jms:true}")
public class ScheduleReceiverJMS implements MessageListener {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private CreateNotificationService service;
	
	@Autowired
	private DecisionTableService dmnService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleReceiverJMS.class);

	@JmsListener(destination = NotificationConstants.JMS_NOTIFICATION_ROUTING_KEY, containerFactory = "jmsFactory")
	@Override
	@Transactional
	public void onMessage(Message message) {
		try {
			if (message instanceof TextMessage) {
				String textMessage = HelperFunctions.extractStringFromMessage((TextMessage) message);
				try {
					NotificationData notificationData = objectMapper.readValue(textMessage, NotificationData.class);
//					Map<String, Object> notificationConfig = new HashMap<String, Object>();
//					notificationConfig.put("emailTemplate", 2);
//					notificationConfig.put("emailFrequency", 1);
					Map<String, Object> notificationConfig = dmnService.executeDecisionTable(notificationData.getDecisionKey(), notificationData.getVariables());
					LOGGER.info("Notification Template: " + notificationData.toString());
					if (notificationConfig.containsKey("smsTemplate")) {
						service.createNotificationObjects(ChannelTypes.SMS, notificationData, notificationConfig);
					}
					if (notificationConfig.containsKey("emailTemplate")) {
						service.createNotificationObjects(ChannelTypes.EMAIL, notificationData, notificationConfig);
					}
					if (notificationConfig.containsKey("pushTemplate")) {
						service.createNotificationObjects(ChannelTypes.PUSH, notificationData, notificationConfig);
					}
				} catch (IOException e) {
					LOGGER.error(CommonHelperFunctions.getStackTrace(e));
				}
			} else if (message instanceof BytesMessage) {
				byte[] byteArray = HelperFunctions.extractByteArrayFromMessage((BytesMessage) message);
			} else if (message instanceof MapMessage) {
				Map<String, Object> mapMessage = HelperFunctions.extractMapFromMessage((MapMessage) message);
			} else if (message instanceof ObjectMessage) {
				Serializable object = HelperFunctions.extractSerializableFromMessage((ObjectMessage) message);
			} else {
				LOGGER.info("Received: " + message);
			}
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		} finally {
			try {
				message.acknowledge();
			} catch (JMSException e) {
				LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			}
		}
	}
}