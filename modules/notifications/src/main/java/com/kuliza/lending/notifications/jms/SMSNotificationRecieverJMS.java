package com.kuliza.lending.notifications.jms;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.TemplateModel;
import com.kuliza.lending.notifications.services.SendNotificationService;
import com.kuliza.lending.notifications.utils.NotificationConstants;

@Component
@EnableJms
public class SMSNotificationRecieverJMS implements MessageListener {

	@Autowired
	NotificationDao notificationDao;
	
	@Autowired
	SendNotificationService sendNotificationService;
	

	private static final Logger logger = LoggerFactory.getLogger(SMSNotificationRecieverJMS.class);

	@JmsListener(destination = NotificationConstants.JMS_SMS_NOTIFICATION_ROUTING_KEY, containerFactory = "jmsFactory")
	@Override
	@Transactional
	public void onMessage(Message message) {
		try {
			if (message instanceof TextMessage) {
				String textMessage = HelperFunctions.extractStringFromMessage((TextMessage) message);
				ObjectMapper mapper = new ObjectMapper();
				try {
					String uuid = mapper.readValue(textMessage, String.class);
					NotificationModel notificationObject = notificationDao.findById(uuid);
					TemplateModel templateObject = notificationObject.getTemplateModel();
					if (templateObject != null) {
						sendNotificationService.sendSms(notificationObject);
					} else {
						logger.info("Template not found.");
					}

				} catch (IOException e) {
					logger.error(CommonHelperFunctions.getStackTrace(e));
				}
			} else {
				logger.info("Received: {} of type {}", message, message.getClass());
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
		} finally {
			try {
				message.acknowledge();
			} catch (JMSException e) {
				logger.error(CommonHelperFunctions.getStackTrace(e));
			}
		}
	}
}