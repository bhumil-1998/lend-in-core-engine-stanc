package com.kuliza.lending.notifications.models;

import java.io.IOException;
import java.util.List;

import javax.persistence.AttributeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.notifications.pojo.ConfigServiceProviderData;

public class ListOfServiceProviderConverter implements AttributeConverter<List<ConfigServiceProviderData>, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ListOfServiceProviderConverter.class);

	@Override
	public String convertToDatabaseColumn(List<ConfigServiceProviderData> customerInfo) {
		ObjectMapper objectMapper = new ObjectMapper();
		String customerInfoJson = null;
		try {
			if (customerInfo != null) {
				customerInfoJson = objectMapper.writeValueAsString(customerInfo);
			}
		} catch (final JsonProcessingException e) {
			LOGGER.error("JSON writing error", e);
		}

		return customerInfoJson;
	}

	@Override
	public List<ConfigServiceProviderData> convertToEntityAttribute(String customerInfoJSON) {
		ObjectMapper objectMapper = new ObjectMapper();
		List<ConfigServiceProviderData> customerInfo = null;
		try {
			if (customerInfoJSON != null) {
				customerInfo = objectMapper.readValue(customerInfoJSON, new TypeReference<List<ConfigServiceProviderData>>() {});
			}
		} catch (final IOException e) {
			LOGGER.error("JSON reading error", e);
		}

		return customerInfo;
	}

}
