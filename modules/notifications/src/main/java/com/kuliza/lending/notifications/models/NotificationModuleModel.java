package com.kuliza.lending.notifications.models;

import static com.kuliza.lending.common.utils.CommonHelperFunctions.getDateStringValue;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "config_notification_module")
public class NotificationModuleModel extends BaseModel {

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Systems system;

	@Column
	private String description;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private ModuleStatus status;

	@Column
	private String decisionTableKey;

	@Column(nullable = false)
	private String assignee;
	
	@Column(nullable = false, columnDefinition = "int default 1") 
	private Integer step = 1;
	
	public String getCreatedTime() {
		return this.getCreated() == null ? null : getDateStringValue(this.getCreated(), NotificationConstants.MODULE_DATE_FORMAT);
	}

	public String getModifiedTime() {
		return this.getModified() == null ? null : getDateStringValue(this.getModified(), NotificationConstants.MODULE_DATE_FORMAT);
	}

	
	public NotificationModuleModel() {
		super();
		this.setIsDeleted(false);
		// TODO Auto-generated constructor stub
	}

	public NotificationModuleModel(String name, Systems system, String description, ModuleStatus status,
			String decisionTableKey, String assignee, Integer step) {
		super();
		this.name = name;
		this.system = system;
		this.description = description;
		this.status = status;
		this.decisionTableKey = decisionTableKey;
		this.assignee = assignee;
		this.setIsDeleted(false);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Systems getSystem() {
		return system;
	}

	public void setSystem(Systems system) {
		this.system = system;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ModuleStatus getStatus() {
		return status;
	}

	public void setStatus(ModuleStatus status) {
		this.status = status;
	}

	public String getDecisionTableKey() {
		return decisionTableKey;
	}

	public void setDecisionTableKey(String decisionTableKey) {
		this.decisionTableKey = decisionTableKey;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

}
