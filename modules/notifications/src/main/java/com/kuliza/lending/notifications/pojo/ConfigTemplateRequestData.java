package com.kuliza.lending.notifications.pojo;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class ConfigTemplateRequestData {

	@NotNull(message = "Channel id cannot be null.")
	Long channelId;

	@NotNull(message = "Template name cannot be null.")
	@NotEmpty(message = "Template name cannot be empty.")
	@Size(max = 255)
	String name;

	@Size(max = 255)
	String fromAddress;

	@NotNull(message = "To address cannot be null.")
	@NotEmpty(message = "To address cannot be empty.")
	@Size(max = 255)
	String toAddress;

	@Size(max = 255)
	String subject;

	String body;

	Map<String, Object> toMetadata;

	List<String> variables;

	Long id;

	public ConfigTemplateRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigTemplateRequestData(Long channelId, String name, String fromAddress, String toAddress, String subject,
			String body, Map<String, Object> toMetadata, List<String> variables, Long id) {
		super();
		this.channelId = channelId;
		this.name = name;
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;
		this.subject = subject;
		this.body = body;
		this.toMetadata = toMetadata;
		this.variables = variables;
		this.id = id;
	}

	public Long getChannelId() {
		return channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Map<String, Object> getToMetadata() {
		return toMetadata;
	}

	public void setToMetadata(Map<String, Object> toMetadata) {
		this.toMetadata = toMetadata;
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
