package com.kuliza.lending.notifications.models;

import java.io.IOException;
import java.util.List;

import javax.persistence.AttributeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ListConverter implements AttributeConverter<List<String>, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ListConverter.class);

	@Override
	public String convertToDatabaseColumn(List<String> customerInfo) {
		ObjectMapper objectMapper = new ObjectMapper();
		String customerInfoJson = null;
		try {
			if (customerInfo != null) {
				customerInfoJson = objectMapper.writeValueAsString(customerInfo);
			}
		} catch (final JsonProcessingException e) {
			LOGGER.error("JSON writing error", e);
		}

		return customerInfoJson;
	}

	@Override
	public List<String> convertToEntityAttribute(String customerInfoJSON) {
		ObjectMapper objectMapper = new ObjectMapper();
		List<String> customerInfo = null;
		try {
			if (customerInfoJSON != null) {
				customerInfo = objectMapper.readValue(customerInfoJSON, new TypeReference<List<String>>() {});
			}
		} catch (final IOException e) {
			LOGGER.error("JSON reading error", e);
		}

		return customerInfo;
	}

}
