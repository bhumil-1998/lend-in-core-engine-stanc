package com.kuliza.lending.notifications.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.ServiceProviderModel;

@Transactional
public interface ServiceProviderDao extends CrudRepository<ServiceProviderModel, Long> {

	public ServiceProviderModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<ServiceProviderModel> findByIsDeleted(boolean isDeleted);

}
