package com.kuliza.lending.notifications.kafka;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

@Service
public class NotificationProducerKafka {

	@Autowired
	KafkaTemplate<String, String> kafkaTemplate;

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationProducerKafka.class);

	public void sendNotification(NotificationModel notificationObject)
			throws JsonProcessingException, JSONException {
		LOGGER.info("In notification sender");
		if (notificationObject.getTemplateModel() != null
				&& notificationObject.getTemplateModel().getNotificationChannelModel() != null) {
			try {
				ChannelTypes channelType = notificationObject.getTemplateModel().getNotificationChannelModel()
						.getChannelType();
				LOGGER.info("Sent notification in queue: " + channelType.toString());
				LOGGER.info(NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(3));
				kafkaTemplate.send(NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(3),
						notificationObject.getId());
			} catch (Exception e) {
				// TODO: handle exception
				LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			}
		} else {
			LOGGER.info("No template or channel found for notification object");
		}
	}

}
