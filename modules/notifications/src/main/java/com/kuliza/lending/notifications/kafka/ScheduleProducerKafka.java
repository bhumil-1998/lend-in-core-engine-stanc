package com.kuliza.lending.notifications.kafka;

import java.util.HashMap;
import java.util.Map;

import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.engine.RuntimeService;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.utils.NotificationConstants;

@Service
// @ConditionalOnExpression("${broker.name.rabbit:true}")
public class ScheduleProducerKafka {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	KafkaTemplate<String, String> kafkaTemplate;

	@Autowired
	CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	RuntimeService runtimeService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleProducerKafka.class);

	public Boolean createNotificationData(String identifier, String decisionKey, SourceType sourceType)
			throws JsonProcessingException, JSONException {
		try {
			Map<String, Object> variables = new HashMap<>();
			if (sourceType.equals(SourceType.cmmn)) {
				variables = cmmnRuntimeService.getVariables(identifier);
			} else {
				variables = runtimeService.getVariables(identifier);
			}
			LOGGER.info("In create notification kafka producer");
			NotificationData notificationData = new NotificationData(identifier, decisionKey, variables);
			kafkaTemplate.send(NotificationConstants.NOTIFICATION_SCHEDULE_TOPIC, objectMapper.writeValueAsString(notificationData));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
