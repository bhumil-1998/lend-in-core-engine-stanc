package com.kuliza.lending.notifications.dao;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;

@Transactional
public interface NotificationDao extends CrudRepository<NotificationModel, Long> {

	public NotificationModel findById(String uuid);
	
	public List<NotificationModel> findByIsSyncAndStatusAndSendTime(boolean isSync, String status, Date sendTime);
	
	public List<NotificationModel> findByIsSyncAndStatus(boolean isSync, String status);

}
