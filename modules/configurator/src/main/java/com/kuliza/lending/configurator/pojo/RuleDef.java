package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class RuleDef {
	@NotNull(message = "ruleId is a required key")
	@Pattern(regexp = Constants.EMPTY_OR_ID_REGEX, message = Constants.INVALID_RULE_ID_MESSAGE)
	String ruleId;

	@NotNull(message = "fn1 is a required key")
	@Pattern(regexp = Constants.LOGICAL_FUNCTIONS_REGEX, message = "Invalid fn1")
	String fn1;

	@NotNull(message = "fn2 is a required key")
	@Pattern(regexp = Constants.EMPTY_OR_LOGICAL_FUNCTIONS_REGEX, message = "Invalid fn2")
	String fn2;

	@NotNull(message = "value1 is a required key")
	String value1;

	@NotNull(message = "value2 is a required key")
	@Pattern(regexp = Constants.EMPTY_OR_VALUE_REGEX, message = "Invalid value2")
	String value2;

	@NotNull(message = "outputValue is a required key")
	@Pattern(regexp = Constants.BOOLEAN_OR_VALUE_REGEX, message = "Invalid outputValue")
	String outputValue;

	String groupId;
	String variableId;

	public RuleDef() {
		this.ruleId = "";
		this.fn1 = "";
		this.fn2 = "";
		this.value1 = "";
		this.value2 = "";
		this.outputValue = "";
	}

	public RuleDef(String ruleId, String fn1, String fn2, String value1, String value2, String outputValue) {
		this.ruleId = ruleId;
		this.fn1 = fn1;
		this.fn2 = fn2;
		this.value1 = value1;
		this.value2 = value2;
		this.outputValue = outputValue;
	}

	public String getFn1() {
		return fn1;
	}

	public void setFn1(String fn1) {
		this.fn1 = fn1;
	}

	public String getFn2() {
		return fn2;
	}

	public void setFn2(String fn2) {
		this.fn2 = fn2;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getOutputValue() {
		return outputValue;
	}

	public void setOutputValue(String outputValue) {
		this.outputValue = outputValue;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getVariableId() {
		return variableId;
	}

	public void setVariableId(String variableId) {
		this.variableId = variableId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("ruleId : " + ruleId + ", ");
	// inputData.append("fn1 : " + fn1 + ", ");
	// inputData.append("fn2 : " + fn2 + ", ");
	// inputData.append("value1 : " + value1 + ", ");
	// inputData.append("value2 : " + value2 + ", ");
	// inputData.append("outputValue : " + outputValue);
	// inputData.append(" }");
	// return inputData.toString();
	//
	// }

}
