package com.kuliza.lending.configurator.pojo;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class SubmitNewExpressions {

	private String productId;
	private String groupId;
	private String userId;

	@NotNull(message = "newExpressionsList is a required key")
	@NotEmpty(message = "newExpressionsList cannot be empty")
	@Valid
	private List<NewExpression> newExpressionsList;

	public SubmitNewExpressions() {
		this.newExpressionsList = new ArrayList<>();
	}

	public SubmitNewExpressions(List<NewExpression> newExpressionsList) {
		this.newExpressionsList = newExpressionsList;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public List<NewExpression> getNewExpressionsList() {
		return newExpressionsList;
	}

	public void setNewExpressionsList(List<NewExpression> newExpressionsList) {
		this.newExpressionsList = newExpressionsList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("newExpressionsList : [ ");
	// for (int i = 0; i < newExpressionsList.size(); i++) {
	// inputData.append(newExpressionsList.get(i).toString());
	// if (i < newExpressionsList.size() - 1) {
	// inputData.append(", ");
	// }
	// }
	// inputData.append("] }");
	// return inputData.toString();
	//
	// }

}