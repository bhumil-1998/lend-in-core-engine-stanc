package com.kuliza.lending.configurator.services;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.UserDao;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewRule;
import com.kuliza.lending.configurator.pojo.ResponseRuleDef;
import com.kuliza.lending.configurator.serializers.ExpressionSerializer;
import com.kuliza.lending.configurator.serializers.PropertyFilterMixIn;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;

@Service
public class ImportExportService {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ProductServices.class);
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private ProductDao productDao;
	@Autowired
	private ProductCategoryDao productCategoryDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private Jackson2ObjectMapperBuilder mapperBuilder;
	@Autowired
	private VariableDao variableDao;
	@Autowired
	private GroupDao groupDao;
	@Autowired
	private ExpressionDao expressionDao;
	@Autowired
	private RuleServices ruleService;

	public String exportProductToJSON(String productId) {
		String jsonInString2 = null;
		try {
			Product product = productDao.findByIdAndIsDeleted(Long.parseLong(productId), false);
			if (product != null) {
				ObjectMapper mapper = mapperBuilder.build().setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
				mapper.addMixIn(Variable.class, PropertyFilterMixIn.class);
				mapper.addMixIn(Group.class, PropertyFilterMixIn.class);
				mapper.addMixIn(ResponseRuleDef.class, PropertyFilterMixIn.class);
				mapper.addMixIn(ExpressionSerializer.class, PropertyFilterMixIn.class);
				mapper.addMixIn(Map.class, PropertyFilterMixIn.class);
				mapper.addMixIn(Map.class, PropertyFilterMixIn.class);
				String[] filtersList = new String[] { "id", "ruleId" };
				FilterProvider filters = new SimpleFilterProvider()

						.addFilter(Constants.PRODUCT_FILTER, SimpleBeanPropertyFilter.serializeAllExcept(filtersList))
						.addFilter(Constants.GROUP_FILTER, SimpleBeanPropertyFilter.serializeAllExcept(filtersList))
						.addFilter("idFilterForExport", SimpleBeanPropertyFilter.serializeAllExcept(filtersList));
				mapper.setFilterProvider(filters);
				jsonInString2 = mapper.writeValueAsString(product);
			}
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException in exportProductToJSON", e);
		}
		return jsonInString2;
	}

	public GenericAPIResponse importProductFromJSON(long userId, String jsonString) throws Exception {
		JsonNode jsonNode = null;
		try {
			jsonNode = objectMapper.readTree(jsonString);
			return createProductIfNotExists(jsonNode, userId);
		} catch (Exception e) {
			return new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.ERROR, e);
		}
	}

	private GenericAPIResponse createProductIfNotExists(JsonNode jsonObj, long userId) throws Exception {
		String categoryName = jsonObj.get("productCategory").get("name").asText(null);
		ProductCategory productCategory = null;
		Product product = null;
		if (CommonHelperFunctions.isNotEmpty(categoryName)) {
			productCategory = productCategoryDao.findByNameAndIsDeleted(categoryName, false);
			if (productCategory == null) {
				productCategory = new ProductCategory(categoryName);
				productCategoryDao.save(productCategory);
			}
			String productName = jsonObj.get("name").asText("");
			String identifier = jsonObj.get("identifier").asText("");
			if (CommonHelperFunctions.isNotEmpty(productName)) {
				product = productDao.findByNameAndUserIdAndIsDeleted(productName, userId, false);
				if (product == null) {
					User user = userDao.findById(userId);
					if (user == null)
						throw new Exception("User doesn't exists in system");
					product = new Product(productName, user, productCategory, identifier);
					productDao.save(product);
				} else {
					// Overwrite or what to do??
					throw new Exception("Product already exists, not allowed to import");
				}

				addProductVariables(jsonObj, product);
				addProductGroups(jsonObj, product);
				addProductRules(jsonObj, product);
				// Finally save product expression
				JsonNode productExpressionNode = jsonObj.get("expressions");
				for (JsonNode expressionString : productExpressionNode) {
					try {

						Expression expression = new Expression(expressionString.get("expressionString").asText(), false,
								true, product, null);

						expression.setExpressionString(
								HelperFunctions.makeStorableExpressionString(expression.getExpressionString(),
										variableDao, groupDao, Long.toString(product.getId()), false));
						expressionDao.save(expression);
					} catch (Exception exception) {
						// TODO Auto-generated catch block
						logger.error("Exception in addProductGroups export ", exception);
					}
				}
					
				return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, product.getId());
			}

		}
		throw new Exception("Category can not be empty");

	}

	private void addProductRules(JsonNode jsonObj, Product productRef) {
		try {
			String productId = Long.toString(productRef.getId());
			JsonNode jsonGroupsNode = jsonObj.get("groups");
			if (jsonGroupsNode.isArray()) {
				for (JsonNode node : jsonGroupsNode) {
					List<NewRule> newRuleList = objectMapper.convertValue(node.get("rules"),
							new TypeReference<List<NewRule>>() {
							});
					String groupName = node.get("name").asText();
					Group group = groupDao.findByNameAndProductIdAndIsDeleted(groupName, productRef.getId(), false);
					ruleService.saveRulesInDB(newRuleList, productId, group);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in addProductRules import ", e);
			throw e;
		}

	}

	private void addProductVariables(JsonNode jsonObj, Product productRef) throws Exception {

		List<Variable> variableSet = objectMapper.convertValue(jsonObj.get("variables"),
				new TypeReference<List<Variable>>() {
				});
		// To ensure we add primitive variable first
		Collections.sort(variableSet, (Variable s1, Variable s2) -> {
			return s2.getCategory().compareToIgnoreCase(s1.getCategory());
		});
		String productId = Long.toString(productRef.getId());
		for (Variable variable : variableSet) {
			try {
				variable.setExpression(HelperFunctions.makeStorableExpressionString(variable.getExpression(),
						variableDao, groupDao, productId, true));

			} catch (Exception e) {
				logger.error("Exception in addProductVariables import ", e);
				throw e;
			}
			variable.setProduct(productRef);
			variableDao.save(variable);
		}

	}

	private void addProductGroups(JsonNode jsonObj, Product productRef) throws Exception {

		List<Group> groupSet = objectMapper.convertValue(jsonObj.get("groups"), new TypeReference<List<Group>>() {
		});
		String productId = Long.toString(productRef.getId());
		for (Group group : groupSet) {
			Group newGroup = new Group(group.getName(), productRef);
			groupDao.save(newGroup);
			for (Expression expression : group.getExpressions()) {
				try {
					expression.setGroup(newGroup);
					expression.setGroup(true);
					expression.setExpressionString(HelperFunctions.makeStorableExpressionString(
							expression.getExpressionString(), variableDao, groupDao, productId, true));
					expressionDao.save(expression);
				} catch (Exception exception) {
					logger.error("Exception in addProductGroups export ", exception);
					throw exception;
				}
			}
		}

	}

}
