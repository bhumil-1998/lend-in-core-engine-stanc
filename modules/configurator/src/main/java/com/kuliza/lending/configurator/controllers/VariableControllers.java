package com.kuliza.lending.configurator.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewVariable;
import com.kuliza.lending.configurator.pojo.SubmitNewVariables;
import com.kuliza.lending.configurator.services.VariableServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.SwaggerConstants;
import com.kuliza.lending.configurator.validators.VariableDataValidator;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/product/{productId:^[1-9]+[0-9]*$}/variables")
public class VariableControllers {

	private static final Logger logger = LoggerFactory.getLogger(VariableControllers.class);

	@Autowired
	private VariableServices variableServices;

	@Autowired
	private VariableDataValidator variableDataValidator;

	// API to get all variable for a product
	@ApiOperation(value = SwaggerConstants.VC_GET_VARIABLES )
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.VC_GET_VARIABLES_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = "")
	public Object getAllVariables(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("--> Entering get all variables for product()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			if (!variableDataValidator.validateGetAllVariables(userId, productId)) {
				logger.error("Invalid productId: " + productId);
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_ID_MESSAGE);
			} else {
				logger.debug("getting all variable for product with Id:" + productId);
				response = variableServices.getAllVariables(productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Succssfully find variables for product");
				variableServices.logSuccessResponse(request, userId, response);
			} else {
				variableServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting get all variables for product()");
			return response;
		} catch (Exception e) {
			return variableServices.handleException(e, request, userId);
		}

	}

	// API to get single variable data for a product
	@ApiOperation(value = SwaggerConstants.VC_SINGLE_VARIABLE )
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.VC_SINGLE_VARIABLE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = "/{variableId:^[1-9]+[0-9]*$}")
	public Object getVariable(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "variableId") String variableId, HttpServletRequest request) {
		logger.info("--> Entering get variables()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			if (!variableDataValidator.validateGetSingleVariable(userId, productId, variableId)) {
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_VARIABLE_ID_MESSAGE);
			} else {
				response = variableServices.getSingleVariable(variableId);
			}
			if (response.getStatus() == 200) {
				variableServices.logSuccessResponse(request, userId, response);
			} else {
				variableServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting get variables()");
			return response;
		} catch (Exception e) {
			return variableServices.handleException(e, request, userId);
		}
	}

	// API to save new variables for a product
	@ApiOperation(value = SwaggerConstants.VC_SAVE_NEW_VARIABLES )
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.VC_SAVE_NEW_VARIABLES_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = "")
	public Object variableSubmit(@PathVariable(value = "productId") String productId,
			@Valid @RequestBody SubmitNewVariables input, BindingResult result, HttpServletRequest request) {
		logger.info("--> Entering save new variables for product()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = variableServices.validateSubmitVariables(input, result, userId, productId);
			if (response == null) {
				logger.debug("Creating new variables for product");
				response = variableServices.createNewVariables(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully saved new variables for product");
				variableServices.logSuccessResponse(request, userId, response, input);
			} else {
				variableServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting save new variables for product()");
			return response;
		} catch (Exception e) {
			return variableServices.handleException(e, request, userId, input);
		}

	}

	// API to update variable in a product
	@ApiOperation(value = SwaggerConstants.VC_UPDATE_VARIABLE )
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.VC_UPDATE_VARIABLE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.PUT, value = "/{variableId:^[1-9]+[0-9]*$}")
	public Object updateVariable(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "variableId") String variableId, @Valid @RequestBody NewVariable input,
			BindingResult result, HttpServletRequest request) {
		logger.info("--> Entering update variables()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response;
			input.setProductId(productId);
			input.setVariableId(variableId);
			variableDataValidator.validateUpdateVariable(input, userId, result);
			response = variableServices.checkErrors(result);
			if (response == null) {
				logger.debug("updating variable for product with productId:" + productId);
				response = variableServices.updateVariable(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully updated variables for product");
				variableServices.logSuccessResponse(request, userId, response, input);
			} else {
				variableServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting update variables()");
			return response;
		} catch (Exception e) {
			return variableServices.handleException(e, request, userId, input);
		}
	}

	// API to delete a variable inside a group
	@ApiOperation(value = SwaggerConstants.VC_DELETE_VARIABLE )
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.VC_DELETE_VARIABLE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = com.kuliza.lending.common.utils.Constants.AUTH, value = com.kuliza.lending.common.utils.Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = com.kuliza.lending.common.utils.Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.DELETE, value = "/{variableId:^[1-9]+[0-9]*$}")
	public Object deleteVariable(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "variableId") String variableId, HttpServletRequest request) {
		logger.info("--> Entering delete variables()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = variableServices.validateDeleteVariable(userId, productId, variableId);
			if (response == null) {
				logger.debug("deleting variable with variableId:" + variableId);
				response = variableServices.deleteVariable(variableId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully deleted variable");
				variableServices.logSuccessResponse(request, userId, response);
			} else {
				variableServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting delete variables()");
			return response;
		} catch (Exception e) {
			return variableServices.handleException(e, request, userId);
		}
	}

}
