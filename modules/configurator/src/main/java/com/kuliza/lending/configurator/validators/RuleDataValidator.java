package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.NewRule;
import com.kuliza.lending.configurator.pojo.RuleDef;
import com.kuliza.lending.configurator.pojo.SubmitNewRules;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class RuleDataValidator extends BasicDataValidator {

	@Autowired
	private VariableDao variableDao;

	public boolean validateGetAllRules(String userId, String productId, String groupId) throws Exception {
		if (!validateProductId(productId, userId)) {
			return false;
		}
		return !validateGroupId(groupId, productId) ? false : true;
	}

	public void validateRule(NewRule obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		Variable variable = variableDao.findByNameAndProductIdAndIsDeleted(obj.getVariableName(),
				Long.parseLong(obj.getProductId()), false);
		if (variable == null) {
			errors.rejectValue(Constants.VARIABLE_NAME, Constants.INVALID_VARIABLE_NAME,
					Constants.INVALID_VARIABLE_NAME_MESSAGE);
		}
	}

	public void validateRuleStructure(RuleDef obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}

		if (!obj.getRuleId().equals("") && !validateRuleId(obj.getRuleId(), obj.getGroupId())) {
			errors.rejectValue(Constants.RULE_ID, Constants.INVALID_RULE_ID, Constants.INVALID_RULE_ID_MESSAGE);
		}

		Variable variable = variableDao.findByIdAndIsDeleted(Long.parseLong(obj.getVariableId()), false);

		if (variable.getType().equals(Constants.BOOLEAN) || variable.getType().equals(Constants.STRING)) {
			if (!obj.getValue2().equals("")) {
				errors.rejectValue(Constants.VALUE_2, Constants.INVALID_VALUE_2,
						"Value 2 not valid for variable type String or Boolean");
			}
			if (!obj.getFn2().equals("")) {
				errors.rejectValue(Constants.FN_2, Constants.INVALID_FN_2,
						"Fn2 not valid for variable type String or Boolean");
			}
		}

		if (variable.getType().equals(Constants.BOOLEAN)) {
			if (!obj.getFn1().matches("^(==)|(!=)$")) {
				errors.rejectValue(Constants.FN_1, Constants.INVALID_FN_1,
						"Fn1 can only be == or != for variable type boolean");
			}

			if (!obj.getValue1().matches(Constants.IS_BOOLEAN_REGEX)) {
				errors.rejectValue(Constants.VALUE_1, Constants.INVALID_VALUE_1,
						"Value 1 should be boolean for boolean type variable");
			}
		}

		else if (variable.getType().equals(Constants.NUMERICAL)) {
			if (!obj.getValue1().matches(Constants.VALUE_REGEX)) {
				errors.rejectValue(Constants.VALUE_1, Constants.INVALID_VALUE_1,
						"Value 1 should be Numerical for Numerical type variable");
			}
			if (!obj.getValue2().equals("") && !obj.getValue2().matches(Constants.VALUE_REGEX)) {
				errors.rejectValue(Constants.VALUE_2, Constants.INVALID_VALUE_2,
						"Value 2 should be Numerical for Numerical type variable");
			}

			if (!obj.getFn2().equals("") && obj.getValue2().equals("")) {
				errors.rejectValue(Constants.VALUE_2, Constants.INVALID_VALUE_2, "Value 2 is required if Fn2 is set");
			}

			if (!obj.getValue2().equals("") && obj.getFn2().equals("")) {
				errors.rejectValue(Constants.FN_2, Constants.INVALID_FN_2, "Fn2 is required if Value2 is set");
			}
		}

		else {
			if (!obj.getFn1().matches("^(==)|(!=)$")) {
				errors.rejectValue(Constants.FN_1, Constants.INVALID_FN_1,
						"Fn1 can only be == or != for variable type String");
			}

			if (obj.getValue1().equals("")) {
				errors.rejectValue(Constants.VALUE_1, Constants.INVALID_VALUE_1, "Value 1 is required");
			}
		}
	}

	public void validateRuleForm(SubmitNewRules obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		validateProductGroup(obj.getUserId(), obj.getProductId(), obj.getGroupId(), errors);
	}

}
