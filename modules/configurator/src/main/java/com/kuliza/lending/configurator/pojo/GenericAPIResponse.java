package com.kuliza.lending.configurator.pojo;

public class GenericAPIResponse {

	private int status;
	private String message;
	private Object data;

	public GenericAPIResponse() {
		super();
	}

	public GenericAPIResponse(int status, String message, Object data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("status : " + status + ", ");
	// inputData.append("message : " + message + ", ");
	// inputData.append("data : " + data.toString());
	// inputData.append(" }");
	// return inputData.toString();
	// }

}