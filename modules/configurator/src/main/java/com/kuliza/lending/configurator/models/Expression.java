package com.kuliza.lending.configurator.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.configurator.serializers.ExpressionSerializer;

@Entity
@Table(name = "ce_expression")
@JsonSerialize(converter = ExpressionSerializer.class)
@Where(clause = "is_deleted=0")
public class Expression extends BaseModel {

	@Column(length = 10000, nullable = false)
	private String expressionString;
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@Column(nullable = false)
	private boolean isGroup = true;
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@Column(nullable = false)
	private boolean isProduct = false;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId")
	private Product product;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "groupId")
	private Group group;

	public Expression() {
		super();
		this.setIsDeleted(false);
	}

	public Expression(long id, String expressionString, boolean isGroup, boolean isProduct, Product product,
			Group group) {
		super();
		this.setId(id);
		this.expressionString = expressionString;
		this.isGroup = isGroup;
		this.isProduct = isProduct;
		this.product = product;
		this.group = group;
	}

	public Expression(String expressionString, boolean isGroup, boolean isProduct, Product product, Group group) {
		this.expressionString = expressionString;
		this.isGroup = isGroup;
		this.isProduct = isProduct;
		this.product = product;
		this.group = group;
		this.setIsDeleted(false);
	}

	public String getExpressionString() {
		return expressionString;
	}

	public void setExpressionString(String expressionString) {
		this.expressionString = expressionString;
	}

	@JsonIgnore
	public boolean isGroup() {
		return isGroup;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	@JsonIgnore
	public boolean isProduct() {
		return isProduct;
	}

	public void setProduct(boolean isProduct) {
		this.isProduct = isProduct;
	}

	@JsonIgnore
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@JsonIgnore
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
