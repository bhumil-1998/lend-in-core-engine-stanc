package com.kuliza.lending.configurator.utils;

import com.kuliza.lending.configurator.models.Variable;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.Comparator;

@SuppressFBWarnings("SE_COMPARATOR_SHOULD_BE_SERIALIZABLE")
public class SortVariablesList implements Comparator<Variable> {

	@Override
	public int compare(Variable v1, Variable v2) {
		if (v1.getId() > v2.getId()) {
			return 1;
		} else {
			return -1;
		}
	}
}
