package com.kuliza.lending.configurator.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@JsonFilter("productFilter")
@Entity
@Table(name = "ce_product", uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "userId" }) })
@Where(clause = "is_deleted=0")
@JsonPropertyOrder({"expressions","groups","variables"})
public class Product extends BaseModelWithName {

	@Column(nullable = false)
	private long parentId;
	@Column(nullable = false)
	private int status;
	@Column(nullable = false)
	private String identifier;

	@Where(clause = "is_deleted=0")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
	@OrderBy(clause = "id ASC")
	private Set<Group> groups;

	@Where(clause = "is_deleted=0")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
	@OrderBy(clause = "id ASC")
	private Set<Expression> expressions;

	@Where(clause = "is_deleted=0")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
	@OrderBy(clause = "id ASC")
	private Set<Variable> variables;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", nullable = false)
	private User user;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productCategoryId", nullable = false)
	private ProductCategory productCategory;

	public Product() {
		super();
		this.setIsDeleted(false);
	}

	public Product(long id, String name, long parentId, int status, String identifier, User user,
			ProductCategory productCategory) {
		super();
		this.setId(id);
		this.setName(name);
		this.parentId = parentId;
		this.status = status;
		this.identifier = identifier;
		this.user = user;
		this.productCategory = productCategory;
		this.setIsDeleted(false);
	}

	public Product(String name, User user, ProductCategory productCategory, String identifier) {
		this.setName(name);
		this.user = user;
		this.productCategory = productCategory;
		this.parentId = 0L;
		this.status = 0;
		this.identifier = identifier;
		this.setIsDeleted(false);
	}

	public Product(String name, User user, ProductCategory productCategory, String identifier, long parentId) {
		this.setName(name);
		this.user = user;
		this.productCategory = productCategory;
		this.parentId = parentId;
		this.status = 0;
		this.identifier = identifier;
		this.setIsDeleted(false);
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	public Set<Expression> getExpressions() {
		return expressions;
	}

	public void setExpressions(Set<Expression> expressions) {
		this.expressions = expressions;
	}

	public Set<Variable> getVariables() {
		return variables;
	}

	public void setVariables(Set<Variable> variables) {
		this.variables = variables;
	}
}