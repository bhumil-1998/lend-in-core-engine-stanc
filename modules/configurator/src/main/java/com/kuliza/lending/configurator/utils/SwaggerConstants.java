package com.kuliza.lending.configurator.utils;

public class SwaggerConstants {
	
	public static final String PC_CATEGORIES_CONFIGURE = "It finds all the categories";
	public static final String PC_CATEGORIES_RESPONSE = "{\"data\":"
			+ "[{\"id\":1,\"name\":\"Personal\"},"
			+ "{\"id\":2,\"name\":\"Business\"},"
			+ "{\"id\":3,\"name\":\"Home\"},"
			+ "{\"id\":4,\"name\":\"Education\"},"
			+ "{\"id\":5,\"name\":\"LAP\"},"
			+ "{\"id\":6,\"name\":\"BusinessLoan\"},"
			+ "{\"id\":7,\"name\":\"PersonalLoan\"}],"
			+ "\"message\":\"SUCCESS\","
			+ "\"status\":200}";
	
	public static final String PC_LIST_CONFIGURE = "This API fetch the list corresponding to a productCategoryId and the status, "
			+ "below is for productCategoryId = 1 and status = 1";
	public static final String PC_LIST_RESPONSE = "{\"data\":"
			+ "[{\"id\":13,\"identifier\":\"1a15fe9f-0b7c-49c8-bb96-ff66ed7c2b31\",\"name\":\"Test1\",\"parentId\":0,"
			+ "\"productCategory\":{\"id\":1,\"name\":\"Personal\"},\"status\":1},"
			+ "{\"id\":23,\"identifier\":\"8331effe-aa2d-46fa-8fd4-29c363d6aaa3\",\"name\":\"Test_Learn_Rule_Engine\",\"parentId\":0,"
			+ "\"productCategory\":{\"id\":1,\"name\":\"Personal\"},\"status\":2},"
			+ "{\"id\":32,\"identifier\":\"0c703882-cc65-4ce2-9045-d3e496750ab9\",\"name\":\"one\",\"parentId\":0,"
			+ "\"productCategory\":{\"id\":1,\"name\":\"Personal\"},\"status\":1}],"
			+ "\"message\":\"SUCCESS\","
			+ "\"status\":200}";
			
	public static final String PC_API_PRODUCT_CONFIGURE = "This api fetch all the credit models";
	public static final String PC_API_PRODUCT_RESPONSE = "{\"data\":["
			+ "{\"expressions\":[],\"groups\":[{\"expressions\":[],"
			+ "\"rules\":[{\"isActive\":\"true\",\"outputWeight\":\"35.0\","
			+ "\"rules\":[{\"fn1\":\"==\",\"fn2\":\"\",\"outputValue\":\"40\",\"ruleId\":\"673\",\"value1\":\"ankur\",\"value2\":\"\"}],"
			+ "\"variableName\":\"username\",\"variableSource\":\"userCreated\",\"variableType\":\"String\"}],"
			+ "\"id\":80,\"name\":\"lnt_users\"}],"
			+ "\"variables\":[{\"category\":\"Primitive\",\"description\":\"user name\",\"expression\":\"\",\"id\":440,"
			+ "\"name\":\"username\",\"source\":\"userCreated\",\"type\":\"String\",\"variableOrder\":0}],"
			+ "\"id\":7,\"identifier\":\"ef8ca37f-d726-4930-9e06-52f4423f4de2\",\"name\":\"ManinderTest\",\"parentId\":0,"
			+ "\"productCategory\":{\"id\":1,\"name\":\"Business\"},\"status\":0}]}";
	
	public static final String PC_PRODUCT_ID_CONFIGURATION = "This API fetch product corresponding to product id..below is for product id = 1";
	public static final String PC_PRODUCT_ID_RESPONSE = "{\r\n\"data\":"
			+ "{\r\n\"expressions\":[\r\n{\r\n\"expressionString\":\"Eligibility\",\r\n\"id\":2\r\n},\r\n{\r\n"
			+ "\"expressionString\":\"Eligibility\",\r\n\"id\":3\r\n}\r\n],\r\n\"groups\":[\r\n{\r\n\"expressions\":[\r\n{\r\n"
			+ "\"expressionString\":\"IF(AND(Age,AND(Income,AND(Property_PinCode,CibilScore))),\\\"TRUE\\\",\\\"FALSE\\\")\",\r\n"
			+ "\"id\":1\r\n}\r\n],\r\n\"rules\":[\r\n{\r\n\"isActive\":\"true\",\r\n\"outputWeight\":\"1.0\",\r\n"
			+ "\"rules\":[\r\n{\r\n\"fn1\":\"<\",\r\n\"fn2\":\"\",\r\n\"outputValue\":\"false\",\r\n"
			+ "\"ruleId\":\"1\",\r\n\"value1\":\"23\",\r\n\"value2\":\"\"\r\n},\r\n{\r\n\"fn1\":\">=\",\r\n\"fn2\":\"<=\",\r\n"
			+ "\"outputValue\":\"true\",\r\n\"ruleId\":\"3\",\r\n\"value1\":\"23\",\r\n\"value2\":\"55\"\r\n},\r\n{\r\n"
			+ "\"fn1\":\">\",\r\n\"fn2\":\"\",\r\n\"outputValue\":\"false\",\r\n\"ruleId\":\"96\",\r\n\"value1\":\"55\",\r\n\"value2\":\"\"\r\n}"
			+ "\r\n],\r\n\"variableName\":\"Age\",\r\n\"variableSource\":\"userCreated\",\r\n\"variableType\":"
			+ "\"Numerical\"\r\n},\r\n{\r\n\"isActive\":\"true\",\r\n\"outputWeight\":\"1.0\",\r\n\"rules\":[\r\n"
			+ "{\r\n\"fn1\":\"<\",\r\n\"fn2\":\"\",\r\n\"outputValue\":\"false\",\r\n\"ruleId\":\"8\",\r\n"
			+ "\"value1\":\"50000\",\r\n\"value2\":\"\"\r\n}\r\n],\r\n\"variableName\":\"Income\",\r\n\"variableSource\":"
			+ "\"userCreated\",\r\n\"variableType\":\"Numerical\"\r\n},\r\n{\r\n\"isActive\":\"true\",\r\n\"outputWeight\":\"1.0\",\r\n"
			+ "\"rules\":[\r\n{\r\n\"fn1\":\">=\",\r\n\"fn2\":\"<=\",\r\n\"outputValue\":\"true\",\r\n\"ruleId\":\"15\",\r\n"
			+ "\"value1\":\"100000\",\r\n\"value2\":\"189999\"\r\n}\r\n],\r\n\"variableName\":\"Property_PinCode\",\r\n\"variableSource\":"
			+ "\"userCreated\",\r\n\"variableType\":\"Numerical\"\r\n},\r\n{\r\n\"isActive\":\"true\",\r\n\"outputWeight\":\"1.0\",\r\n"
			+ "\"rules\":[\r\n{\r\n\"fn1\":\"<\",\r\n\"fn2\":\"\",\r\n\"outputValue\":\"false\",\r\n\"ruleId\":\"18\",\r\n\"value1\":\"500\",\r\n\"value2\":\"\"\r\n}\r\n],\r\n\"variableName\":\"CibilScore\",\r\n\"variableSource\":"
			+ "\"userCreated\",\r\n\"variableType\":\"Numerical\"\r\n}\r\n],\r\n\"id\":1,\r\n\"name\":\"Eligibility\"\r\n}\r\n],\r\n\"variables\":[\r\n{\r\n\"category\":\"Primitive\",\r\n\"description\":\"theageoftheapplicant\",\r\n"
			+ "\"expression\":\"\",\r\n\"id\":1,\r\n\"name\":\"Age\",\r\n\"source\":\"userCreated\",\r\n\"type\":\"Numerical\",\r\n\"variableOrder\":0\r\n},\r\n"
			+ "{\r\n\"category\":\"Primitive\",\r\n\"description\":\"IncomeoftheBorrower\",\r\n\"expression\":\"\",\r\n\"id\":2,\r\n\"name\":\"Income\",\r\n\"source\":\"userCreated\",\r\n\"type\":\"Numerical\",\r\n\"variableOrder\":0\r\n},"
			+ "\r\n{\r\n\"category\":\"Primitive\",\r\n\"description\":\"PincodeoftheProperty\",\r\n\"expression\":\"\",\r\n\"id\":3,\r\n\"name\":\"Property_PinCode\",\r\n\"source\":\"userCreated\",\r\n\"type\":\"Numerical\",\r\n\"variableOrder\":0\r\n},"
			+ "\r\n{\r\n\"category\":\"Primitive\",\r\n\"description\":\"CibilScoreoftheBorrower\",\r\n\"expression\":\"\",\r\n\"id\":4,\r\n\"name\":\"CibilScore\",\r\n\"source\":\"userCreated\",\r\n\"type\":\"Numerical\",\r\n\"variableOrder\":0\r\n},\r\n"
			+ "{\r\n\"category\":\"Primitive\",\r\n\"description\":\"RegionofProperty\",\r\n\"expression\":\"\",\r\n\"id\":11,\r\n\"name\":\"region\",\r\n\"source\":\"userCreated\",\r\n\"type\":\"String\",\r\n\"variableOrder\":0\r\n}\r\n],\r\n\"id\":1,\r\n"
			+ "\"identifier\":\"90509dee-a7fc-4df7-8f2a-42bd655d2cd5\",\r\n\"name\":\"LAP_Eligibility\",\r\n\"parentId\":0,\r\n\"productCategory\":{\r\n\"id\":5,\r\n\"name\":\"LAP\"\r\n},\r\n\"status\":2\r\n},\r\n\"message\":\"SUCCESS\",\r\n\"status\":200\r\n}";
	
	public static final String PC_SUBMIT_NEW_PRODUCT = "Submit new product";
	
	public static final String PC_CREATE_NEW_PRODUCT_VERSION = "Create new version of the product if it's deleted and status < 2";
	public static final String PC_CREATE_NEW_PRODUCT_VERSION_RESPONSE = "{\"data\":"
			+ "{\"id\":56,"
			+ "\"identifier\":\"8331effe-aa2d-46fa-8fd4-29c363d6aaa3\","
			+ "\"name\":\"Test_Learn_Rule_Engine_56\","
			+ "\"parentId\":23,"
			+ "\"productCategory\":{\"id\":1,\"name\":\"Personal\"},"
			+ "\"status\":0},"
			+ "\"message\":\"SUCCESS\","
			+ "\"status\":200}";
	
	public static final String PC_UPDATE_PRODUCT_NAME = "Publish the product";
	public static final String PC_UPDATE_PRODUCT_NAME_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"expressions\": [\n" + 
			"            {\n" + 
			"                \"expressionString\": \"group1 * group2\",\n" + 
			"                \"id\": 94\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"groups\": [\n" + 
			"            {\n" + 
			"                \"expressions\": [\n" + 
			"                    {\n" + 
			"                        \"expressionString\": \"age + slary\",\n" + 
			"                        \"id\": 92\n" + 
			"                    }\n" + 
			"                ],\n" + 
			"                \"rules\": [\n" + 
			"                    {\n" + 
			"                        \"isActive\": \"true\",\n" + 
			"                        \"outputWeight\": \"2.0\",\n" + 
			"                        \"rules\": [\n" + 
			"                            {\n" + 
			"                                \"fn1\": \"<\",\n" + 
			"                                \"fn2\": \"\",\n" + 
			"                                \"outputValue\": \"0\",\n" + 
			"                                \"ruleId\": \"664\",\n" + 
			"                                \"value1\": \"15\",\n" + 
			"                                \"value2\": \"\"\n" + 
			"                            },\n" + 
			"                            {\n" + 
			"                                \"fn1\": \">=\",\n" + 
			"                                \"fn2\": \"<=\",\n" + 
			"                                \"outputValue\": \"3\",\n" + 
			"                                \"ruleId\": \"665\",\n" + 
			"                                \"value1\": \"15\",\n" + 
			"                                \"value2\": \"40\"\n" + 
			"                            },\n" + 
			"                            {\n" + 
			"                                \"fn1\": \">\",\n" + 
			"                                \"fn2\": \"\",\n" + 
			"                                \"outputValue\": \"1\",\n" + 
			"                                \"ruleId\": \"666\",\n" + 
			"                                \"value1\": \"40\",\n" + 
			"                                \"value2\": \"\"\n" + 
			"                            }\n" + 
			"                        ],\n" + 
			"                        \"variableName\": \"age\",\n" + 
			"                        \"variableSource\": \"userCreated\",\n" + 
			"                        \"variableType\": \"Numerical\"\n" + 
			"                    }\n" + 
			"                ],\n" + 
			"                \"id\": 64,\n" + 
			"                \"name\": \"group1\"\n" + 
			"            },\n" + 
			"            {\n" + 
			"                \"expressions\": [\n" + 
			"                    {\n" + 
			"                        \"expressionString\": \"slary * creditCardLimit\",\n" + 
			"                        \"id\": 93\n" + 
			"                    }\n" + 
			"                ],\n" + 
			"                \"rules\": [\n" + 
			"                    {\n" + 
			"                        \"isActive\": \"true\",\n" + 
			"                        \"outputWeight\": \"2.0\",\n" + 
			"                        \"rules\": [\n" + 
			"                            {\n" + 
			"                                \"fn1\": \"<\",\n" + 
			"                                \"fn2\": \"\",\n" + 
			"                                \"outputValue\": \"1\",\n" + 
			"                                \"ruleId\": \"667\",\n" + 
			"                                \"value1\": \"100000\",\n" + 
			"                                \"value2\": \"\"\n" + 
			"                            },\n" + 
			"                            {\n" + 
			"                                \"fn1\": \">=\",\n" + 
			"                                \"fn2\": \"\",\n" + 
			"                                \"outputValue\": \"3\",\n" + 
			"                                \"ruleId\": \"668\",\n" + 
			"                                \"value1\": \"1000000\",\n" + 
			"                                \"value2\": \"\"\n" + 
			"                            }\n" + 
			"                        ],\n" + 
			"                        \"variableName\": \"slary\",\n" + 
			"                        \"variableSource\": \"userCreated\",\n" + 
			"                        \"variableType\": \"Numerical\"\n" + 
			"                    }\n" + 
			"                ],\n" + 
			"                \"id\": 65,\n" + 
			"                \"name\": \"group2\"\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"variables\": [\n" + 
			"            {\n" + 
			"                \"category\": \"Primitive\",\n" + 
			"                \"description\": \"age of the candidate\",\n" + 
			"                \"expression\": \"\",\n" + 
			"                \"id\": 419,\n" + 
			"                \"name\": \"age\",\n" + 
			"                \"source\": \"userCreated\",\n" + 
			"                \"type\": \"Numerical\",\n" + 
			"                \"variableOrder\": 0\n" + 
			"            },\n" + 
			"            {\n" + 
			"                \"category\": \"Primitive\",\n" + 
			"                \"description\": \"sal.\",\n" + 
			"                \"expression\": \"\",\n" + 
			"                \"id\": 420,\n" + 
			"                \"name\": \"slary\",\n" + 
			"                \"source\": \"userCreated\",\n" + 
			"                \"type\": \"Numerical\",\n" + 
			"                \"variableOrder\": 0\n" + 
			"            },\n" + 
			"            {\n" + 
			"                \"category\": \"Derived\",\n" + 
			"                \"description\": \"\",\n" + 
			"                \"expression\": \"( slary * 1.5 ) * ( age / 10 )\",\n" + 
			"                \"id\": 421,\n" + 
			"                \"name\": \"creditCardLimit\",\n" + 
			"                \"source\": \"userCreated\",\n" + 
			"                \"type\": \"Numerical\",\n" + 
			"                \"variableOrder\": 0\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"id\": 33,\n" + 
			"        \"identifier\": \"b7259702-d241-49ca-bf2e-9a5788f53c4b\",\n" + 
			"        \"name\": \"temp1\",\n" + 
			"        \"parentId\": 0,\n" + 
			"        \"productCategory\": {\n" + 
			"            \"id\": 1,\n" + 
			"            \"name\": \"Business\"\n" + 
			"        },\n" + 
			"        \"status\": 1\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
			
	public static final String PC_PUBLISH_PRODUCT = "Publish the product";
	public static final String PC_PUBLISH_PRODUCT_RESPONSE = "{\"data\":"
			+ "\"Product Published Successfully\","
			+ "\"message\":\"SUCCESS\","
			+ "\"status\":200}";
	
	public static final String PC_PRODUCT_DEPLOYED = "To deploy the product";
	public static final String PC_PRODUCT_DEPLOYED_RESPONSE = "{\n" + 
			"    \"data\": \"Product Deployed Successfully\",\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String PC_EXPORT_PRODUCT = "To export the product";
	public static final String PC_EXPORT_PRODUCT_RESPONSE = "{\n" + 
			"    \"expressions\": [\n" + 
			"        {\n" + 
			"            \"expressionString\": \"Demographic\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"groups\": [\n" + 
			"        {\n" + 
			"            \"expressions\": [\n" + 
			"                {\n" + 
			"                    \"expressionString\": \"Age\"\n" + 
			"                }\n" + 
			"            ],\n" + 
			"            \"rules\": [\n" + 
			"                {\n" + 
			"                    \"isActive\": \"true\",\n" + 
			"                    \"outputWeight\": \"1.0\",\n" + 
			"                    \"rules\": [\n" + 
			"                        {\n" + 
			"                            \"fn1\": \"<\",\n" + 
			"                            \"fn2\": \">\",\n" + 
			"                            \"outputValue\": \"1.0\",\n" + 
			"                            \"value1\": \"20\",\n" + 
			"                            \"value2\": \"60\"\n" + 
			"                        }\n" + 
			"                    ],\n" + 
			"                    \"variableName\": \"Age\",\n" + 
			"                    \"variableSource\": \"userCreated\",\n" + 
			"                    \"variableType\": \"Numerical\"\n" + 
			"                }\n" + 
			"            ],\n" + 
			"            \"name\": \"Demographic\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"variables\": [\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Age of the Borrower\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"name\": \"Age\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"Numerical\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"identifier\": \"e7013047-83a1-4516-90a2-4341d829e03c\",\n" + 
			"    \"name\": \"Test\",\n" + 
			"    \"parentId\": 0,\n" + 
			"    \"productCategory\": {\n" + 
			"        \"id\": 1,\n" + 
			"        \"name\": \"Business\"\n" + 
			"    },\n" + 
			"    \"status\": 1\n" + 
			"}";
 	
	public static final String EC_PRODUCT_EXPRESSION = "To get the expression of a product based on the product id";
	public static final String EC_PRODUCT_EXPRESSION_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"expressionString\": \"Demographic\",\n" + 
			"            \"id\": 4\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String EC_GROUP_EXPRESSION = "To get the group expressions";
	public static final String EC_GROUP_EXPRESSION_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"expressionString\": \"IF ( OR ( propertyRestriction = \\\"agricultutal\\\" , propertyRestriction = \\\"minorProperty\\\" , propertyRestriction = \\\"cantArea\\\" , propertyRestriction = \\\"underConservance\\\" , propertyRestriction = \\\"niRes\\\" , propertyRestriction = \\\"redGreen\\\" , propertyRestriction = \\\"none\\\" ) , \\\"FAIL\\\" , \\\"PASS\\\" )\",\n" + 
			"            \"id\": 155\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String EC_NEW_EXPRESSION = "To create new expression string";
	public static final String EC_NEW_EXPRESSION_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"expressionString\": \"WorkExperience\",\n" + 
			"            \"id\": 157\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String EC_NEW_GROUP_EXPRESSION = "To create new group expression";
	public static final String EC_NEW_GROUP_EXPRESSION_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"expressionString\": \"WorkExperience\",\n" + 
			"            \"id\": 159\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String EC_UPDATE_PRODUCT_EXPRESSION = "To update product expression";
	public static final String EC_UPDATE_PRODUCT_EXPRESSION_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"expressionString\": \"WorkExperience\",\n" + 
			"        \"id\": 159\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String EC_UPDATE_GROUP_EXPRESSION = "To update group expression";
	public static final String EC_UPDATE_GROUP_EXPRESSION_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"expressionString\": \"WorkExperience\",\n" + 
			"        \"id\": 159\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String VC_GET_VARIABLES = "To get the list of variables";
	public static final String VC_GET_VARIABLES_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Age of Applicant\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 658,\n" + 
			"            \"name\": \"ageApp\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"Numerical\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Total Work Experience Salaried\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 659,\n" + 
			"            \"name\": \"totalWorkSal\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"Numerical\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Cibil Score Applicant\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 660,\n" + 
			"            \"name\": \"cibilScoreApp\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"Numerical\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Net Monthly Income Salaried\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 661,\n" + 
			"            \"name\": \"netMonthlyIncome\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"Numerical\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Rera specification\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 662,\n" + 
			"            \"name\": \"propertyRera\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"String\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Property is Serviceable or not\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 663,\n" + 
			"            \"name\": \"propertyServiceable\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"String\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        },\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Does the property come under any of these restrictions?\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 664,\n" + 
			"            \"name\": \"propertyRestriction\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"String\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String VC_SINGLE_VARIABLE = "To get single variable data for a product";
	public static final String VC_SINGLE_VARIABLE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"category\": \"Primitive\",\n" + 
			"        \"description\": \"Does the property come under any of these restrictions?\",\n" + 
			"        \"expression\": \"\",\n" + 
			"        \"id\": 664,\n" + 
			"        \"name\": \"propertyRestriction\",\n" + 
			"        \"source\": \"userCreated\",\n" + 
			"        \"type\": \"String\",\n" + 
			"        \"variableOrder\": 0\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";

	public static final String VC_SAVE_NEW_VARIABLES = "To save new variables for a product";
	public static final String VC_SAVE_NEW_VARIABLES_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"category\": \"Primitive\",\n" + 
			"            \"description\": \"Just a temporary variable\",\n" + 
			"            \"expression\": \"\",\n" + 
			"            \"id\": 665,\n" + 
			"            \"name\": \"temporaryVariable\",\n" + 
			"            \"source\": \"userCreated\",\n" + 
			"            \"type\": \"String\",\n" + 
			"            \"variableOrder\": 0\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String VC_UPDATE_VARIABLE = "To update variable in a product";
	public static final String VC_UPDATE_VARIABLE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"category\": \"Primitive\",\n" + 
			"        \"description\": \"It's a temporary variable\",\n" + 
			"        \"expression\": \"\",\n" + 
			"        \"id\": 665,\n" + 
			"        \"name\": \"temporaryVariable\",\n" + 
			"        \"source\": \"userCreated\",\n" + 
			"        \"type\": \"String\",\n" + 
			"        \"variableOrder\": 0\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String VC_DELETE_VARIABLE = "To delete a variable.";
	public static final String VC_DELETE_VARIABLE_RESPONSE = "{\n" + 
			"    \"data\": \"Variable Deleted Successfully\",\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String FRC_PRODUCTION_FIRE_RULES = "To fire rules in production environment";
	public static final String FRC_PRODUCTION_FIRE_RULES_RESPONSE = "";
	
	public static final String RC_SAVE_OR_UPDATE = "To save the new rule or update the existing one";
	public static final String RC_SAVE_OR_UPDATE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"allSavedRules\": [\n" + 
			"            {\n" + 
			"                \"isActive\": \"true\",\n" + 
			"                \"outputWeight\": \"35.0\",\n" + 
			"                \"rules\": [\n" + 
			"                    {\n" + 
			"                        \"fn1\": \"==\",\n" + 
			"                        \"fn2\": \"\",\n" + 
			"                        \"outputValue\": \"40\",\n" + 
			"                        \"ruleId\": \"673\",\n" + 
			"                        \"value1\": \"ankur\",\n" + 
			"                        \"value2\": \"\"\n" + 
			"                    }\n" + 
			"                ],\n" + 
			"                \"variableName\": \"username\",\n" + 
			"                \"variableSource\": \"userCreated\",\n" + 
			"                \"variableType\": \"String\"\n" + 
			"            }\n" + 
			"        ]\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String RC_DELETE = "To delete a rule within a group";
	public static final String RC_DELETE_RESPONSE = "{\n" + 
			"    \"data\": \"Rule Successfully Deleted\",\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String GC_GET_GROUPS = "To get list of groups inside a single Product";
	public static final String GC_GET_GROUPS_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"expressions\": [],\n" + 
			"            \"id\": 80,\n" + 
			"            \"name\": \"lnt_users\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String GC_GET_ALL_GROUPS = "To get all Groups data inside a Product";
	public static final String GC_GET_ALL_GROUPS_RESPONSE = "{\n" + 
			"    \"data\": [\n" + 
			"        {\n" + 
			"            \"expressions\": [\n" + 
			"                {\n" + 
			"                    \"expressionString\": \"IF ( emi_paying_capability < = 0 , 0 , ( emi_paying_capability / ( PMT ( rate_of_interest / 12 , Limit_Tenure , - 100 ) ) ) ) * 100\",\n" + 
			"                    \"id\": 21\n" + 
			"                }\n" + 
			"            ],\n" + 
			"            \"rules\": [\n" + 
			"                {\n" + 
			"                    \"isActive\": \"false\",\n" + 
			"                    \"outputWeight\": \"1.0\",\n" + 
			"                    \"rules\": [\n" + 
			"                        {\n" + 
			"                            \"fn1\": \"<\",\n" + 
			"                            \"fn2\": \"\",\n" + 
			"                            \"outputValue\": \"1\",\n" + 
			"                            \"ruleId\": \"229\",\n" + 
			"                            \"value1\": \"0\",\n" + 
			"                            \"value2\": \"\"\n" + 
			"                        }\n" + 
			"                    ],\n" + 
			"                    \"variableName\": \"Limit_Tenure\",\n" + 
			"                    \"variableSource\": \"userCreated\",\n" + 
			"                    \"variableType\": \"Numerical\"\n" + 
			"                }\n" + 
			"            ],\n" + 
			"            \"id\": 15,\n" + 
			"            \"name\": \"Credit_Limit_Assessment\"\n" + 
			"        }\n" + 
			"    ],\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String GC_SINGLE_GROUPS = "To get single groups data inside a Product";
	public static final String GC_SINGLE_GROUPS_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"expressions\": [\n" + 
			"            {\n" + 
			"                \"expressionString\": \"( valueOfGoodAPCDLSG )\",\n" + 
			"                \"id\": 111\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"rules\": [\n" + 
			"            {\n" + 
			"                \"isActive\": \"true\",\n" + 
			"                \"outputWeight\": \"10.0\",\n" + 
			"                \"rules\": [\n" + 
			"                    {\n" + 
			"                        \"fn1\": \">=\",\n" + 
			"                        \"fn2\": \"<\",\n" + 
			"                        \"outputValue\": \"4\",\n" + 
			"                        \"ruleId\": \"710\",\n" + 
			"                        \"value1\": \"0\",\n" + 
			"                        \"value2\": \"300\"\n" + 
			"                    },\n" + 
			"                    {\n" + 
			"                        \"fn1\": \">=\",\n" + 
			"                        \"fn2\": \"<\",\n" + 
			"                        \"outputValue\": \"3\",\n" + 
			"                        \"ruleId\": \"711\",\n" + 
			"                        \"value1\": \"300\",\n" + 
			"                        \"value2\": \"600\"\n" + 
			"                    },\n" + 
			"                    {\n" + 
			"                        \"fn1\": \">=\",\n" + 
			"                        \"fn2\": \"<\",\n" + 
			"                        \"outputValue\": \"2\",\n" + 
			"                        \"ruleId\": \"712\",\n" + 
			"                        \"value1\": \"600\",\n" + 
			"                        \"value2\": \"1000\"\n" + 
			"                    },\n" + 
			"                    {\n" + 
			"                        \"fn1\": \">=\",\n" + 
			"                        \"fn2\": \"<\",\n" + 
			"                        \"outputValue\": \"1\",\n" + 
			"                        \"ruleId\": \"713\",\n" + 
			"                        \"value1\": \"1000\",\n" + 
			"                        \"value2\": \"1000000000\"\n" + 
			"                    }\n" + 
			"                ],\n" + 
			"                \"variableName\": \"valueOfGoodAPCDLSG\",\n" + 
			"                \"variableSource\": \"userCreated\",\n" + 
			"                \"variableType\": \"Numerical\"\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"id\": 90,\n" + 
			"        \"name\": \"Product_risk\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	
	public static final String GC_SAVE_GROUP = "To save new Group inside a product";
	public static final String GC_SAVE_GROUP_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"id\": 138,\n" + 
			"        \"name\": \"temp_group\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}" ;
	
	public static final String GC_UPDATE_GROUP = "To save new Group inside a product";
	public static final String GC_UPDATE_GROUP_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"id\": 80,\n" + 
			"        \"name\": \"lnt_users_updated\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String AC_PRODUCT_CATEGORY = "To create new product categories";
	public static final String AC_PRODUCT_CATEGORY_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"id\": 2,\n" + 
			"        \"name\": \"PersonalAndBusiness\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}"; 
	
	public static final String UC_REGISTER_USER = "To register a new user";
	public static final String UC_REGISTER_USER_RESPONSE = "{\n" + 
			"    \"access\": {\n" + 
			"        \"manageGroupMembership\": true,\n" + 
			"        \"view\": true,\n" + 
			"        \"mapRoles\": true,\n" + 
			"        \"impersonate\": true,\n" + 
			"        \"manage\": true\n" + 
			"    },\n" + 
			"    \"applicationRoles\": null,\n" + 
			"    \"attributes\": {\n" + 
			"        \"user_id\": [\n" + 
			"            \"8\"\n" + 
			"        ]\n" + 
			"    },\n" + 
			"    \"clientConsents\": null,\n" + 
			"    \"clientRoles\": null,\n" + 
			"    \"createdTimestamp\": 1578998006166,\n" + 
			"    \"credentials\": null,\n" + 
			"    \"disableableCredentialTypes\": [\n" + 
			"        \"password\"\n" + 
			"    ],\n" + 
			"    \"email\": \"udit@kuliza.com\",\n" + 
			"    \"emailVerified\": false,\n" + 
			"    \"enabled\": true,\n" + 
			"    \"federatedIdentities\": null,\n" + 
			"    \"federationLink\": null,\n" + 
			"    \"firstName\": null,\n" + 
			"    \"groups\": null,\n" + 
			"    \"id\": \"686c96c7-21b4-4189-943a-b3a3c4035162\",\n" + 
			"    \"lastName\": null,\n" + 
			"    \"notBefore\": 0,\n" + 
			"    \"origin\": null,\n" + 
			"    \"realmRoles\": null,\n" + 
			"    \"requiredActions\": [],\n" + 
			"    \"self\": null,\n" + 
			"    \"serviceAccountClientId\": null,\n" + 
			"    \"socialLinks\": null,\n" + 
			"    \"totp\": false,\n" + 
			"    \"username\": \"udit@kuliza.com\"\n" + 
			"}";
}
