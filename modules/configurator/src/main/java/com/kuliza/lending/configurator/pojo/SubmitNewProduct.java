package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SubmitNewProduct {

	@NotNull(message = "productName is a required key")
	@Pattern(regexp = Constants.NAME_REGEX, message = Constants.INVALID_PRODUCT_NAME_MESSAGE)
	@Size(max = Constants.MAX_LENGTH_NAME_STRING, message = Constants.PRODUCT_NAME_TOO_BIG_MESSAGE)
	private String productName;

	@NotNull(message = "templateProductId is a required key")
	@Pattern(regexp = Constants.EMPTY_OR_ID_REGEX, message = Constants.INVALID_TEMPLATE_PRODUCT_ID_MESSAGE)
	private String templateProductId;

	@NotNull(message = "productCategoryId is a required key")
	@Pattern(regexp = Constants.ID_REGEX, message = Constants.INVALID_PRODUCT_CATEGORY_ID_MESSAGE)
	private String productCategoryId;

	private String userId;

	public SubmitNewProduct() {
		this.productName = "";
		this.productCategoryId = "";
		this.templateProductId = "";
	}

	public SubmitNewProduct(String productName, String productCategoryId, String templateProductId) {
		this.productName = productName;
		this.productCategoryId = productCategoryId;
		this.templateProductId = templateProductId;
	}

	public String getProductCategoryId() {
		return productCategoryId;
	}

	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getTemplateProductId() {
		return templateProductId;
	}

	public void setTemplateProductId(String templateProductId) {
		this.templateProductId = templateProductId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("productName : " + productName + ", ");
	// inputData.append("templateProductId : " + templateProductId + ", ");
	// inputData.append("productCategoryId : " + productCategoryId);
	// inputData.append(" }");
	// return inputData.toString();
	// }

}
