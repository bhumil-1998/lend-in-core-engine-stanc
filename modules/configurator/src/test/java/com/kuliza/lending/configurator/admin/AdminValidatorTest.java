package com.kuliza.lending.configurator.admin;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.pojo.SubmitNewProductCategory;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.AdminDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class AdminValidatorTest {

	@Mock
	private ProductCategoryDao productCategoryDao;

	@InjectMocks
	private AdminDataValidator adminDataValidator;

	private ProductCategory pc;

	@Before
	public void init() {
		this.pc = new ProductCategory(1, "ScoreCard");
	}

	// Test For Create Product Category When Same Name Category Exists
	@Test
	public void validateProductCategory1() throws Exception {
		SubmitNewProductCategory input = new SubmitNewProductCategory("ScoreCard");
		when(productCategoryDao.findByNameAndIsDeleted(input.getProductCategoryName(), false)).thenReturn(this.pc);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProductCategory");
		adminDataValidator.validateNewProductCategory(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Create Product Category When Field Level Error Exists
	@Test
	public void validateProductCategory2() throws Exception {
		SubmitNewProductCategory input = new SubmitNewProductCategory("Score Card");
		when(productCategoryDao.findByNameAndIsDeleted(input.getProductCategoryName(), false)).thenReturn(this.pc);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProductCategory");
		result.rejectValue("productCategoryName", "productCategoryName.invalid",
				Constants.INVALID_PRODUCT_CATEGORY_NAME_MESSAGE);
		adminDataValidator.validateNewProductCategory(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Create Product Category When No Error Occurs
	@Test
	public void validateProductCategory3() throws Exception {
		SubmitNewProductCategory input = new SubmitNewProductCategory("Eligibility");
		when(productCategoryDao.findByNameAndIsDeleted(input.getProductCategoryName(), false)).thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProductCategory");
		adminDataValidator.validateNewProductCategory(input, result);
		assertEquals(false, result.hasErrors());
	}

}
