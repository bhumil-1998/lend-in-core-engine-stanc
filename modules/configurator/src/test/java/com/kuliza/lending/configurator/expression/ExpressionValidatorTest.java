package com.kuliza.lending.configurator.expression;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.NewExpression;
import com.kuliza.lending.configurator.pojo.SubmitNewExpressions;
import com.kuliza.lending.configurator.pojo.UpdateExpression;
import com.kuliza.lending.configurator.validators.ExpressionDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class ExpressionValidatorTest {

	@Mock
	private ProductDao productDao;

	@Mock
	private GroupDao groupDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private ExpressionDao expressionDao;

	@InjectMocks
	private ExpressionDataValidator expressionDataValidator;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Group g1;
	private Variable v1;
	private Expression e2;
	private String userId;
	private String productId;
	private String groupId;
	private String expressionId;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.groupId = "1";
		this.expressionId = "2";
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.e2 = new Expression(2, "AND ( ID_1 , TRUE )", false, true, this.p1, null);
	}

	// Test For Get Product Expressions When Product Belong To User
	@Test
	public void validateGetProductExpressions1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		assertEquals(true, expressionDataValidator.validateGetProductExpressions(this.userId, this.productId));
	}

	// Test For Get Product Expressions When Product Doesnn't Belong To User
	@Test
	public void validateGetProductExpressions2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false, expressionDataValidator.validateGetProductExpressions(this.userId, this.productId));
	}

	// Test For Get Group Expressions When Product Belong To User And Group
	// Belongs To Product
	@Test
	public void validateGetGroupExpressions1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		assertEquals(true,
				expressionDataValidator.validateGetGroupExpressions(this.userId, this.productId, this.groupId));
	}

	// Test For Get Group Expressions When Product Doesnn't Belong To User
	@Test
	public void validateGetGroupExpressions2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false,
				expressionDataValidator.validateGetGroupExpressions(this.userId, this.productId, this.groupId));
	}

	// Test For Get Group Expressions When Product Belong To User But Group
	// Doen't Belongs To Product
	@Test
	public void validateGetGroupExpressions3() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		assertEquals(false,
				expressionDataValidator.validateGetGroupExpressions(this.userId, this.productId, this.groupId));
	}

	// Test For Validate Product Expression When No Validation Error Occurs
	@Test
	public void validateExpression1() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		newExpression.setProductId(this.productId);
		newExpression.setGroupId("");
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong(this.productId), false))
				.thenReturn(this.g1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(newExpression, "newExpressions");
		expressionDataValidator.validateExpression(newExpression, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Product Expression When Field Level Validation Error
	// Occurs
	@Test
	public void validateExpression2() throws Exception {
		NewExpression newExpression = new NewExpression("");
		newExpression.setProductId(this.productId);
		newExpression.setGroupId("");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(newExpression, "newExpressions");
		result.rejectValue("expressionString", "expressionString.invalid", "expressionString is a required Key");
		expressionDataValidator.validateExpression(newExpression, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Product Expression When Invalid Expression Occurs
	@Test
	public void validateExpression3() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		newExpression.setProductId(this.productId);
		newExpression.setGroupId("");
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong(this.productId), false))
				.thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(newExpression, "newExpressions");
		expressionDataValidator.validateExpression(newExpression, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Group Expression When No Validation Error Occurs
	@Test
	public void validateExpression4() throws Exception {
		NewExpression newExpression = new NewExpression("Age+2*Age");
		newExpression.setProductId(this.productId);
		newExpression.setGroupId(this.groupId);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(newExpression, "newExpressions");
		expressionDataValidator.validateExpression(newExpression, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate New Product Expression When No Validation
	// Error Occurs
	@Test
	public void validateNewExpression1() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		this.p1.setStatus(0);
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		input.setGroupId("");
		input.setProductId(this.productId);
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		expressionDataValidator.validateNewExpression(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate New Product Expression When Field Level Validation
	// Error Occurs
	@Test
	public void validateNewExpression2() throws Exception {
		SubmitNewExpressions input = new SubmitNewExpressions(new ArrayList<>());
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpressions");
		result.rejectValue("newExpressionsList", "newExpressionsList.invalid", "newExpressionsList cannot be empty");
		expressionDataValidator.validateNewExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate New Product Expression When Product Doesn't Belong To
	// User
	@Test
	public void validateNewExpression3() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		this.p1.setStatus(1);
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		input.setGroupId("");
		input.setProductId(this.productId);
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		expressionDataValidator.validateNewExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate New Product Expression When Product Belongs To User But
	// Product Is In Deployed/Undeployed State
	@Test
	public void validateNewExpression4() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		this.p1.setStatus(2);
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		input.setGroupId("");
		input.setProductId(this.productId);
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		expressionDataValidator.validateNewExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate New Group Expression When Product Belongs To User And
	// Product Is In Edit State And Group Belongs To Product
	@Test
	public void validateNewExpression5() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		this.p1.setStatus(0);
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		input.setGroupId(this.groupId);
		input.setProductId(this.productId);
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		expressionDataValidator.validateNewExpression(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate New Group Expression When Product Belongs To User And
	// Product Is In Edit/Published State But Group Doesn't Belongs To Product
	@Test
	public void validateNewExpression6() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		this.p1.setStatus(1);
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		input.setGroupId(this.groupId);
		input.setProductId(this.productId);
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		expressionDataValidator.validateNewExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Product Update Expression When Field Level Validation Error
	// Occurs
	@Test
	public void validateUpdateExpression1() throws Exception {
		this.p1.setStatus(0);
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setExpressionId(this.expressionId);
		input.setGroupId("");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(expressionDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.productId), false)).thenReturn(this.e2);
		expressionDataValidator.validateUpdateExpression(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Product Update Expression When Field Level Validation Error
	// Occurs
	@Test
	public void validateUpdateExpression2() throws Exception {
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setExpressionId(this.expressionId);
		input.setGroupId("");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpressions");
		result.rejectValue("newExpression", "newExpression.invalid", "newExpression is a required key");
		expressionDataValidator.validateUpdateExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Product Expression When Product Doesn't Belong to User
	@Test
	public void validateUpdateExpression3() throws Exception {
		this.p1.setStatus(0);
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setExpressionId(this.expressionId);
		input.setGroupId("");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		expressionDataValidator.validateUpdateExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Product Expression When Product Belong to User But
	// Product Is In Deployed/UnDeployed State
	@Test
	public void validateUpdateExpression4() throws Exception {
		this.p1.setStatus(2);
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setExpressionId(this.expressionId);
		input.setGroupId("");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		expressionDataValidator.validateUpdateExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Product Update Expression When Product Belongs To User And
	// Product Is In Edit State But
	// Expression Doesn't Belong To Product
	@Test
	public void validateUpdateExpression5() throws Exception {
		this.p1.setStatus(0);
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setExpressionId(this.expressionId);
		input.setGroupId("");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(expressionDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.productId), false)).thenReturn(null);
		expressionDataValidator.validateUpdateExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Group Update Expression When Product Belongs To User And
	// Product Is In Edit State And Expression Belongs To Group
	@Test
	public void validateUpdateExpression6() throws Exception {
		this.p1.setStatus(0);
		NewExpression newExpression = new NewExpression("Age + 3 * Age");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setExpressionId(this.expressionId);
		input.setGroupId(this.groupId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(expressionDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.groupId), false)).thenReturn(null);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		expressionDataValidator.validateUpdateExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Group Update Expression When Product Belongs To User And
	// Product Is In Edit State And Expression Doesn't Belongs To Group
	@Test
	public void validateUpdateExpression7() throws Exception {
		this.p1.setStatus(0);
		NewExpression newExpression = new NewExpression("Age + 3 * Age");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setExpressionId(this.expressionId);
		input.setGroupId(this.groupId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpressions");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(expressionDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.groupId), false)).thenReturn(null);
		expressionDataValidator.validateUpdateExpression(input, result);
		assertEquals(true, result.hasErrors());
	}

}
