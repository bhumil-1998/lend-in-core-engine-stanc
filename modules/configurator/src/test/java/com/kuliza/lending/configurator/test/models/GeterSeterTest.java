package com.kuliza.lending.configurator.test.models;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.openpojo.reflection.filters.FilterPackageInfo;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

@SpringBootTest(classes = CreditEngineApplication.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class GeterSeterTest {

	// The package to test
	private static final String POJO_PACKAGE = "com.kuliza.lending.configurator.models";

	@Test
	public void testGetterSetter() {
		Validator validator = ValidatorBuilder.create()
				// Add Rules to validate structure for POJO_PACKAGE
				// See com.openpojo.validation.rule.impl for more ...
				.with(new GetterMustExistRule()).with(new SetterMustExistRule())
				// Add Testers to validate behaviour for POJO_PACKAGE
				// See com.openpojo.validation.test.impl for more ...
				.with(new SetterTester()).with(new GetterTester()).build();

		validator.validate(POJO_PACKAGE, new FilterPackageInfo());
	}
}