package com.kuliza.lending.configurator.rule;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewRule;
import com.kuliza.lending.configurator.pojo.RuleDef;
import com.kuliza.lending.configurator.pojo.SubmitNewRules;
import com.kuliza.lending.configurator.services.RuleServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.RuleDataValidator;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
@SuppressFBWarnings("SIC_INNER_SHOULD_BE_STATIC_ANON")
public class RuleServiceTest {

	@Mock
	private ObjectMapper objectMapper;

	@Mock
	private GroupDao groupDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private RuleDao ruleDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private RuleDataValidator ruleDataValidator;

	@InjectMocks
	private RuleServices ruleServices;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Variable v1;
	private Group g1;
	private Rule r1;
	private Rule r2;
	private String userId;
	private String productId;
	private String groupId;
	private String ruleId;
	private RuleDef r1RuleDef;
	private RuleDef r2RuleDef;
	private NewRule newRule;
	private SubmitNewRules submitNewRules;

	@Before
	public void init() {
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1,"arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">= 30 && <= 50", "5", 2.0, true, this.v1, this.g1);
		this.r2 = new Rule(2, "> 50", "4", 2.0, true, this.v1, this.g1);
		this.userId = "1";
		this.productId = "1";
		this.groupId = "2";
		this.ruleId = "2";
		this.r1RuleDef = new RuleDef("", ">=", "<=", "30", "50", "5");
		this.r2RuleDef = new RuleDef("2", ">", "", "51", "", "4");
		this.newRule = new NewRule(Arrays.asList(r1RuleDef, r2RuleDef), "true", "2.0", "Age");
		this.submitNewRules = new SubmitNewRules(Arrays.asList(newRule));
	}

	// Test For Creating Or Updating Rules
	@Test
	public void createNewRule() throws Exception {
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		GenericAPIResponse response = ruleServices.createNewRules(this.productId, this.groupId, this.submitNewRules);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Validate Rule When No Validation Error Occurs
	@Test
	public void validateSubmitRules1() throws Exception {
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		doNothing().when(ruleDataValidator).validateRuleForm(any(SubmitNewRules.class), any(BindingResult.class));
		doNothing().when(ruleDataValidator).validateRule(any(NewRule.class), any(BindingResult.class));
		doNothing().when(ruleDataValidator).validateRuleStructure(any(RuleDef.class), any(BindingResult.class));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		GenericAPIResponse response = ruleServices.validateSubmitRules(this.submitNewRules, result, this.userId,
				this.productId, this.groupId);
		assertNull(response);
	}

	// Test For Validate Rule When SubmitNewRules Field Level Validation Errors
	@Test
	public void validateSubmitRules2() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		result.rejectValue("newRulesList", "newRulesList.invalid", "newRulesList cannot be empty");
		GenericAPIResponse response = ruleServices.validateSubmitRules(this.submitNewRules, result, this.userId,
				this.productId, this.groupId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Rule When NewRule Field Level Validation Errors
	@Test
	public void validateSubmitRules3() throws Exception {
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		doNothing().when(ruleDataValidator).validateRuleForm(any(SubmitNewRules.class), any(BindingResult.class));
		doAnswer(new Answer<BeanPropertyBindingResult>() {
			@Override
			public BeanPropertyBindingResult answer(final InvocationOnMock invocation) throws Throwable {

				final BeanPropertyBindingResult result = (BeanPropertyBindingResult) (invocation.getArguments())[1];
				result.rejectValue("variableName", "variableName.invalid", Constants.INVALID_VARIABLE_NAME_MESSAGE);
				return result;
			}
		}).when(ruleDataValidator).validateRule(any(NewRule.class), any(BindingResult.class));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		GenericAPIResponse response = ruleServices.validateSubmitRules(this.submitNewRules, result, this.userId,
				this.productId, this.groupId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Rule When Same Variable Is Used More Than Once To
	// Create Rule
	@Test
	public void validateSubmitRules4() throws Exception {
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		doNothing().when(ruleDataValidator).validateRuleForm(any(SubmitNewRules.class), any(BindingResult.class));
		doAnswer(new Answer<BeanPropertyBindingResult>() {
			@Override
			public BeanPropertyBindingResult answer(final InvocationOnMock invocation) throws Throwable {

				final BeanPropertyBindingResult result = (BeanPropertyBindingResult) (invocation.getArguments())[1];
				result.rejectValue("variableName", "variableName.invalid", Constants.VARIABLE_ALREADY_USED);
				return result;
			}
		}).when(ruleDataValidator).validateRule(any(NewRule.class), any(BindingResult.class));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		NewRule newRule2 = new NewRule(Arrays.asList(r1RuleDef, r2RuleDef), "true", "2.0", "Age");
		this.submitNewRules = new SubmitNewRules(Arrays.asList(newRule, newRule2));
		GenericAPIResponse response = ruleServices.validateSubmitRules(this.submitNewRules, result, this.userId,
				this.productId, this.groupId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Rule When RuleDef Field Level Validation Errors
	@Test
	public void validateSubmitRules5() throws Exception {
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		doNothing().when(ruleDataValidator).validateRuleForm(any(SubmitNewRules.class), any(BindingResult.class));
		doNothing().when(ruleDataValidator).validateRule(any(NewRule.class), any(BindingResult.class));
		doAnswer(new Answer<BeanPropertyBindingResult>() {
			@Override
			public BeanPropertyBindingResult answer(final InvocationOnMock invocation) throws Throwable {

				final BeanPropertyBindingResult result = (BeanPropertyBindingResult) (invocation.getArguments())[1];
				result.rejectValue("value2", "value2.invalid", "Value 2 is required if Fn2 is set");
				return result;
			}
		}).when(ruleDataValidator).validateRuleStructure(any(RuleDef.class), any(BindingResult.class));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		GenericAPIResponse response = ruleServices.validateSubmitRules(this.submitNewRules, result, this.userId,
				this.productId, this.groupId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Delete Rule When No Validation Errors
	@Test
	public void validateDeleteRule1() throws Exception {
		this.p1.setStatus(0);
		when(ruleDataValidator.validateProductId(eq(this.productId), eq(this.userId))).thenReturn(true);
		when(ruleDataValidator.validateGroupId(eq(this.groupId), eq(this.productId))).thenReturn(true);
		when(ruleDataValidator.validateRuleId(eq(this.ruleId), eq(this.groupId))).thenReturn(true);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(ruleDao.findByIdAndIsDeleted(Long.parseLong(this.ruleId), false)).thenReturn(this.r2);
		when(ruleDao.findByVariableIdAndIsDeleted(this.v1.getId(), false)).thenReturn(Arrays.asList(this.r1, this.r2));
		GenericAPIResponse response = ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId,
				this.ruleId);
		assertNull(response);
	}

	// Test For Validate Delete Rule When Product Doesn't Belong to User
	@Test
	public void validateDeleteRule2() throws Exception {
		this.p1.setStatus(0);
		when(ruleDataValidator.validateProductId(eq(this.productId), eq(this.userId))).thenReturn(false);
		GenericAPIResponse response = ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId,
				this.ruleId);
		assertEquals(Constants.INVALID_PRODUCT_ID_MESSAGE, response.getData());
	}

	// Test For Validate Delete Rule When Group Doesn't Belong to Product
	@Test
	public void validateDeleteRule3() throws Exception {
		this.p1.setStatus(0);
		when(ruleDataValidator.validateProductId(eq(this.productId), eq(this.userId))).thenReturn(true);
		when(ruleDataValidator.validateGroupId(eq(this.groupId), eq(this.productId))).thenReturn(false);
		GenericAPIResponse response = ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId,
				this.ruleId);
		assertEquals(Constants.INVALID_GROUP_ID_MESSAGE, response.getData());
	}

	// Test For Validate Delete Rule When Rule Doesn't Belong to Group
	@Test
	public void validateDeleteRule4() throws Exception {
		this.p1.setStatus(0);
		when(ruleDataValidator.validateProductId(eq(this.productId), eq(this.userId))).thenReturn(true);
		when(ruleDataValidator.validateGroupId(eq(this.groupId), eq(this.productId))).thenReturn(true);
		when(ruleDataValidator.validateRuleId(eq(this.ruleId), eq(this.groupId))).thenReturn(false);
		GenericAPIResponse response = ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId,
				this.ruleId);
		assertEquals(Constants.INVALID_RULE_ID_MESSAGE, response.getData());
	}

	// Test For Validate Delete Rule When Product Is In Deployed/Undeployed
	// State
	@Test
	public void validateDeleteRule5() throws Exception {
		this.p1.setStatus(2);
		when(ruleDataValidator.validateProductId(eq(this.productId), eq(this.userId))).thenReturn(true);
		when(ruleDataValidator.validateGroupId(eq(this.groupId), eq(this.productId))).thenReturn(true);
		when(ruleDataValidator.validateRuleId(eq(this.ruleId), eq(this.groupId))).thenReturn(true);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId,
				this.ruleId);
		assertEquals(Constants.PRODUCT_NOT_EDITABLE_MESSAGE, response.getData());
	}

	// Test For Validate Delete Rule When Variable Has Only 1 Rule
	@Test
	public void validateDeleteRule6() throws Exception {
		this.p1.setStatus(0);
		when(ruleDataValidator.validateProductId(eq(this.productId), eq(this.userId))).thenReturn(true);
		when(ruleDataValidator.validateGroupId(eq(this.groupId), eq(this.productId))).thenReturn(true);
		when(ruleDataValidator.validateRuleId(eq(this.ruleId), eq(this.groupId))).thenReturn(true);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(ruleDao.findByIdAndIsDeleted(Long.parseLong(this.ruleId), false)).thenReturn(this.r2);
		when(ruleDao.findByVariableIdAndIsDeleted(this.v1.getId(), false)).thenReturn(Arrays.asList(this.r2));
		GenericAPIResponse response = ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId,
				this.ruleId);
		assertEquals(Constants.CANNOT_DELETE_RULE_MESSAGE, response.getData());
	}

	// Test For Delete Rule Service
	@Test
	public void deleteRule1() throws Exception {
		when(ruleDao.findByIdAndIsDeleted(Long.parseLong(this.ruleId), false)).thenReturn(this.r2);
		GenericAPIResponse response = ruleServices.deleteRule(this.ruleId);
		assertEquals(Constants.RULE_DELETED_SUCCESS_MESSAGE, response.getData());
	}

}
