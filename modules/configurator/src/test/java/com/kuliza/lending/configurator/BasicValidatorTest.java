package com.kuliza.lending.configurator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.NewExpression;
import com.kuliza.lending.configurator.validators.BasicDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class BasicValidatorTest {

	@Mock
	private RuleDao ruleDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private GroupDao groupDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private ExpressionDao expressionDao;

	@InjectMocks
	private BasicDataValidator basicDataValidator;

	private User user;
	private ProductCategory pc1;
	private Product p1;
	private Variable v1;
	private Group g1;
	private Rule r1;
	private Expression e1;
	private Expression e2;
	private String userId;
	private String productId;
	private String variableId;
	private String groupId;
	private String ruleId;
	private String expressionId;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.variableId = "1";
		this.groupId = "1";
		this.ruleId = "1";
		this.expressionId = "1";
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">=30 && <=50", "5", 2.0, true, this.v1, this.g1);
		this.e1 = new Expression(1, "Age + 2*Age", true, false, null, this.g1);
		this.e2 = new Expression(1, "AND(Personal_Eligibility, TRUE)", false, true, this.p1, null);
	}

	// Test For validate ProductId When Product Belongs to User
	@Test
	public void validateProductId1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		assertEquals(true, basicDataValidator.validateProductId(this.productId, this.userId));
	}

	// Test For validate ProductId When Product Doesn't Belongs to User
	@Test
	public void validateProductId2() throws Exception {
		this.productId = "2";
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false, basicDataValidator.validateProductId(this.productId, this.userId));
	}

	// Test For validate GroupId When Group Belongs to Product
	@Test
	public void validateGroupId1() throws Exception {
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		assertEquals(true, basicDataValidator.validateGroupId(this.groupId, this.productId));
	}

	// Test For validate GroupId When Group Doesn't Belongs to Product
	@Test
	public void validateGroupId2() throws Exception {
		this.groupId = "2";
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		assertEquals(false, basicDataValidator.validateGroupId(this.groupId, this.productId));
	}

	// Test For validate VariableId When Variable Belongs to Product
	@Test
	public void validateVariableId1() throws Exception {
		when(variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.variableId), Long.parseLong(productId),
				false)).thenReturn(this.v1);
		assertEquals(true, basicDataValidator.validateVariableId(this.variableId, this.productId));
	}

	// Test For validate VariableId When Variable Doesn't Belongs to Product
	@Test
	public void validateVariableId2() throws Exception {
		this.variableId = "2";
		when(variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.variableId), Long.parseLong(productId),
				false)).thenReturn(null);
		assertEquals(false, basicDataValidator.validateVariableId(this.variableId, this.productId));
	}

	// Test For validate RuleId When Rule Belongs to Group
	@Test
	public void validateRuleId1() throws Exception {
		when(ruleDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.ruleId), Long.parseLong(this.groupId), false))
				.thenReturn(this.r1);
		assertEquals(true, basicDataValidator.validateRuleId(this.ruleId, this.groupId));
	}

	// Test For validate RuleId When Rule Doesn't Belongs to Group
	@Test
	public void validateRuleId2() throws Exception {
		this.ruleId = "2";
		when(ruleDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.ruleId), Long.parseLong(this.groupId), false))
				.thenReturn(null);
		assertEquals(false, basicDataValidator.validateRuleId(this.ruleId, this.groupId));
	}

	// Test For validate ExpressionId When Expression Belongs to Product
	@Test
	public void validateExpressionId1() throws Exception {
		when(expressionDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.productId), false)).thenReturn(this.e2);
		assertEquals(true, basicDataValidator.validateExpressionId(this.expressionId, "", this.productId));
	}

	// Test For validate ExpressionId When Expression Doesn't Belongs to Product
	@Test
	public void validateExpressionId2() throws Exception {
		this.expressionId = "1";
		when(expressionDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.productId), false)).thenReturn(null);
		assertEquals(false, basicDataValidator.validateExpressionId(this.expressionId, "", this.productId));
	}

	// Test For validate ExpressionId When Expression Belongs to Group
	@Test
	public void validateExpressionId3() throws Exception {
		when(expressionDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.groupId), false)).thenReturn(this.e1);
		assertEquals(true, basicDataValidator.validateExpressionId(this.expressionId, this.groupId, ""));
	}

	// Test For validate ExpressionId When Expression Doesn't Belongs to Group
	@Test
	public void validateExpressionId4() throws Exception {
		this.expressionId = "2";
		when(expressionDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.expressionId),
				Long.parseLong(this.groupId), false)).thenReturn(null);
		assertEquals(false, basicDataValidator.validateExpressionId(this.expressionId, this.groupId, ""));
	}

	// Test For Validate Group Expression String When No Validation Error Occurs
	@Test
	public void validateExpressionString1() throws Exception {
		String expressionString = this.e1.getExpressionString();
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(new NewExpression(), "newExpression");
		assertEquals(true, basicDataValidator.validateExpressionString(expressionString, true, this.productId, result));
	}

	// Test For Validate Group Expression String When Variable Doesn't Exist
	@Test
	public void validateExpressionString2() throws Exception {
		String expressionString = this.e1.getExpressionString();
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(new NewExpression(), "newExpression");
		assertEquals(false,
				basicDataValidator.validateExpressionString(expressionString, true, this.productId, result));
	}

	// Test For Validate Product Expression String When No Validation Error
	// Occurs
	@Test
	public void validateExpressionString3() throws Exception {
		String expressionString = this.e2.getExpressionString();
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong(productId), false))
				.thenReturn(this.g1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(new NewExpression(), "newExpression");
		assertEquals(true,
				basicDataValidator.validateExpressionString(expressionString, false, this.productId, result));
	}

	// Test For Validate Product Expression String When Group Doesn't Exist
	@Test
	public void validateExpressionString4() throws Exception {
		String expressionString = this.e2.getExpressionString();
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong(productId), false))
				.thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(new NewExpression(), "newExpression");
		assertEquals(false,
				basicDataValidator.validateExpressionString(expressionString, false, this.productId, result));
	}

	// Test For Validate Expression When Expression String is Invalid
	@Test
	public void validateExpressionString5() throws Exception {
		String expressionString = "Personal_Eligibility * 2 +";
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong(productId), false))
				.thenReturn(this.g1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(new NewExpression(), "newExpression");
		assertEquals(false,
				basicDataValidator.validateExpressionString(expressionString, false, this.productId, result));
	}
}
