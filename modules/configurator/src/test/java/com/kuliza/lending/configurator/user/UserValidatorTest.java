package com.kuliza.lending.configurator.user;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.UserDao;
import com.kuliza.lending.configurator.pojo.SubmitNewUser;
import com.kuliza.lending.configurator.validators.UserDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class UserValidatorTest {

	@Mock
	private UserDao userDao;

	@InjectMocks
	private UserDataValidator userDataValidator;

	private User user;

	@Before
	public void init() {
		this.user = new User(1, "arpit.agrawal@kuliza.com");
	}

	// Test For Create User When Same UserName Exists
	@Test
	public void validateUser1() throws Exception {
		SubmitNewUser input = new SubmitNewUser("", "asdfghj", "Minjar", "arpit.agrawal@kuliza.com");
		when(userDao.findByUserName(input.getUserName())).thenReturn(this.user);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewUser");
		userDataValidator.validateUser(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Create User When Field Level Validation Fails
	@Test
	public void validateUser2() throws Exception {
		SubmitNewUser input = new SubmitNewUser("Arpit", "asdfghj", "Minjar", "arpit.agrawal@kuliza.com");
		when(userDao.findByUserName(input.getUserName())).thenReturn(this.user);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewUser");
		result.rejectValue("name", "name.invalid", "Name cannot be Empty");
		userDataValidator.validateUser(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Create User When No Error Occurs
	@Test
	public void validateUser3() throws Exception {
		SubmitNewUser input = new SubmitNewUser("Arpit", "asdfghj", "Minjar", "arpit.agrawal@minjar.com");
		when(userDao.findByUserName(input.getUserName())).thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewUser");
		userDataValidator.validateUser(input, result);
		assertEquals(false, result.hasErrors());
	}
}
