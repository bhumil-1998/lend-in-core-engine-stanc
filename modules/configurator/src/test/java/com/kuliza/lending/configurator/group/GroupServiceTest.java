package com.kuliza.lending.configurator.group;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewGroup;
import com.kuliza.lending.configurator.pojo.UpdateGroup;
import com.kuliza.lending.configurator.services.GroupServices;
import com.kuliza.lending.configurator.utils.Constants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class GroupServiceTest {

	@Mock
	private ObjectMapper objectMapper;

	@Mock
	private GroupDao groupDao;

	@Mock
	private ProductDao productDao;

	@InjectMocks
	private GroupServices groupServices;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Group g1;
	private Group g2;

	@Before
	public void init() {
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.g2 = new Group(2, "Business_Eligibility", p1);
	}

	// Test For Groups List
	@Test
	public void getGroupsList() throws Exception {
		when(groupDao.findByProductIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.g1, this.g2));
		GenericAPIResponse response = groupServices.getGroupsList("1");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Arrays.asList(this.g1, this.g2), response.getData());
	}

	// Test For All Groups Data
	@Test
	public void getGroupsData() throws Exception {
		when(groupDao.findByProductIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.g1, this.g2));
		GenericAPIResponse response = groupServices.getGroupsData("1");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Arrays.asList(this.g1, this.g2), response.getData());
	}

	// Test For Single Group Data
	@Test
	public void getGroupData() throws Exception {
		when(groupDao.findByIdAndIsDeleted(2, false)).thenReturn(g2);
		GenericAPIResponse response = groupServices.getGroupData("2");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(this.g2, response.getData());
	}

	// Test For Create New Group
	@Test
	public void createNewGroup() throws Exception {
		SubmitNewGroup input = new SubmitNewGroup("Personal_Eligibility");
		input.setProductId("1");
		when(productDao.findByIdAndIsDeleted(1, false)).thenReturn(p1);
		GenericAPIResponse response = groupServices.createNewGroup(input);
		Group responseGroup = (Group) response.getData();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(input.getGroupName(), responseGroup.getName());
	}

	// Test For Update Group
	@Test
	public void updateGroup() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility_New");
		input.setGroupId("1");
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(g1);
		GenericAPIResponse response = groupServices.updateGroup(input);
		Group responseGroup = (Group) response.getData();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(input.getGroupName(), responseGroup.getName());
	}

}
