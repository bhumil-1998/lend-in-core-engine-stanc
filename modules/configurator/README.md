***
# About Configurator module

*    Configurator is one of the most crucial modules of the product.
*    This module allows us to create variables, groups, rules etc. which collectively is known as rule model or credit model
*    This module gets input variables and the corresponding rule model runs the rules over this input and gives us the output
*    We are using DROOLS Rule Engine (version 6.5.0) to evaluate rules. 
*    Drools is a business rule management system with a forward and backward chaining inference based rules engine, more correctly known as a production rule system, using an enhanced implementation of the Rete algorithm.
*    The properties file is Lend-in-module.properties where database, keycloak, dms etc.properties are present which get the values from bash profile.
*    Dependencies:
    *    Logging
    *    Common
    *    Authorization
