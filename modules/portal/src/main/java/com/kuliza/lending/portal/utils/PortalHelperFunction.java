package com.kuliza.lending.portal.utils;

import com.kuliza.lending.common.pojo.ExtendedApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.models.CustomURLMapper;
import com.kuliza.lending.engine_common.utils.EnumConstants.URL_CONVERTER_IDENTIFIER;
import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.kuliza.lending.pojo.GroupDetailWithMembers;
import com.kuliza.lending.portal.models.PortalCommentHistory;
import com.kuliza.lending.portal.pojo.ActionConfiguration;
import com.kuliza.lending.portal.pojo.BucketConfiguration;
import com.kuliza.lending.portal.pojo.BucketvariableConfiguration;
import com.kuliza.lending.portal.pojo.CustomActionHeaders;
import com.kuliza.lending.utils.AuthConstants;
import org.flowable.cmmn.api.runtime.PlanItemInstance;
import org.flowable.form.model.FormField;
import org.flowable.task.api.Task;
import org.json.JSONArray;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.*;

import static com.kuliza.lending.portal.utils.PortalConstants.*;

public class PortalHelperFunction extends CommonHelperFunctions {

  private static final Logger LOGGER = LoggerFactory.getLogger(PortalHelperFunction.class);

  public static ExtendedApiResponse makeResponse(HttpStatus status, String message,
      String caseInstanceId,
      Task task, Map<String, Object> data, Map<String, FormField> formFields) throws Exception {
    String taskDefinitionKey = task.getTaskDefinitionKey();
    String taskId = task.getId();
    Map<String, Object> formData = HelperFunctions.getPresentScreenInfo(formFields,
        URL_CONVERTER_IDENTIFIER.PORTAL);
    Map<String, Object> responseData = new HashMap<>();
    responseData.put(Constants.TASK_ID_KEY, taskId);
    responseData.put(Constants.CASE_INSTANCE_ID_KEY, caseInstanceId);
    responseData.put(Constants.DATA_KEY, data);
    responseData.put(taskDefinitionKey, formData);
    responseData.put(Constants.STEPPER_DATA_KEY, task.getDescription());
    return new ExtendedApiResponse(status, message, responseData, taskDefinitionKey,
        task.getName(), "");
  }

  public static ExtendedApiResponse makePortalResponse(HttpStatus status, String message,
      Task task, Map<String, Object> data, Map<String, FormField> formFields,
      Map<String, FormField> globalFormFields, List<PortalCommentHistory> commentData)
      throws Exception {
    String taskDefinitionKey = task.getTaskDefinitionKey();
    String taskId = task.getId();
    Map<String, Object> formData = new HashMap<>();
    formData.put(ASSIGNED_LIST, data.getOrDefault(ASSIGNED_LIST, null));
    formData.put(BUCKET_CONFIGURATION, data.getOrDefault(BUCKET_CONFIGURATION, null));
    if (!taskDefinitionKey.contains(PortalConstants.PORTFOLIO_SUFFIX)) {
      formData = HelperFunctions.getPresentScreenInfo(formFields,
          URL_CONVERTER_IDENTIFIER.PORTAL);
    }
    Map<String, Object> globalFormData = HelperFunctions.getPresentScreenInfo(globalFormFields,
        URL_CONVERTER_IDENTIFIER.PORTAL);
    Map<String, Object> responseData = new HashMap<>();
    responseData.put(Constants.TASK_ID_KEY, taskId);
    responseData.put(Constants.CASE_INSTANCE_ID_KEY, data.get(CASE_INSTANCE_ID));
    responseData.put(taskDefinitionKey, formData);
    responseData.put(GLOBAL_VARIABLES, globalFormData);
    responseData.put(Constants.COMMENTS_KEY, commentData);
    responseData.put(COMMENT_CONFIGURATION, data.get(COMMENT_CONFIGURATION));
    responseData.put(Constants.STEPPER_DATA_KEY, task.getDescription());
    responseData.put(PORTAL_VISTED_TAB, data.get(PORTAL_VISTED_TAB));
    return new ExtendedApiResponse(status, message, responseData, taskDefinitionKey,
        task.getName(), "");
  }

  public static ArrayList<BucketConfiguration> getBucketConfigList(
      Object bucketConfigStringListObject) {
    ArrayList<BucketConfiguration> response = new ArrayList<>();
    if (bucketConfigStringListObject != null) {
      JSONArray bucketConfigStringList = new JSONArray(
          CommonHelperFunctions.getStringValue(bucketConfigStringListObject));
      for (int i = 0; i < bucketConfigStringList.length(); i++) {
        if (bucketConfigStringList.get(i) != null) {
          BucketConfiguration bucketConfiguration = new BucketConfiguration();
          Map<String, Object> bucketConfigurationMap = CommonHelperFunctions
              .fromJson(bucketConfigStringList.getJSONObject(i));
          bucketConfiguration.setBucketActions(getActionConfigurationList(
              bucketConfigurationMap.getOrDefault(BUCKET_ACTIONS, null)));
          bucketConfiguration.setVariables(getBucketVariableConfiguration(
              bucketConfigurationMap.getOrDefault(VARIABLES, null)));
          bucketConfiguration.setLabel(CommonHelperFunctions
              .getStringValue(bucketConfigurationMap.getOrDefault(Constants.LABEL, null)));
          bucketConfiguration.setOnLoadId(CommonHelperFunctions
                  .getStringValue(bucketConfigurationMap.getOrDefault("onLoadId", null)));
          bucketConfiguration.setId(CommonHelperFunctions
              .getStringValue(bucketConfigurationMap.getOrDefault(Constants.ID_KEY, null)));
          bucketConfiguration.setEditable(CommonHelperFunctions
              .getBooleanValue(bucketConfigurationMap.getOrDefault(EDITABLE, null)));
          bucketConfiguration.setComment(CommonHelperFunctions
              .getIntegerValue(bucketConfigurationMap.getOrDefault(Constants.COMMENTS_KEY, null)));
          bucketConfiguration.setHierarchyLevel(CommonHelperFunctions
                  .getIntegerValue(bucketConfigurationMap.getOrDefault(Constants.HIERARCHY_LEVEL_KEY, null)));
          bucketConfiguration.setRedirectToDetails(CommonHelperFunctions
              .getBooleanValue(bucketConfigurationMap.getOrDefault(REDIRECT_TO_DETAILS_KEY, true)));
          response.add(bucketConfiguration);
        }
      }
    }
    return response;
  }

  public static ArrayList<ActionConfiguration> getActionConfigurationListFromString(
      Object actionConfigurationListString) {
    ArrayList<ActionConfiguration> response = new ArrayList<>();
    if (actionConfigurationListString != null) {
      JSONArray actionConfigStringList = new JSONArray(
          CommonHelperFunctions.getStringValue(actionConfigurationListString));
      for (int i = 0; i < actionConfigStringList.length(); i++) {
        if (actionConfigStringList.get(i) != null) {
          ActionConfiguration actionConfiguration = new ActionConfiguration();
          Map<String, Object> actionConfigurationMap = CommonHelperFunctions
              .fromJson(actionConfigStringList.getJSONObject(i));
          actionConfiguration.setId(
              CommonHelperFunctions
                  .getStringValue(actionConfigurationMap.getOrDefault(Constants.ID_KEY, null)));
          actionConfiguration.setLabel(
              CommonHelperFunctions
                  .getStringValue(actionConfigurationMap.getOrDefault(Constants.LABEL, null)));
          actionConfiguration
              .setParams((Map<String, Object>) actionConfigurationMap
                  .getOrDefault(Constants.PARAMS_KEY, null));
          actionConfiguration.setIconKey(
              CommonHelperFunctions
                  .getStringValue(actionConfigurationMap.getOrDefault(ICON_KEY, null)));
          actionConfiguration.setValue(
              CommonHelperFunctions
                  .getBooleanValue(actionConfigurationMap.getOrDefault(Constants.VALUE_KEY, null)));
          response.add(actionConfiguration);
        }
      }
    }
    return response;
  }

  public static ArrayList<CustomActionHeaders> getCustomActionHeadersList(
      Object customActionHeadersListObject) {
    ArrayList<CustomActionHeaders> response = new ArrayList<>();
    if (customActionHeadersListObject != null) {
      JSONArray customActionHeaderStringList = null;
      if (customActionHeadersListObject instanceof ArrayList) {
        customActionHeaderStringList = new JSONArray(
            (List<Map<String, Object>>) customActionHeadersListObject);
      } else {
        customActionHeaderStringList = new JSONArray(
            CommonHelperFunctions.getStringValue(customActionHeadersListObject));
      }
      for (int i = 0; i < customActionHeaderStringList.length(); i++) {
        if (customActionHeaderStringList.get(i) != null) {
          CustomActionHeaders customActionHeadersConfiguration = new CustomActionHeaders();
          Map<String, Object> customActionHeadersConfigurationMap = CommonHelperFunctions
              .fromJson(customActionHeaderStringList.getJSONObject(i));
          customActionHeadersConfiguration.setKey(CommonHelperFunctions
              .getStringValue(
                  customActionHeadersConfigurationMap.getOrDefault(Constants.KEY, null)));
          customActionHeadersConfiguration.setValue(CommonHelperFunctions
              .getStringValue(
                  customActionHeadersConfigurationMap.getOrDefault(Constants.VALUE_KEY, null)));
          response.add(customActionHeadersConfiguration);
        }
      }
    }
    return response;
  }


  public static ArrayList<BucketvariableConfiguration> getBucketVariableConfiguration(
      Object bucketVariableConfigList) {
    ArrayList<BucketvariableConfiguration> response = new ArrayList<>();
    if (bucketVariableConfigList != null) {
      List<Map<String, Object>> bucketVariableMapList = (List<Map<String, Object>>) bucketVariableConfigList;
      for (Map<String, Object> bucketVariableMap : bucketVariableMapList) {
        if (bucketVariableMap != null) {
          BucketvariableConfiguration bucketvariableConfiguration = new BucketvariableConfiguration();
          bucketvariableConfiguration
              .setId(CommonHelperFunctions
                  .getStringValue(bucketVariableMap.getOrDefault(Constants.ID_KEY, null)));
          bucketvariableConfiguration.setLabel(
              CommonHelperFunctions
                  .getStringValue(bucketVariableMap.getOrDefault(Constants.LABEL, null)));
          bucketvariableConfiguration.setType(
              CommonHelperFunctions
                  .getStringValue(bucketVariableMap.getOrDefault(Constants.TYPE_KEY, null)));
          bucketvariableConfiguration.setActionConfigurationList(getActionConfigurationList(
              bucketVariableMap.getOrDefault(ACTION_CONFIGURATION_LIST_KEY, null)));
          response.add(bucketvariableConfiguration);
        }
      }
    }
    return response;
  }

  public static ArrayList<ActionConfiguration> getActionConfigurationList(
      Object actionConfigurationList) {
    ArrayList<ActionConfiguration> response = new ArrayList<>();
    if (actionConfigurationList != null) {
      List<Map<String, Object>> actionConfigurationMapList = (List<Map<String, Object>>) actionConfigurationList;
      for (Map<String, Object> actionConfigurationMap : actionConfigurationMapList) {
        if (actionConfigurationMap != null) {
          ActionConfiguration actionConfiguration = new ActionConfiguration();
          actionConfiguration.setId(
              CommonHelperFunctions
                  .getStringValue(actionConfigurationMap.getOrDefault(Constants.ID_KEY, null)));
          actionConfiguration.setLabel(
              CommonHelperFunctions
                  .getStringValue(actionConfigurationMap.getOrDefault(Constants.LABEL, null)));
          actionConfiguration
              .setParams((Map<String, Object>) actionConfigurationMap
                  .getOrDefault(Constants.PARAMS_KEY, null));
          actionConfiguration.setIconKey(
              CommonHelperFunctions
                  .getStringValue(actionConfigurationMap.getOrDefault(ICON_KEY, null)));
          actionConfiguration.setValue(
              CommonHelperFunctions
                  .getBooleanValue(actionConfigurationMap.getOrDefault(Constants.VALUE_KEY, null)));
          response.add(actionConfiguration);
        }
      }
    }
    return response;
  }

  public static Map<String, Object> getRoleMembersList(
      List<GroupDetailWithMembers> groupDetailWithMembersList, String groupId, String emailId) {
    Map<String, Object> response = new HashMap<>();
    for (GroupDetailWithMembers groupDetailWithMembers : groupDetailWithMembersList) {
      Set<String> memberList = new HashSet<>();
      for (UserRepresentation userRepresentation : groupDetailWithMembers.getMemberList()) {
        if (userRepresentation.getEmail() != null && !userRepresentation.getEmail().isEmpty()) {
          if (!groupDetailWithMembers.getId().equals(groupId) || userRepresentation.getEmail()
              .equals(emailId)) {
            memberList.add(userRepresentation.getEmail());
          }
        }
      }
      if (!memberList.isEmpty()) {
        memberList.addAll((Set<String>) response
            .getOrDefault(groupDetailWithMembers.getRealmRoles().get(0), new HashSet<>()));
        memberList.add(emailId);
        response.put(groupDetailWithMembers.getRealmRoles().get(0), memberList);
      }
    }
    return response;
  }

  public static String setPathVariables(String url, Map<String, String> pathVariables) {
    String pathVariablesUrl = url;
    if (pathVariables != null && !pathVariables.isEmpty() && pathVariables.size() > 0) {
      for (String key : pathVariables.keySet()) {
        pathVariablesUrl = pathVariablesUrl.replace("/" + key + "/",
            "/" + pathVariables.get(key) + "/");
        if ((pathVariablesUrl.indexOf("/" + key) + ("/" + key).length()) == pathVariablesUrl
            .length()) {
          pathVariablesUrl = pathVariablesUrl.replace("/" + key, "/" + pathVariables.get(key));
        }
      }
    }
    return pathVariablesUrl;
  }

  public static String setRequestParams(String url, Map<String, String> requestParams) {
    String requestParamUrl = url;
    if (requestParams != null && !requestParams.isEmpty() && requestParams.size() > 0) {
      requestParamUrl = requestParamUrl + "?";
      int mapSize = requestParams.size(), count = 1;
      for (String key : requestParams.keySet()) {
        requestParamUrl = requestParamUrl + key + "=" + requestParams.get(key);
        if (count < mapSize) {
          requestParamUrl = requestParamUrl + "&";
        }
        count++;
      }
    }
    return requestParamUrl;
  }

  public static Map<String, Object> getVisitedTab(Map<String, Object> processVariables,
      String roleName, String assignee, String tabKey) {
    Map<String, Object> response = new HashMap<>();
    Map<String, Object> visitedTabsMap = (Map<String, Object>) processVariables
        .getOrDefault(PORTAL_VISTED_TAB, new HashMap<>());
    Set<String> oldVistedTabs = new HashSet<>();
    try {
      oldVistedTabs = (Set<String>) visitedTabsMap
          .getOrDefault(roleName + "-" + assignee, new HashSet<>());
    } catch (Exception e) {
      oldVistedTabs = new HashSet<String>((List<String>) visitedTabsMap
          .getOrDefault(roleName + "-" + assignee, new HashSet<>()));
    }
    Set<String> newVistedTabs = new HashSet<>(oldVistedTabs);
    newVistedTabs.add(tabKey);
    visitedTabsMap.put(roleName + "-" + assignee, newVistedTabs);
    response.put(OLD_VISITED_TAB, oldVistedTabs);
    response.put(NEW_VISITED_TAB, newVistedTabs);
    response.put(NEW_VISITED_MAP, visitedTabsMap);
    return response;
  }

  public static ArrayList<CustomURLMapper> getCustomURLMapperList(Object customURLMapperList) {
    ArrayList<CustomURLMapper> response = new ArrayList<>();
    if (customURLMapperList != null) {
      List<Map<String, Object>> customURLMapperMapList = (List<Map<String, Object>>) customURLMapperList;
      for (Map<String, Object> customURLMapperMap : customURLMapperMapList) {
        if (customURLMapperMap != null) {
          CustomURLMapper customURLMapper = new CustomURLMapper();
          customURLMapper.setSlug(
              CommonHelperFunctions
                  .getStringValue(customURLMapperMap.getOrDefault(SLUG_KEY, null)));
          customURLMapper.setUserId(
              CommonHelperFunctions
                  .getStringValue(customURLMapperMap.getOrDefault(AuthConstants.USER_ID, null)));
          customURLMapper.setOpenApi(
              CommonHelperFunctions
                  .getBooleanValue(customURLMapperMap.getOrDefault(OPEN_API, null)));
          customURLMapper.setUrl(
              CommonHelperFunctions
                  .getStringValue(customURLMapperMap.getOrDefault(Constants.URL_KEY, null)));
          customURLMapper.setPortalType(
              CommonHelperFunctions
                  .getStringValue(customURLMapperMap.getOrDefault(PORTAL_TYPE, null)));
//          customURLMapper.setHeaders(getCustomURLMapperList(customURLMapperMap.getOrDefault("headers", null)));
          customURLMapper.setExternalApi(
              CommonHelperFunctions
                  .getBooleanValue(customURLMapperMap.getOrDefault(EXTERNAL_API, null)));
//          customURLMapper.setRequestType(
//              REQUEST_TYPE.valueOf(CommonHelperFunctions.getStringValue(customURLMapperMap.getOrDefault("requestType", null))));
          response.add(customURLMapper);
        }
      }
    }
    return response;
  }

  public static String portalFetchingCaseQuery(Map<String, Set<String>> roleMembersMap, Map<String, String> filters, String productType, String bucketKey, List<String> bucketVariables, String databaseType) {
	    String queryFilterJoin="";
	    String queryFilterWhereClause="";
	    String queryFilterBaseQuery="";
	    if (filters.keySet().size() > 2) {
	      List<String> queryFilterKey = getFilterKey(filters);
	      int queryFilterKeySize = queryFilterKey.size();
	      String tableName = null;
	      if(databaseType.equals("mysql"))
	         tableName = PortalConstants.TABLE_ACT_HI_VARINST;
	      else if(databaseType.equals("postgres"))
	         tableName = PortalConstants.TABLE_ACT_HI_VARINST.toLowerCase();
	      String currentInstance = tableName + "_" + PortalConstants.DEFAULT_NUMBER_OF_INSTANCES.toString();
	      
	         /*
	          * We're using this condition in case we're doing global search. In that case
	          * only one instance of new table would be needed.
	          */
	      
	      if(queryFilterKeySize == 1 && queryFilterKey.get(0).equals(GLOBAL_SEARCH_KEY)) {
	         queryFilterJoin = portalFetchQueryMap(databaseType).get(FILTER_JOIN_SYNTAX);
	         queryFilterWhereClause = portalFetchQueryMap(databaseType).get(GLOBAL_FILTER_WHERE_CLAUSE);
	         queryFilterBaseQuery = portalFetchQueryMap(databaseType).get(GLOBAL_ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY);
	         
	         queryFilterJoin = queryFilterJoin.replace(PortalConstants.CURRENT_TABLE_INSTANCE, currentInstance);
	         queryFilterWhereClause = queryFilterWhereClause.replace(PortalConstants.CURRENT_TABLE_INSTANCE, currentInstance);
	         
	         queryFilterWhereClause = queryFilterWhereClause.replace(PortalConstants.QUERY_FILTER_KEY, queryFilterKey.get(0));
	         queryFilterWhereClause = queryFilterWhereClause.replace(PortalConstants.QUERY_FILTER_VALUE, filters.get(queryFilterKey.get(0)));
	         
	         queryFilterBaseQuery = queryFilterBaseQuery.replace(PortalConstants.QUERY_FILTER_KEY, queryFilterKey.get(0));
	         queryFilterBaseQuery = queryFilterBaseQuery.replace(PortalConstants.QUERY_FILTER_VALUE, filters.get(queryFilterKey.get(0)));
	      }
	      
	         /*
	          * This else part would be executed, if we have multiple filters(key,value pair)
	          * and accordingly new instances of the same table would be created dynamically.
	          */
	      
	      else {
	         String filterJoinSyntax = portalFetchQueryMap(databaseType).get(FILTER_JOIN_SYNTAX);
	         String filterWhereClause = portalFetchQueryMap(databaseType).get(FILTER_WHERE_CLAUSE);
	         String allProductBucketFilterBaseQuery = portalFetchQueryMap(databaseType).get(ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY);
	         for(int additionalInstance=0;additionalInstance<queryFilterKey.size();additionalInstance++) {
	             String currentQueryFilterWhereClause = filterWhereClause;
	             String currentQueryFilterBaseQuery = allProductBucketFilterBaseQuery;
	             String currentQueryFilterJoin = filterJoinSyntax;
	             
	             Integer tableInstanceNumber = additionalInstance + PortalConstants.DEFAULT_NUMBER_OF_INSTANCES;
	            currentInstance = tableName + "_" + tableInstanceNumber.toString();
	             
	             currentQueryFilterJoin = currentQueryFilterJoin.replace(PortalConstants.CURRENT_TABLE_INSTANCE, currentInstance);
	             currentQueryFilterWhereClause = currentQueryFilterWhereClause.replace(PortalConstants.CURRENT_TABLE_INSTANCE, currentInstance);
	             
	             currentQueryFilterWhereClause = currentQueryFilterWhereClause.replace(PortalConstants.QUERY_FILTER_KEY, queryFilterKey.get(additionalInstance));
	             currentQueryFilterWhereClause = currentQueryFilterWhereClause.replace(PortalConstants.QUERY_FILTER_VALUE, filters.get(queryFilterKey.get(additionalInstance)));
	             
	             currentQueryFilterBaseQuery = currentQueryFilterBaseQuery.replace(PortalConstants.QUERY_FILTER_KEY, queryFilterKey.get(additionalInstance));
	             currentQueryFilterBaseQuery = currentQueryFilterBaseQuery.replace(PortalConstants.QUERY_FILTER_VALUE, filters.get(queryFilterKey.get(additionalInstance)));
	             
	             queryFilterWhereClause += currentQueryFilterWhereClause;
	             queryFilterBaseQuery += currentQueryFilterBaseQuery;
	             queryFilterJoin += currentQueryFilterJoin;
	          }
	      }
	    }

	    String portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
	    if (productType.equals(ALL_PRODUCT_TYPE) && bucketKey.equals(ALL_BUCKET_KEY)) {
	      portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(ALL_PRODUCT_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
	    } else if (productType.equals(ALL_PRODUCT_TYPE)) {
	      portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(ALL_PRODUCT_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
	    } else if (bucketKey.equals(ALL_BUCKET_KEY)) {
	      portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(ALL_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
	    }
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(PortalConstants.QUERY_BUCKET_VARIABLES, "'".concat(String.join("','", bucketVariables)).concat("'"));
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(PortalConstants.QUERY_BUCKET_KEY, bucketKey.split("-")[0]);
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(QUERY_PRODUCT_TYPE, productType);
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(QUERY_FILTER_WHERE_CLAUSE, queryFilterWhereClause);
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(QUERY_FILTER_JOIN, queryFilterJoin);
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(QUERY_FILTER_BASE_QUERY, queryFilterBaseQuery);
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(QUERY_ROLE_NAME_STATUS_LIST, "'".concat(String.join("Status','", roleMembersMap.keySet())).concat("Status'"));
	    portalFetchingCaseQuery = portalFetchingCaseQuery.replace(QUERY_TASKNAME_ASSIGNEE_LIST, getTaskNameAssigneeListQuery(roleMembersMap, bucketKey, databaseType));

//	    portalFetchingCaseQuery = "select  concat('{\"caseInstanceId\":\"',act_ru_task.scope_id_,'\"}') as caseInstanceId, concat('{',string_agg(concat('\"',TRANSLATE('created',  '\"', '\"'),'\":\"', TRANSLATE(cast(act_ru_task.create_time_ as text), '\"', '\"'),'\"'), ','),'}') as asdf, concat('{',string_agg(concat('\"',TRANSLATE(act_hi_varinst.name_,  '\"', '\"'),'\":\"', TRANSLATE(act_hi_varinst.text_, '\"', '\"'),'\"'), ','),'}') as variables from act_ru_task, act_hi_varinst where act_ru_task.scope_type_ = 'cmmn' and (act_ru_task.task_def_key_ in ('RcuHunterSampler-claim') ) and act_ru_task.scope_id_ in (select scope_id_ from act_hi_varinst where act_hi_varinst.name_ in ('RcuHunterSamplerStatus') and act_hi_varinst.text_ = 'claim')  and act_hi_varinst.name_ in ('firstname','applicantMobileNumber','claimbulk','claim') and act_ru_task.scope_id_ = act_hi_varinst.scope_id_ group by act_ru_task.scope_id_ order by  max(act_ru_task.create_time_) desc;";
	    LOGGER.info("portalFetchingCaseQuery ::::::::::: " + portalFetchingCaseQuery);
	    return portalFetchingCaseQuery;
	  }

  public static String getTaskNameAssigneeListQuery(Map<String, Set<String>> roleMembersMap,
      String bucketKey, String databaseType) {
    String taskNameAssigneeListCompleteQuery = "";
    List<String> taskAssigneeQueryList = new ArrayList<>();
    String taskNameSuffix = LIST_VIEW_KEY;
    if (bucketKey.equals(CLAIM_BUCKET_KEY) || bucketKey.contains(CLAIM_BUCKET_KEY)) {
      taskNameSuffix = CLAIM_VIEW_KEY;
    } else if (bucketKey.equals(HISTORY_BUCKET_KEY) || bucketKey.contains(HISTORY_BUCKET_KEY)) {
      taskNameSuffix = HISTORY_VIEW_KEY;
    }
    if (bucketKey.equals(CLAIM_BUCKET_KEY) || bucketKey.contains(ALL_KEY_SUFFIX)) {
      taskNameAssigneeListCompleteQuery = portalFetchQueryMap(databaseType)
          .get(CLAIM_TASK_ASSIGNEE_BASE_QUERY);
      taskNameAssigneeListCompleteQuery = taskNameAssigneeListCompleteQuery
          .replace(QUERY_CLAIM_TASK_LIST,
              "'".concat(String.join(taskNameSuffix.concat("','"), roleMembersMap.keySet()))
                  .concat(taskNameSuffix).concat("'"));
    } else {
      for (String roleName : roleMembersMap.keySet()) {
        String taskAssigneeBaseQuery = portalFetchQueryMap(databaseType)
            .get(TASK_ASSIGNEE_BASE_QUERY);
        if (bucketKey.equals(ALL_BUCKET_KEY)) {
          taskAssigneeBaseQuery = portalFetchQueryMap(databaseType)
              .get(ALL_BUCKET_TASK_ASSIGNEE_BASE_QUERY);
          taskAssigneeBaseQuery = taskAssigneeBaseQuery.replace(QUERY_ASSIGNED_TASKNAME, "'".
              concat(roleName).concat(LIST_VIEW_KEY).concat("', '").
              concat(roleName).concat(HISTORY_BUCKET_KEY).concat("', '").
              concat(roleName).concat(HISTORY_BUCKET_KEY).concat("'"));
        }
        taskAssigneeBaseQuery = taskAssigneeBaseQuery.replace(QUERY_ASSIGNED_TASKNAME,
            "'".concat(roleName).concat(taskNameSuffix).concat("'"));
        taskAssigneeBaseQuery = taskAssigneeBaseQuery.replace(QUERY_ASSIGNEE_LIST,
            "'".concat(String.join("','", roleMembersMap.get(roleName))).concat("'"));
        taskAssigneeQueryList.add(taskAssigneeBaseQuery);
      }
      taskNameAssigneeListCompleteQuery = String.join(OR_CLAUSE, taskAssigneeQueryList);
    }

    LOGGER
        .info("taskNameAssigneeListCompleteQuery ::::::::::: " + taskNameAssigneeListCompleteQuery);
    return taskNameAssigneeListCompleteQuery;
  }

  public static List<Map<String, Object>> assignedCasesFromResultSet(List<Object[]> resultSet,
      String roleName, String bucketKey, List<String> bucketVariables,
      String parentProcessInstanceId, Boolean redirectToDetails, Map<String, Object> globalScopeVariables) {
    ArrayList<Map<String, Object>> assignedCasesList = new ArrayList<Map<String, Object>>();
    Map<String, Object> bucketVariablesMap = new HashMap<>();
    bucketVariablesMap.putAll(CommonHelperFunctions.getMapFromKeySet(bucketVariables, null));
    for (Object[] row : resultSet) {
      Map<String, Object> assignedCase = new HashMap<>();
      assignedCase.putAll(CommonHelperFunctions.getMap(globalScopeVariables, bucketVariables));
      //TODO: need to validate for dashboard view action value
//      assignedCase.putAll(bucketVariablesMap);
      assignedCase.put(Constants.PARENT_PROCESS_INSTANCE_KEY, parentProcessInstanceId);
      for (Object object : row) {
        assignedCase.putAll(CommonHelperFunctions
            .getHashMapFromJsonString(CommonHelperFunctions.getStringValue(object)));
      }
      if (!redirectToDetails && assignedCase.containsKey(Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
        assignedCase.put(Constants.PROCESS_TASKS_ASSIGNEE_KEY, null);
      }
      assignedCase.put(ROLE_NAME_KEY, roleName);
      assignedCase.put(ASSIGNED_ROLE_NAME,
          CommonHelperFunctions.getStringValue(assignedCase.get(TASKNAME_KEY)).split("-")[0]);
      assignedCase.put(BUCKET_KEY, bucketKey);
      assignedCasesList.add(assignedCase);
    }
    return assignedCasesList;
  }


  public static List<String> getFilterKey(Map<String, String> filters){
    Map<String, Object> tempMap = new HashMap<>();
    List<String> listOfUpdatedQueryParameters = new ArrayList<>();
    tempMap.putAll(filters);
    tempMap.remove(FILTER_OFFSET_KEY);
    tempMap.remove(FILTER_PAGE_WIDTH_KEY);
    if (tempMap.containsKey(PQL_QUERY_STRING_KEY)) { tempMap.remove(PQL_QUERY_STRING_KEY); }
    if (tempMap.containsKey(PQL_QUERY_MAP_KEY)) { tempMap.remove(PQL_QUERY_MAP_KEY); }
    for(String filterKey:tempMap.keySet())
      listOfUpdatedQueryParameters.add(filterKey);
    return listOfUpdatedQueryParameters;
  }

  public static String getStageInstanceId(List<PlanItemInstance> planItemInstanceList,
                                          String stageName) {
//		List<PlanItemInstance> planItemInstanceList = cmmnRuntimeService.createPlanItemInstanceQuery().caseInstanceId(caseInstanceId).planItemInstanceStateActive().list();
    String stageInstanceId = null;
    for (PlanItemInstance planItemInstance : planItemInstanceList) {
      if (planItemInstance.isStage() && planItemInstance.getPlanItemDefinitionId()
              .equals(stageName)) {
        stageInstanceId = planItemInstance.getId();
        break;
      }
    }
    return stageInstanceId;
  }

  public static List<String> getPlanItemInstanceDefinitionIdByStage(
          List<PlanItemInstance> planItemInstanceList, String stageInstanceId) {
    List<String> planItemInstanceSet = new ArrayList<>();
    for (PlanItemInstance planItemInstance : planItemInstanceList) {
      if (planItemInstance.getPlanItemDefinitionType().equals(HUMAN_TASK) && planItemInstance
              .getStageInstanceId().equals(stageInstanceId)) {
        planItemInstanceSet.add(planItemInstance.getPlanItemDefinitionId());
      }
    }
    return planItemInstanceSet;
  }

  public static List<PlanItemInstance> getPlanItemInstanceByStage(
          List<PlanItemInstance> planItemInstanceList, String stageInstanceId) {
    List<PlanItemInstance> planItemInstanceSet = new ArrayList<>();
    for (PlanItemInstance planItemInstance : planItemInstanceList) {
      if (!planItemInstance.isStage() && planItemInstance.getStageInstanceId()
              .equals(stageInstanceId)) {
        planItemInstanceSet.add(planItemInstance);
      }
    }
    return planItemInstanceSet;
  }

  public static String getOnLoadId(String description) {
    String onLoadId = null;
    try {
      onLoadId = CommonHelperFunctions.getStringValue(CommonHelperFunctions.getHashMapFromJsonString(description.trim()).getOrDefault("onLoadId", ""));
    } catch (Exception e){
      LOGGER.warn("getTabConfig onLoadId not present" + e);
    }
    return onLoadId;
  }

  public static String getPQLString(Map<String, String> filterMap){
    String pqlString = filterMap.getOrDefault(PQL_QUERY_STRING_KEY, null);
    if (isNotEmpty(pqlString)){
      pqlString = CommonHelperFunctions.decodeBase64EncodedString(pqlString);
    }
    return pqlString;
  }

  public static List<Map<String, Object>> getPQLMap(Map<String, String> filterMap){
    String pqlMapString = filterMap.getOrDefault(PQL_QUERY_MAP_KEY, null);
    List<Map<String, Object>> pqlMap = new ArrayList<>();
    if (isNotEmpty(pqlMapString)){
      pqlMap = (List<Map<String, Object>>) CommonHelperFunctions.getObjectInJsonFormat(CommonHelperFunctions.decodeBase64EncodedString(pqlMapString));
    }
    return pqlMap;
  }

}
