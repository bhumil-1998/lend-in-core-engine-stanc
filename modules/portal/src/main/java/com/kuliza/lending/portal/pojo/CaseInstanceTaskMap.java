package com.kuliza.lending.portal.pojo;

import java.util.List;
import java.util.Map;
import org.flowable.cmmn.api.runtime.CaseInstance;

public class CaseInstanceTaskMap {

  List<CaseInstance> caseInstanceList;
  Map<String, TaskDetails> taskMap;

  public List<CaseInstance> getCaseInstanceList() {
    return caseInstanceList;
  }

  public void setCaseInstanceList(
      List<CaseInstance> caseInstanceList) {
    this.caseInstanceList = caseInstanceList;
  }


  public Map<String, TaskDetails> getTaskMap() {
    return taskMap;
  }

  public void setTaskMap(
      Map<String, TaskDetails> taskMap) {
    this.taskMap = taskMap;
  }

  @Override
  public String toString() {
    return "CaseInstanceTaskMap{" +
        "caseInstanceList=" + caseInstanceList +
        ", taskMap=" + taskMap +
        '}';
  }
}
