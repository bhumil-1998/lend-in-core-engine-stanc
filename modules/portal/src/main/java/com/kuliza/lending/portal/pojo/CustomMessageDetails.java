package com.kuliza.lending.portal.pojo;

import java.io.Serializable;

import com.kuliza.lending.portal.utils.EnumConstants.Events;
import org.apache.kafka.common.protocol.types.Field;

public class CustomMessageDetails implements Serializable {

	/**
	 * CustomMessageDetails class carries the details that are fetched out from the incoming request
	 * This class acts as the data sent to RabbitMQ listener to consumed
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Events event;
	private String attributes;
	private String parameters;
	private String roleName;
	private String userId;
	private String requestId;
	private String url;
	private String uri;
	private String requestMethod;
	private String caseInstanceId;
	private String tabKey;
	private String requestBody;
	private String slug;
	private String transactionId;

	public Events getEvent() {
		return event;
	}

	public void setEvent(Events event) {
		this.event = event;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public String getTabKey() {
		return tabKey;
	}

	public void setTabKey(String tabKey) {
		this.tabKey = tabKey;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public CustomMessageDetails(Events event, String attributes, String parameters, String roleName,
			String userId, String url, String uri, String requestMethod, String caseInstanceId, String tabKey,
			String requestBody, String requestId, String slug, String transactionId) {
		super();
		this.event = event;
		this.attributes = attributes;
		this.parameters = parameters;
		this.roleName = roleName;
		this.userId = userId;
		this.url = url;
		this.uri = uri;
		this.requestMethod = requestMethod;
		this.caseInstanceId = caseInstanceId;
		this.tabKey = tabKey;
		this.requestBody = requestBody;
		this.requestId = requestId;
		this.slug = slug;
		this.transactionId = transactionId;
	}

	public CustomMessageDetails() {
		super();
	}

	@Override
	public String toString() {
		return "CustomMessageDetails{" +
				"event=" + event +
				", attributes='" + attributes + '\'' +
				", parameters='" + parameters + '\'' +
				", roleName='" + roleName + '\'' +
				", userId='" + userId + '\'' +
				", requestId='" + requestId + '\'' +
				", url='" + url + '\'' +
				", uri='" + uri + '\'' +
				", requestMethod='" + requestMethod + '\'' +
				", caseInstanceId='" + caseInstanceId + '\'' +
				", tabKey='" + tabKey + '\'' +
				", requestBody='" + requestBody + '\'' +
				", slug='" + slug + '\'' +
				", transactionId='" + transactionId + '\'' +
				'}';
	}
}
