package com.kuliza.lending.portal.pojo;

import java.util.List;
import javax.validation.constraints.NotNull;

public class PortalConfigurationPOJO {

  @NotNull(message = "Role name can not be null")
  private String roleName;

  @NotNull(message = "Role Label can not be null")
  private String roleLabel;

  private String productType;

  private String productLabel;

  private List<BucketConfiguration> bucketList;

  private List<ActionConfiguration> productActionList;

  private Object productVariableList;

  private String userId;

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleLabel() {
    return roleLabel;
  }

  public void setRoleLabel(String roleLabel) {
    this.roleLabel = roleLabel;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public String getProductLabel() {
    return productLabel;
  }

  public void setProductLabel(String productLabel) {
    this.productLabel = productLabel;
  }

  public List<BucketConfiguration> getBucketList() {
    return bucketList;
  }

  public void setBucketList(List<BucketConfiguration> bucketList) {
    this.bucketList = bucketList;
  }

  public List<ActionConfiguration> getProductActionList() {
    return productActionList;
  }

  public void setProductActionList(List<ActionConfiguration> productActionList) {
    this.productActionList = productActionList;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Object getProductVariableList() {
    return productVariableList;
  }

  public void setProductVariableList(Object productVariableList) {
    this.productVariableList = productVariableList;
  }


  @Override
  public String toString() {
    return "PortalConfigurationPOJO [roleName=" + roleName + ", roleLabel=" + roleLabel
        + ", productType=" + productType + ", productLabel="
        + productLabel + ", bucketList=" + bucketList + ", userId=" + userId + "]";
  }

}
