package com.kuliza.lending.portal.pojo;

import com.kuliza.lending.engine_common.pojo.CustomActionHeaders;
import com.kuliza.lending.engine_common.utils.Enums;
import java.util.List;

public class CustomURLMapperRequestBody {
  private String slug;

  private String url;

  private String portalType;

  private List<com.kuliza.lending.engine_common.pojo.CustomActionHeaders> headers;

  private Enums.REQUEST_TYPE requestType;

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getPortalType() {
    return portalType;
  }

  public void setPortalType(String portalType) {
    this.portalType = portalType;
  }

  public Enums.REQUEST_TYPE getRequestType() {
    return requestType;
  }

  public void setRequestType(Enums.REQUEST_TYPE requestType) {
    this.requestType = requestType;
  }

  public List<com.kuliza.lending.engine_common.pojo.CustomActionHeaders> getHeaders() {
    return headers;
  }

  public void setHeaders(List<CustomActionHeaders> headers) {
    this.headers = headers;
  }

  @Override
  public String toString() {
    return "CustomURLMapper{" +
        "slug='" + slug + '\'' +
        ", url='" + url + '\'' +
        ", portalType='" + portalType + '\'' +
        ", headers=" + headers +
        ", requestType=" + requestType +
        '}';
  }


}
