package com.kuliza.lending.portal.misc;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.kuliza.lending.portal.exceptions.AuditNotFoundException;
import com.kuliza.lending.portal.pojo.CustomMessageDetails;
import com.kuliza.lending.portal.pojo.CustomMessageVariables;
import com.kuliza.lending.portal.pojo.CustomUpdateMessage;
import com.kuliza.lending.portal.service.AuditService;

@Service
public class AuditEventHandler {

	@Autowired
	AuditService auditService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditEventHandler.class);

	public void handler(CustomMessageDetails customMessage) throws JSONException {
		if (customMessage != null && customMessage.getEvent() != null) {
			LOGGER.info("publish IncomingRequestEvent" + customMessage);
			auditService.saveAuditEntry(customMessage);
		}
	}

	@Retryable(
            value = {AuditNotFoundException.class},
            maxAttempts = 3, backoff = @Backoff(delay=2000,multiplier=2))
	public void handler(CustomUpdateMessage customMessage) throws AuditNotFoundException {
		if (customMessage != null && customMessage.getEvent() != null) {
			LOGGER.info("publish UpdateAuditEvent " + customMessage);
			auditService.updateAuditEntry(customMessage);
		}
	}
	
	@Retryable(
            value = {AuditNotFoundException.class},
            maxAttempts = 3, backoff = @Backoff(delay=2000,multiplier=2))
	public void handler(CustomMessageVariables customMessage) throws AuditNotFoundException {
		if (customMessage != null && customMessage.getEvent() != null) {
			LOGGER.info("publish VariableUpdate " + customMessage);
			auditService.updateOrCreateAuditEntry(customMessage);
		}
	}
}
