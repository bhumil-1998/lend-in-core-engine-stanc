package com.kuliza.lending.portal.pojo;

public class CustomBulkActionURLBody {
    private String caseInstanceId;
    private CustomActionURLBody customActionURLBody;

    public CustomBulkActionURLBody() {
        super();
    }

    public CustomBulkActionURLBody(long id, CustomActionURLBody customActionURLBody, String caseInstanceId) {
        this.customActionURLBody = customActionURLBody;
        this.caseInstanceId = caseInstanceId;
    }

    public String getCaseInstanceId() {
        return caseInstanceId;
    }

    public void setCaseInstanceId(String caseInstanceId) {
        this.caseInstanceId = caseInstanceId;
    }

    public CustomActionURLBody getCustomActionURLBody() {
        return customActionURLBody;
    }

    public void setCustomActionURLBody(CustomActionURLBody customActionURLBody) {
        this.customActionURLBody = customActionURLBody;
    }

    @Override
    public String toString() {
        return "CustomBulkActionURLBody{" +
                ", caseInstanceId='" + caseInstanceId + '\'' +
                ", customActionURLBody=" + customActionURLBody +
                '}';
    }
}
