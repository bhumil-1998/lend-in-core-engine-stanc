package com.kuliza.lending.portal.pojo;

public class PortalCommentRequestBody {

  private String message;

  private String commentType;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCommentType() {
    return commentType;
  }

  public void setCommentType(String commentType) {
    this.commentType = commentType;
  }

}
