package com.kuliza.lending.portal.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Configuration
public class AuditPointcuts {

	@Pointcut("within(com.kuliza.lending.portal.service.PortalService)")
	public void auditPortalServiceClass() {
	}

	@Pointcut("within(com.kuliza.lending.portal.controllers.PortalController)  && @within(org.springframework.web.bind.annotation.RestController)")
	public void portalRestControllers() {
	}
	
	@Pointcut("within(com.kuliza.lending.portal.controllers.CMMNController)  && @within(org.springframework.web.bind.annotation.RestController)")
	public void cmmnRestControllers() {
	}

	@Pointcut("within(com.kuliza.lending.engine_common.controller.CustomActionController)  && @within(org.springframework.web.bind.annotation.RestController)")
	public void engineCommonRestControllers() {
	}

	@Pointcut("within(com.kuliza.lending.authorization.controller.AuthorizationControllerAdvice)  && @within(org.springframework.web.bind.annotation.RestControllerAdvice)")
	public void authRestControllers() {
	}

	@Pointcut("execution(* com.kuliza.lending.engine_common.controller.CustomActionController.firePortalCustomAction(..))")
	public void customAPI() {
	}

	@Pointcut("execution(* com.kuliza.lending.engine_common.controller.CustomActionController.fireCustomAction(..))")
	public void customNewAPI() {
	}
	
	@Pointcut("execution(* com.kuliza.lending.portal.controllers.CMMNController.submitVariables(..))")
	public void submitVariablesAPI() {
	}
	
	@Pointcut("execution(* com.kuliza.lending.portal.controllers.CMMNController.submitForm(..))")
	public void submitFormAPI() {
	}

	@Pointcut("execution(* com.kuliza.lending.engine_common.controller.CustomActionController.firePortalBulkCustomActions(..))")
	public void BulkAPI() {
	}


	@Pointcut("(authRestControllers() || cmmnRestControllers() || portalRestControllers() || engineCommonRestControllers()) "
			+ "&& (submitVariablesAPI() || submitFormAPI() || customAPI() || BulkAPI() || customNewAPI())")
	public void combinedAuditAPIs() {
	}
	
	@Pointcut("execution(* com.kuliza.lending.engine_common.services.CustomActionService.getHttpResponse(..))")
	public void auditPortalServiceCustomMethod() {
	}
	
	@Pointcut("execution(* com.kuliza.lending.portal.service.CMMNService.submitOrGetCaseVariables(..))")
	public void setVariablesServiceCMMN() {
	}
	
	@Pointcut("execution(* com.kuliza.lending.portal.service.CMMNService.submitFormData(..))")
	public void submitFormService() {
		
	}

}
