package com.kuliza.lending.portal.utils;

import com.kuliza.lending.common.utils.Constants;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.flowable.form.model.FormField;
import org.flowable.form.model.Option;
import org.flowable.form.model.OptionFormField;

public class PortalValidation {

	private PortalValidation() {
		throw new UnsupportedOperationException();
	}

	public static Map<Object, Object> submitValidation(List<FormField> listOfFormProp,
			Map<String, Object> inputFormPropertiesMap, boolean isRequired) {
		HashSet<String> writeAbleFields = new HashSet<>();
		HashSet<String> allFields = new HashSet<>();
		Map<Object, Object> validationMap = new HashMap<>();
		validationMap.put(Constants.STATUS_KEY, true);
		for (FormField formField : listOfFormProp) {
			if (formField.isRequired() && isRequired && inputFormPropertiesMap.get(formField.getId()) == null) {
				validationMap.put(Constants.STATUS_KEY, false);
				validationMap.put(Constants.MESSAGE_KEY, formField.getId() + " is required field.");
				return validationMap;
			}
			if (formField.isRequired() && (formField.getType().equals("dropdown") || formField.getType().equals("radio-buttons"))) {
				OptionFormField field = (OptionFormField) formField;
				List<Option> options = field.getOptions();
				Map<String, Object> values = new HashMap<>();
				for (Option option : options) {
					if (option.getId() != null) {
						values.put(option.getId(), option.getName());
					}
				}
				if (values != null && !values.containsKey(inputFormPropertiesMap.get(formField.getId()))) {
					validationMap.put(Constants.STATUS_KEY, false);
					validationMap.put(Constants.MESSAGE_KEY, formField.getId() + " has invalid key.");
					return validationMap;
				}
			}
			if (!formField.isReadOnly()) {
				writeAbleFields.add(formField.getId());
			}

			allFields.add(formField.getId());
		}
		for (Map.Entry<String, Object> entry : inputFormPropertiesMap.entrySet()) {
			if (!allFields.contains(entry.getKey())) {
				validationMap.put(Constants.STATUS_KEY, false);
				validationMap.put(Constants.MESSAGE_KEY, entry.getKey() + " is extra field.");
				return validationMap;
			}
			if (!writeAbleFields.contains(entry.getKey())) {
				validationMap.put(Constants.STATUS_KEY, false);
				validationMap.put(Constants.MESSAGE_KEY, entry.getKey() + " is non-writeable field.");
				return validationMap;
			}
		}
		return validationMap;
	}
}
