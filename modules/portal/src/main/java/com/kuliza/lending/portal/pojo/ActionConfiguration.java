package com.kuliza.lending.portal.pojo;

import java.util.Map;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class ActionConfiguration {

  @NotNull(message = "id is a required key")
  @NotEmpty(message = "id cannot be empty")
  String id;

  @NotNull(message = "label is a required key")
  @NotEmpty(message = "label cannot be empty")
  String label;

  @NotNull(message = "params is a required key")
  @NotEmpty(message = "params cannot be empty")
  Map<String, Object> params;

  @NotNull(message = "iconKey is a required key")
  @NotEmpty(message = "iconKey cannot be empty")
  String iconKey;

  Boolean value;

  public String getId() {
    return id;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  public void setParams(Map<String, Object> params) {
    this.params = params;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }



  public String getIconKey() {
    return iconKey;
  }

  public void setIconKey(String iconKey) {
    this.iconKey = iconKey;
  }

  public Boolean getValue() {
    return value;
  }

  public void setValue(Boolean value) {
    this.value = value;
  }
}
