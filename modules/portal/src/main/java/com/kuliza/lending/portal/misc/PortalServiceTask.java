package com.kuliza.lending.portal.misc;

import static com.kuliza.lending.common.utils.Constants.NO_TASKS_MESSAGE;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.pojo.HTTPResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.configs.EditableTableQuery;
import com.kuliza.lending.engine_common.portal.PortalJourneyServiceTask;
import com.kuliza.lending.engine_common.services.EngineCommonService;
import com.kuliza.lending.portal.models.PortalUser;
import com.kuliza.lending.portal.models.PortalUserDao;
import com.kuliza.lending.portal.models.PortalUserSource;
import com.kuliza.lending.portal.models.PortalUserSourceDao;
import com.kuliza.lending.portal.service.CMMNService;
import com.kuliza.lending.portal.utils.PortalConstants;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("PortalServiceTask")
public class PortalServiceTask {

  @Autowired
  private PortalUserSourceDao portalUserSourceDao;

  @Autowired
  private PortalUserDao portalUserDao;

  @Autowired
  private CmmnRuntimeService cmmnRuntimeService;

  @Autowired
  private PortalJourneyServiceTask portalJourneyServiceTask;

  @Autowired
  private EngineCommonService engineCommonService;

  private static final Logger LOGGER = LoggerFactory.getLogger(PortalJourneyServiceTask.class);

    public Object fireJourney(String caseInstanceId, String destinationKey, String destinationType, Boolean closeTaskFlag, String waitingTaskList) throws IOException, InterruptedException {
      HTTPResponse response = null;
      if (caseInstanceId != null && !caseInstanceId.isEmpty()){
        PortalUserSource portalUserSource = portalUserSourceDao.findBySourceProcessNameAndSourceTypeAndPortalUserCaseInstanceId(destinationKey, destinationType, caseInstanceId);
        Map<String, Object> requestPayload = new HashMap<>();
        requestPayload.put("applicationId", portalUserSource.getPortalUser().getApplicationNumber());
        requestPayload.put("processInstanceId", portalUserSource.getSourceId());
        requestPayload.put("closeTask", closeTaskFlag);
        requestPayload.put("journeyType", destinationKey);
        requestPayload.put("variablesToSave", cmmnRuntimeService.getVariables(caseInstanceId));
        if (waitingTaskList != null && !waitingTaskList.isEmpty()) {
          requestPayload.put("taskList", Arrays.asList(waitingTaskList.split(",")));
        }
        JSONObject request = new JSONObject(requestPayload);
        response = portalJourneyServiceTask.sendJourneyRequest(request);
        LOGGER.info("fireJourney response : " + response.getResponse());
        if(response.getStatusCode() != 200){
          throw new InterruptedException();
        }
      }
      return response;
    }

  public Object portalToPortal(String caseInstanceId, String destinationKey, String destinationType, Boolean portfolioFlag) throws IOException, InterruptedException {
    PortalUser portalUser = portalUserDao.findByCaseInstanceIdAndIsDeleted(caseInstanceId, false);
    Map<String, Object> requestPayload = new HashMap<>();
    requestPayload.put("variablesToSave", cmmnRuntimeService.getVariables(caseInstanceId));
    requestPayload.put("portalType", destinationType);
    requestPayload.put("sourceProcessName", portalUser.getCaseName());
    requestPayload.put("sourceType", portalUser.getPortalType());
    requestPayload.put("sourceId", caseInstanceId);
    String queryParams = Constants.CASENAME + "="+destinationKey+"&" + Constants.APPLICATION_NUMBER + "=" +
            portalUser.getApplicationNumber() + "&" + Constants.PORTFOLIO_FLAG + "=" + portfolioFlag;
    JSONObject request = new JSONObject(requestPayload);

    HTTPResponse response = portalJourneyServiceTask.sendPortalRequest(request, queryParams);
    LOGGER.info("portalToPortal response : " + response.getResponse());
    if(response.getStatusCode() == 200 || (response.getStatusCode() == 400 && CommonHelperFunctions.getHashMapFromJsonString(response.getResponse()).getOrDefault("message", "").equals(NO_TASKS_MESSAGE))){
      return response;
    }
    throw new InterruptedException();
  }

  public Object getQueryResult(EditableTableQuery editableTableQuery, String caseInstanceId) throws IOException, InterruptedException{
    Object result = engineCommonService.getQueryResult(editableTableQuery, caseInstanceId);
    HTTPResponse response = null;
    if(result != null){
      response = new HTTPResponse();
      response.setStatusCode(200);
      response.setResponse(CommonHelperFunctions.getStringValue(result));
      return response;
    }
    throw new InterruptedException();
  }

//
//  public Object getQueryResult(EditableTableQuery editableTableQuery, String caseInstanceId) throws IOException, InterruptedException{
//    Object result = engineCommonService.getQueryResult(editableTableQuery, caseInstanceId);
//    HTTPResponse response = null;
//    if(result != null){
//      response = new HTTPResponse();
//      response.setStatusCode(200);
//      response.setResponse(CommonHelperFunctions.getStringValue(result));
//      return response;
//    }
//    throw new InterruptedException();
//  }
  
  public Object fireBackOfficeCaseJourney(String caseInstanceId, String initiator, String journeyKey) throws IOException, InterruptedException {
      HTTPResponse response = null;
      if (initiator ==  null || initiator.equals("")) {
    	  initiator = CommonHelperFunctions.getStringValue(cmmnRuntimeService.getVariable(caseInstanceId, CommonHelperFunctions.getStringValue(
    			  cmmnRuntimeService.getVariable(caseInstanceId, PortalConstants.ROLE_PROCESS_VARIABLE_KEY)) + PortalConstants.ROLE_ASSIGNEE_VALUE ));
      }
      if (caseInstanceId != null && !caseInstanceId.isEmpty()){
        Map<String, Object> requestPayload = new HashMap<>();
        requestPayload.put("initiator", initiator);
        requestPayload.put("journeyKey", journeyKey);
        requestPayload.put("variablesToSave", cmmnRuntimeService.getVariables(caseInstanceId));
        JSONObject request = new JSONObject(requestPayload);
        response = portalJourneyServiceTask.sendJourneyRequestfromPortal(request);
        LOGGER.info("fire Journey By Portal response : " + response.getResponse());
        if(response.getStatusCode() != 200){
        	throw new InterruptedException();
        }
      }
      return response;
    }
}
