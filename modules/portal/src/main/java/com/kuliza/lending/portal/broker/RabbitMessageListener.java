package com.kuliza.lending.portal.broker;

import java.io.IOException;

import org.apache.logging.log4j.ThreadContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.kuliza.lending.portal.utils.PortalConstants;

@Component
@EnableRabbit
@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ")
public class RabbitMessageListener {

	@Autowired
	private CommonListenerService commonListenerService;

	private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMessageListener.class);

	@RabbitListener(queues = PortalConstants.QUEUE_GENERIC_NAME)
	public void receiveMessageGeneric(final String message) {
		LOGGER.info("Received message by generic queue");
	}

	@RabbitListener(queues = PortalConstants.AUDIT_QUEUE_NAME)
	public void receiveMessage(final String message) {
		LOGGER.info("Received message as specific class: {}", message.toString());
	  commonListenerService.forwardMessageToService(message);
	}
}
