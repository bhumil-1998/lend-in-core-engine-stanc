package com.kuliza.lending.portal.models;

import com.kuliza.lending.common.model.BaseModel;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.sound.sampled.Port;

@Entity
@Table(name = "portal_user_source")
public class PortalUserSource extends BaseModel {

  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(referencedColumnName = "id")
  private PortalUser portalUser;

  @Column
  private String sourceId;

  @Column
  private String sourceType;

  @Column
  private String sourceProcessName;

  public PortalUser getPortalUser() {
    return portalUser;
  }

  public void setPortalUser(PortalUser portalUser) {
    this.portalUser = portalUser;
  }

  public String getSourceId() {
    return sourceId;
  }

  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }



  public String getSourceProcessName() {
    return sourceProcessName;
  }

  public void setSourceProcessName(String sourceProcessName) {
    this.sourceProcessName = sourceProcessName;
  }

  public PortalUserSource(){
    super();
    this.setIsDeleted(false);
  }

  public PortalUserSource(PortalUser portalUser, String sourceProcessName, String sourceType, String sourceId){
    this.setPortalUser(portalUser);
    this.setSourceProcessName(sourceProcessName);
    this.setSourceType(sourceType);
    this.setSourceId(sourceId);
    this.setIsDeleted(false);
  }
}
