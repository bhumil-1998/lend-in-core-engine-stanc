package com.kuliza.lending.portal.models;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.engine_common.models.CustomURLMapper;

import javax.persistence.*;

@Entity
public class PortalAudit extends BaseModel {
	@Column
	private String caseInstanceId;

	@Column
	private String tabKey;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(referencedColumnName = "id")
	private PortalUser portalUser;
	
	private String userId;
	
	@Column
	private String roleName;
	
	@Column
	private String requestURI;
	
	//In case of service task entries will be empty
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(referencedColumnName = "id")
	private  CustomURLMapper mapper;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(referencedColumnName = "id")
	private PortalCommentHistory portalCommentHistory;
	
	@Column(columnDefinition = "LONGTEXT")
	private String parameters;
	
	@Column(columnDefinition = "LONGTEXT")
	private String attributes;
	
	@Column(columnDefinition = "LONGTEXT")
	private String requestBody;

	@Column(columnDefinition = "LONGTEXT")
	private String responseBody;

	@Column(columnDefinition = "LONGTEXT")
	private String intermediateRequestBody;

	@Column(columnDefinition = "LONGTEXT")
	private String intermediateResponseBody;
	
	@Column(columnDefinition = "LONGTEXT")
	private String newProcessVariables;
	
	@Column(columnDefinition = "LONGTEXT")
	private String oldProcessVariables;
	
	@Column
	private int status;
	
	@Column(unique=true)
	private String requestId;

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public String getTabKey() {
		return tabKey;
	}

	public void setTabKey(String tabKey) {
		this.tabKey = tabKey;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public CustomURLMapper getMapper() {
		return mapper;
	}

	public void setMapper(CustomURLMapper mapper) {
		this.mapper = mapper;
	}

	public PortalCommentHistory getPortalCommentHistory() {
		return portalCommentHistory;
	}

	public void setPortalCommentHistory(PortalCommentHistory portalCommentHistory) {
		this.portalCommentHistory = portalCommentHistory;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	public String getIntermediateRequestBody() {
		return intermediateRequestBody;
	}

	public void setIntermediateRequestBody(String intermediateRequestBody) {
		this.intermediateRequestBody = intermediateRequestBody;
	}

	public String getIntermediateResponseBody() {
		return intermediateResponseBody;
	}

	public void setIntermediateResponseBody(String intermediateResponseBody) {
		this.intermediateResponseBody = intermediateResponseBody;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public PortalUser getPortalUser() {
		return portalUser;
	}

	public void setPortalUser(PortalUser portalUser) {
		this.portalUser = portalUser;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNewProcessVariables() {
		return newProcessVariables;
	}

	public void setNewProcessVariables(String newProcessVariables) {
		this.newProcessVariables = newProcessVariables;
	}

	public String getOldProcessVariables() {
		return oldProcessVariables;
	}

	public void setOldProcessVariables(String oldProcessVariables) {
		this.oldProcessVariables = oldProcessVariables;
	}

	public PortalAudit() {
		super();
		this.setIsDeleted(false);
	}

	public PortalAudit(String caseInstanceId, String tabKey, String userId, String roleName,
			CustomURLMapper mapperId, String requestURI, PortalCommentHistory portalCommentHistory, String requestBody,
			String requestId, String parameters, String attributes, PortalUser portalUser) {
		super();
		this.caseInstanceId = caseInstanceId;
		this.tabKey = tabKey;
		this.userId = userId;
		this.roleName = roleName;
		this.mapper = mapperId;
		this.portalCommentHistory = portalCommentHistory;
		this.requestBody = requestBody;
		this.requestId = requestId;
		this.requestURI = requestURI;
		this.parameters = parameters;
		this.attributes = attributes;
		this.portalUser = portalUser;
		this.status = 1; // Can be moved to enum
		this.setIsDeleted(false);
	}

  @Override
  public String toString() {
    return "PortalAudit [caseInstanceId=" + caseInstanceId + ", tabKey=" + tabKey + ", portalUser="
        + portalUser + ", userId=" + userId + ", roleName=" + roleName + ", requestURI="
        + requestURI + ", mapper=" + mapper + ", portalCommentHistory=" + portalCommentHistory
        + ", parameters=" + parameters + ", attributes=" + attributes + ", requestBody="
        + requestBody + ", responseBody=" + responseBody + ", intermediateRequestBody="
        + intermediateRequestBody + ", intermediateResponseBody=" + intermediateResponseBody
        + ", newProcessVariables=" + newProcessVariables + ", oldProcessVariables="
        + oldProcessVariables + ", status=" + status + ", requestId=" + requestId + "]";
  }
	
	
	
}
