package com.kuliza.lending.portal.utils;

public class SwaggerConstants {

    public static final String PC_SET_COMMENT_RESPONSE = "{\n" +
            "\t\"status\": 200,\n" +
            "\t\"message\": \"SUCCESS!\",\n" +
            "\t\"data\": {\n" +
            "\t\t\"id\": 1,\n" +
            "\t\t\"portalUser\": {\n" +
            "\t\t\t\"id\": 8,\n" +
            "\t\t\t\"caseInstanceId\": \"5cfd4e36-5a18-11e9-880c-02426ed2fcfa\",\n" +
            "\t\t\t\"applicationNumber\": \"279\",\n" +
            "\t\t\t\"portalType\": \"BO\",\n" +
            "\t\t\t\"caseName\": \"portalPOCV2\"\n" +
            "\t\t},\n" +
            "\t\t\"userId\": \"user@168.com\",\n" +
            "\t\t\"roleName\": \"user\",\n" +
            "\t\t\"taskKey\": \"personalDetailScreen\",\n" +
            "\t\t\"message\": \"testing purpose 1\"\n" +
            "\t}\n" +
            "}";

    public static final String PC_SET_COMMENT_RESPONSE_NULL_TAB_KEY = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"id\": 2,\n" +
            "        \"portalUser\": {\n" +
            "            \"id\": 8,\n" +
            "            \"caseInstanceId\": \"5cfd4e36-5a18-11e9-880c-02426ed2fcfa\",\n" +
            "            \"applicationNumber\": \"279\",\n" +
            "            \"portalType\": \"BO\",\n" +
            "            \"caseName\": \"portalPOCV2\"\n" +
            "        },\n" +
            "        \"userId\": \"user@168.com\",\n" +
            "        \"roleName\": \"user\",\n" +
            "        \"taskKey\": null,\n" +
            "        \"message\": \"testing purpose 1\"\n" +
            "    }\n" +
            "}";

    public static final String PC_GET_COMMENT_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 1,\n" +
            "            \"portalUser\": {\n" +
            "                \"id\": 8,\n" +
            "                \"caseInstanceId\": \"5cfd4e36-5a18-11e9-880c-02426ed2fcfa\",\n" +
            "                \"applicationNumber\": \"279\",\n" +
            "                \"portalType\": \"BO\",\n" +
            "                \"caseName\": \"portalPOCV2\"\n" +
            "            },\n" +
            "            \"userId\": \"user@168.com\",\n" +
            "            \"roleName\": \"user\",\n" +
            "            \"taskKey\": \"personalDetailScreen\",\n" +
            "            \"message\": \"testing purpose 1\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String PC_GET_COMMENT_RESPONSE_NULL_TAB_KEY = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 1,\n" +
            "            \"portalUser\": {\n" +
            "                \"id\": 8,\n" +
            "                \"caseInstanceId\": \"5cfd4e36-5a18-11e9-880c-02426ed2fcfa\",\n" +
            "                \"applicationNumber\": \"279\",\n" +
            "                \"portalType\": \"BO\",\n" +
            "                \"caseName\": \"portalPOCV2\"\n" +
            "            },\n" +
            "            \"userId\": \"user@168.com\",\n" +
            "            \"roleName\": \"user\",\n" +
            "            \"taskKey\": \"personalDetailScreen\",\n" +
            "            \"message\": \"testing purpose 1\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2,\n" +
            "            \"portalUser\": {\n" +
            "                \"id\": 8,\n" +
            "                \"caseInstanceId\": \"5cfd4e36-5a18-11e9-880c-02426ed2fcfa\",\n" +
            "                \"applicationNumber\": \"279\",\n" +
            "                \"portalType\": \"BO\",\n" +
            "                \"caseName\": \"portalPOCV2\"\n" +
            "            },\n" +
            "            \"userId\": \"user@168.com\",\n" +
            "            \"roleName\": \"user\",\n" +
            "            \"taskKey\": null,\n" +
            "            \"message\": \"testing purpose 1\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String PC_GET_PORTAL_CONFIG = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"id\": 7,\n" +
            "        \"roleName\": \"cmmnCreditAnalystPOCAdmin\",\n" +
            "        \"productType\": \"kulizaPersonalLoan\",\n" +
            "        \"bucketList\": [\n" +
            "            {\n" +
            "                \"id\": \"pending\",\n" +
            "                \"label\": \"PENDING\",\n" +
            "                \"editable\": false,\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"id\": \"journeyType\",\n" +
            "                        \"label\": \"journeyType\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"applicationId\",\n" +
            "                        \"label\": \"applicationId\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\",\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ]\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"approved\",\n" +
            "                \"label\": \"APPROVED\",\n" +
            "                \"editable\": false,\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\",\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ]\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"history\",\n" +
            "                \"label\": \"HISTORY\",\n" +
            "                \"editable\": false,\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\",\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ]\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"claim\",\n" +
            "                \"label\": \"CLAIM\",\n" +
            "                \"editable\": false,\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\",\n" +
            "                        \"actionConfigurationList\": []\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\",\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ]\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ]\n" +
            "            }\n" +
            "        ],\n" +
            "        \"userId\": \"cmmnuser@kuliza.com\"\n" +
            "    }\n" +
            "}";

    public static final String PC_GET_PRODUCT_CONFIG = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 7,\n" +
            "            \"roleName\": \"cmmnCreditAnalystPOCAdmin\",\n" +
            "            \"productType\": \"kulizaPersonalLoan\",\n" +
            "            \"bucketList\": [\n" +
            "                {\n" +
            "                    \"id\": \"pending\",\n" +
            "                    \"label\": \"PENDING\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"journeyType\",\n" +
            "                            \"label\": \"journeyType\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"applicationId\",\n" +
            "                            \"label\": \"applicationId\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"id\": \"approved\",\n" +
            "                    \"label\": \"APPROVED\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"v1\",\n" +
            "                            \"label\": \"V1\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v2\",\n" +
            "                            \"label\": \"V2\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"id\": \"history\",\n" +
            "                    \"label\": \"HISTORY\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"v1\",\n" +
            "                            \"label\": \"V1\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v2\",\n" +
            "                            \"label\": \"V2\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"id\": \"claim\",\n" +
            "                    \"label\": \"CLAIM\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"v1\",\n" +
            "                            \"label\": \"V1\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v2\",\n" +
            "                            \"label\": \"V2\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                }\n" +
            "            ],\n" +
            "            \"userId\": \"cmmnuser@kuliza.com\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 8,\n" +
            "            \"roleName\": \"cmmnCreditAnalystPOCAdmin\",\n" +
            "            \"productType\": \"personalLoan\",\n" +
            "            \"bucketList\": [\n" +
            "                {\n" +
            "                    \"id\": \"pending\",\n" +
            "                    \"label\": \"PENDING\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"journeyType\",\n" +
            "                            \"label\": \"journeyType\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"applicationId\",\n" +
            "                            \"label\": \"applicationId\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"id\": \"approved\",\n" +
            "                    \"label\": \"APPROVED\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"v1\",\n" +
            "                            \"label\": \"V1\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v2\",\n" +
            "                            \"label\": \"V2\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"id\": \"history\",\n" +
            "                    \"label\": \"HISTORY\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"v1\",\n" +
            "                            \"label\": \"V1\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v2\",\n" +
            "                            \"label\": \"V2\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"id\": \"claim\",\n" +
            "                    \"label\": \"CLAIM\",\n" +
            "                    \"editable\": false,\n" +
            "                    \"variables\": [\n" +
            "                        {\n" +
            "                            \"id\": \"v1\",\n" +
            "                            \"label\": \"V1\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v2\",\n" +
            "                            \"label\": \"V2\",\n" +
            "                            \"type\": \"String\",\n" +
            "                            \"actionConfigurationList\": []\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"v3\",\n" +
            "                            \"label\": \"V3\",\n" +
            "                            \"type\": \"listOfAction\",\n" +
            "                            \"actionConfigurationList\": [\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"DELETE\",\n" +
            "                                    \"value\": false\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"id\": \"ca1\",\n" +
            "                                    \"label\": \"CA1\",\n" +
            "                                    \"meta\": {},\n" +
            "                                    \"iconKey\": \"EDIT\",\n" +
            "                                    \"value\": false\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"bucketActions\": [\n" +
            "                        {\n" +
            "                            \"id\": \"ba1\",\n" +
            "                            \"label\": \"BA1\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"DELETE\",\n" +
            "                            \"value\": false\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"id\": \"ba2\",\n" +
            "                            \"label\": \"BA2\",\n" +
            "                            \"meta\": {},\n" +
            "                            \"iconKey\": \"ADD\",\n" +
            "                            \"value\": false\n" +
            "                        }\n" +
            "                    ]\n" +
            "                }\n" +
            "            ],\n" +
            "            \"userId\": \"cmmnuser@kuliza.com\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String PC_ADD_BUCKET_CONFIG = "{\n" +
            "    \"data\": {\n" +
            "        \"bucketList\": [\n" +
            "            {\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"editable\": false,\n" +
            "                \"id\": \"pending\",\n" +
            "                \"label\": \"PENDING\",\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ],\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"editable\": false,\n" +
            "                \"id\": \"approved\",\n" +
            "                \"label\": \"APPROVED\",\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ],\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"editable\": false,\n" +
            "                \"id\": \"history\",\n" +
            "                \"label\": \"HISTORY\",\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ],\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"editable\": false,\n" +
            "                \"id\": \"claim\",\n" +
            "                \"label\": \"CLAIM\",\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ],\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"id\": \"ba31\",\n" +
            "                        \"label\": \"BA31\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"id\": \"bae2\",\n" +
            "                        \"label\": \"BAE2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": false\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"editable\": false,\n" +
            "                \"id\": \"pending\",\n" +
            "                \"label\": \"PENDING\",\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"vw1\",\n" +
            "                        \"label\": \"VW1\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [],\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"id\": \"ca31\",\n" +
            "                                \"label\": \"CA31\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"id\": \"ca13\",\n" +
            "                                \"label\": \"CA13\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": false\n" +
            "                            }\n" +
            "                        ],\n" +
            "                        \"id\": \"v33\",\n" +
            "                        \"label\": \"V33\",\n" +
            "                        \"type\": \"listOfAction\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            }\n" +
            "        ],\n" +
            "        \"id\": 4,\n" +
            "        \"productType\": \"personalLoan\",\n" +
            "        \"roleName\": \"cmmnCreditAnalyst\",\n" +
            "        \"userId\": \"user@168.com\"\n" +
            "    },\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"status\": 200\n" +
            "}\n";

    public static final String PC_UPDATE_PRODUCT_CONFIG = "{\n" +
            "    \"data\": {\n" +
            "        \"bucketList\": [\n" +
            "            {\n" +
            "                \"bucketActions\": [\n" +
            "                    {\n" +
            "                        \"iconKey\": \"DELETE\",\n" +
            "                        \"id\": \"ba1\",\n" +
            "                        \"label\": \"BA1\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": null\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"iconKey\": \"ADD\",\n" +
            "                        \"id\": \"ba2\",\n" +
            "                        \"label\": \"BA2\",\n" +
            "                        \"meta\": {},\n" +
            "                        \"value\": null\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"editable\": false,\n" +
            "                \"id\": \"claim\",\n" +
            "                \"label\": \"CLAIM\",\n" +
            "                \"variables\": [\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": null,\n" +
            "                        \"id\": \"v1\",\n" +
            "                        \"label\": \"V1\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": null,\n" +
            "                        \"id\": \"v2\",\n" +
            "                        \"label\": \"V2\",\n" +
            "                        \"type\": \"String\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"actionConfigurationList\": [\n" +
            "                            {\n" +
            "                                \"iconKey\": \"DELETE\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": null\n" +
            "                            },\n" +
            "                            {\n" +
            "                                \"iconKey\": \"EDIT\",\n" +
            "                                \"id\": \"ca1\",\n" +
            "                                \"label\": \"CA1\",\n" +
            "                                \"meta\": {},\n" +
            "                                \"value\": null\n" +
            "                            }\n" +
            "                        ],\n" +
            "                        \"id\": \"v3\",\n" +
            "                        \"label\": \"V3\",\n" +
            "                        \"type\": \"listOfAction\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            }\n" +
            "        ],\n" +
            "        \"id\": 5,\n" +
            "        \"productType\": \"personalLoan\",\n" +
            "        \"roleName\": \"cmmnCreditAnalystPOCAdmin\",\n" +
            "        \"userId\": \"user@168.com\"\n" +
            "    },\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"status\": 200\n" +
            "}\n";

    public static final String PC_DELETE_PORTAL_CONFIG = "{\n" +
            "    \"data\": null,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"status\": 200\n" +
            "}\n";

    public static final String PC_GET_TAB_CONFIG = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": \"Address Details\",\n" +
            "            \"label\": \"Address Details\",\n" +
            "            \"childPresent\": true,\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"id\": \"professionalAddressDetail\",\n" +
            "                    \"label\": \"Professional Address\",\n" +
            "                    \"childPresent\": false,\n" +
            "                    \"children\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"id\": \"personalAddressDetail\",\n" +
            "                    \"label\": \"Personal Address\",\n" +
            "                    \"childPresent\": false,\n" +
            "                    \"children\": null\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": \"cmmnCreditAnalystPOCSummary\",\n" +
            "            \"label\": \"Summary\",\n" +
            "            \"childPresent\": false,\n" +
            "            \"children\": null\n" +
            "        }\n" +
            "    ]\n" +
            "}\n";

    public static final String PC_GET_ASSIGNED_LIST = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"OK\",\n" +
            "    \"data\": {\n" +
            "        \"meta\": {\n" +
            "            \"showPagination\": true,\n" +
            "            \"totalCount\": 1\n" +
            "        },\n" +
            "        \"history\": [\n" +
            "            {\n" +
            "                \"bucket\": \"history\",\n" +
            "                \"ba2\": null,\n" +
            "                \"ca1\": null,\n" +
            "                \"ba1\": null,\n" +
            "                \"caseInstanceId\": \"635eb2ea-60ad-11e9-a5bd-42010aa0001b\",\n" +
            "                \"portalId\": \"342\",\n" +
            "                \"roleName\": \"cmmnCreditAnalystPOC\",\n" +
            "                \"v1\": null,\n" +
            "                \"assignee\": \"cmmnuser@kuliza.com\",\n" +
            "                \"v2\": null,\n" +
            "                \"productType\": \"kulizaPersonalLoan\"\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}\n";

    public static final String PC_GET_CARD_CONFIG = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"data\": null,\n" +
            "        \"caseInstanceId\": \"635eb2ea-60ad-11e9-a5bd-42010aa0001b\",\n" +
            "        \"professionalAddressDetail\": {\n" +
            "            \"proofOfIdentity\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Proof of Identity\",\n" +
            "                \"id\": \"proofOfIdentity\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 1,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"pan\",\n" +
            "                        \"name\": \"PAN Card\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"dl\",\n" +
            "                        \"name\": \"Driving License\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"aadhaar\",\n" +
            "                        \"name\": \"Aadhaar Card\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"passport\",\n" +
            "                        \"name\": \"Passport\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"legalExistenceProofUpload\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Upload Legal Existence Proof\",\n" +
            "                \"id\": \"legalExistenceProofUpload\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"upload\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 6,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": {\n" +
            "                            \"url\": \"http://35.200.244.172:8001/api/documents/documents/\",\n" +
            "                            \"method\": \"POST\",\n" +
            "                            \"data\": {\n" +
            "                                \"document_type\": 1\n" +
            "                            },\n" +
            "                            \"type\": \"fileUpload\"\n" +
            "                        },\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"proofOfAddress\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Proof of Address\",\n" +
            "                \"id\": \"proofOfAddress\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 3,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"aadhaar\",\n" +
            "                        \"name\": \"Aadhaar\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"rationCard\",\n" +
            "                        \"name\": \"Ration Card\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"electricityBill\",\n" +
            "                        \"name\": \"Electricity Bill\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"passport\",\n" +
            "                        \"name\": \"Passport\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"proofOfAddessUpload\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Upload Proof of Address\",\n" +
            "                \"id\": \"proofOfAddessUpload\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"upload\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 4,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": {\n" +
            "                            \"url\": \"http://35.200.244.172:8001/api/documents/documents/\",\n" +
            "                            \"method\": \"POST\",\n" +
            "                            \"data\": {\n" +
            "                                \"document_type\": 1\n" +
            "                            },\n" +
            "                            \"type\": \"fileUpload\"\n" +
            "                        },\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"proofOfIdentityUpload\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Upload Proof of Identity\",\n" +
            "                \"id\": \"proofOfIdentityUpload\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"upload\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 2,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": {\n" +
            "                            \"url\": \"http://35.200.244.172:8001/api/documents/documents/\",\n" +
            "                            \"method\": \"POST\",\n" +
            "                            \"data\": {\n" +
            "                                \"document_type\": 1\n" +
            "                            },\n" +
            "                            \"type\": \"fileUpload\"\n" +
            "                        },\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"legalExistenceProof\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Legal Existence Proof\",\n" +
            "                \"id\": \"legalExistenceProof\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 5,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"incorporation\",\n" +
            "                        \"name\": \"Certificate of Incorporation\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"serviceTax\",\n" +
            "                        \"name\": \"Service Tax Regn Certificate\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"shopandestablishment\",\n" +
            "                        \"name\": \"Shop and Establishment Certificate\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"tradeCertificate\",\n" +
            "                        \"name\": \"Trade Certificate\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"businessAddressProof\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Business Address Proof\",\n" +
            "                \"id\": \"businessAddressProof\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 7,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"electricityBill\",\n" +
            "                        \"name\": \"Electricity Bill\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"purchaseAgreement\",\n" +
            "                        \"name\": \"Purchase Agreement\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"taxreceipt\",\n" +
            "                        \"name\": \"Property Tax Receipt\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"businessAddressProofUpload\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Upload Business Address Proof\",\n" +
            "                \"id\": \"businessAddressProofUpload\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"upload\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 8,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": {\n" +
            "                            \"url\": \"http://35.200.244.172:8001/api/documents/documents/\",\n" +
            "                            \"method\": \"POST\",\n" +
            "                            \"data\": {\n" +
            "                                \"document_type\": 1\n" +
            "                            },\n" +
            "                            \"type\": \"fileUpload\"\n" +
            "                        },\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            }\n" +
            "        },\n" +
            "        \"comment\": null,\n" +
            "        \"stepperData\": null,\n" +
            "        \"taskId\": \"6364a678-60ad-11e9-a5bd-42010aa0001b\",\n" +
            "        \"gloablVariables\": {\n" +
            "            \"permanentaddressline2\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Address Line 2\",\n" +
            "                \"id\": \"permanentaddressline2\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 15,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 6,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"officeState\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"State\",\n" +
            "                \"id\": \"officeState\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 44,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"addressCheck\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Is Current Address same as Permanent Address\",\n" +
            "                \"id\": \"addressCheck\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"boolean\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"checkbox\",\n" +
            "                        \"default\": true,\n" +
            "                        \"order\": 19,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 12\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"directorsPan\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Borrower's PAN\",\n" +
            "                \"id\": \"directorsPan\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"rest\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 2,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"api\": {\n" +
            "                            \"url\": \"https://3171tk50lc.execute-api.ap-southeast-1.amazonaws.com:443/pilot/nsdl/verify_pan/?format=json\",\n" +
            "                            \"method\": \"POST\",\n" +
            "                            \"data\": {\n" +
            "                                \"pan_id\": \"directorsPan\",\n" +
            "                                \"customer_id\": \"\",\n" +
            "                                \"process_instance_id\": \"\",\n" +
            "                                \"mobile\": \"borrowerMobileNo\"\n" +
            "                            },\n" +
            "                            \"mapping\": {\n" +
            "                                \"borrowerFirstName\": \"data.result.First-Name\",\n" +
            "                                \"borrowerLastName\": \"data.result.Last-Name\",\n" +
            "                                \"borrowerMiddleName\": \"data.result.Middle-Name\"\n" +
            "                            }\n" +
            "                        },\n" +
            "                        \"validator\": \"[\\\"pan\\\"]\"\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"permanentpincode\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Pin Code\",\n" +
            "                \"id\": \"permanentpincode\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"integer\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"number\",\n" +
            "                        \"form_type\": \"number\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 16,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[\\\"length\\\"]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": true,\n" +
            "                        \"minLength\": 6,\n" +
            "                        \"maxLength\": 6\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"businessStartDate\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Business Start Date\",\n" +
            "                \"id\": \"businessStartDate\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"date\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"datepicker\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 32,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"minAge\": -19,\n" +
            "                        \"newRow\": false,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"uploadAddressProof\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Upload Address Proof\",\n" +
            "                \"id\": \"uploadAddressProof\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 25,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"aadhaarCard\",\n" +
            "                        \"name\": \"Aadhaar Card\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"voterID\",\n" +
            "                        \"name\": \"Voter ID\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"passport\",\n" +
            "                        \"name\": \"Passport\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"drivingLicense\",\n" +
            "                        \"name\": \"Driving License\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"businessName\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Business Name\",\n" +
            "                \"id\": \"businessName\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 33,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"addressDetails\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Address Details\",\n" +
            "                \"id\": \"addressDetails\",\n" +
            "                \"isWritable\": false,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"accordion\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 12,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": true,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 12,\n" +
            "                        \"children\": [\n" +
            "                            \"permanentAddress\",\n" +
            "                            \"permanentaddressline1\",\n" +
            "                            \"permanentaddressline2\",\n" +
            "                            \"permanentpincode\",\n" +
            "                            \"permanentCity\",\n" +
            "                            \"permanentState\",\n" +
            "                            \"addressCheck\",\n" +
            "                            \"uploadAddressProof\",\n" +
            "                            \"idNumber\",\n" +
            "                            \"documentUpload\",\n" +
            "                            \"noOfYearsAtCurrentAddress\",\n" +
            "                            \"residenceType\"\n" +
            "                        ]\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"industry\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Industry\",\n" +
            "                \"id\": \"industry\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 34,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerFatherName\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Father's Name\",\n" +
            "                \"id\": \"borrowerFatherName\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 9,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"permanentaddressline1\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Address Line 1\",\n" +
            "                \"id\": \"permanentaddressline1\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 14,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 6,\n" +
            "                        \"newRow\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"idNumber\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"ID Number\",\n" +
            "                \"id\": \"idNumber\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 26,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"documentUpload\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Upload\",\n" +
            "                \"id\": \"documentUpload\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"upload\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 27,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": {\n" +
            "                            \"url\": \"http://35.200.244.172:8001/api/documents/documents/\",\n" +
            "                            \"method\": \"POST\",\n" +
            "                            \"data\": {\n" +
            "                                \"document_type\": 1\n" +
            "                            },\n" +
            "                            \"type\": \"fileUpload\"\n" +
            "                        },\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"residenceType\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Residence Type\",\n" +
            "                \"id\": \"residenceType\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 29,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": false,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"rented\",\n" +
            "                        \"name\": \"Rented\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"owned\",\n" +
            "                        \"name\": \"Owned\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"ownedbyfamily\",\n" +
            "                        \"name\": \"Owned by Family\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"leased\",\n" +
            "                        \"name\": \"Leased\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"basicInformation\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Basic Information\",\n" +
            "                \"id\": \"basicInformation\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"accordion\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 1,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": true,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 12,\n" +
            "                        \"children\": [\n" +
            "                            \"directorsPan\",\n" +
            "                            \"borrowerFirstName\",\n" +
            "                            \"borrowerMiddleName\",\n" +
            "                            \"borrowerLastName\",\n" +
            "                            \"borrowerDob\",\n" +
            "                            \"borrowerMaritalStatus\",\n" +
            "                            \"borrowerFatherName\",\n" +
            "                            \"borrowerMotherName\",\n" +
            "                            \"borrowerGender\",\n" +
            "                            \"borrowerCitizenship\"\n" +
            "                        ]\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerGender\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Gender\",\n" +
            "                \"id\": \"borrowerGender\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 11,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"male\",\n" +
            "                        \"name\": \"Male\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"female\",\n" +
            "                        \"name\": \"Female\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"permanentAddress\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Permanent Address\",\n" +
            "                \"id\": \"permanentAddress\",\n" +
            "                \"isWritable\": false,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"heading\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 13,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": true,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 12\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"noOfYearsAtCurrentAddress\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"No of years at Current Address\",\n" +
            "                \"id\": \"noOfYearsAtCurrentAddress\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"number\",\n" +
            "                        \"form_type\": \"number\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 28,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"typeOfBusiness\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Type of Business\",\n" +
            "                \"id\": \"typeOfBusiness\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 36,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": false,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"trader\",\n" +
            "                        \"name\": \"Trader\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"manufacturer\",\n" +
            "                        \"name\": \"Manufacturer\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"businessConstitution\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Business Constitution\",\n" +
            "                \"id\": \"businessConstitution\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 37,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": false,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"proprietorship\",\n" +
            "                        \"name\": \"Proprietorship\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"company\",\n" +
            "                        \"name\": \"Company\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"partnership\",\n" +
            "                        \"name\": \"Partnership\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"llp\",\n" +
            "                        \"name\": \"LLP\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"firmPan\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Firm's PAN\",\n" +
            "                \"id\": \"firmPan\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 31,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[\\\"length\\\"]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": true,\n" +
            "                        \"minLength\": 10,\n" +
            "                        \"maxLength\": 10\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"officeCity\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"City\",\n" +
            "                \"id\": \"officeCity\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 42,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerCitizenship\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Citizenship\",\n" +
            "                \"id\": \"borrowerCitizenship\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 11,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": false,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"indian\",\n" +
            "                        \"name\": \"Indian\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"nri\",\n" +
            "                        \"name\": \"Non-resident Indian\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"businessDetails\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Business Details\",\n" +
            "                \"id\": \"businessDetails\",\n" +
            "                \"isWritable\": false,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"accordion\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 30,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": true,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 12,\n" +
            "                        \"children\": [\n" +
            "                            \"firmPan\",\n" +
            "                            \"businessStartDate\",\n" +
            "                            \"businessName\",\n" +
            "                            \"industry\",\n" +
            "                            \"typeOfBusiness\",\n" +
            "                            \"businessConstitution\",\n" +
            "                            \"officeAddress\",\n" +
            "                            \"officeAddressLine1\",\n" +
            "                            \"officeAddressLine2\",\n" +
            "                            \"officePinCode\",\n" +
            "                            \"officeCity\",\n" +
            "                            \"officeState\"\n" +
            "                        ]\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"permanentState\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"State\",\n" +
            "                \"id\": \"permanentState\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 18,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"officeAddress\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Office Address\",\n" +
            "                \"id\": \"officeAddress\",\n" +
            "                \"isWritable\": false,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"heading\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 40,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": true,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 12\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerMotherName\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Mother's Name\",\n" +
            "                \"id\": \"borrowerMotherName\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 10,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerMiddleName\": {\n" +
            "                \"isRequired\": false,\n" +
            "                \"name\": \"Middle Name\",\n" +
            "                \"id\": \"borrowerMiddleName\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 4,\n" +
            "                        \"required\": false,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerMaritalStatus\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Marital Status\",\n" +
            "                \"id\": \"borrowerMaritalStatus\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"dropdown\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"dropdown\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 7,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": false,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": [\n" +
            "                    {\n" +
            "                        \"id\": \"married\",\n" +
            "                        \"name\": \"Married\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"unmarried\",\n" +
            "                        \"name\": \"Unmarried\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"separated\",\n" +
            "                        \"name\": \"Separated\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"id\": \"divorced\",\n" +
            "                        \"name\": \"Divorced\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"borrowerFirstName\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"First Name\",\n" +
            "                \"id\": \"borrowerFirstName\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 3,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"officePinCode\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Pin Code\",\n" +
            "                \"id\": \"officePinCode\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"integer\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"number\",\n" +
            "                        \"form_type\": \"number\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 43,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[\\\"length\\\"]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": true,\n" +
            "                        \"minLength\": 6,\n" +
            "                        \"maxLength\": 6\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"officeAddressLine2\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Office Address Line 2\",\n" +
            "                \"id\": \"officeAddressLine2\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 41,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 6,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerLastName\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Last Name\",\n" +
            "                \"id\": \"borrowerLastName\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 5,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"borrowerDob\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Date of Birth\",\n" +
            "                \"id\": \"borrowerDob\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"date\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"datepicker\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 6,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[\\\"dob\\\"]\",\n" +
            "                        \"minAge\": 0,\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"permanentCity\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"City\",\n" +
            "                \"id\": \"permanentCity\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 17,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 4,\n" +
            "                        \"newRow\": false\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            },\n" +
            "            \"officeAddressLine1\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Office Address Line 1\",\n" +
            "                \"id\": \"officeAddressLine1\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 40,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": false,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"colWidth\": 6,\n" +
            "                        \"newRow\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            }\n" +
            "        }\n" +
            "    },\n" +
            "    \"screenInfo\": \"professionalAddressDetail\",\n" +
            "    \"screenName\": \"Address Details -  Professional Address\",\n" +
            "    \"errorMessage\": \"\"\n" +
            "}\n";

    public static final String PC_FIRE_CUSTOM_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 1,\n" +
            "            \"method\": \"http\",\n" +
            "            \"userId\": \"cmmnuser@kuliza.com\",\n" +
            "            \"openApi\": false,\n" +
            "            \"url\": \"http://localhost:8080/lending/portal/configurator/roleName/productType\",\n" +
            "            \"portalType\": \"localhost\",\n" +
            "            \"headers\": [\n" +
            "                {\n" +
            "                    \"key\": \"Content-Type\",\n" +
            "                    \"value\": \"application/json\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"key\": \"Authorization\",\n" +
            "                    \"value\": \"Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBNVFJLUVsYmFGV1d1YXZleGpFVEtFRUROQ2JvM2hkUVREYm9iSFlndldNIn0.eyJqdGkiOiJlNzQ1ZmRlZC1mMjAyLTRlOTMtYmU0Ny1jNTAwNzBjZjZhMzciLCJleHAiOjE1NTY4MjI2OTcsIm5iZiI6MCwiaWF0IjoxNTU1NTA1MDk3LCJpc3MiOiJodHRwOi8vMzUuMjAwLjE4OC4xMjE6ODAwNS9hdXRoL3JlYWxtcy9rdWxpemFfY2UiLCJhdWQiOiJsZW5kaW5nIiwic3ViIjoiYmM0ZjYyMmUtMGExNi00YzY4LWJmNjQtZDE1MjgxZjViMWFlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibGVuZGluZyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjIzYTFiZGEyLWVmOTAtNDEyMS1hMmNlLWY2N2VkNDA1ZDI3ZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiY21tbkNyZWRpdEFuYWx5c3RQT0NTVUIxIiwiY21tbkNyZWRpdEFuYWx5c3RQT0NBZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsibGVuZGluZyI6eyJyb2xlcyI6WyJjbW1uQ3JlZGl0QW5hbHlzdCJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwibmFtZSI6ImNtbW4gdXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6ImNtbW51c2VyIiwiZ2l2ZW5fbmFtZSI6ImNtbW4iLCJmYW1pbHlfbmFtZSI6InVzZXIiLCJlbWFpbCI6ImNtbW51c2VyQGt1bGl6YS5jb20ifQ.hoy3YzQOb_EUedqGIkqZaYLrnktFUL__YofNdLzwJ0SPfiq_0XRojLe6qjIpmiCHBJwoYirTc8wOtLtmkkyloPqD9dyOR6QIZG5GtNTidRl9GbMwd4BBm67g47frtviIbRdDSsoORGCGxNKSVUganvYe4iECapLjOhDT6hUSJmjHbTHC2wF6wNg2RRwD60vSE5hYyn9onkjjYxGy79VM5Zw5_qcQTmyrev-v1MOQO6SCLODdB_KGsrKOzC9nXIzwmxL-GDsP0j2FZmbzRs1wNs6yJmu7BlO1OJEkQKekvSt7djzSHN4XCPDexVbwuvuorvqtFHeUuqEiCMiUUzJjNQ\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"externalApi\": false,\n" +
            "            \"requestType\": \"GET\",\n" +
            "            \"httpProtocol\": \"http\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String PC_FIRE_BULK_ACTION_RESPONSE = "{\n"+
            "    \"status\": 200,\n"+
            "    \"message\": \"SUCCESS!\",\n"+
            "    \"data\": {\n"+
            "        \"4b03cf82-557b-11e9-ac94-42010aa0001b\": {\n"+
            "            \"status\": 200,\n"+
            "            \"message\": \"SUCCESS!\",\n"+
            "            \"data\": {\n"+
            "                \"id\": 4,\n"+
            "                \"roleName\": \"cmmnCreditAnalyst\",\n"+
            "                \"productType\": \"personalLoan\",\n"+
            "                \"bucketList\": [\n"+
            "                    {\n"+
            "                        \"id\": \"pending\",\n"+
            "                        \"label\": \"PENDING\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    },\n"+
            "                    {\n"+
            "                        \"id\": \"approved\",\n"+
            "                        \"label\": \"APPROVED\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    },\n"+
            "                    {\n"+
            "                        \"id\": \"history\",\n"+
            "                        \"label\": \"HISTORY\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    },\n"+
            "                    {\n"+
            "                        \"id\": \"claim\",\n"+
            "                        \"label\": \"CLAIM\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    }\n"+
            "                ],\n"+
            "                \"userId\": \"cmmnuser@kuliza.com\"\n"+
            "            }\n"+
            "        }\n"+
            "    }\n"+
            "}";

    public static final String PC_GET_CUSTOM_RESPOSNE = "{\n"+
            "    \"status\": 200,\n"+
            "    \"message\": \"SUCCESS!\",\n"+
            "    \"data\": {\n"+
            "        \"id\": -3,\n"+
            "        \"method\": \"configurePortal\",\n"+
            "        \"userId\": null,\n"+
            "        \"openApi\": false,\n"+
            "        \"url\": \"/lending/portal/configurator/add-product-config\",\n"+
            "        \"portalType\": \"backOffice\",\n"+
            "        \"headers\": [],\n"+
            "        \"externalApi\": false,\n"+
            "        \"requestType\": \"POST\",\n"+
            "        \"httpProtocol\": \"http\"\n"+
            "    }\n"+
            "}";

    public static final String PC_CREATE_CUSTOM_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 1,\n" +
            "            \"method\": \"http\",\n" +
            "            \"userId\": \"cmmnuser@kuliza.com\",\n" +
            "            \"openApi\": false,\n" +
            "            \"url\": \"http://localhost:8080/lending/portal/configurator/roleName/productType\",\n" +
            "            \"portalType\": \"localhost\",\n" +
            "            \"headers\": [\n" +
            "                {\n" +
            "                    \"key\": \"Content-Type\",\n" +
            "                    \"value\": \"application/json\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"key\": \"Authorization\",\n" +
            "                    \"value\": \"Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBNVFJLUVsYmFGV1d1YXZleGpFVEtFRUROQ2JvM2hkUVREYm9iSFlndldNIn0.eyJqdGkiOiJlNzQ1ZmRlZC1mMjAyLTRlOTMtYmU0Ny1jNTAwNzBjZjZhMzciLCJleHAiOjE1NTY4MjI2OTcsIm5iZiI6MCwiaWF0IjoxNTU1NTA1MDk3LCJpc3MiOiJodHRwOi8vMzUuMjAwLjE4OC4xMjE6ODAwNS9hdXRoL3JlYWxtcy9rdWxpemFfY2UiLCJhdWQiOiJsZW5kaW5nIiwic3ViIjoiYmM0ZjYyMmUtMGExNi00YzY4LWJmNjQtZDE1MjgxZjViMWFlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibGVuZGluZyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjIzYTFiZGEyLWVmOTAtNDEyMS1hMmNlLWY2N2VkNDA1ZDI3ZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiY21tbkNyZWRpdEFuYWx5c3RQT0NTVUIxIiwiY21tbkNyZWRpdEFuYWx5c3RQT0NBZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsibGVuZGluZyI6eyJyb2xlcyI6WyJjbW1uQ3JlZGl0QW5hbHlzdCJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwibmFtZSI6ImNtbW4gdXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6ImNtbW51c2VyIiwiZ2l2ZW5fbmFtZSI6ImNtbW4iLCJmYW1pbHlfbmFtZSI6InVzZXIiLCJlbWFpbCI6ImNtbW51c2VyQGt1bGl6YS5jb20ifQ.hoy3YzQOb_EUedqGIkqZaYLrnktFUL__YofNdLzwJ0SPfiq_0XRojLe6qjIpmiCHBJwoYirTc8wOtLtmkkyloPqD9dyOR6QIZG5GtNTidRl9GbMwd4BBm67g47frtviIbRdDSsoORGCGxNKSVUganvYe4iECapLjOhDT6hUSJmjHbTHC2wF6wNg2RRwD60vSE5hYyn9onkjjYxGy79VM5Zw5_qcQTmyrev-v1MOQO6SCLODdB_KGsrKOzC9nXIzwmxL-GDsP0j2FZmbzRs1wNs6yJmu7BlO1OJEkQKekvSt7djzSHN4XCPDexVbwuvuorvqtFHeUuqEiCMiUUzJjNQ\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"externalApi\": false,\n" +
            "            \"requestType\": \"GET\",\n" +
            "            \"httpProtocol\": \"http\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String PC_UPDATE_CUSTOM_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"id\": -7,\n" +
            "        \"method\": \"http\",\n" +
            "        \"userId\": null,\n" +
            "        \"openApi\": false,\n" +
            "        \"url\": \"http://localhost:8080/configurator/roleName/productType\",\n" +
            "        \"portalType\": \"localhost\",\n" +
            "        \"headers\": [],\n" +
            "        \"externalApi\": false,\n" +
            "        \"requestType\": \"GET\",\n" +
            "        \"httpProtocol\": \"http\"\n" +
            "    }\n" +
            "}";

    public static final String PC_SUBMIT_CASE_VARIABLES_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"userId\": \"aman1234\"\n" +
            "    }\n" +
            "}";

    public static final String PC_ALLOCATION_LOGIC_RESPONSE = "mandip.gothadiya@kuliza.com";

    public static final String PC_GET_CARD_CONFIG_MAP = "}";

    public static final String PC_DELETE_CUSTOM_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"id\": -3,\n" +
            "        \"method\": \"configurePortal\",\n" +
            "        \"userId\": null,\n" +
            "        \"openApi\": false,\n" +
            "        \"url\": \"/lending/portal/configurator/add-product-config\",\n" +
            "        \"portalType\": \"backOffice\",\n" +
            "        \"headers\": [],\n" +
            "        \"externalApi\": false,\n" +
            "        \"requestType\": \"POST\",\n" +
            "        \"httpProtocol\": \"http\"\n" +
            "    }\n" +
            "}";

    public static final String SUCCESS_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": null\n" +
            "}";

    public static final String CC_GET_FORM = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"data\": {\n" +
            "            \"applicationId\": \"123456\"\n" +
            "        },\n" +
            "        \"caseInstanceId\": \"fd62b153-4e59-11e9-b267-0242a94e724a\",\n" +
            "        \"stepperData\": null,\n" +
            "        \"screen1_3\": {\n" +
            "            \"emailId\": {\n" +
            "                \"isRequired\": true,\n" +
            "                \"name\": \"Email Id\",\n" +
            "                \"id\": \"emailId\",\n" +
            "                \"isWritable\": true,\n" +
            "                \"type\": \"text\",\n" +
            "                \"params\": {\n" +
            "                    \"meta\": {\n" +
            "                        \"type\": \"string\",\n" +
            "                        \"form_type\": \"text\",\n" +
            "                        \"default\": \"\",\n" +
            "                        \"order\": 20,\n" +
            "                        \"required\": true,\n" +
            "                        \"disabled\": false,\n" +
            "                        \"placeholder\": true,\n" +
            "                        \"api\": \"\",\n" +
            "                        \"validator\": \"[]\",\n" +
            "                        \"newRow\": true,\n" +
            "                        \"colWidth\": 4\n" +
            "                    }\n" +
            "                },\n" +
            "                \"value\": null,\n" +
            "                \"enumValues\": null\n" +
            "            }\n" +
            "        },\n" +
            "        \"taskId\": \"fd6681ed-4e59-11e9-b267-0242a94e724a\"\n" +
            "    },\n" +
            "    \"screenInfo\": \"screen1_3\",\n" +
            "    \"screenName\": \"Test 1-3\",\n" +
            "    \"errorMessage\": \"\"\n" +
            "}";

    public static final String CC_GET_VARIABLES = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"INITIATOR\": 8676787,\n" +
            "        \"initiator\": \"mandip\"\n" +
            "    }\n" +
            "}";

    public static final String CC_SUBMIT_VARIABLES = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"KEY\": 8676787\n" +
            "    }\n" +
            "}";

    public static final String CC_CASE_VARAIBLES = "{\"KEY\":\"8676787\"}";

    // Constants for portal controller
    public static final String PC_PORTAL_CONFIGURE = "Saves the new configure portal on the basis of rolename and productType";
    public static final String PC_UPDATE_PORTAL_CONFIGURE = "Returns the updated portal configuration model on the basis of rolename and productType";
    public static final String PC_GET_PORTAL_CONFIGURE = "Returns the already existing portal configuration model on the basis of rolename and productType";
    public static final String PC_DEL_PORTAL_CONFIGURE = "Deletes the portal configuration model on the basis of rolename and productType";
    public static final String PC_ADD_BUCKET_CONFIG_DESC = "Adds the list of bucket config to portal configuration model on the basis of rolename and productType";
    public static final String PC_GET_BUCKET_CONFIG = "Returns the bucket config list from portal configuration model on the basis of rolename and productType";
    public static final String PC_GET_PROD_CONFIG = "Returns the set of portal configuration model on the basis of rolename";
    public static final String PC_GET_TAB_CONFIG_DESC = "Get list of Tab config using assignee and case instance id";
    public static final String PC_SET_COMMENT = "Sets Comment on basis of assignee";
    public static final String PC_GET_COMMENT = "Gets Comment on basis of caseInstanceId";
    public static final String PC_PRODUCT_GET_PORTAL = "Product type-Used to fetch portal config";
    public static final String PC_ROLENAME_GET_PORTAL = "Role name-With product type fetch portal config";
    public static final String PC_PRODUCT_DEL_PORTAL = "Product type-Used to delete from portal config";
    public static final String PC_ROLENAME_DEL_PORTAL = "Role name-With product type deletes from portal config";
    public static final String PC_ROLENAME_ADD_BUCKET = "Role name-With product type adds the bucket list in portal config";
    public static final String PC_PRODUCT_ADD_BUCKET = "Product type-Adds bucket list in portal config";
    public static final String PC_ROLENAME_GET_BUCKET = "Role name-With product type fetch the bucket list from portal config";
    public static final String PC_GET_BUCKET = "Bucket to fetch bucket information based on particular role and product type";
    public static final String PC_PRODUCT_GET_BUCKET = "Product type-Used to fetch bucket list from portal config";
    public static final String PC_ROLENAME_GET_PROD_CONFIG = "Role name-Fetch from portal config";
    public static final String PC_GET_ASSIGNED_LIST_DESC = "Returns the list of assigned cases on particular role name, product type and bucket";
    public static final String PC_GET_CARD_CONFIG_DESC = "Returns the cards on particular user and tab key";
    public static final String PC_CREATE_CUSTOM_ACTION = "Create entry of api info in database for firing using id only";
    public static final String PC_UPDATE_CUSTOM_ACTION = "Update entry of api info in database using id";
    public static final String PC_DELETE_CUSTOM_ACTION = "Delete api entry from database using id";
    public static final String PC_DELETE_ALL_CUSTOM_ACTION = "Delete all api info entries from database";
    public static final String PC_GET_CUSTOM_ACTION = "Get api info from database using id";
    public static final String PC_FIRE_CUSTOM_ACTION = "Fire custom action with id and proper request body";
    public static final String PC_FIRE_BULK_ACTION = "Fire bulk action api with id and list of proper request bodies";
    public static final String PC_CLAIM = "To claim or assign a particular application";
    public static final String PC_UNCLAIM = "To unclaim a particular application";
    public static final String PC_REASSIGN = "To reassign a particular application";
    public static final String PC_SUBMIT_PARENT_CASE_VARIABLES = "To submit parent caseInstance case variables";
    public static final String PC_SUBMIT_CHILD_CASE_VARIABLES = "To submit child caseInstance case variables";
    public static final String PC_ALLOCATION_LOGIC = "To get and set user allocation logic value";
    public static final String IMPORT_CUSTOM_ACTION_CONFIGURATION = "To import custom action configuration";
    public static final String EXPORT_CUSTOM_ACTION_CONFIGURATION = "To export custom action configuration";

    // Constants for CMMN Controller
    public static final String CC_START_OR_RESUME = "Checks valid caseName and checks if PortalUser is there and creates or update portal user and case instance id";
    public static final String CC_SUBMIT_FORM = "Returns the next task, completes the present task and saves the form details";
    public static final String CC_GET_PARTIAL_SUBMIT_FORM = "Returns the current task data for the given user associated with given case instance id";
    public static final String CC_POST_PARTIAL_SUBMIT_FORM = "Saves the form details and return the same task";
    public static final String CC_GET_VARIABLES_DESC = "Get Case variables for the given case instance id";
    public static final String CC_SUBMIT_VARIABLES_DESC = "Submit Case variables for the given case instance id";


}
