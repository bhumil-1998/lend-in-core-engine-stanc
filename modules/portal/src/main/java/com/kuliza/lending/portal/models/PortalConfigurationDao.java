package com.kuliza.lending.portal.models;

import java.util.Set;
import javax.sound.sampled.Port;
import javax.validation.constraints.Max.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PortalConfigurationDao extends CrudRepository<PortalConfigurationModel, Long> {

  public PortalConfigurationModel findByRoleNameAndProductType(String roleName, String productType);

  public Set<PortalConfigurationModel> findByRoleName(String roleName);
}
