package com.kuliza.lending.portal.broker;

import java.io.IOException;

import org.apache.logging.log4j.ThreadContext;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@EnableKafka
@Component
@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA")
public class KafkaMessageListener {
  @Autowired
  private CommonListenerService commonListenerService;

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMessageListener.class);

  @KafkaListener(topics = "test", groupId = "group-id")
  public void receiveMessage(final String message) throws Exception {
    LOGGER.info("Received message as specific class: {}", message.toString());
    commonListenerService.forwardMessageToService(message);
  }
}
