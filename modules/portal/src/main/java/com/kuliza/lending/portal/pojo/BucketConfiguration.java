package com.kuliza.lending.portal.pojo;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class BucketConfiguration {

  @NotNull(message = "id is a required key")
  @NotEmpty(message = "id cannot be empty")
  String id;

  @NotNull(message = "label is a required key")
  @NotEmpty(message = "label cannot be empty")
  String label;

  Boolean editable;

  @NotNull(message = "redirectToDetails is a required key")
  Boolean redirectToDetails;

  @NotNull(message = "comment is a required key")
  Integer comment;

  Integer hierarchyLevel;

  List<BucketvariableConfiguration> variables;

  List<ActionConfiguration> bucketActions;

  String onLoadId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public List<BucketvariableConfiguration> getVariables() {
    return variables;
  }

  public void setVariables(
      List<BucketvariableConfiguration> variables) {
    this.variables = variables;
  }

  public List<ActionConfiguration> getBucketActions() {
    return bucketActions;
  }

  public void setBucketActions(
      List<ActionConfiguration> bucketActions) {
    this.bucketActions = bucketActions;
  }

  public Boolean getEditable() {
    return editable;
  }

  public void setEditable(Boolean editable) {
    this.editable = editable;
  }

  public Integer getComment() {
    return comment;
  }

  public void setComment(Integer comment) {
    this.comment = comment;
  }

  public Boolean getRedirectToDetails() {
    return redirectToDetails;
  }

  public void setRedirectToDetails(Boolean redirectToDetails) {
    this.redirectToDetails = redirectToDetails;
  }

  public String getOnLoadId() {
    return onLoadId;
  }

  public void setOnLoadId(String onLoadId) {
    this.onLoadId = onLoadId;
  }

  public Integer getHierarchyLevel() {
    return hierarchyLevel;
  }

  public void setHierarchyLevel(Integer hierarchyLevel) {
    this.hierarchyLevel = hierarchyLevel;
  }
}
