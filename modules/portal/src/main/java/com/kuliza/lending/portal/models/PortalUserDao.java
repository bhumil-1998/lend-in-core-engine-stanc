package com.kuliza.lending.portal.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PortalUserDao extends CrudRepository<PortalUser, Long> {

  public PortalUser findByApplicationNumberAndCaseName(String applicationNumber, String caseName);

	public PortalUser findByCaseInstanceIdAndIsDeleted(String caseInstanceId, boolean isDeleted);

}
