package com.kuliza.lending.portal.pojo;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class ClaimUnclaimInput {

  private String bucketKey;

  @NotNull(message = "roleName is a required key")
  @NotEmpty(message = "roleName cannot be empty")
  private String roleName;
  private String currentAssignee;
  private String newAssignee;

  @NotNull(message = "caseInstanceId is a required key")
  @NotEmpty(message = "caseInstanceId cannot be empty")
  private String caseInstanceId;

  public String getBucketKey() {
    return bucketKey;
  }

  public void setBucketKey(String bucketKey) {
    this.bucketKey = bucketKey;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getCurrentAssignee() {
    return currentAssignee;
  }

  public void setCurrentAssignee(String currentAssignee) {
    this.currentAssignee = currentAssignee;
  }

  public String getNewAssignee() {
    return newAssignee;
  }

  public void setNewAssignee(String newAssignee) {
    this.newAssignee = newAssignee;
  }

  public String getCaseInstanceId() {
    return caseInstanceId;
  }

  public void setCaseInstanceId(String caseInstanceId) {
    this.caseInstanceId = caseInstanceId;
  }

  @Override
  public String toString() {
    return "ClaimUnclaimInput{" +
        "bucketKey='" + bucketKey + '\'' +
        ", roleName='" + roleName + '\'' +
        ", currentAssignee='" + currentAssignee + '\'' +
        ", newAssignee='" + newAssignee + '\'' +
        ", caseInstanceId='" + caseInstanceId + '\'' +
        '}';
  }
}
