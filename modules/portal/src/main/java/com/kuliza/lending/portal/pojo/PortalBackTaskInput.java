package com.kuliza.lending.portal.pojo;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class PortalBackTaskInput {

	@NotNull(message = "caseInstanceId is a required key")
	@NotEmpty(message = "caseInstanceId cannot be empty")
	String caseInstanceId;

	@NotNull(message = "currentTaskId is a required key")
	@NotEmpty(message = "currentTaskId cannot be empty")
	String currentTaskId;

	@NotNull(message = "backTaskDefinitionKey is a required key")
	@NotEmpty(message = "backTaskDefinitionKey cannot be empty")
	String backTaskDefinitionKey;

	public PortalBackTaskInput() {
		super();
	}

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public PortalBackTaskInput(String caseInstanceId, String currentTaskId, String backTaskDefinitionKey) {
		super();
		this.caseInstanceId = caseInstanceId;
		this.currentTaskId = currentTaskId;
		this.backTaskDefinitionKey = backTaskDefinitionKey;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public String getCurrentTaskId() {
		return currentTaskId;
	}

	public void setCurrentTaskId(String currentTaskId) {
		this.currentTaskId = currentTaskId;
	}

	public String getBackTaskDefinitionKey() {
		return backTaskDefinitionKey;
	}

	public void setBackTaskDefinitionKey(String backTaskDefinitionKey) {
		this.backTaskDefinitionKey = backTaskDefinitionKey;
	}

}
