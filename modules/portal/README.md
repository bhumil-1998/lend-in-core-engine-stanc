***
# All about Portal
    
### Some of the major actions that portal module does are :
*    Portal uses CMMN engine of Flowable (Flowable is a business process engine written in Java).
*    All the cases are uploaded through portal(CMMNController.java).
*    The products are added,updated through Portal module (PortalController).
*    We get the product configuration through Portal module.
*    We get the entire list of assigned cases(to some role) through Portal module.

### Portal module contains dependencies of following modules :
*    engine-common
*    logging
*    common
*    authorization

    these dependencies can be seen in pom.xml of portal module.
    
***
***
    
**When we run portal module, above mentioned modules also run.**

__To run the application, write the following command:__

*    mvn spring-boot:run
    
**If any changes are done in any module (not in portal) then before running portal module, all these 4 modules need to be rebuild. 
This can be done by using following command.**

*     mvn clean install

**In order to skip test cases, command given below is used.**

*    mvn clean install -Dmaven.test.skip=true
    
***
***
	
**There are two properties file present in Portal **

* application.properties
* lend-in-modules.properties
        
    We don't make any change in application.properties except the port number on which server is running. The changes are done in 
    lend-in-modules.properties file.The value of all the variables present in lend-in-modules.properties file can be changed according to 
    your server details.For more details please refer to the README written at lend-in-core-engine level.
    
    For example :  
    Let's say we have database configurations as follows:  
      
    
        spring.datasource.platform=mysql
        spring.datasource.url=jdbc:mysql://35.200.206.224/dev232018?characterEncoding=UTF-8
        spring.datasource.username=root
        spring.datasource.password=Digital@2017$#
        spring.datasource.driver-class-name=com.mysql.jdbc.Driver
        server.port=7200
    
    Now , you might have postgres as your database, your application might be running on some other port and so on. So do the changes accordingly.
    You need to add these configurations to your bashrc file. For more details please refer to the README written at lend-in-core-engine level.  
    There are many functions which are generic,(common for all module, for example..convert a json file to excel). All the generic functions are present in Common module. 
    Some functions which are generic from portal point of view are present in PortalHelperFunction.java in Portal module itself.
