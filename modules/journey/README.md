***
## About Journey module ##


Journey is the very first step, this module uses Business Process Model and Notation (BPMN). A user has to go through various stages before the case
reaches portal. There will be various forms a user has to fill, submitting his/her details. Journey module is used to initiate the process variables.

*	Do not make any change in application.properties file. If any changes that are specific to journey module only can be written in application.properties file.
*	To run journey module, do all the server related changes in lend-in-modules.properties file and then write the command __mvn spring-boot:run__. 
*	To change the server port go to application.properties file.
	* server.port = 8090 (this can be changed)
*	This module provides us with generic functionalities such as Initiate journey, submit form, get form, partial submit etc.
*	Some of the module’s API will soon be deprecated
*	This module contains following dependencies :
	* 	common
	*	engine-common
	*	authorization
	*	config_manager
	*	portal
	*	logging

