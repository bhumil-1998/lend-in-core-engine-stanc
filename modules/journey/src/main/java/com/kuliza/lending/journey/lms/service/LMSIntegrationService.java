package com.kuliza.lending.journey.lms.service;

import com.kuliza.lending.journey.lms.data.ClientConfiguration;
import com.kuliza.lending.journey.lms.data.DatatableConfiguration;
import com.kuliza.lending.journey.lms.data.LoanConfiguration;
import org.flowable.engine.RuntimeService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class LMSIntegrationService extends AbstractLMSIntegrationService{

    private static final Logger LOGGER = LoggerFactory.getLogger(LMSIntegrationService.class);

    private RuntimeService runtimeService;

    @Autowired
    public LMSIntegrationService(RuntimeService runtimeService){
        this.runtimeService = runtimeService;
    }


    public void processClientConfigurations(String processInstanceId,
                                            Collection<ClientConfiguration> clientConfigurations) throws Exception{

        JSONArray results = new JSONArray();
        Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
        for (ClientConfiguration clientConfiguration : clientConfigurations)
            results.put(this.processClientConfiguration(clientConfiguration, processVariables));
        LOGGER.info("process client configs result : " +  results.toString());
        runtimeService.setVariable(processInstanceId, "clientIdsFromLms", results.toString());
    }

    private JSONObject processClientConfiguration(ClientConfiguration clientConfiguration,
                                                  Map<String, Object> processVariables) throws Exception{

        Map<String, Object> partialRequestBody = new HashMap<>();
        ClientConfiguration.populateMap(clientConfiguration, partialRequestBody, processVariables);
        String baseUrl = String.valueOf(processVariables.getOrDefault("lmsBaseUrl", this.baseUrl).toString());
        String tenant = String.valueOf(processVariables.getOrDefault("lmsTenant", this.tenant).toString());
        String token = String.valueOf(processVariables.getOrDefault("lmsToken", this.token).toString());
        String officeId = String.valueOf(processVariables.getOrDefault("lmsOfficeId", this.officeId).toString());
        JSONObject result = this.createClient(clientConfiguration.getClientType(), partialRequestBody, baseUrl, tenant, token, officeId);
        for (DatatableConfiguration datatableConfiguration : clientConfiguration.getDataTableConfigurations())
            this.processDatatableConfiguration(result.getString("clientId"), datatableConfiguration, processVariables);
        this.activateClient(result.getString("clientId"), baseUrl, tenant, token, officeId);
        return result;

    }


    public void processDatatableConfiguration(String clientId, DatatableConfiguration datatableConfiguration,
                                              Map<String, Object> processVariables) throws Exception{

        Map<String, Object> partialRequestBody = new HashMap<>();
        DatatableConfiguration.populateMap(datatableConfiguration, partialRequestBody, processVariables);
        String baseUrl = String.valueOf(processVariables.getOrDefault("lmsBaseUrl", this.baseUrl).toString());
        String tenant = String.valueOf(processVariables.getOrDefault("lmsTenant", this.tenant).toString());
        String token = String.valueOf(processVariables.getOrDefault("lmsToken", this.token).toString());
        String officeId = String.valueOf(processVariables.getOrDefault("lmsOfficeId", this.officeId).toString());
        this.createDataTable(clientId, datatableConfiguration.getDataTableName(), partialRequestBody, baseUrl, tenant, token, officeId);

    }


    public void processLoanConfigurations(String processInstanceId,
                                            Collection<LoanConfiguration> loanConfigurations) throws Exception{

        JSONArray results = new JSONArray();
        Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
        for (LoanConfiguration loanConfiguration : loanConfigurations)
            results.put(this.processLoanConfiguration(loanConfiguration, processVariables));
        LOGGER.info("process loans configs result : " +  results.toString());
        runtimeService.setVariable(processInstanceId, "loanIdsFromLms", results.toString());
    }

    private JSONObject processLoanConfiguration(LoanConfiguration loanConfiguration,
                                                  Map<String, Object> processVariables) throws Exception{

        Map<String, Object> partialRequestBody = new HashMap<>();
        LoanConfiguration.populateMap(loanConfiguration, partialRequestBody, processVariables);
        String baseUrl = String.valueOf(processVariables.getOrDefault("lmsBaseUrl", this.baseUrl).toString());
        String tenant = String.valueOf(processVariables.getOrDefault("lmsTenant", this.tenant).toString());
        String token = String.valueOf(processVariables.getOrDefault("lmsToken", this.token).toString());
        String officeId = String.valueOf(processVariables.getOrDefault("lmsOfficeId", this.officeId).toString());
        return this.createLoan(loanConfiguration.getProductId(), partialRequestBody, baseUrl, tenant, token, officeId);
    }
}
