package com.kuliza.lending.journey.lms.model;

import org.springframework.data.repository.CrudRepository;

public interface LMSIntegrationConfigRepository extends CrudRepository<LMSIntegrationConfig, Long> {


    LMSIntegrationConfig findByJourneyIdentifier(String journeyIdentifier);

}
