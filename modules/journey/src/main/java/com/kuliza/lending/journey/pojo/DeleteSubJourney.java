package com.kuliza.lending.journey.pojo;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class DeleteSubJourney {

	@NotNull(message = "processInstanceId is a required key")
	@NotEmpty(message = "processInstanceId cannot be empty")
	String processInstanceId;
	
	@NotNull(message = "subJourneyKey is required")
	@NotEmpty(message = "subJourneyKey cannot be empty")
	String subJourneyKey;
	
	String dependentKeys;

	@NotNull(message = "subJourneyNumber is required")
	Integer subJourneyNumber;
	
	@NotNull(message = "subJourneyCounterKey is required")
	@NotEmpty(message = "subJourneyCounterKey cannot be empty")
	String subJourneyCounterKey;

	public DeleteSubJourney() {
		super();
	}
	
	public DeleteSubJourney(String processInstanceId, String subJourneyKey, String dependentKeys,
			Integer subJourneyNumber, String subJourneyCounterKey) {
		super();
		this.processInstanceId = processInstanceId;
		this.subJourneyKey = subJourneyKey;
		this.dependentKeys = dependentKeys;
		this.subJourneyNumber = subJourneyNumber;
		this.subJourneyCounterKey = subJourneyCounterKey;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getSubJourneyKey() {
		return subJourneyKey;
	}

	public void setSubJourneyKey(String subJourneyKey) {
		this.subJourneyKey = subJourneyKey;
	}

	public String getDependentKeys() {
		return dependentKeys;
	}

	public void setDependentKeys(String dependentKeys) {
		this.dependentKeys = dependentKeys;
	}

	public Integer getSubJourneyNumber() {
		return subJourneyNumber;
	}

	public void setSubJourneyNumber(Integer subJourneyNumber) {
		this.subJourneyNumber = subJourneyNumber;
	}

	public String getSubJourneyCounterKey() {
		return subJourneyCounterKey;
	}

	public void setSubJourneyCounterKey(String subJourneyCounterKey) {
		this.subJourneyCounterKey = subJourneyCounterKey;
	}
	
}
