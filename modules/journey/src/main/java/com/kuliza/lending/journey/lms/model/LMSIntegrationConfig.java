package com.kuliza.lending.journey.lms.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.journey.lms.data.ClientConfiguration;
import com.kuliza.lending.journey.lms.data.DatatableConfiguration;
import com.kuliza.lending.journey.lms.data.LoanConfiguration;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "los_lms_integration_config")
public class LMSIntegrationConfig extends BaseModel {


    private String journeyIdentifier;

    @Column(columnDefinition = "longtext", nullable = false)
    private String clients;

    @Column(columnDefinition = "longtext", nullable = false)
    private String loans;

    @Transient
    private Collection<ClientConfiguration> clientConfigurations;

    @Transient
    private Collection<LoanConfiguration> loanConfigurations;

    public LMSIntegrationConfig() {
        super();
        this.setIsDeleted(false);
        this.clients = new JSONArray().toString();
        this.loans = new JSONArray().toString();
        this.populateClientConfigurations();
        this.populateLoanConfigurations();

    }

    public String getJourneyIdentifier() {
        return journeyIdentifier;
    }

    public void setJourneyIdentifier(String journeyIdentifier) {
        this.journeyIdentifier = journeyIdentifier;
    }

    public Collection<ClientConfiguration> getClientConfigurations(){
        this.populateClientConfigurations();
        return this.clientConfigurations;
    }

    public Collection<LoanConfiguration> getLoanConfigurations() {
        this.populateLoanConfigurations();
        return this.loanConfigurations;
    }

    public void setLoanConfigurations(Collection<LoanConfiguration> loanConfigurations) {
        this.loanConfigurations = loanConfigurations;
    }

    private void populateClientConfigurations(){
        if (Objects.nonNull(this.clientConfigurations) && !this.clientConfigurations.isEmpty())
            return;
        JSONArray clientConfigurations = new JSONArray(this.clients);
        this.clientConfigurations = new ArrayList<>(clientConfigurations.length());
        for (int i=0;i < clientConfigurations.length(); i++){
            ClientConfiguration clientConfiguration = new ClientConfiguration(clientConfigurations.optJSONObject(i));
            this.clientConfigurations.add(clientConfiguration);
        }
    }

    private void populateLoanConfigurations(){
        if (Objects.nonNull(this.loanConfigurations) && !this.loanConfigurations.isEmpty())
            return;
        JSONArray loanConfigurations = new JSONArray(this.loans.isEmpty()? "[]" : this.loans);
        this.loanConfigurations = new ArrayList<>(loanConfigurations.length());
        for (int i=0;i < loanConfigurations.length(); i++){
            LoanConfiguration loanConfiguration = new LoanConfiguration(loanConfigurations.optJSONObject(i));
            this.loanConfigurations.add(loanConfiguration);
        }
    }

    public void createOrUpdateClient(ClientConfiguration.ClientConfigurationRequest request){
        String clientType = request.getClientType();
        ClientConfiguration clientConfiguration = this.getClientConfigurationByClientType(clientType);

        if (Objects.isNull(clientConfiguration)) {
            clientConfiguration = new ClientConfiguration(request);
            this.getClientConfigurations().add(clientConfiguration);
            return;
        }
        clientConfiguration.setAttributeToVariableMappingForCreation(new JSONObject(request.getAttributeToVariableMappingForCreation()));
    }

    public void deleteClient(ClientConfiguration.ClientConfigurationRequest request){
        String clientType = request.getClientType();
        ClientConfiguration clientConfiguration = this.getClientConfigurationByClientType(clientType);
        if (Objects.isNull(clientConfiguration)) {
            throw new RuntimeException("client: " + clientType + " doesn't exist for this journeyIdentifier");
        }
        this.getClientConfigurations().remove(clientConfiguration);
    }


    public void createOrUpateDatatable(String clientType, DatatableConfiguration.DatatableConfigurationRequest request){
        ClientConfiguration clientConfiguration = this.getClientConfigurationByClientType(clientType);
        if (Objects.isNull(clientConfiguration)) {
            throw new RuntimeException("client: " + clientType + " doesn't exist for this journeyIdentifier");
        }
        clientConfiguration.createOrUpateDatatable(request);
    }

    public void deleteDataTable(String clientType, DatatableConfiguration.DatatableConfigurationRequest request){
        ClientConfiguration clientConfiguration = this.getClientConfigurationByClientType(clientType);
        if (Objects.isNull(clientConfiguration)) {
            throw new RuntimeException("client: " + clientType + " doesn't exist for this journeyIdentifier");
        }
        clientConfiguration.deleteDatatable(request);

    }


    public void createOrUpdateLoan(LoanConfiguration.LoanConfigurationRequest request){

        Integer productId = Integer.parseInt(request.getProductId());
        LoanConfiguration loanConfiguration = this.getLoanConfigurationByProductId(productId);
        if (Objects.isNull(loanConfiguration)) {
            loanConfiguration = new LoanConfiguration(request);
            this.getLoanConfigurations().add(loanConfiguration);
            return;
        }
        loanConfiguration.setAttributeToVariableMapping(new JSONObject(request.getAttributeToVariableMapping()));
        loanConfiguration.setDisbursementData(new JSONObject(request.getDisbursementData()));

    }

    public void deleteLoan(LoanConfiguration.LoanConfigurationRequest request){
        Integer productId = Integer.parseInt(request.getProductId());
        LoanConfiguration loanConfiguration = this.getLoanConfigurationByProductId(productId);
        if (Objects.isNull(loanConfiguration)) {
            throw new RuntimeException("loan: " + productId + " doesn't exist for this journeyIdentifier");
        }
        this.getLoanConfigurations().remove(loanConfiguration);
    }

    public ClientConfiguration getClientConfigurationByClientType(String clientType){
        for (ClientConfiguration clientConfiguration : this.getClientConfigurations()){
            if (clientConfiguration.getClientType().equalsIgnoreCase(clientType))
                return clientConfiguration;
        }
        return null;
    }

    private LoanConfiguration getLoanConfigurationByProductId(Integer productId){
        for (LoanConfiguration loanConfiguration : this.getLoanConfigurations()){
            if (loanConfiguration.getProductId().equals(productId))
                return loanConfiguration;
        }
        return null;
    }

    public void ConvertToPersistable(){
            getLoanConfigurations();
            getClientConfigurations();
        try {
            ObjectMapper objectMapper = new ObjectMapper().registerModule(new JsonOrgModule());
            this.clients = objectMapper.writeValueAsString(new JSONArray(this.clientConfigurations));
            this.loans = objectMapper.writeValueAsString(new JSONArray(this.loanConfigurations));
        }catch (JsonProcessingException e){
            this.clients = "Error";
        }
    }



}
