package com.kuliza.lending.journey.utils;

import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.utils.EnumConstants.URL_CONVERTER_IDENTIFIER;
import java.util.HashMap;
import java.util.Map;

import org.flowable.form.model.FormField;
import org.flowable.task.api.Task;
import org.springframework.http.HttpStatus;

import com.kuliza.lending.common.pojo.ExtendedApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;

public class HelperFunctions extends CommonHelperFunctions {


	public static ExtendedApiResponse makeResponse(HttpStatus status, String message, String processInstanceId,
			Task task, Map<String, Object> data, Map<String, FormField> formFields) throws Exception {
		return makeResponse(status, message, processInstanceId, task.getId(), task.getTaskDefinitionKey(), task.getName(), task.getDescription(), data, formFields);
	}
	
	
	public static ExtendedApiResponse makeResponse(HttpStatus status, String message, String processInstanceId,
			String taskId, String taskDefinitionKey, String taskName, String taskDesc, Map<String,
			Object> data, Map<String, FormField> formFields) throws Exception {
		
		Map<String, Object> formData = com.kuliza.lending.engine_common.utils.HelperFunctions.getPresentScreenInfo(formFields, URL_CONVERTER_IDENTIFIER.JOURNEY);
		Map<String, Object> responseData = new HashMap<>();
		responseData.put(Constants.TASK_ID_KEY, taskId);
		responseData.put(Constants.PROCESS_INSTANCE_KEY, processInstanceId);
		responseData.put(Constants.DATA_KEY, data);
		responseData.put(taskDefinitionKey, formData);
		responseData.put(Constants.STEPPER_DATA_KEY, taskDesc);
		return new ExtendedApiResponse(status, message, responseData, taskDefinitionKey, taskName, "");
	}

}
