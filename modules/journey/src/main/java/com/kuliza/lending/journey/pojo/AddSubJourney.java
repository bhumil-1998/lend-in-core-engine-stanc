package com.kuliza.lending.journey.pojo;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class AddSubJourney {

	@NotNull(message = "processInstanceId is a required key")
	@NotEmpty(message = "processInstanceId cannot be empty")
	String processInstanceId;

	@NotNull(message = "currentTaskId is a required key")
	@NotEmpty(message = "currentTaskId cannot be empty")
	String currentTaskId;

	@NotNull(message = "backTaskDefinitionKey is a required key")
	@NotEmpty(message = "backTaskDefinitionKey cannot be empty")
	String backTaskDefinitionKey;
	
	@NotNull(message = "Key for Counter Not Present")
	@NotEmpty(message = "Key for Counter Not Present")
	String subJourneyCounterKey;

	public AddSubJourney() {
		super();
	}

	public AddSubJourney(String processInstanceId, String currentTaskId, String backTaskDefinitionKey,
			String subJourneyCounterKey) {
		super();
		this.processInstanceId = processInstanceId;
		this.currentTaskId = currentTaskId;
		this.backTaskDefinitionKey = backTaskDefinitionKey;
		this.subJourneyCounterKey = subJourneyCounterKey;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getCurrentTaskId() {
		return currentTaskId;
	}

	public void setCurrentTaskId(String currentTaskId) {
		this.currentTaskId = currentTaskId;
	}

	public String getBackTaskDefinitionKey() {
		return backTaskDefinitionKey;
	}

	public void setBackTaskDefinitionKey(String backTaskDefinitionKey) {
		this.backTaskDefinitionKey = backTaskDefinitionKey;
	}

	public String getSubJourneyCounterKey() {
		return subJourneyCounterKey;
	}

	public void setSubJourneyCounterKey(String subJourneyCounterKey) {
		this.subJourneyCounterKey = subJourneyCounterKey;
	}
	
}
