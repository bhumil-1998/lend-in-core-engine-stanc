package com.kuliza.lending.journey.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Type;

@MappedSuperclass
public class AbstractWfUserLeads {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @ApiModelProperty(required = false, hidden = true)
  @OrderBy(clause = "id ASC")
  private long id;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  private Date created;

  @Type(type = "org.hibernate.type.NumericBooleanType")
  @ColumnDefault("0")
  @Column(nullable=false)
  private Boolean expired = false;

  @Column(nullable = false, unique = true)
  private String leadId;

  @Column
  private String leadProcessInstanceId;

  @Column(nullable = false)
  private String uniqueIdentifier;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @JsonIgnore
  public Date getCreated() {
    return created == null ? null : (Date) created.clone();
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @JsonIgnore
  public Boolean getExpired() {
    return expired;
  }

  public void setExpired(Boolean expired) {
    this.expired = expired;
  }

  public String getLeadId() {
    return leadId;
  }

  public void setLeadId(String leadId) {
    this.leadId = leadId;
  }

  public String getLeadProcessInstanceId() {
    return leadProcessInstanceId;
  }

  public void setLeadProcessInstanceId(String leadProcessInstanceId) {
    this.leadProcessInstanceId = leadProcessInstanceId;
  }

  public String getUniqueIdentifier() {
    return uniqueIdentifier;
  }

  public void setUniqueIdentifier(String uniqueIdentifier) {
    this.uniqueIdentifier = uniqueIdentifier;
  }

  public AbstractWfUserLeads(String leadId, String leadProcessInstanceId,
      String uniqueIdentifier) {
    super();
    this.leadId = leadId;
    this.leadProcessInstanceId = leadProcessInstanceId;
    this.uniqueIdentifier = uniqueIdentifier;
  }

  public AbstractWfUserLeads(String leadId, String uniqueIdentifier) {
    super();
    this.leadId = leadId;
    this.uniqueIdentifier = uniqueIdentifier;
  }

  public AbstractWfUserLeads() {
  }

  @Override
  public String toString() {
    return "AbstractWfUserLeads{" +
        "id=" + id +
        ", created=" + created +
        ", expired=" + expired +
        ", leadId='" + leadId + '\'' +
        ", leadProcessInstanceId='" + leadProcessInstanceId + '\'' +
        ", uniqueIdentifier='" + uniqueIdentifier + '\'' +
        '}';
  }
}
