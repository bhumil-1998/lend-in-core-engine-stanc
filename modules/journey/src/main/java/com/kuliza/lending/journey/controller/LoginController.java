package com.kuliza.lending.journey.controller;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.journey.service.OtpServices;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.kuliza.lending.engine_common.utils.EngineCommonSwaggerConstants.GET_USER_TOKEN_RESPONSE;

@RestController
@RequestMapping("/lending")
public class LoginController {

    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    public OtpServices otpService;

    @ApiOperation(value = "To get user's Token form Portal or Authorised source")
    @ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = GET_USER_TOKEN_RESPONSE, code = 200)})
    @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
            required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
    @RequestMapping(method = RequestMethod.GET, value = "/get-user-token")
    public ResponseEntity<Object> getUserToken(HttpServletRequest request, @ApiParam(value = "Unique Identifier" , required = true,
            example = "99264832Ahs@&&A") @RequestParam(required=true, value="uniqueIdentifier") String uniqueIdentifier) throws Exception{
        return CommonHelperFunctions
                .buildResponseEntity(otpService.getUserTokenWithoutOTPValidation(uniqueIdentifier));
    }
}
