package com.kuliza.lending.journey.pojo;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class InitiateFromPortalInput {
	@NotEmpty(message = "initator cannot be empty")
	@NotNull(message = "initator cannot be empty")
	String initiator;

	@NotEmpty(message = "journeyType cannot be empty")
	@NotNull(message = "initator cannot be empty")
	String journeyKey;

	Map<String, Object> variablesToSave;

	public String getInitiator() {
		return initiator;
	}

	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}

	public String getJourneyKey() {
		return journeyKey;
	}

	public void setJourneyKey(String journeyKey) {
		this.journeyKey = journeyKey;
	}

	public Map<String, Object> getVariablesToSave() {
		return variablesToSave;
	}

	public void setVariablesToSave(Map<String, Object> variablesToSave) {
		this.variablesToSave = variablesToSave;
	}

}
