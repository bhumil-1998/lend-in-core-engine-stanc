package com.kuliza.lending.journey.lms.data;

import java.math.BigDecimal;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.kuliza.lending.journey.lms.service.AbstractLMSIntegrationService.convertDate;
import static com.kuliza.lending.journey.lms.service.AbstractLMSIntegrationService.staticDefaultDateFormat;

public class LoanConfiguration {

    private Integer productId;

    private JSONObject attributeToVariableMapping;

    private JSONObject disbursementData;

    public LoanConfiguration(JSONObject json){

        this.productId = Integer.parseInt(json.optString("productId", "0"));
        this.attributeToVariableMapping = json.optJSONObject("attributeToVariableMapping");
        this.attributeToVariableMapping = this.attributeToVariableMapping == null ?
                new JSONObject() : this.attributeToVariableMapping;
        this.disbursementData = json.optJSONObject("disbursementData");
        this.disbursementData = this.disbursementData == null ?
                new JSONObject() : this.disbursementData;
    }

    public LoanConfiguration(LoanConfiguration.LoanConfigurationRequest request){
        this.productId = Integer.parseInt(request.getProductId());
        this.attributeToVariableMapping = new JSONObject(request.getAttributeToVariableMapping());
        this.disbursementData = new JSONObject(request.getDisbursementData());
    }


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public JSONObject getAttributeToVariableMapping() {
        return attributeToVariableMapping;
    }

    public void setAttributeToVariableMapping(JSONObject attributeToVariableMapping) {
        this.attributeToVariableMapping = attributeToVariableMapping;
    }

    public JSONObject getDisbursementData() {
        return disbursementData;
    }

    public void setDisbursementData(JSONObject disbursementData) {
        this.disbursementData = disbursementData;
    }



    public static void populateMap(LoanConfiguration loanConfiguration, Map<String, Object> map,
                                   Map<String, Object> processVariables){

        JSONObject mapping = loanConfiguration.getAttributeToVariableMapping();
        for (Object attributeName : mapping.keySet()){
            map.put(attributeName.toString(),
                    String.valueOf(processVariables.getOrDefault(mapping.optString(attributeName.toString()), "")));
        }
        Integer tenure = Integer.parseInt(processVariables.get(
            mapping.getString("numberOfRepayments")).toString());
        map.put("numberOfRepayments", tenure.toString());
        map.put("loanTermFrequency", tenure.toString());

        map.put("expectedDisbursementDate", convertDate(new Date(), staticDefaultDateFormat));

        JSONArray clientIds = new JSONArray(String.valueOf(processVariables.getOrDefault("clientIdsFromLms", "[]")));
        String clientIdToUse = "";
        for (int i=0;i< clientIds.length();i++){
            JSONObject result = clientIds.optJSONObject(i);
            if (result.optString("clientType", "").equalsIgnoreCase("main")){
                clientIdToUse = result.optString("clientId", "");
                break;
            }
        }
        map.put("clientId", clientIdToUse);
        Integer[] amortizationTypeValue  = new Integer[1];
        amortizationTypeValue[0] = 1;

        setLoanType(amortizationTypeValue,  map, loanConfiguration, processVariables );

        map.put("amortizationType", amortizationTypeValue[0]);
    }

    public static class LoanConfigurationRequest{

        private String productId;

        private Map<String, String> attributeToVariableMapping;

        private Map<String, String> disbursementData;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public Map<String, String> getAttributeToVariableMapping() {
            return attributeToVariableMapping;
        }

        public void setAttributeToVariableMapping(Map<String, String> attributeToVariableMapping) {
            this.attributeToVariableMapping = attributeToVariableMapping;
        }

        public Map<String, String> getDisbursementData() {
            return disbursementData;
        }

        public void setDisbursementData(Map<String, String> disbursementData) {
            this.disbursementData = disbursementData;
        }
    }

    static void setLoanType(Integer [] amortizationTypeValue, Map<String, Object> map,
        LoanConfiguration loanConfiguration, Map<String, Object> processVariables ){

        String loanType = loanConfiguration.getDisbursementData().getString("loanType");

        Map<String, Object> stepIntervals = new HashMap<>();
        if(loanType.equalsIgnoreCase("stepup") || loanType.equalsIgnoreCase("stepdown")) {
            if (loanType.equalsIgnoreCase("stepup")) {
                amortizationTypeValue[0] = 3;
            } else {
                amortizationTypeValue[0] = 4;
            }
            stepIntervals.put("intervalContinue", true);
            String baseEmiString = String.valueOf(processVariables.getOrDefault(
                    loanConfiguration.getDisbursementData().getString("baseEmi"), "1"));
            BigDecimal baseEmi = new BigDecimal(new Double(baseEmiString));
            stepIntervals.put("baseEmi", baseEmi.toString());

            String thresholdBaseEmiString = String.valueOf(processVariables.getOrDefault(
                    loanConfiguration.getDisbursementData().getString("thresholdBaseEmi"), 1));
            BigDecimal thresholdBaseEmi = new BigDecimal(new Double(thresholdBaseEmiString));
            stepIntervals.put("thresholdBaseEmi", thresholdBaseEmi.toString());

            List<Map<String, Object>> intervals = new ArrayList<>();
            Map<String, Object> intervalMap = new HashMap<>();
            Integer stepInterval = Integer.parseInt(processVariables.getOrDefault(
                loanConfiguration.getDisbursementData().getString("interval"), 1).toString());
            intervalMap.put("interval", stepInterval.toString());
            String stepPercentageString = String.valueOf(processVariables.getOrDefault(
                    loanConfiguration.getDisbursementData().getString("percentage"), 1));
           BigDecimal stepPercentage = new BigDecimal(new Double(stepPercentageString));
            intervalMap.put("percentage", stepPercentage.toString());
            intervals.add(intervalMap);
            stepIntervals.put("intervals", intervals);
            stepIntervals.put("locale", "en");
            map.put("stepIntervals", stepIntervals);

        }else if(loanType.equalsIgnoreCase("balloon")) {
            amortizationTypeValue[0] = 2;
            Integer numberOfAmortizationRepayments = Integer.parseInt(processVariables.getOrDefault(
                loanConfiguration.getDisbursementData().getString("numberOfAmortizationRepayments"), 1).toString());
            map.put("numberOfAmortizationRepayments", numberOfAmortizationRepayments.toString());

        }else if(loanType.equalsIgnoreCase("tranche")) {
            JSONArray disbursementData = new JSONArray();
            if (loanConfiguration.getDisbursementData().has("principal")) {
                Map<String, Object> disbursementMap = new HashMap<>();
                disbursementMap.put("principal",
                    String.valueOf(processVariables.getOrDefault(
                        loanConfiguration.getDisbursementData().getString("principal"), "")));
                String expectedDisbursementDate = convertDate(
                    String.valueOf(processVariables.get("firstDisbursalDate")),
                    "dd-MM-yyyy", staticDefaultDateFormat);
                disbursementMap.put("expectedDisbursementDate", expectedDisbursementDate);
                disbursementData.put(disbursementMap);
                map.put("expectedDisbursementDate", expectedDisbursementDate);
                map.put("maxOutstandingLoanBalance", "100000000");
            }
            map.put("disbursementData", disbursementData);
        }
    }

}
