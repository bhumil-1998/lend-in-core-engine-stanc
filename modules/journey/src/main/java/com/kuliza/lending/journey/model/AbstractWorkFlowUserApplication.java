package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.kuliza.lending.common.model.BaseModel;

@MappedSuperclass
public class AbstractWorkFlowUserApplication extends BaseModel {

	@Column(nullable = false)
	private String userIdentifier;

	@Column(nullable = true)
	private String procInstId;

	@Column(nullable = true)
	private String parentProcInstId;
	
	@Column(nullable = false)
	private String processName;

	public AbstractWorkFlowUserApplication() {
		super();
		this.setIsDeleted(false);
	}

	public AbstractWorkFlowUserApplication(String userIdentifier,
			String procInstId, String parentProcInstId, String processName) {
		super();
		this.userIdentifier = userIdentifier;
		this.procInstId = procInstId;
		this.parentProcInstId = parentProcInstId;
		this.processName = processName;
		this.setIsDeleted(false);
	}

	public String getUserIdentifier() {
		return userIdentifier;
	}

	public void setUserIdentifier(String userIdentifier) {
		this.userIdentifier = userIdentifier;
	}

	public String getProcInstId() {
		return procInstId;
	}

	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}

	public String getParentProcInstId() {
		return parentProcInstId;
	}

	public void setParentProcInstId(String parentProcInstId) {
		this.parentProcInstId = parentProcInstId;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

}
