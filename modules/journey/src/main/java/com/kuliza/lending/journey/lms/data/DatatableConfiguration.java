package com.kuliza.lending.journey.lms.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

public class DatatableConfiguration {


    private String dataTableName;
    private JSONObject attributeToVariableMapping;

    private static final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JsonOrgModule());


    public DatatableConfiguration(DatatableConfiguration.DatatableConfigurationRequest request){
        this.dataTableName = request.getDataTableName();
        this.attributeToVariableMapping = new JSONObject(request.getAttributeToVariableMapping());
    }

    public DatatableConfiguration(JSONObject json){
        this.dataTableName = json.optString("dataTableName", "");
        this.attributeToVariableMapping = json.optJSONObject("attributeToVariableMapping");
        this.attributeToVariableMapping = Objects.isNull(this.attributeToVariableMapping)
                ? new JSONObject() : this.attributeToVariableMapping;
    }

    public String getDataTableName() {
        return dataTableName;
    }

    public void setDataTableName(String dataTableName) {
        this.dataTableName = dataTableName;
    }

    public JSONObject getAttributeToVariableMapping() {
        return attributeToVariableMapping;
    }

    public void setAttributeToVariableMapping(JSONObject attributeToVariableMapping) {
        this.attributeToVariableMapping = attributeToVariableMapping;
    }



    public static void populateMap(DatatableConfiguration datatableConfiguration, Map<String, Object> map,
                                                  Map<String, Object> processVariables){

        JSONObject mapping = datatableConfiguration.getAttributeToVariableMapping();
        for (Object attributeName : mapping.keySet()){
            map.put(attributeName.toString(),
                    String.valueOf(processVariables.getOrDefault(mapping.optString(attributeName.toString()), "")));
        }
    }

    public static class DatatableConfigurationRequest{

        private String dataTableName;
        private Map<String, String> attributeToVariableMapping;

        public String getDataTableName() {
            return dataTableName;
        }

        public void setDataTableName(String dataTableName) {
            this.dataTableName = dataTableName;
        }

        public Map<String, String> getAttributeToVariableMapping() {
            return attributeToVariableMapping;
        }

        public void setAttributeToVariableMapping(Map<String, String> attributeToVariableMapping) {
            this.attributeToVariableMapping = attributeToVariableMapping;
        }
    }

    @Override
    public String toString(){
        try {
            return objectMapper.writeValueAsString(this);
        }catch (JsonProcessingException e){
            return "JsonProcessingException";
        }
    }


}
