package com.kuliza.lending.journey.lms.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class ClientConfiguration {

    private String clientType;

    private JSONObject attributeToVariableMappingForCreation;

    private Collection<DatatableConfiguration> dataTableConfigurations;

    private static final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JsonOrgModule());

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public JSONObject getAttributeToVariableMappingForCreation() {
        return attributeToVariableMappingForCreation;
    }

    public void setAttributeToVariableMappingForCreation(JSONObject attributeToVariableMappingForCreation) {
        this.attributeToVariableMappingForCreation = attributeToVariableMappingForCreation;
    }

    public Collection<DatatableConfiguration> getDataTableConfigurations() {
        return dataTableConfigurations;
    }

    public void setDataTableConfigurations(Collection<DatatableConfiguration> dataTableConfigurations) {
        this.dataTableConfigurations = dataTableConfigurations;
    }


    public ClientConfiguration(ClientConfigurationRequest request){
        this.clientType = request.getClientType();
        this.attributeToVariableMappingForCreation = new JSONObject(request.getAttributeToVariableMappingForCreation());
        this.dataTableConfigurations = new ArrayList<>(0);
    }

    public ClientConfiguration(JSONObject json){
        this.clientType = json.optString("clientType", "");
        this.attributeToVariableMappingForCreation = json.optJSONObject("attributeToVariableMappingForCreation");
        this.attributeToVariableMappingForCreation = this.attributeToVariableMappingForCreation == null ?
                new JSONObject() : this.attributeToVariableMappingForCreation;
        JSONArray dataTables = json.optJSONArray("dataTableConfigurations");
        dataTables = dataTables == null ? new JSONArray() : dataTables;
        this.dataTableConfigurations = new ArrayList<>(dataTables.length());
        for (int i=0;i < dataTables.length(); i++){
            this.dataTableConfigurations.add(new DatatableConfiguration(dataTables.optJSONObject(i)));
        }
    }


    public static void populateMap(ClientConfiguration clientConfiguration, Map<String, Object> map,
                                   Map<String, Object> processVariables){

        JSONObject mapping = clientConfiguration.getAttributeToVariableMappingForCreation();
        for (Object attributeName : mapping.keySet()){
            map.put(attributeName.toString(),
                    String.valueOf(processVariables.getOrDefault(mapping.optString(attributeName.toString()), "")));
        }
    }


    public void createOrUpateDatatable(DatatableConfiguration.DatatableConfigurationRequest request){
        String dataTableName = request.getDataTableName();
        DatatableConfiguration datatableConfiguration = this.getDatatableConfigurationByDataTableName(dataTableName);
        if (Objects.isNull(datatableConfiguration)) {
            datatableConfiguration = new DatatableConfiguration(request);
            this.dataTableConfigurations.add(datatableConfiguration);
            return;
        }
        datatableConfiguration.setAttributeToVariableMapping(new JSONObject(request.getAttributeToVariableMapping()));
    }

    public void deleteDatatable(DatatableConfiguration.DatatableConfigurationRequest request){
        String dataTableName = request.getDataTableName();
        DatatableConfiguration datatableConfiguration = this.getDatatableConfigurationByDataTableName(dataTableName);
        if (Objects.isNull(datatableConfiguration)) {
            throw new RuntimeException("datatable: " + dataTableName + " doesn't exist for this (journeyIdentifier,client) pair");
        }
        this.dataTableConfigurations.remove(datatableConfiguration);
    }

    private DatatableConfiguration getDatatableConfigurationByDataTableName(String dataTableName){
        for (DatatableConfiguration datatableConfiguration : this.dataTableConfigurations){
            if (datatableConfiguration.getDataTableName().equalsIgnoreCase(dataTableName))
                return datatableConfiguration;
        }
        return null;
    }


    public static class ClientConfigurationRequest{
        private String clientType;

        private Map<String, String> attributeToVariableMappingForCreation;

        public String getClientType() {
            return clientType;
        }

        public void setClientType(String clientType) {
            this.clientType = clientType;
        }

        public Map<String, String> getAttributeToVariableMappingForCreation() {
            return attributeToVariableMappingForCreation;
        }

        public void setAttributeToVariableMappingForCreation(Map<String, String> attributeToVariableMappingForCreation) {
            this.attributeToVariableMappingForCreation = attributeToVariableMappingForCreation;
        }
    }

    @Override
    public String toString(){
        try {
            return objectMapper.writeValueAsString(this);
        }catch (JsonProcessingException e){
            return "JsonProcessingException";
        }
    }
}
