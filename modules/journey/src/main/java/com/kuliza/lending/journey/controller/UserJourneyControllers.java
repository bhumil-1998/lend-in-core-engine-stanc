package com.kuliza.lending.journey.controller;

import com.kuliza.lending.authorization.constants.Constants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.journey.pojo.*;
import com.kuliza.lending.journey.service.UserJourneyServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/lending/journey")
public class UserJourneyControllers {

	@Autowired
	private UserJourneyServices userJourneyServices;

	@RequestMapping(method = RequestMethod.GET, value = "/initiate")
	public ResponseEntity<Object> initiateProcess(Principal principal,
			@RequestParam(required = true, value = "journeyName") String requestJourneyName, @ApiParam(value = "Map" , required = true, example = "currentUserName=user1@kuiza.com") @RequestParam Map<String, Object> map) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.startOrResumeProcess(principal.getName(), requestJourneyName, map));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/submit-form")
	public ResponseEntity<Object> allSubmit(Principal principal, @RequestBody SubmitFormClass input) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.submitFormData(principal.getName(), input));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/submit-form")
	public ResponseEntity<Object> getPresentState(Principal principal, @RequestParam String processInstanceId) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.getFormData(principal.getName(), processInstanceId));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/back")
	public ResponseEntity<Object> moveBack(Principal principal, @Valid @RequestBody BackTaskInput input,
			BindingResult result) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.changeProcessState(principal.getName(), input, result));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public ResponseEntity<Object> loginUser(@RequestBody @Valid CustomerLoginInput customerLoginInput) {
		return CommonHelperFunctions.buildResponseEntity(userJourneyServices.loginUser(customerLoginInput));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/back-to-journey")
	public ResponseEntity<Object> backToJourney(@RequestBody @Valid BackToJourneyInput backToJourneyInput) {
		return CommonHelperFunctions.buildResponseEntity(userJourneyServices.setUserData(backToJourneyInput));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/initiate-by-portal")
	public ResponseEntity<Object> initiateProcessByPortal(
			@RequestBody @Valid InitiateFromPortalInput initiateFromPortalInput) {
		return CommonHelperFunctions.buildResponseEntity(
				userJourneyServices.startOrResumeProcessFromPortal(initiateFromPortalInput.getInitiator(),
						initiateFromPortalInput.getJourneyKey(), initiateFromPortalInput.getVariablesToSave()));
	}

	//TODO: Delete below code once all implementations are using this branch as base branch.
	/***
	 * Password hashing for all journey users.
	 *
	 * @return Success if all users hashing is completed else Error.
	 */
	@GetMapping("/encode-user-passwords")
	@ApiOperation("Encode journey users password")
	public ResponseEntity<Object> encodeJourneyUsersPasswords() {
		return CommonHelperFunctions.buildResponseEntity(userJourneyServices.hashPasswordForAllJourneyUsers());
	}

}
