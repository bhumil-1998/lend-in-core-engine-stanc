package com.kuliza.lending.journey;

import static org.junit.Assert.assertEquals;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.test.Deployment;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@LOSAppSpringBootTest
@ExtendWith(SpringExtension.class)
public class HttpTaskIntegrationTest {

  @Autowired
  private RuntimeService runtimeService;
  @Autowired
  private TaskService taskService;

  @Test
  @Deployment(resources = {"HttpTaskIBIntegrationTest.bpmn20.xml"})
  public void ibIntegrationTest() {
    String procId = runtimeService.startProcessInstanceByKey("lallalnnsdfdrs").getId();
    Task task = taskService.createTaskQuery().processInstanceId(procId).active().list().get(0);
    Object value = taskService.getVariable(task.getId(), "IB_Status_Code");
    assertEquals("200", value.toString());
    taskService.complete(task.getId());
  }

}
