package com.kuliza.lending.engine_common.models;

import java.io.IOException;
import java.util.List;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectConverter implements AttributeConverter<Object, String>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectConverter.class);

	@Override
	public String convertToDatabaseColumn(Object originalValue) {
		ObjectMapper objectMapper = new ObjectMapper();
		String dbValue = null;
		try {
			if (originalValue != null) {
				dbValue = objectMapper.writeValueAsString(originalValue);
			}
		} catch (final JsonProcessingException e) {
			LOGGER.error("JSON writing error", e);
		}

		return dbValue;
	}

	@Override
	public Object convertToEntityAttribute(String dbValue) {
		ObjectMapper objectMapper = new ObjectMapper();
		Object originalValue = null;
		try {
			if (dbValue != null) {
				originalValue = objectMapper.readValue(dbValue, new TypeReference<Object>() {});
			}
		} catch (final IOException e) {
			LOGGER.error("JSON reading error", e);
		}

		return originalValue;
	}
}
