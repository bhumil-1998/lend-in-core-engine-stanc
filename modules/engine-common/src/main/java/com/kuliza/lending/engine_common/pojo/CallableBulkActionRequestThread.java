package com.kuliza.lending.engine_common.pojo;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.engine_common.services.CustomActionService;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.Callable;

public class CallableBulkActionRequestThread implements Callable<ApiResponse> {
    private String slug;
    private CustomActionService customActionService;
    private CustomBulkActionRequest customBulkActionRequest;
    private String contentType;
    private String token;
    private RequestAttributes context;
    private String roleName;
    private String userId;
    private HttpServletRequest request;

    public CallableBulkActionRequestThread(HttpServletRequest request, String slug, CustomActionService customActionService, CustomBulkActionRequest customBulkActionRequest,
                                           String contentType, String token, RequestAttributes context, String roleName, String userId) {
        this.slug = slug;
        this.customActionService = customActionService;
        this.customBulkActionRequest = customBulkActionRequest;
        this.contentType = contentType;
        this.token = token;
        this.context = context;
        this.roleName = roleName;
        this.userId = userId;
        this.request = request;
    }

    public ApiResponse call() throws Exception {
        ApiResponse response = null;
        if (context != null) {
            RequestContextHolder.setRequestAttributes(context);
        }
        response = this.customActionService.fireCustomAction(request, slug, customBulkActionRequest.getCustomActionRequest(),
            contentType, token, customBulkActionRequest.getCaseInstanceId(), null, roleName, userId);
        return response;
    }
}
