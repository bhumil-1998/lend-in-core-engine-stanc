package com.kuliza.lending.engine_common.configs;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "dms")
public class DmsConfig {

    private String startServiceNameSymbol;

    private String endServiceNameSymbol;

    private String DMSServiceName;

    private String host;

    private String subUrl;

    private String protocol;

    private int port;

    public String getStartServiceNameSymbol() {
        return startServiceNameSymbol;
    }

    public void setStartServiceNameSymbol(String startServiceNameSymbol) {
        this.startServiceNameSymbol = startServiceNameSymbol;
    }

    public String getEndServiceNameSymbol() {
        return endServiceNameSymbol;
    }

    public void setEndServiceNameSymbol(String endServiceNameSymbol) {
        this.endServiceNameSymbol = endServiceNameSymbol;
    }

    public String getDMSServiceName() {
        return DMSServiceName;
    }

    public void setDMSServiceName(String DMSServiceName) {
        this.DMSServiceName = DMSServiceName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSubUrl() {
        return subUrl;
    }

    public void setSubUrl(String subUrl) {
        this.subUrl = subUrl;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
