package com.kuliza.lending.engine_common.utils;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.pojo.CustomActionHeaders;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomActionHelperFunctions {

    public static ArrayList<CustomActionHeaders> getCustomActionHeadersList(Object customActionHeadersListObject) {
        ArrayList<CustomActionHeaders> response = new ArrayList<>();
        if (customActionHeadersListObject != null) {
            JSONArray customActionHeaderStringList = null;
            if (customActionHeadersListObject instanceof ArrayList) {
                customActionHeaderStringList = new JSONArray((List<Map<String, Object>>) customActionHeadersListObject);
            } else {
                customActionHeaderStringList = new JSONArray(CommonHelperFunctions.getStringValue(customActionHeadersListObject));
            }
            for (int i=0; i < customActionHeaderStringList.length(); i++) {
                if (customActionHeaderStringList.get(i) != null) {
                    CustomActionHeaders customActionHeadersConfiguration = new CustomActionHeaders();
                    Map<String, Object> customActionHeadersConfigurationMap = CommonHelperFunctions.fromJson(customActionHeaderStringList.getJSONObject(i));
                    customActionHeadersConfiguration.setKey(CommonHelperFunctions
                            .getStringValue(customActionHeadersConfigurationMap.getOrDefault("key", null)));
                    customActionHeadersConfiguration.setValue(CommonHelperFunctions
                            .getStringValue(customActionHeadersConfigurationMap.getOrDefault("value", null)));
                    response.add(customActionHeadersConfiguration);
                }
            }
        }
        return response;
    }


    public static String setPathVariables(String url, Map<String, String> pathVariables){
        String pathVariablesUrl = url;
        if(pathVariables != null && !pathVariables.isEmpty() && pathVariables.size() > 0){
            for(String key : pathVariables.keySet()){
                pathVariablesUrl = pathVariablesUrl.replace("/"+key+"/",
                        "/" + pathVariables.get(key) + "/");
                if((pathVariablesUrl.indexOf("/"+key) + ("/"+key).length() ) == pathVariablesUrl.length()){
                    pathVariablesUrl = pathVariablesUrl.replace("/"+key, "/" + pathVariables.get(key));
                }
            }
        }
        return pathVariablesUrl;
    }

    public static String setRequestParams(String url, Map<String, String> requestParams){
        String requestParamUrl = url;
        if(requestParams != null && !requestParams.isEmpty() && requestParams.size() > 0){
            requestParamUrl = requestParamUrl + "?";
            int mapSize = requestParams.size(), count = 1;
            for(String key: requestParams.keySet()) {
                requestParamUrl = requestParamUrl + key + "=" + requestParams.get(key);
                if(count < mapSize)
                {
                    requestParamUrl = requestParamUrl + "&";
                }
                count++;
            }
        }
        return requestParamUrl;
    }

}
