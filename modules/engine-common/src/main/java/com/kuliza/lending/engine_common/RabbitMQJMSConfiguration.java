package com.kuliza.lending.engine_common;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rabbitmq.jms.admin.RMQConnectionFactory;

import org.springframework.jms.core.JmsTemplate;

@Configuration
@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQJMS")
public class RabbitMQJMSConfiguration {
	
	 @Value(value = "${broker.host}")
	 private String hostname;
	
	 @Value(value="${broker.port}")
	 private Integer port;
	 
	 @Value(value="${broker.username}")
	 private String username;
	 
	 @Value(value="${broker.password}")
	 private String password;
	 
	 @Value(value="${broker.VHost}")
	 private String virtualHost;
	 
	 RMQConnectionFactory connectionFactory = null;
	
	@Bean
	@ConditionalOnProperty(name = "broker.name", havingValue = "RABBITMQJMS", matchIfMissing = true)
	public ConnectionFactory jmsConnectionFactory() {
		connectionFactory = new RMQConnectionFactory();
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);
		connectionFactory.setVirtualHost(virtualHost);
		connectionFactory.setHost(hostname);
		connectionFactory.setPort(port);
		return connectionFactory;
	}

	// @Bean
	// public Destination jmsDestination() {
	// RMQDestination jmsDestination = new RMQDestination();
	// jmsDestination.setDestinationName(Constants.JMS_AUDIT_ROUTING_KEY);
	// jmsDestination.setAmqp(true);
	// jmsDestination.setAmqpQueueName(Constants.JMS_AUDIT_ROUTING_KEY);
	// return jmsDestination;
	// }

	@Bean(name = "jmsTemplateRMQ")
	public JmsTemplate jmsTemplateRMQ(ConnectionFactory connectionFactory) throws JMSException {
		return new JmsTemplate(connectionFactory);
	}

	// @Bean
	// public DefaultMessageListenerContainer jmsListener(ConnectionFactory
	// connectionFactory) {
	// DefaultMessageListenerContainer jmsListener = new
	// DefaultMessageListenerContainer();
	// jmsListener.setConnectionFactory(connectionFactory);
	// jmsListener.setDestinationName(Constants.JMS_AUDIT_ROUTING_KEY);
	// jmsListener.setConcurrency("4-6");
	// jmsListener.setPubSubDomain(true);
	// MessageListenerAdapter adapter = new MessageListenerAdapter(new Receiver());
	// adapter.setDefaultListenerMethod("receive");
	// jmsListener.setMessageListener(adapter);
	// return jmsListener;
	// }

	

}
