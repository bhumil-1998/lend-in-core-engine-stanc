package com.kuliza.lending.engine_common.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "broker")
public class BrokerConfigs {
	@Value("${broker.name.jms}")
	boolean jmsEnable;
	
	@Value("${broker.name}")
	String brokerName;

	public boolean isJmsEnable() {
		return jmsEnable;
	}

	public void setJmsEnable(boolean jmsEnable) {
		this.jmsEnable = jmsEnable;
	}

	public String getBrokerName() {
		return brokerName;
	}

	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
}
