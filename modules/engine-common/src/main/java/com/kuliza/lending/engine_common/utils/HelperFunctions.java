package com.kuliza.lending.engine_common.utils;

import static com.kuliza.lending.common.utils.Constants.ID_KEY;
import static com.kuliza.lending.common.utils.Constants.PARAMS_KEY;
import static com.kuliza.lending.common.utils.Constants.TYPE_KEY;
import static com.kuliza.lending.common.utils.Constants.URL_KEY;
import static com.kuliza.lending.common.utils.Constants.VALUE_KEY;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.configs.URLConfiguratorComponent;
import com.kuliza.lending.engine_common.utils.EnumConstants.URL_CONVERTER_IDENTIFIER;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.DmsConfig;
import org.flowable.form.model.FormField;
import org.flowable.form.model.Option;
import org.flowable.form.model.OptionFormField;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelperFunctions {


	private static final Logger LOGGER = LoggerFactory.getLogger(HelperFunctions.class);

    	/**
	 * Extract a String from the given TextMessage.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting String
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static String extractStringFromMessage(TextMessage message) throws JMSException {
		return message.getText();
	}

	/**
	 * Extract a byte array from the given {@link BytesMessage}.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting byte array
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static byte[] extractByteArrayFromMessage(BytesMessage message) throws JMSException {
		byte[] bytes = new byte[(int) message.getBodyLength()];
		message.readBytes(bytes);
		return bytes;
	}

	/**
	 * Extract a Map from the given {@link MapMessage}.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting Map
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static Map<String, Object> extractMapFromMessage(MapMessage message) throws JMSException {
		Map<String, Object> map = new HashMap<>();
		Enumeration<?> en = message.getMapNames();
		while (en.hasMoreElements()) {
			String key = (String) en.nextElement();
			map.put(key, message.getObject(key));
		}
		return map;
	}

	/**
	 * Extract a Serializable object from the given {@link ObjectMessage}.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting Serializable object
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static Serializable extractSerializableFromMessage(ObjectMessage message) throws JMSException {
		return message.getObject();
	}

    public static String getDMSUrl(){
        return StaticContextAccessor.getBean(DmsConfig.class).getProtocol() + "://" +
                StaticContextAccessor.getBean(DmsConfig.class).getHost() + ":" +
                StaticContextAccessor.getBean(DmsConfig.class).getPort() +
                StaticContextAccessor.getBean(DmsConfig.class).getSubUrl();
    }
    public static List<Map<String, Object>> getMasterDataList(Map<String, Object> map) {
		List<Map<String,Object> > masterDataList = new ArrayList<>();
		for(String key : map.keySet()) {
			Map<String,Object> m = new HashMap<>();
			if(key == null || key.equals(""))
				continue;
			m.put("key" , key);
			m.put("value" , map.get(key));
			masterDataList.add(m);
		}
		return masterDataList;
    }

	public static String replaceServiceNameWithUrl(String url, URL_CONVERTER_IDENTIFIER identifier) {
		// URL Replacement
		Pattern hostNameMeta = Pattern.compile(Constants.HOSTNAME_META_REGEX);
		Matcher matchPattern = hostNameMeta.matcher(url);
		String finalURL = url;
		while (matchPattern.find()) {
			String serviceName = matchPattern.group().replaceAll(" ", "");
			serviceName = serviceName.substring(2, serviceName.length()-2).toUpperCase();
			String replacementURL = "";
			switch (identifier) {
				case PORTAL:
					replacementURL = StaticContextAccessor.getBean(URLConfiguratorComponent.class)
							.getPortalServiceServiceNamesToHostNames()
							.getOrDefault(serviceName, "");
					break;
				case JOURNEY:
					replacementURL = StaticContextAccessor.getBean(URLConfiguratorComponent.class)
							.getServiceServiceNamesToHostNames()
							.getOrDefault(serviceName, "");
					break;
			}
			finalURL = matchPattern.replaceFirst(replacementURL);
		}
		return finalURL;
	}

	public static Map<String, Object> getPresentScreenInfo(Map<String, FormField> formFields,
			URL_CONVERTER_IDENTIFIER identifier) throws Exception {
		Map<String, Object> formData = new HashMap<>();
		for (Map.Entry<String, FormField> entry : formFields.entrySet()) {
			FormField property = entry.getValue();
			Map<String, Object> tempMap = new HashMap<>();
			LOGGER.info(String
					.format(Constants.PRESENT_INFO_LOGGER, entry.getKey(), entry.getValue().getName(),
							entry.getValue().getType(), entry.getValue().getId(), entry.getValue().getValue()));
			if (property.getType().equals(Constants.DROPDOWN_KEY) || property.getType()
					.equals(Constants.RADIO_BUTTONS_KEY)) {
				OptionFormField property2 = (OptionFormField) property;
				List<Option> options = property2.getOptions();
				if (options.get(0).getId() == null) {
					options.remove(0);
				}
				tempMap.put(Constants.ENUM_VALUES_KEY, options);
			} else {
				tempMap.put(Constants.ENUM_VALUES_KEY, null);
			}
			tempMap.put(Constants.NAME_KEY, property.getName());
			tempMap.put(ID_KEY, property.getId());
			tempMap.put(Constants.IS_WRITABLE_KEY, !property.isReadOnly());
			tempMap.put(Constants.IS_REQUIRED_KEY, property.isRequired());
			tempMap.put(VALUE_KEY, property.getValue());
			tempMap.put(TYPE_KEY, property.getType());
			Map<String, Object> params = property.getParams();
			Map<String, Object> metaData = (Map<String, Object>) params
					.getOrDefault(Constants.META_KEY, new HashMap<>());
			if ((CommonHelperFunctions.getStringValue(metaData.get(TYPE_KEY))
					.equals(Constants.REST_KEY)
					|| CommonHelperFunctions.getStringValue(metaData.get(Constants.FORM_TYPE_KEY))
					.equals(Constants.REST_DROPDOWN_KEY)
					|| CommonHelperFunctions.getStringValue(metaData.get(Constants.FORM_TYPE_KEY))
					.equals(Constants.UPLOAD_KEY)
					|| CommonHelperFunctions.getStringValue(metaData.get(TYPE_KEY))
					.equals(Constants.UPLOAD_KEY))) {
				if (metaData.get(Constants.API_KEY) instanceof Map) {
					Map<String, Object> apiData = (Map<String, Object>) metaData
							.getOrDefault(Constants.API_KEY, new HashMap<>());
					apiData.put(URL_KEY, replaceServiceNameWithUrl(
									CommonHelperFunctions.getStringValue(apiData.get(URL_KEY)),
									identifier));
					metaData.put(Constants.API_KEY, apiData);
				} else if (metaData.get(Constants.API_KEY) instanceof String
						&& metaData.get(Constants.API_KEY).toString().length() > 0) {
					JSONArray apisData = new JSONArray(metaData.get(Constants.API_KEY).toString());
					JSONArray newApisData = new JSONArray();
					for (int i = 0; i < apisData.length(); i++) {
						JSONObject apiData = apisData.getJSONObject(i);
						apiData.put(URL_KEY, replaceServiceNameWithUrl(
										CommonHelperFunctions.getStringValue(apiData.get(URL_KEY)),
										identifier));
						newApisData.put(apiData);
					}
					metaData.put(Constants.API_KEY, newApisData.toString());
				}
			}
			params.put(Constants.META_KEY, metaData);
			tempMap.put(PARAMS_KEY, params);
			formData.put(property.getId(), tempMap);
		}
		return formData;
	}
}
