package com.kuliza.lending.engine_common.utils;

import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static final String PORTAL_INITIATE_API_URL = "/lending/case/initiate";
	public static final String BACK_TO_JOURNEY_API_URL = "/lending/journey/back-to-journey";
	public static final String JOURNEY_INITIATE_BY_PORTAL_API_URL = "/lending/journey/initiate-by-portal";
    public static final String MASTER_DATA = "/get-master-data/{slug}";

	public static final String EXCHANGE_NAME = "exchange";
	public static final String QUEUE_GENERIC_NAME = "generic-queue";
	public static final String QUEUE_SPECIFIC_NAME = "task-queue";
	public static final String AUDIT_ROUTING_KEY = "route.audit";
	public static final String AUDIT_QUEUE_NAME = "audit-queue";
	public static final String ROUTING_KEY = "route.generic";
	public static final Integer AUDIT_MAX_RETRY_COUNT = 3;
	public static final Integer AUDIT_INITIAL_INTERVAL = 2000;
	public static final Integer AUDIT_MULTIPLIER = 2;
	public static final Integer AUDIT_MAX_INTERVAL = 10000;
	public static final String JMS_AUDIT_ROUTING_KEY = "route.jms.auditQueue";
	
	//Constants for JMS
	public static final String JMS_CONTAINER_CONCURRENCY = "5-10";
	
	public static final String EMPTY_STRING = "";

	//Constants for Logging
	public static final String LOG_RESPONSE = "For user {} for processInstanceId {} for ApplicationId {} for LosId {} Outgoing requestbody is: {}";
	public static final String LOG_REQUEST = "For user {} for processInstanceId {} for ApplicationId {} for LosId {} Incoming response is: {}";

	public static final String HOSTNAME_META_REGEX = "\\{\\{(.*?)}}";

	// Present Screen Info Constants
	public static final String FORM_TYPE_KEY = "form_type";
	public static final String API_KEY = "api";
	public static final String REST_KEY = "rest";
	public static final String REST_DROPDOWN_KEY = "rest-dropdown";
	public static final String UPLOAD_KEY = "upload";
	public static final String META_KEY = "meta";
	public static final String RADIO_BUTTONS_KEY = "radio-buttons";
	public static final String DROPDOWN_KEY = "dropdown";
	public static final String ENUM_VALUES_KEY = "enumValues";
	public static final String NAME_KEY = "name";
	public static final String IS_WRITABLE_KEY = "isWritable";
	public static final String IS_REQUIRED_KEY = "isRequired";
	public static final String PRESENT_INFO_LOGGER = "getPresentScreenInfo ======== entry.getKey() : %s , === Name : %s , === Type : %s , === ID : %s , === Value : %s";


	public static final String UPDATE_SERVICE_TASK_SUCCESS_RESPONSE = "%s Rows updated Sucessfully for model: %s";
	public static final String SET_UPDATE_PARAM_MESSAGE = "Setting parameter : %s having value: %s";
	public static final String UPDATE_QUERY_MESSAGE = "Update Query using modelUpdateQuery Task : %s";
	public static final String UNIQUE_ID_NOT_FOUND_MESSAGE = "Unique Identifier not found for: %s";
	public static final String SET_CLAUSE_NOT_FOUND_MESSAGE = "Set Clause is missing in the query";
	public static final String REGEX_FOR_QUERY_PARAMETER = ":\\b\\S+";
	public static final String UPDATE_QUERY_UPDATE_KEYWORD = "UPDATE ";
	public static final String SELECT_QUERY_KEYWORD = "SELECT ";
	public static final String UPDATE_QUERY_SET_KEYWORD = " SET ";
	public static final String QUERY_WHERE_KEYWORD = " WHERE ";
	public static final String QUERY_TABLE_DUMMY_KEYWORD = " tableName ";
	public static final String SELECT_QUERY_MESSAGE = "Select Query using modelQuery Task : %s";
	public static final String PROJECT_PACKAGE_PREFIX = "com.kuliza.lending.";
	public static final String MAX_LEAD_ID_QUERY_WHERE_CLAUSE = "";
	public static final String MAX_LEAD_ID_QUERY_SELECT_CLAUSE = " max(leadId) ";
	public static final String DEFAULT_LEAD_ID_CLASSPATH = "wf_implementation.models.WfUserLeads";
	public static final String LEAD_ID_DATE_FORMAT = "yyMMdd";
	public static final String SELECT_QUERY_FROM_KEYWORD = " from ";
	public static final String INCREMENTAL_LEAD_FORMAT = "%06d";


	public static final Map<String, String> modelIdentifierMap;
	static {
		modelIdentifierMap = new HashMap<String, String>();
		modelIdentifierMap.put("WorkFlowUser", "idmUserName");
		modelIdentifierMap.put("WorkFlowUserApplication", "procInstId");
		modelIdentifierMap.put("WorkFlowUserDocs", "documentId");
		modelIdentifierMap.put("WfUserLeads", "leadId");
	}

}
