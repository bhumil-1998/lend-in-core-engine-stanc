package com.kuliza.lending.engine_common;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQPrefetchPolicy;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.broker.region.policy.RedeliveryPolicyMap;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.kuliza.lending.engine_common.utils.Constants;

@Component
@ConditionalOnProperty(name="broker.name", havingValue="ACTIVEMQ")
public class ActiveMQconfiguration {
	@Value(value = "${broker.host}")
	 private String hostname;
	 
	 @Value(value="${broker.username}")
	 private String username;
	 
	 @Value(value="${broker.password}")
	 private String password;
	
	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="ACTIVEMQ",matchIfMissing=true)
	public ActiveMQConnectionFactory connectionFactory(){
	    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	    connectionFactory.setBrokerURL(hostname);
	    connectionFactory.setPassword(password);
	    connectionFactory.setUserName(username);
	    connectionFactory.setPrefetchPolicy(getPrefetchPolicy());
	    connectionFactory.setRedeliveryPolicy(getRedeliveryPolicy());
	    return connectionFactory;
	}
	
	public RedeliveryPolicy getRedeliveryPolicy() {
		RedeliveryPolicy queuePolicy = new RedeliveryPolicy();
		queuePolicy.setInitialRedeliveryDelay(1000);
		queuePolicy.setRedeliveryDelay(1000);
		queuePolicy.setUseExponentialBackOff(true);
		queuePolicy.setMaximumRedeliveries(3);
		queuePolicy.setBackOffMultiplier(2.0);
		return queuePolicy;
	}
	
	public ActiveMQPrefetchPolicy getPrefetchPolicy() {
		ActiveMQPrefetchPolicy prefetchPolicy = new ActiveMQPrefetchPolicy();
		prefetchPolicy.setQueuePrefetch(1);
		return prefetchPolicy;
	}
	public RedeliveryPolicyMap getQueueBasedRetryPolicy() {
		//RedeliveryPolicyMap map = connection.getRedeliveryPolicyMap();
		RedeliveryPolicyMap map = new RedeliveryPolicyMap();
		map.put(new ActiveMQQueue(Constants.JMS_AUDIT_ROUTING_KEY), getRedeliveryPolicy());
		return map;
	}

}
