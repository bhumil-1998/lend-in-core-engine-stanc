package com.kuliza.lending.engine_common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GoogleRecaptchaConfig {
    
	@Value("${google.recaptcha.secret}")
	String recaptchaSecret;

	@Value("${google.recaptcha.verify.url}")
	String recaptchaVerifyURL;

	@Value("${google.recaptcha.score.threshold}")
	Double recaptchaScoreThreshold;

	public String getRecaptchaSecret() {
		return recaptchaSecret;
	}

	public void setRecaptchaSecret(String recaptchaSecret) {
		this.recaptchaSecret = recaptchaSecret;
	}

	public String getRecaptchaVerifyURL() {
		return recaptchaVerifyURL;
	}

	public void setRecaptchaVerifyURL(String recaptchaVerifyURL) {
		this.recaptchaVerifyURL = recaptchaVerifyURL;
	}

	public Double getRecaptchaScoreThreshold() {
		return recaptchaScoreThreshold;
	}

	public void setRecaptchaScoreThreshold(Double recaptchaScoreThreshold) {
		this.recaptchaScoreThreshold = recaptchaScoreThreshold;
	}
}
