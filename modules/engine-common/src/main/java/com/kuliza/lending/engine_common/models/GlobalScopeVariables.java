package com.kuliza.lending.engine_common.models;

import com.kuliza.lending.common.model.BaseModel;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "global_scope_variables")
public class GlobalScopeVariables extends BaseModel{
	
	@Column
	private String variableKey;
	
	@Column
	@Convert(converter = ObjectConverter.class)
	private Object variableValue;
	
	public GlobalScopeVariables() {
		
	}
	
	public GlobalScopeVariables(String variableKey, Object variableValue) {
		this.variableKey = variableKey;
		this.variableValue = variableValue;
	}

	public String getVariableKey() {
		return variableKey;
	}

	public void setVariableKey(String variableKey) {
		this.variableKey = variableKey;
	}

	public Object getVariableValue() {
		return variableValue;
	}

	public void setVariableValue(Object variableValue) {
		this.variableValue = variableValue;
	}
	
	@Override
	public String toString() {
		return "GlobalScopeVariables [variableKey=" + variableKey + ", variableValue=" + variableValue + "]";
	}
	
}
