package com.kuliza.lending.engine_common.services;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.utils.EngineCommonConstants;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.engine_common.GoogleRecaptchaConfig;
import com.mashape.unirest.http.HttpResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.kuliza.lending.common.connection.UnirestConnection;

@Service
public class RecaptchaService {

	private static final Logger logger = LoggerFactory
			.getLogger(RecaptchaService.class);
	
	@Autowired
	private GoogleRecaptchaConfig recaptchaConfig;

	@LogMethodDetails
	public ApiResponse verifyRecaptcha(String ip, String recaptchaResponse) {
		Map<String, Object> body = new HashMap<>();
		body.put("secret", recaptchaConfig.getRecaptchaSecret());
		body.put("response", recaptchaResponse);
		body.put("remoteip", ip);
		logger.info("Request body for recaptcha: {}", body);
		try {
			HttpResponse<?> recaptchaResponseObj = UnirestConnection.sendPOST(body, null, recaptchaConfig.getRecaptchaVerifyURL()
					+ "?secret=" + body.get("secret") + "&response=" + body.get("response") + "&remoteip=" + body.get("remoteip"));
			
			HashMap<String, Object> responseBody = CommonHelperFunctions.getHashMapFromJsonString(CommonHelperFunctions.
					getStringValue(recaptchaResponseObj.getBody()));
			logger.info("Response from recaptcha: {}", responseBody);
			if (!CommonHelperFunctions.getBooleanValue(responseBody.get("success"))) {
				List<String> errorCodes = (List) responseBody.get("error-codes");
				String errorMessage = errorCodes.stream()
						.map(s -> EngineCommonConstants.RECAPTCHA_ERROR_CODE.get(s))
						.collect(Collectors.joining(", "));
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, errorMessage);
			} else {
				Double score = CommonHelperFunctions.getDoubleValue(responseBody.get("score"));
				logger.info("Score from google recaptcha: {}", score);
				if (score <= recaptchaConfig.getRecaptchaScoreThreshold()) {
					return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, EngineCommonConstants.RECAPTCHA_SCORE_ERROR_MESSAGE);
				}
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseBody);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}

}
