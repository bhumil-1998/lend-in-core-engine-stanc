package com.kuliza.lending.engine_common.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;

public class RetryRecoverer implements MessageRecoverer{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RetryRecoverer.class);

	@Override
	public void recover(Message message, Throwable cause) {
		// customize logger here
		if (LOGGER.isErrorEnabled()) {
			LOGGER.error("Retries exhausted for RabbitMQ for message " + message, cause);
		}
		throw new ListenerExecutionFailedException("Retry Policy Exhausted",
					new AmqpRejectAndDontRequeueException(cause), message);
		
	}
}
