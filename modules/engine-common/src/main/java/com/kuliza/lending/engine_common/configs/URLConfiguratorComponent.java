package com.kuliza.lending.engine_common.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@PropertySource("classpath:lend-in-modules.properties")
public class URLConfiguratorComponent {

	@Value("#{${ServiceNamesToHostNames}}")
	private Map<String, String> serviceServiceNamesToHostNames;

	@Value("#{${PortalServiceNamesToHostNames}}")
	private Map<String, String> portalServiceServiceNamesToHostNames;

	public Map<String, String> getPortalServiceServiceNamesToHostNames() {
		return portalServiceServiceNamesToHostNames;
	}

	public void setPortalServiceServiceNamesToHostNames(
			Map<String, String> portalServiceServiceNamesToHostNames) {
		this.portalServiceServiceNamesToHostNames = portalServiceServiceNamesToHostNames;
	}

	public Map<String, String> getServiceServiceNamesToHostNames() {
		return serviceServiceNamesToHostNames;
	}

	public void setServiceServiceNamesToHostNames(
			Map<String, String> serviceServiceNamesToHostNames) {
		this.serviceServiceNamesToHostNames = serviceServiceNamesToHostNames;
	}
}
