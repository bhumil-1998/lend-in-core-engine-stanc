package com.kuliza.lending.engine_common.services;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.utils.Constants;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import lombok.Synchronized;
import org.apache.kafka.common.protocol.types.Field.Str;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DatabaseQueryFunctions {

  private static final Logger logger = LoggerFactory.getLogger(DatabaseQueryFunctions.class);

  @PersistenceContext
  private EntityManager entityManager;

  /**This Service updates models using set and where clause
   *
   * @param processVariables
   * @param classPath : wf_implementation.models.WorkFlowUser
   * @param setClause : email = :email
   * @param whereClause : username = :assignee and isDeleted = 0 (Right side after : is proc var key)
   * @param identifierValue : workflowuser@15.com
   * @return Object
   */
  @Transactional
  public ApiResponse modelUpdateQuery(Map<String, Object> processVariables, String classPath,
      String setClause, String whereClause, Object identifierValue) {
    try {
      Class<?> clazz = Class.forName(Constants.PROJECT_PACKAGE_PREFIX + classPath);
      String sqlString = Constants.UPDATE_QUERY_UPDATE_KEYWORD	+ clazz.getSimpleName();
      if (setClause != null && !setClause.equals("")) {
        sqlString = sqlString + Constants.UPDATE_QUERY_SET_KEYWORD + setClause;
      } else {
        return new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase(),
            Constants.SET_CLAUSE_NOT_FOUND_MESSAGE);
      }
      sqlString = sqlString + Constants.QUERY_WHERE_KEYWORD;
      if (whereClause != null && !whereClause.equals("")) {
        sqlString = sqlString + " ( " + whereClause + " ) and ";
      }
      String idField = null;
      if (Constants.modelIdentifierMap.containsKey(clazz.getSimpleName())) {
        idField = Constants.modelIdentifierMap.get(clazz.getSimpleName());
      } else {
        while (!clazz.equals(Object.class) && idField == null) {
          for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Id.class)) {
              idField = field.getName();
              break;
            }
          }
          clazz = clazz.getSuperclass();
        }
      }
      if (idField != null && !idField.equals("") &&
          identifierValue != null && !identifierValue.equals("")) {
        sqlString = sqlString + idField + " = :" + idField;
      } else {
        return new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase(),
            String.format(Constants.UNIQUE_ID_NOT_FOUND_MESSAGE, classPath));
      }
      logger.debug(String.format(Constants.UPDATE_QUERY_MESSAGE, sqlString));
      Query query = entityManager.createQuery(sqlString);
      query = createQueryWithParameters(sqlString, query, processVariables);
      Integer count = query.executeUpdate();
      return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(),
          String.format(Constants.UPDATE_SERVICE_TASK_SUCCESS_RESPONSE,
              CommonHelperFunctions.getStringValue(count), classPath));
    } catch (Exception e) {
      logger.error(CommonHelperFunctions.getStackTrace(e));
      return new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase(),
          e.getMessage());

    }
  }

  /**
   * Sets the parameters from the input for the query processing
   * */
  public Query createQueryWithParameters(String sqlString, Query query,
      Map<String, Object> processVariables){
    Pattern pattern = Pattern.compile(Constants.REGEX_FOR_QUERY_PARAMETER);
    Matcher matcher = pattern.matcher(sqlString);
    while (matcher.find()) {
      String variable = matcher.group().substring(1);
      logger.debug(String.format(Constants.SET_UPDATE_PARAM_MESSAGE,
          variable, processVariables.get(variable)));
      query.setParameter(variable, processVariables.get(variable));
    }
    return query;
  }

  /**
   * Creates default leadPrefix - yymmdd
   * */
  public  String createPrefix(){
    return CommonHelperFunctions.getDateStringValue(new Date(), Constants.LEAD_ID_DATE_FORMAT);
  }

  /**
   * Creates default leadSuffix, default starting from 000000 incrementing by 1 each time
   * */
  private String createSuffix(String currentDate, Map<String, Object> processVariables) {
    ApiResponse apiResponse = modelSelectQuery(processVariables, Constants.DEFAULT_LEAD_ID_CLASSPATH,
        Constants.MAX_LEAD_ID_QUERY_SELECT_CLAUSE, Constants.MAX_LEAD_ID_QUERY_WHERE_CLAUSE);
    List<String> resultList = (List<String>) apiResponse.getData();
    String maxLeadId = resultList.get(0);
    String datePrefix = null;
    String counter = "0";
    if(maxLeadId!=null && !maxLeadId.isEmpty()){
      datePrefix = maxLeadId.substring(0,6);
      counter = maxLeadId.substring(6, 12);
    }
    int newLeadCounter = 0;
    if(currentDate.equalsIgnoreCase(datePrefix)) {
      newLeadCounter = Integer.parseInt(counter);
      newLeadCounter++;
    }
    return String.format(Constants.INCREMENTAL_LEAD_FORMAT,newLeadCounter);
  }

  /**
   * Creates default leadId
   * */
  @Synchronized
  public String createLead(Map<String, Object> processVariables){
    String datePrefix = createPrefix();
    return datePrefix + createSuffix(datePrefix, processVariables);
  }


  /**This Service updates models using set and where clause
   *
   * @param classPath : wf_implementation.models.WfUserLeads
   * @param selectClause : leadId
   * @param whereClause : username = :assignee and isDeleted = 0 (Right side after : is proc var key)
   * @return Object
   */
  @Transactional
  public ApiResponse modelSelectQuery(Map<String, Object> processVariables, String classPath,
      String selectClause, String whereClause) {
    try {
      Class<?> clazz = Class.forName(Constants.PROJECT_PACKAGE_PREFIX + classPath);
      String sqlString = Constants.SELECT_QUERY_KEYWORD;
      if (selectClause != null && !selectClause.equals("")) {
        sqlString = sqlString + selectClause;
      } else {
        sqlString = sqlString + Constants.QUERY_TABLE_DUMMY_KEYWORD;
      }
      sqlString = sqlString + Constants.SELECT_QUERY_FROM_KEYWORD + clazz.getSimpleName() +
          Constants.QUERY_TABLE_DUMMY_KEYWORD;
      if (whereClause != null && !whereClause.isEmpty()) {
        sqlString = sqlString + Constants.QUERY_WHERE_KEYWORD + " ( " + whereClause + " ) ";
      }
      logger.debug(String.format(Constants.SELECT_QUERY_MESSAGE, sqlString));
      Query query = entityManager.createQuery(sqlString);
      query = createQueryWithParameters(sqlString, query, processVariables);
      List resultList = query.getResultList();
      if (resultList == null || resultList.isEmpty()) {
        return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
      } else {
        return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), resultList);
      }
    } catch (Exception e) {
      logger.error(CommonHelperFunctions.getStackTrace(e));
      return new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase(),
          e.getMessage());

    }
  }

}
