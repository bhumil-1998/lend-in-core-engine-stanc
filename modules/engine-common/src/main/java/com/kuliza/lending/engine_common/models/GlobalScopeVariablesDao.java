package com.kuliza.lending.engine_common.models;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface GlobalScopeVariablesDao extends CrudRepository<GlobalScopeVariables, Long>{
	public GlobalScopeVariables findByVariableKey(String variableKey);
	public List<GlobalScopeVariables> findByIsDeleted(Boolean isDeleted);
	public GlobalScopeVariables findByVariableKeyAndIsDeleted(String variableKey,Boolean isDeleted);
	public List<GlobalScopeVariables> findAll();
}
