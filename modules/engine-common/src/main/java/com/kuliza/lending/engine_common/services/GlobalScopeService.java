package com.kuliza.lending.engine_common.services;

import com.kuliza.lending.engine_common.models.GlobalScopeVariables;
import com.kuliza.lending.engine_common.models.GlobalScopeVariablesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GlobalScopeService {

	@Autowired
	private GlobalScopeVariablesDao globalScopeVariablesDao;

	public Map<String, Object> getVariable(String key) {
		Map<String, Object> variables = new HashMap<>();
		GlobalScopeVariables globalScopeVariable = globalScopeVariablesDao.findByVariableKeyAndIsDeleted(key,false);
		if (globalScopeVariable != null) {
			variables.put(globalScopeVariable.getVariableKey(), globalScopeVariable.getVariableValue());
		}
		return variables;
	}

	public Map<String, Object> getVariables() {
		Map<String, Object> variables = new HashMap<>();
		List<GlobalScopeVariables> globalScopeVariables = new ArrayList<GlobalScopeVariables>();
		globalScopeVariables = globalScopeVariablesDao.findByIsDeleted(false);
		if (globalScopeVariables != null) {
			variables = getHashMap(globalScopeVariables);
		}
		return variables;
	}

	private Map<String, Object> getHashMap(List<GlobalScopeVariables> globalScopeVariables) {
		Map<String, Object> map = new HashMap<>();
		int size = globalScopeVariables.size();
		for (int i = 0; i < size; i++)
			map.put(globalScopeVariables.get(i).getVariableKey(), globalScopeVariables.get(i).getVariableValue());
		return map;
	}

	public void setVariable(String variableKey, Object variableValue) {
		GlobalScopeVariables globalScopeVariable = globalScopeVariablesDao.findByVariableKey(variableKey);
		if (globalScopeVariable == null) {
			GlobalScopeVariables variable = new GlobalScopeVariables(variableKey, variableValue);
			globalScopeVariablesDao.save(variable);
		} else {
			globalScopeVariable.setVariableValue(variableValue);
			globalScopeVariable.setIsDeleted(false);
			globalScopeVariablesDao.save(globalScopeVariable);
		}
	}

	public void setVariables(Map<String, Object> variableMap) {
		List<GlobalScopeVariables> globalScopeVariables = globalScopeVariablesDao.findAll();
		List<GlobalScopeVariables> variablesToSave = new ArrayList<>();
		for (String varibaleKey : variableMap.keySet()) {
			boolean isVariableExist = false;
			for (GlobalScopeVariables existingVariable : globalScopeVariables) {
				if (varibaleKey.equals(existingVariable.getVariableKey())) {
					existingVariable.setVariableValue(variableMap.get(varibaleKey));
					existingVariable.setIsDeleted(false);
					variablesToSave.add(existingVariable);
					isVariableExist = true;
					break;
				}
			}
			if (!isVariableExist) {
				GlobalScopeVariables newVariable = new GlobalScopeVariables(varibaleKey, variableMap.get(varibaleKey));
				variablesToSave.add(newVariable);
			}
		}
		globalScopeVariablesDao.save(variablesToSave);
	}

	private List<GlobalScopeVariables> getGlobalVariablesListFromMap(Map<String, Object> map) {
		List<GlobalScopeVariables> variables = new ArrayList<>();
		for (String key : map.keySet()) {
			GlobalScopeVariables variable = new GlobalScopeVariables(key, map.get(key));
			variables.add(variable);
		}
		return variables;
	}

	public void deleteVariable(String key) {
		GlobalScopeVariables globalScopeVariablesInDB = globalScopeVariablesDao.findByVariableKey(key);
		if (globalScopeVariablesInDB != null) {
			globalScopeVariablesInDB.setIsDeleted(true);
			globalScopeVariablesDao.save(globalScopeVariablesInDB);
		}
	}

	public void deleteAllVariables() {
		List<GlobalScopeVariables> globalScopeVariables = globalScopeVariablesDao.findAll();
		if (globalScopeVariables != null)
			for (GlobalScopeVariables variable : globalScopeVariables) {
				variable.setIsDeleted(true);
			}
	}

}
