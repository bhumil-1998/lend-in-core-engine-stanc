package com.kuliza.lending.engine_common.models;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.engine_common.converters.CustomActionHeadersConverter;
import com.kuliza.lending.engine_common.pojo.CustomActionHeaders;
import com.kuliza.lending.engine_common.utils.Enums;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

import static com.kuliza.lending.engine_common.utils.EngineCommonConstants.*;

@Entity
@Table(name = "custom_url_mapper", uniqueConstraints = {@UniqueConstraint(columnNames = {"slug"})})
public class CustomURLMapper extends BaseModel {

    @Column
//    @NotNull
//    @NotEmpty
    private String slug;

    @Column
    @ApiModelProperty(required = false, hidden = true)
    private String userId;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(nullable=false)
    @ColumnDefault("0")
    private Boolean openApi = false;

    @Column
    @NotNull
    @NotEmpty
    private String url;

    @Column
    private String portalType;

    @Column(columnDefinition = "LONGTEXT")
    @Convert(converter = CustomActionHeadersConverter.class)
    private List<CustomActionHeaders> headers;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @ColumnDefault("0")
    @Column(nullable=false)
    private Boolean externalApi = false;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Enums.REQUEST_TYPE requestType;

    @Transient
    @ApiModelProperty(required = false, hidden = true)
    private String bulkActionUrl;

    @Transient
    @ApiModelProperty(required = false, hidden = true)
    private String customActionUrl;

    public CustomURLMapper() {
        super();
        this.setIsDeleted(false);
    }

    public CustomURLMapper(String slug, String userId, String url, String portalType, List<CustomActionHeaders> headers,
                           boolean externalApi, Enums.REQUEST_TYPE requestType, boolean openApi) {
        this.slug = slug;
        this.userId = userId;
        this.url = url;
        this.portalType = portalType;
        this.headers = headers;
        this.externalApi = externalApi;
        this.requestType = requestType;
        this.openApi = openApi;
        this.bulkActionUrl = BULK_ACTION_URL + this.getId();
        this.customActionUrl = CUSTOM_ACTION_URL + this.getId();
        this.setIsDeleted(false);
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPortalType() {
        return portalType;
    }

    public void setPortalType(String portalType) {
        this.portalType = portalType;
    }

    public Enums.REQUEST_TYPE getRequestType() {
        return requestType;
    }

    public void setRequestType(Enums.REQUEST_TYPE requestType) {
        this.requestType = requestType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<CustomActionHeaders> getHeaders() {
        return headers;
    }

    public void setHeaders(List<CustomActionHeaders> headers) {
        this.headers = headers;
    }

    public boolean isExternalApi() {
        return externalApi;
    }

    public void setExternalApi(boolean externalApi) {
        this.externalApi = externalApi;
    }

    public boolean isOpenApi() {
        return openApi;
    }

    public void setOpenApi(boolean openApi) {
        this.openApi = openApi;
    }

    public String getBulkActionUrl() {
        return bulkActionUrl;
    }

    public void setBulkActionUrl() {
        this.bulkActionUrl = LENDIN_URL + BULK_ACTION_URL + this.slug;
    }

    public String getCustomActionUrl() {
        return customActionUrl;
    }

    public void setCustomActionUrl() {
        this.customActionUrl = LENDIN_URL + CUSTOM_ACTION_URL + this.slug;
    }

    @Override
    public String toString() {
        return "CustomURLMapper{" +
                "slug='" + slug + '\'' +
                ", userId='" + userId + '\'' +
                ", openApi=" + openApi +
                ", url='" + url + '\'' +
                ", portalType='" + portalType + '\'' +
                ", headers=" + headers +
                ", externalApi=" + externalApi +
                ", requestType=" + requestType +
                ", bulkActionUrl='" + bulkActionUrl + '\'' +
                ", customActionUrl='" + customActionUrl + '\'' +
                '}';
    }
}

