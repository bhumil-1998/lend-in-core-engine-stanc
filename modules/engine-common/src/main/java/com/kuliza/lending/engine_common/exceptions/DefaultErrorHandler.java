package com.kuliza.lending.engine_common.exceptions;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.listener.ErrorHandler;

import com.kuliza.lending.engine_common.KafkaConfiguration;

public class DefaultErrorHandler implements ErrorHandler {

    private static Logger log = LoggerFactory.getLogger(KafkaConfiguration.class);
	@Override
	public void handle(Exception t, ConsumerRecord<?, ?> record) {
		log.error("spring kafka custom error handler: " + record);
        log.error(t.getCause().getMessage());
	}
}