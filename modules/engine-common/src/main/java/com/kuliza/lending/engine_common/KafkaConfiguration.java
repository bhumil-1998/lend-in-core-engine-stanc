package com.kuliza.lending.engine_common;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import com.kuliza.lending.engine_common.exceptions.DefaultErrorHandler;

@EnableKafka
@Configuration
//@ConditionalOnExpression("${broker.name.kafka:true}")
public class KafkaConfiguration {

	@Value(value = "${broker.host}")
	private String bootstrapAddress;

	@Value(value = "${kafka.groupId}")
	private String groupId;

	@Bean
	//@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA", matchIfMissing = true)
	public ConsumerFactory<String, String> consumerFactory() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		return new DefaultKafkaConsumerFactory<>(props);
	}

	@Bean
	//@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA", matchIfMissing = true)
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> configs = new HashMap<>();
		configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		return new KafkaAdmin(configs);
	}

	@Bean
	@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA", matchIfMissing = true)
	public NewTopic topic1() {
		return new NewTopic("test", 1, (short) 1);
	}

	@Bean
	//@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA", matchIfMissing = true)
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		factory.setErrorHandler(new DefaultErrorHandler());
		return factory;
	}

	@Bean
	@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA", matchIfMissing = true)
	public ProducerFactory<String, String> producerFactory() {
		Map<String, Object> configProps = new HashMap<>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		return new DefaultKafkaProducerFactory<>(configProps);
	}

	@Bean
	@ConditionalOnProperty(name = "broker.name", havingValue = "KAFKA", matchIfMissing = true)
	public KafkaTemplate<String, String> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}

	// protected RetryTemplate retryTemplate() {
	// RetryTemplate template = new RetryTemplate();
	//
	// template.setRetryPolicy(retryPolicy());
	// template.setBackOffPolicy(backOffPolicy());
	//
	// return template;
	// }
	//
	// protected RetryPolicy retryPolicy() {
	// SimpleRetryPolicy policy = new SimpleRetryPolicy();
	// policy.setMaxAttempts(3);
	// return policy;
	// }
	//
	// protected BackOffPolicy backOffPolicy() {
	// ExponentialBackOffPolicy policy = new ExponentialBackOffPolicy();
	// policy.setInitialInterval(1000);
	// return policy;
	// }

}
