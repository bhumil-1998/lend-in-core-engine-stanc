package com.kuliza.lending.engine_common.pojo;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.kuliza.lending.engine_common.utils.EnumConstants;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonSubTypes({ @Type(value = Map.class), @Type(value = List.class), })
public class CustomMessageVariables implements Serializable {
	private static final long serialVersionUID = 1L;
	private String requestId;
	private EnumConstants.Events event;
	private Map<String, Object> functionArguments;

	public EnumConstants.Events getEvent() {
		return event;
	}

	public void setEvent(EnumConstants.Events event) {
		this.event = event;
	}

	public Map<String, Object> getFunctionArguments() {
		return functionArguments;
	}

	public void setFunctionArguments(Map<String, Object> functionArguments) {
		this.functionArguments = functionArguments;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public CustomMessageVariables() {
		super();
	}

	public CustomMessageVariables(String requestId, EnumConstants.Events event, Map<String, Object> functionArguments) {
		super();
		this.requestId = requestId;
		this.event = event;
		this.functionArguments = functionArguments;
	}

	@Override
	public String toString() {
		return "CustomMessageVariables [requestId=" + requestId + ", event=" + event + ", functionArguments="
				+ functionArguments + "]";
	}

}
