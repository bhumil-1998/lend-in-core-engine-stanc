***
# About engine-common module

*    All the custom actions are done through engine-common module. 
*    One of the examples of custom actions is like, if you want some functionality on the click of a button, you can configure that using this module.
*    Go through CustomActionController for the same.
*    The package 'configs' has some classes corresponding to some module. Every class contains variables. The value of these variables are fethed from application.properties file.
*    These properties(variables) must be present in application.properties.
*    Properties related to OTP(wf-implementation module) are present in lend-in-otp.properties. Add the following properties to bashrc. Modify the properties value according to your configuration.
    * export OTP_GENERATE_FLAG=true
    * export OTP_VALIDATE_DEFAULT_FLAG=false
    * export OTP_USE_CACHE_FLAG=false
    * export OTP_THRESHOLD_MINUTES=30
    * export OTP_LENGTH=6
    * export USER_BLOCK_TIME=30
    * export OTP_EXPIRY_TIME=3
    * export OTP_MAX_GENERATE_ATTEMPTS=5
    * export OTP_MAX_VALIDATE_ATTEMPTS=5
*    Properties related to audit queue(portal module) are present in lend-in-queue.properties.Add the following properties to bashrc. Modify the properties value according to your configuration.
    * export BROKER_HOST:'vm://embe?broker.persistent=false,useShutdownHook=false'
    * export BROKER_PORT:5672
    * export BROKER_USERNAME:'username'
    * export BROKER_PASSWORD:'password'
    * export BROKER_VHOST:'/'
    * export BROKER_NAME_JMS:true
    * export BROKER_NAME:'broker_name'  
    
    * export JMS_CONTAINER_CONCURRENCY=5-10  
    
    * export KAFKA_BOOTSTRAP_ADDRESS=localhost:8001
    * export KAFKA_GROUP_ID='group-id'
    
    * export MANAGEMENT_ENDPOINTS_WEB_EXPOSURE_INCLUDE=*
    * export MANAGEMENT_ENDPOINT_HEALTH_SHOW_DETAILS='always'

    * export SECURITY_USER_PASSWORD='password'
    * export SECURITY_USER_NAME='username'

    * export MANAGEMENT_SECURITY_ROLE='role'
    * export MANAGEMENT_SECURITY_ENABLED=false

    * export ACTUATOR_USERNAME=actuator
    * export ACTUATOR_PASSWORD=actuator
    
*    This module contains the dependency of common module.
