package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowUser;

@Repository
public interface WorkflowUserDao extends CrudRepository<WorkFlowUser, Long> {

	public WorkFlowUser findById(long id);

	public WorkFlowUser findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowUser findByUsernameAndIsDeleted(String username, boolean isDeleted);
	
	public WorkFlowUser findByIdmUserNameAndIsDeleted(String username, boolean isDeleted);

}
