package com.kuliza.lending.wf_implementation.utils;

import java.util.HashMap;
import java.util.Map;

public class WfImplConstants {
	
	private WfImplConstants() {
        throw new UnsupportedOperationException();
    }

	//Workflow Service Constants
	public static final String WORFKFLOW_USERNAME_CONSTANT = "workflowuser@%s.com";
	public static final String WORKFLOW_LIST_SUFFIX = "_list";
	public static final String WORKFLOW_TYPE = "workflowName";
	public static final String END_PROCESS_SUCCESS_MESSAGE = "Ended  Process Successfully.";
	public static final String NO_WF_APP_ERROR_MESSAGE = "No Root Workflow App for given Execution";
	public static final String NO_ROOT_PROC_ID_ERROR_MESSAGE = "No Root Proc Id Found";
	public static final String NO_ASSIGNEE_KEY_ERROR_MESSAGE = "No assignee key Found.";
	public static final String NO_UNIQUE_LEAD_ERROR_MESSAGE = "No unique lead found for the application.";
	public static final String ASSIGNEE_CHANGE_SUCCESS_MESSAGE = "Assignee Changed Successfully.";
	public static final String LIST_KEY_INVALID_ERROR_MESSAGE = "List Key Invalid.";
	public static final String VALIDATION_STATUS_KEY = "status";
	public static final String EXISTING_LEAD_KEY = "isExistingLead";

	public static final String WORKFLOW_API_ENDPOINT = "/workflow";
	public static final String WORKFLOW_INITIATE_API_ENDPOINT = "/initiate";
	public static final String WORKFLOW_INITIATE_BY_ASSIGNEE_API_ENDPOINT = "/initiate-by-assignee";
	public static final String WORKFLOW_PRIMARY_FORM_API_ENDPOINT = "/primary-form";
	public static final String WORKFLOW_SUBMIT_FORM_API_ENDPOINT = "/submit-form";
	public static final String WORKFLOW_BACK_API_ENDPOINT = "/back";
	public static final String WORKFLOW_ADD_SUB_JOURNEY_API_ENDPOINT = "/add-sub-journey";
	public static final String WORKFLOW_DELETE_SUB_JOURNEY_API_ENDPOINT = "/delete-sub-journey";
	public static final String WORKFLOW_BACK_TO_JOURNEY_API_ENDPOINT = "/back-to-journey";
	public static final String LEAD_KEYWORD = "lead";
	public static final String NO_LEAD_FORM_ERROR_MESSAGE = "No lead form found with key: ";
	public static final String LEAD_FORM_KEY_ERROR_MESSAGE = "Nomenclature is not correct for form key : ";
	public static final String LEAD_ASSIGNEE_ERROR_MESSAGE = "Assignee is null.";
	protected static final Map<String, String> modelIdentifierMap;
	public static final String LEAD_UNIQUE_KEY = "leadUniqueKey";
	public static final String PARENT_PROC_INST_ID_KEY = "parentProcessInstanceId";
	public static final String PROC_INST_ID_KEY = "processInstanceId";
	public static final String LEAD_API_ENDPOINT = "/lead";
	public static final String BACK_TO_CALL_ACTIVITY_ERROR_MESSAGE = "Some error is going back to call activity";
	public static final String SUB_JOURNEY_LIST_ERROR_MESSAGE = "Sub Journey list not found";
	public static final String PROCESS_NOT_ASSINED_ERROR_MESSAGE = "Process is not assigned to the user.";
	
	static {
		modelIdentifierMap = new HashMap<>();
		modelIdentifierMap.put("WorkFlowUser", "idmUserName");
		modelIdentifierMap.put("WorkFlowUserApplication", "procInstId");
		modelIdentifierMap.put("WorkFlowUserDocs", "documentId");
		modelIdentifierMap.put("WfUserLeads", "leadId");
	}
	
	//Validation Service Constants
	public static final String VALIDATION_API_ENDPOINT = "/validate";
	public static final String GENERATE_PASSWORD_API_ENDPOINT = "/generate-password";
	public static final String VALIDATE_USER_API_ENDPOINT = "/validate-user";
	public static final String SET_MPIN_API_ENDPOINT = "/set-mpin";
	public static final String VALIDATE_MPIN_API_ENDPOINT = "/validate-mpin";
	public static final String MPIN_SET_FLAG_RESPONSE_KEY = "isMpinSet";

	//DMS Constants
	public static final String DMS_API_ENDPOINT = "/document";
	public static final String UPLOAD_API_ENDPOINT = "/upload";
	public static final String DOWNLOAD_API_ENDPOINT = "/download";
	public static final String DOWNLOAD_BY_PORTAL_API_ENDPOINT = "/portal-download";
	public static final String DMS_UPLOAD_RESPONSE_KEY = "response";
	public static final String DMS_UPLOAD_RESPONSE_DOC_ID_KEY = "id";
	public static final String DMS_UPLOAD_RESPONSE_DOC_LABEL_KEY = "label";
	
}
