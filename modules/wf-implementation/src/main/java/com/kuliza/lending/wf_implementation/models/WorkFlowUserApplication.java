package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.journey.model.AbstractWorkFlowUserApplication;

@Entity
@Table(name = "workflow_user_app")
public class WorkFlowUserApplication extends AbstractWorkFlowUserApplication {

	// More custom fields can be added as shown below.

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	private WorkFlowApplication wfApplication;

	public WorkFlowUserApplication() {
		super();
		this.setIsDeleted(false);
	}

	public WorkFlowUserApplication(String userIdentifier, String procInstId,
			String parentProcInstId, WorkFlowApplication wfApplication, String processName) {
		super(userIdentifier, procInstId, parentProcInstId, processName);
		this.wfApplication = wfApplication;
	}

	public WorkFlowApplication getWfApplication() {
		return wfApplication;
	}

	public void setWfApplication(WorkFlowApplication wfApplication) {
		this.wfApplication = wfApplication;
	}
	
}
