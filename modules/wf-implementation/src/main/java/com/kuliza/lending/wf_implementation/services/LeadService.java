package com.kuliza.lending.wf_implementation.services;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.service.GenericServices;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.services.DatabaseQueryFunctions;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.journey.utils.HelperFunctions;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserLeadsDao;
import com.kuliza.lending.wf_implementation.models.WfUserLeads;
import com.kuliza.lending.wf_implementation.pojo.WorkflowInitiateRequest;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Synchronized;
import org.flowable.engine.RuntimeService;
import org.flowable.form.model.SimpleFormModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;


@Service("LeadService")
public class LeadService extends GenericServices {

	private static final Logger logger = LoggerFactory.getLogger(LeadService.class);

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private DatabaseQueryFunctions databaseQueryFunctions;

	@Autowired
	private WorkflowUserLeadsDao workflowUserLeadsDao;

	@Autowired
	private RuntimeService runtimeService;



	/**This service returns a lead form.
	 * @param workflowName
	 * @return
	 */
	@LogMethodDetails
	public ApiResponse getLeadForm(String workflowName) {
		try {
			String formName = workflowName + "_" + WfImplConstants.LEAD_KEYWORD;
			SimpleFormModel simpleFormModel = workflowService.getSimpleFormModel(formName, null);
			if (simpleFormModel != null) {
				return HelperFunctions.makeResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					null,  simpleFormModel.getKey(), simpleFormModel.getKey(), simpleFormModel.getName(),
					simpleFormModel.getDescription(), null, simpleFormModel.allFieldsAsMap());
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, 
						WfImplConstants.NO_LEAD_FORM_ERROR_MESSAGE  + formName);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}
	
	
	/**This service calls start or resume lead journey
	 * @param wfInitiateRequest
	 * @return
	 */
	@LogMethodDetails
	public ApiResponse initiateLead(WorkflowInitiateRequest wfInitiateRequest) {
		try {
			String assignee = null;
			if (wfInitiateRequest.getProcessVariables() != null && wfInitiateRequest.getProcessVariables().containsKey(WfImplConstants.LEAD_UNIQUE_KEY)) {
				assignee = CommonHelperFunctions.getStringValue(wfInitiateRequest.getProcessVariables().get(
						CommonHelperFunctions.getStringValue(wfInitiateRequest.getProcessVariables().get(WfImplConstants.LEAD_UNIQUE_KEY))));
			}
			if (assignee != null && !assignee.isEmpty()) {
				return workflowService.startOrResumeProcess(assignee, wfInitiateRequest);
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						WfImplConstants.LEAD_ASSIGNEE_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}
	
	@LogMethodDetails
	public ApiResponse submitLeadForm(SubmitFormClass input) {
		try {
			String procId = input.getProcessInstanceId();
			String assignee = null;
			if (procId != null && !procId.isEmpty() && runtimeService.hasVariable(procId, Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
				assignee = CommonHelperFunctions.getStringValue(runtimeService.getVariable(procId, Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			}
			if (assignee != null && !assignee.isEmpty()) {
				return workflowService.submitFormData(assignee, input);
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						WfImplConstants.LEAD_ASSIGNEE_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}

	@LogMethodDetails(processInstanceId = "processInstanceId")
	public ApiResponse getFormData(String processInstanceId) {
		try {
			String assignee = null;
			if (processInstanceId != null && !processInstanceId.isEmpty() && runtimeService.hasVariable(processInstanceId, Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
				assignee = CommonHelperFunctions.getStringValue(runtimeService.getVariable(processInstanceId, Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			}
			if (assignee != null && !assignee.isEmpty()) {
				return workflowService.getFormData(assignee, processInstanceId);
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						WfImplConstants.LEAD_ASSIGNEE_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}
	
	@LogMethodDetails
	public ApiResponse changeProcessState(BackTaskInput input,
			BindingResult result) {
		try {
			String processInstanceId = input.getProcessInstanceId();
			String assignee = null;
			if (processInstanceId != null && !processInstanceId.isEmpty() && runtimeService.hasVariable(processInstanceId, Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
				assignee = CommonHelperFunctions.getStringValue(runtimeService.getVariable(processInstanceId, Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			}
			if (assignee != null && !assignee.isEmpty()) {
				return workflowService.changeProcessState(assignee, input, result);
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						WfImplConstants.LEAD_ASSIGNEE_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
	}
	
	public ApiResponse modelUpdateQueryTask(String procInstId, String classPath, String setClause,
			String whereClause, Object identifierValue) {
		Map<String, Object> processVariables = runtimeService.getVariables(procInstId);
		return databaseQueryFunctions.modelUpdateQuery(processVariables, classPath, setClause,
				whereClause, identifierValue);
	}

	@Synchronized
	@LogMethodDetails(processInstanceId = "processInstanceId")
	public WfUserLeads saveLead(String processInstanceId, String uniqueIdentifier){
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		String leadId = databaseQueryFunctions.createLead(processVariables);
		runtimeService.setVariable(processInstanceId, "leadId", leadId);
		WfUserLeads wfUserLead = new WfUserLeads(leadId, processInstanceId, uniqueIdentifier);
		workflowUserLeadsDao.save(wfUserLead);
		return wfUserLead;
	}

	@LogMethodDetails(processInstanceId = "procInstId")
	public ApiResponse leadDedupeServiceTask(String procInstId, String classPath, String selectClause,
			String whereClause, String uniqueIdentifier){
		Map<String, Object> result = new HashMap<>();
		Map<String, Object> processVariables = runtimeService.getVariables(procInstId);
		ApiResponse apiResponse = databaseQueryFunctions.modelSelectQuery(processVariables, classPath,
				selectClause, whereClause);
		if(apiResponse.getStatus()== Constants.SUCCESS_CODE){
			if(apiResponse.getData()!=null){
				Object data = apiResponse.getData();
				List<String> wfUserLeadsList = (List<String>) data;
				runtimeService.setVariable(procInstId, WfImplConstants.EXISTING_LEAD_KEY, true);
				result.put("leadId", wfUserLeadsList.get(0));
				result.put(WfImplConstants.EXISTING_LEAD_KEY, true);
			} else{
				result.put(WfImplConstants.EXISTING_LEAD_KEY, false);
				runtimeService.setVariable(procInstId, WfImplConstants.EXISTING_LEAD_KEY, false);
				result.put("leadId", "");
			}
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, result);
		}
		return apiResponse;
	}

}


