package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;

public class FileUploadResponse {

	String documentName;

	@NotNull(message = "Doc Id cannot be null.")
	String documentId;

	public FileUploadResponse() {
		super();
	}

	public FileUploadResponse(String documentName, String documentId) {
		super();
		this.documentName = documentName;
		this.documentId = documentId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

}
