package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.FileDownloadInput;
import com.kuliza.lending.wf_implementation.pojo.FileUploadForm;
import com.kuliza.lending.wf_implementation.services.DMSService;
import com.kuliza.lending.wf_implementation.utils.SwaggerConstants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping(WfImplConstants.DMS_API_ENDPOINT)
public class DMSController {

	@Autowired
	public DMSService dmsService;

	@ApiOperation(value = SwaggerConstants.DMS_UPLOAD)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.DMS_UPLOAD_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.UPLOAD_API_ENDPOINT)
	public ResponseEntity<Object> upload(Principal principal, @Valid FileUploadForm uploadForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(dmsService.upload(principal.getName(), uploadForm));
		} catch (Exception e) {
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage()));
		}
	}

	@ApiOperation(value = SwaggerConstants.DMS_DOWNLOAD)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.DMS_DOWNLOAD_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.DOWNLOAD_API_ENDPOINT)
	public ResponseEntity<Object> download(Principal principal, @RequestBody @Valid FileDownloadInput fileDownloadInput) {
		try {
			return new ResponseEntity<>(dmsService.download(principal.getName(), fileDownloadInput), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value = SwaggerConstants.DMS_PORTAL_DOWNLOAD)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.DMS_PORTAL_DOWNLOAD_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.DOWNLOAD_BY_PORTAL_API_ENDPOINT)
	public ResponseEntity<Object> downloadByPortal(Principal principal, @RequestBody @Valid FileDownloadInput fileDownloadInput) {
		try {
			return new ResponseEntity<>(dmsService.downloadFile(principal.getName(), fileDownloadInput), HttpStatus.OK);
		} catch (Exception e) {
			CommonHelperFunctions.getStackTrace(e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

}
