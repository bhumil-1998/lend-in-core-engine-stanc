package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;

@Repository
public interface WorkflowUserApplicationDao extends CrudRepository<WorkFlowUserApplication, Long> {

	public WorkFlowUserApplication findById(long id);

	public WorkFlowUserApplication findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowUserApplication findFirstByUserIdentifierAndProcInstIdAndIsDeleted(String username, String procInstId, boolean isDeleted);
	
	public List<WorkFlowUserApplication> findByProcInstIdAndIsDeleted(String procInstId, boolean isDeleted);
	
	public List<WorkFlowUserApplication> findByUserIdentifierAndParentProcInstIdAndProcessNameAndIsDeleted(String username, 
			String parentProcInst, String processname, boolean isDeleted);
	

	public List<WorkFlowUserApplication> findByUserIdentifierAndParentProcInstIdAndIsDeleted(String username, 
			String parentProcInst, boolean isDeleted);

}
