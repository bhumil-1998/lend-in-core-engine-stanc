package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.journey.model.AbstractWFUserDocs;

@Entity
@Table(name = "wf_user_docs")
public class WorkFlowUserDocs extends AbstractWFUserDocs {
	
	public WorkFlowUserDocs() { 
		super();
	}
	
	public WorkFlowUserDocs(String userId, String documentId,
			String documentLabel, String documentType, String docExtension,
			String docName) {
		super(userId, documentId, documentLabel, documentType, docExtension, docName);
	}

//	Add more fields to this table if required in the format given below
	
//	@Column(nullable = true)
//	private String destinationType;	
//
//
//	public String getDestinationType() {
//		return destinationType;
//	}
//
//	public void setDestinationType(String destinationType) {
//		this.destinationType = destinationType;
//	}
	
}
