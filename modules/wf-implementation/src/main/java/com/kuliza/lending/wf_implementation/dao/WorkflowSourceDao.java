package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowSource;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;

@Repository
public interface WorkflowSourceDao extends CrudRepository<WorkFlowSource, Long> {

	public WorkFlowSource findById(long id);

	public WorkFlowSource findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowSource findFirstByWfUserAppAndIsDeleted(WorkFlowUserApplication wfUserApp, boolean isDeleted);
}
