package com.kuliza.lending.wf_implementation.controllers;

import com.kuliza.lending.authorization.service.DefaultOTPService;
import com.kuliza.lending.authorization.service.MPinService;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.pojo.UserMPINInputForm;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.wf_implementation.services.UserValidationService;
import com.kuliza.lending.wf_implementation.utils.SwaggerConstants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

import static com.kuliza.lending.engine_common.utils.EngineCommonSwaggerConstants.GET_USER_TOKEN_RESPONSE;

@RestController
@RequestMapping(WfImplConstants.VALIDATION_API_ENDPOINT)
public class UserValidationController {

	@Autowired
	private UserValidationService userValidationService;

	@Autowired
	private DefaultOTPService defaultOTPService;

	@Autowired
	private MPinService mPinService;

	private static final Logger logger = LoggerFactory.getLogger(UserValidationController.class);

	@ApiOperation(value = SwaggerConstants.UVC_GENERATE_PASSWORD)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.UVC_GENERATE_PASSWORD_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.GENERATE_PASSWORD_API_ENDPOINT)
	public ResponseEntity<Object> generateOtpController(
			@RequestBody @Valid UserPasswordGenerationForm userForm, HttpServletRequest request) {
		try {
			// Note: In case, implementation wants to have their own implementation, then write
			// implementations in UserValidationService, and use its reference variable here
			return CommonHelperFunctions.buildResponseEntity(defaultOTPService.generateUserPassword(request, userForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

	@ApiOperation(value = SwaggerConstants.UVC_VALIDATE_USER)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.UVC_VALIDATE_USER_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.VALIDATE_USER_API_ENDPOINT)
	public ResponseEntity<Object> validateOtpController(
			@RequestBody @Valid UserValidationinputForm userValidationForm, HttpServletRequest request) {
		try {
			// Note: In case, implemetation wants to have their own implementation, then write
			// implementations in UserValidationService, and use its reference variable here
			return CommonHelperFunctions.buildResponseEntity(userValidationService.validateAndRegisterUser(request, userValidationForm, false));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
	@ApiOperation(value = SwaggerConstants.UVC_SET_MPIN)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.UVC_SET_MPIN_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.SET_MPIN_API_ENDPOINT)
	public ResponseEntity<Object> setMpin(Principal principal,
			@RequestBody @Valid UserMPINInputForm mpinForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(mPinService.setMpin(principal.getName(), mpinForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
	@ApiOperation(value = SwaggerConstants.UVC_VALIDATE_MPIN)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.UVC_VALIDATE_MPIN_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.VALIDATE_MPIN_API_ENDPOINT)
	public ResponseEntity<Object> validateMpin(Principal principal,
			@RequestBody @Valid UserMPINInputForm mpinForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(mPinService.validateMpin(principal.getName(), mpinForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

	@ApiOperation(value = "To get user's Token form Portal or Authorised source")
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = GET_USER_TOKEN_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = "/get-user-token")
	public ResponseEntity<Object> getUserToken(javax.servlet.http.HttpServletRequest request,
											   @ApiParam(value = "Unique Identifier" , required = true, example = "99264832Ahs@&&A") @RequestParam(required=true, value="uniqueIdentifier") String uniqueIdentifier,
											   @ApiParam(value = "Identifier type" , required = false, example = "mobile") @RequestParam(required=false, value="identifireType", defaultValue = "mobile") String identifierType
											   ) throws Exception{
		return CommonHelperFunctions
				.buildResponseEntity(userValidationService.validateAndRegisterUserWithoutOTP(request, uniqueIdentifier, identifierType));
	}

}
