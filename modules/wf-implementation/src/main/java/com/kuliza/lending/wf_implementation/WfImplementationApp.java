package com.kuliza.lending.wf_implementation;

import static com.kuliza.lending.common.utils.Constants.JASYPT_ENCRYPTOR_PASSWORD;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@ComponentScan(basePackages = {"com.kuliza"})
@EnableConfigurationProperties(LiquibaseProperties.class)
@PropertySources({
	@PropertySource("classpath:lend-in-modules.properties")
})
public class WfImplementationApp  extends SpringBootServletInitializer {

	@Autowired
	private LiquibaseProperties properties;
	
	@Autowired
	private DataSource dataSource;

	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(WfImplementationApp.class);
	}

	public static void main(String[] args) {
		System.setProperty("jasypt.encryptor.password", JASYPT_ENCRYPTOR_PASSWORD);
		SpringApplication.run(WfImplementationApp.class);
	}

	@Bean
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog(this.properties.getChangeLog());
		liquibase.setContexts(this.properties.getContexts());
		liquibase.setDefaultSchema(this.properties.getDefaultSchema());
		liquibase.setDropFirst(this.properties.isDropFirst());
		liquibase.setShouldRun(this.properties.isEnabled());
		liquibase.setLabels(this.properties.getLabels());
		liquibase.setChangeLogParameters(this.properties.getParameters());
		liquibase.setRollbackFile(this.properties.getRollbackFile());
		liquibase.setDatabaseChangeLogLockTable("LOS_DATABASECHANGELOGLOCK");
		liquibase.setDatabaseChangeLogTable("LOS_DATABASECHANGELOG");
		return liquibase;
	}
}
