package com.kuliza.lending.wf_implementation.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.form.api.FormService;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.service.GenericServices;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.journey.pojo.AddSubJourney;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.BackToJourneyInput;
import com.kuliza.lending.journey.pojo.DeleteSubJourney;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.journey.utils.HelperFunctions;
import com.kuliza.lending.journey.utils.Validation;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowSourceDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserLeadsDao;
import com.kuliza.lending.wf_implementation.models.WfUserLeads;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowSource;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.pojo.WorkflowInitiateRequest;
import com.kuliza.lending.wf_implementation.pojo.WorkflowSourceForm;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("WorkflowService")
public class WorkflowService extends GenericServices {

	private static final Logger logger = LoggerFactory.getLogger(WorkflowService.class);

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	private FormService formService;

	@Autowired
	private WorkflowSourceDao workflowSourceDao;
	
	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	private WorkflowUserLeadsDao workflowUserLeadsDao;
	
	@Autowired
	private WorkflowUserDao workflowUserDao;

	/**
	 * This functions takes Parent/Root Process Instance Id and returns List of All
	 * Process Instance Ids of Running Child Executions
	 *
	 * @return List of String Process Instance Ids
	 * 
	 * @author Shrey Bhasin
	 */
	@LogMethodDetails(processInstanceId = "parentProcInst")
	public List<String> allProcessInstances(String parentProcInst) {
		List<Execution> allRunningExecutions = runtimeService.createExecutionQuery()
				.rootProcessInstanceId(parentProcInst).list();
		List<String> allProcessInstanceIds = new ArrayList<>();
		for (Execution singleExecution : allRunningExecutions) {
			allProcessInstanceIds.add(singleExecution.getProcessInstanceId());
		}
		allProcessInstanceIds.add(parentProcInst);
		return allProcessInstanceIds;
	}

	
	/** This service returns the current task data be it in the sub-process or main process
	 * 
	 * @param username
	 * @param parentProcInst
	 * @param errorcode
	 * @param errorMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	@LogMethodDetails(processInstanceId = "parentProcInst", userId = "username")
	public ApiResponse getTaskData(String username, String parentProcInst, HttpStatus errorcode,
			String errorMessage) throws Exception {
		ApiResponse response;
		List<String> allProcessInstanceIds = allProcessInstances(parentProcInst);

		List<Task> allTasks = taskService.createTaskQuery().processInstanceIdIn(allProcessInstanceIds)
				.taskAssignee(username).active().list();
		if (allTasks.size() > 1) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.TOO_MANY_TASKS);
		} else {
			if (allTasks.size() != 1) {
				response = new ApiResponse(errorcode, errorMessage);
			} else {
				Task task = allTasks.get(0);
				Map<String, Object> processVariables = runtimeService.getVariables(task.getProcessInstanceId());
				Map<String, Object> data = new HashMap<>();
				Map<String, Object> rootProcessVariables = runtimeService.getVariables(parentProcInst);
				data.put(Constants.LOAN_APPLICATION_ID_KEY, rootProcessVariables.get(Constants.APPLICATION_ID_KEY));
				data.put(Constants.APPLICATION_NUMBER, rootProcessVariables.get(Constants.APPLICATION_NUMBER));
				SimpleFormModel formModel = getSimpleFormModel(allTasks.get(0), processVariables);
				Map<String, FormField> formFields = formModel.allFieldsAsMap();
				response = HelperFunctions.makeResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						parentProcInst, task, data, formFields);
			}
		}
		return response;
	}
	
	/**This service either starts a new process or resume existing processes.
	 * 
	 * @param username
	 * @param workflowInitiateRequest
	 * @return
	 */
	@LogMethodDetails(userId = "username")
	@Transactional
	public ApiResponse startOrResumeProcess(String username, WorkflowInitiateRequest workflowInitiateRequest) {
		ApiResponse response;
		ProcessInstance processInstance = null;
		WorkFlowUserApplication wfUserAppObj = null;
		try {
			if (repositoryService.createDeploymentQuery().processDefinitionKey(workflowInitiateRequest.getWorkflowName()).count() < 1) {
				// no process deployed with given journey name
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						Constants.NO_PROCESS_DEPLOYED_FOR_GIVEN_KEY_MESSAGE);
				return response;
			}
			WorkflowSourceForm wfSource = workflowInitiateRequest.getWorkflowSource();
			String parentProcInstId = null;
			List<WorkFlowUserApplication> workflowUserApps = workflowUserApplicationDao
					.findByUserIdentifierAndParentProcInstIdAndProcessNameAndIsDeleted(
					username, parentProcInstId, workflowInitiateRequest.getWorkflowName(), false);
			if (workflowUserApps == null || workflowUserApps.isEmpty() || 
				(workflowInitiateRequest.getIsNew() != null && workflowInitiateRequest.getIsNew())) {
				WorkFlowApplication wfAppObj = null;
				if (workflowInitiateRequest.getApplicationNumber() != null) {
					wfAppObj = workflowApplicationDao.findByIdAndIsDeleted(
							workflowInitiateRequest.getApplicationNumber(), false);
				}
				Map<String, Object> processVariables = null;
				if (workflowInitiateRequest.getProcessVariables() != null) {
					processVariables = workflowInitiateRequest.getProcessVariables();
				} else {
					processVariables = new HashMap<String, Object>();
				}
				if (wfSource != null) {
					if (wfSource.getType().equals(SourceType.bpmn)) {
						List<WorkFlowUserApplication> parentUserApps = workflowUserApplicationDao.findByProcInstIdAndIsDeleted(
								wfSource.getId(), false);
						if (parentUserApps != null && !parentUserApps.isEmpty()) {
							wfAppObj = parentUserApps.get(0).getWfApplication();
						}
					}
					if (wfSource.getCopyFromSource() != null && wfSource.getCopyFromSource()) {
						if (wfSource.getType().equals(SourceType.bpmn)) {
							processVariables.putAll(runtimeService.getVariables(wfSource.getId()));
						} else {
							processVariables.putAll(cmmnRuntimeService.getVariables(wfSource.getId()));
						}
					}
				}
				if(wfAppObj == null) {
					wfAppObj = new WorkFlowApplication();
					wfAppObj = workflowApplicationDao.save(wfAppObj);
				}
				wfUserAppObj = new WorkFlowUserApplication();
				wfUserAppObj.setWfApplication(wfAppObj);
				wfUserAppObj.setUserIdentifier(username);
				wfUserAppObj.setProcessName(workflowInitiateRequest.getWorkflowName());
				wfUserAppObj = workflowUserApplicationDao.save(wfUserAppObj);
				WorkFlowSource wfSourceObj = registerSource(wfSource, wfUserAppObj);
				processVariables.put(Constants.PROCESS_TASKS_ASSIGNEE_KEY, username);				
				processVariables.put(Constants.APPLICATION_ID_KEY, wfUserAppObj.getId());
				processVariables.put(Constants.APPLICATION_NUMBER, wfAppObj.getId());
				processVariables.put(WfImplConstants.WORKFLOW_TYPE, workflowInitiateRequest.getWorkflowName());
				processVariables.put(Constants.JOURNEY_TYPE, workflowInitiateRequest.getWorkflowName());
				WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(username, false);
				if (wfUser != null) {
					if (wfUser.getMobileNumber() != null) {
						processVariables.put(Constants.APPLICANT_MOBILE_NUMBER, wfUser.getMobileNumber());
					}
					if (wfUser.getEmail() != null) {
						processVariables.put(Constants.EMAIL_ID_KEY, wfUser.getEmail());
					}
				}
				// Please write pre-initiate custom code in the class given below.
				InitiateWorkflowScripts.preInitiateScript(username, workflowInitiateRequest, wfAppObj, wfUserAppObj,
						processVariables, wfSourceObj);
				Authentication.setAuthenticatedUserId(username);
				processInstance = runtimeService.startProcessInstanceByKey(workflowInitiateRequest.getWorkflowName(),
						CommonHelperFunctions.getStringValue(wfUserAppObj.getId()), processVariables);
				wfUserAppObj.setParentProcInstId(parentProcInstId);
				wfUserAppObj.setProcInstId(processInstance.getId());
				runtimeService.setVariable(processInstance.getId(), WfImplConstants.PROC_INST_ID_KEY, processInstance.getId());
				wfUserAppObj = workflowUserApplicationDao.save(wfUserAppObj);
				// Please write post-initiate custom code in the class given below.
				InitiateWorkflowScripts.postInitiateScript(processInstance, wfAppObj, wfUserAppObj, wfSourceObj);
			}
			if (workflowUserApps == null) {
				workflowUserApps = new ArrayList<>();
			} 
			if (wfUserAppObj != null) {
				workflowUserApps.add(wfUserAppObj);
			}
			if (workflowUserApps.size() == 1) {
				response = getTaskData(username,workflowUserApps.get(0).getProcInstId(), HttpStatus.BAD_REQUEST,
						Constants.NO_TASKS_MESSAGE);
			} else {
				List<Object> taskList = new ArrayList<Object>();
				for (WorkFlowUserApplication wfUserApp : workflowUserApps) {
					ApiResponse taskResponse = getTaskData(username, wfUserApp.getProcInstId(), HttpStatus.BAD_REQUEST,
							Constants.NO_TASKS_MESSAGE);
					taskList.add(taskResponse.getData());
				}
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						taskList);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e.getMessage());
		}
		return response;
	}

	
	/**This function registers or return source from which process is initiated
	 * 
	 * @param workflowSource
	 * @param wfUserApp
	 * @return
	 */
	private WorkFlowSource registerSource(WorkflowSourceForm workflowSource, WorkFlowUserApplication wfUserApp) {
		WorkFlowSource workflowSourceObj = null;
		if (workflowSource != null) {
			workflowSourceObj = new WorkFlowSource(workflowSource.getId(), workflowSource.getType(), workflowSource.getName(), wfUserApp);
			workflowSourceObj = workflowSourceDao.save(workflowSourceObj);
		}
		return workflowSourceObj;
	}


	/**This service returns current state of the process
	 * 
	 * @param username
	 * @param parentProcInstId
	 * @return
	 */
	@LogMethodDetails(processInstanceId = "parentProcInstId", userId = "username")
	public ApiResponse getFormData(String username, String parentProcInstId) {
		ApiResponse response;
		try {
			if (parentProcInstId == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			} else {
				response = getTaskData(username, parentProcInstId, HttpStatus.BAD_REQUEST,
						Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			}
		} catch (Exception e) {
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

	/**This service submits the proc variables and add user level variables in WorkflowUserVariables Model
	 * 
	 * @param username
	 * @param input
	 * @return
	 */
	@LogMethodDetails(userId = "username")
	public ApiResponse submitFormData(String username, SubmitFormClass input) {
		ApiResponse response;
		try {
			
			Task task = taskService.createTaskQuery().taskAssignee(username).active().taskId(input.getTaskId())
					.singleResult();
			if (task == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
			} else {
				List<Execution> executionProcessInstance = runtimeService.createExecutionQuery()
						.processInstanceId(task.getProcessInstanceId()).list();
				if (!executionProcessInstance.get(0).getRootProcessInstanceId().equals(input.getProcessInstanceId())) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
				} else {
					SimpleFormModel formModel = getSimpleFormModel(task, null);
					List<FormField> listOfFormProp = formModel.getFields();
					Map<String, Object> formPropertiesMap = (!input.getFormProperties().isEmpty()
							? input.getFormProperties()
							: new HashMap<String, Object>());
					Map<Object, Object> validationResult = Validation.submitValidation(listOfFormProp,
							formPropertiesMap, true);
					if (!(boolean) validationResult.get(WfImplConstants.VALIDATION_STATUS_KEY)) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST,
								validationResult.get(Constants.MESSAGE_KEY).toString());
					} else {
						runtimeService.setVariables(task.getProcessInstanceId(), formPropertiesMap);
						taskService.complete(input.getTaskId());
						response = getTaskData(username, input.getProcessInstanceId(), HttpStatus.OK,
								Constants.SUCCESS_MESSAGE);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;

	}

	/**This service is used for back functionality and takes the execution to a previous state
	 * 
	 * @param username
	 * @param input
	 * @param result
	 * @return
	 */
	@LogMethodDetails(userId = "username")
	public ApiResponse changeProcessState(String username, BackTaskInput input, BindingResult result) {
		ApiResponse response;
		try {
			response = checkErrors(result);
			if (response == null) {
				Task task = taskService.createTaskQuery().taskId(input.getCurrentTaskId())
						.taskAssignee(username).active().singleResult();
				if (task == null) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
				} else {
					List<HistoricTaskInstance> taskToGoTo = historyService.createHistoricTaskInstanceQuery()
							.processInstanceId(task.getProcessInstanceId()).taskDefinitionKey(input.getBackTaskDefinitionKey())
							.list();
					if (!taskToGoTo.isEmpty()) {
						runtimeService.createChangeActivityStateBuilder()
								.processInstanceId(task.getProcessInstanceId())
								.moveActivityIdTo(task.getTaskDefinitionKey(), input.getBackTaskDefinitionKey())
								.changeState();
						response = getTaskData(username, input.getProcessInstanceId(), HttpStatus.BAD_REQUEST,
								Constants.INVALID_TASK_ID_MESSAGE);
					} else  {
						response = new ApiResponse(HttpStatus.BAD_REQUEST,
								Constants.INVALID_BACK_TASK_DEFINITION_KEY);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

//	/**
//	 * This functions copies all variables of root process to sub processes and vice
//	 * versa.
//	 * 
//	 * @param execution
//	 * @param copyToRoot
//	 * @return Execution
//	 * 
//	 * @author Kunal Arneja
//	 */
//	public Execution copyVariablesFromToRootProcess(Execution execution, boolean copyToRoot) {
//		String processInstanceId = execution.getProcessInstanceId();
//		String rootProcessInstanceId = execution.getRootProcessInstanceId();
//		if (processInstanceId != null && rootProcessInstanceId != null) {
//			if (copyToRoot) {
//				runtimeService.setVariables(rootProcessInstanceId, runtimeService.getVariables(processInstanceId));
//			} else {
//				runtimeService.setVariables(processInstanceId, runtimeService.getVariables(rootProcessInstanceId));
//			}
//		}
//		return execution;
//	}
	
	
//	/**
//	 * @param businessKey:
//	 *            applicationId for the user
//	 * @param journeyType:
//	 *            Process Definition Key
//	 * 
//	 * @return processInstanceId
//	 * 
//	 *         The function takes processDefinitionKey and businessKey as input and
//	 *         returns processInstanceId
//	 * 
//	 **/
//	public String getProcessInstanceId(String businessKey, String journeyType) {
//		String processInstanceId = "";
//		List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
//				.processInstanceBusinessKey(businessKey, journeyType).desc().list();
//		if(!processInstanceList.isEmpty()) {
//			ProcessInstance processInstance = processInstanceList.get(0);
//			processInstanceId = processInstance.getProcessInstanceId();
//		}
//		return processInstanceId;
//	}

	/**
	 * Function updates the processVariables for the given businessKey from the POJO
	 * BackToJourneyInput
	 * 
	 * @param backToJourneyInput
	 * 
	 * @return ApiResponse
	 * 
	 **/
	@LogMethodDetails
	public ApiResponse setUserData(BackToJourneyInput backToJourneyInput) {
		String processInstanceId = backToJourneyInput.getProcessInstanceId();
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
		if (!processInstanceId.isEmpty()) {
			runtimeService.setVariables(processInstanceId, backToJourneyInput.getVariablesToSave());
			if (backToJourneyInput.isCloseTask()) {
				for (String taskKey: backToJourneyInput.getTaskList()){
					if (taskKey != null && !taskKey.isEmpty()) {
						try {
							Task task = taskService.createTaskQuery().processInstanceId(processInstanceId)
									.active()
									.taskDefinitionKey(taskKey.trim())
									.singleResult();
							if (task != null) {
								taskService.complete(task.getId());
							}
						} catch (Exception e) {
							logger.error(CommonHelperFunctions.getStackTrace(e));
						}
					}
				}
			}
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		}
		return response;

	}

	public SimpleFormModel getSimpleFormModel(String key, Map<String, Object> processVariables) {
		return ((SimpleFormModel) formService.getFormModelWithVariablesByKey(key, "", processVariables).getFormModel());
	}

	public SimpleFormModel getSimpleFormModel(Task task, Map<String, Object> processVariables) {
		SimpleFormModel simpleFormModel = null;
		if (task.getFormKey() != null && !task.getFormKey().isEmpty()) {
			simpleFormModel = getSimpleFormModel(task.getFormKey(), processVariables);
		} else {
			simpleFormModel = (SimpleFormModel) taskService.getTaskFormModel(task.getId())
					.getFormModel();
		}
		return simpleFormModel;
	}
	
	/**This service Changes assignee and make an entry into WorkflowuserApp Model
	 * 
	 * @param procInstId
	 * @param assignee
	 * 
	 * @return Object
	 */
	@LogMethodDetails(processInstanceId = "procInstId")
	public Object changeAssignee(String procInstId, String newAssigneeKey , String assignee) 
			throws Exception {
		try {
			String currentAssignee = CommonHelperFunctions.getStringValue(runtimeService.getVariable(procInstId, Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			WorkFlowUserApplication wfUserApp = workflowUserApplicationDao.findFirstByUserIdentifierAndProcInstIdAndIsDeleted(
					currentAssignee, procInstId, false);
			if (runtimeService.hasVariable(procInstId, assignee)) {
				assignee = CommonHelperFunctions.getStringValue(runtimeService.getVariable(procInstId, assignee));
			}
			if (wfUserApp != null) {
				WorkFlowUserApplication wfUserAppNew = null;
				wfUserAppNew = workflowUserApplicationDao.findFirstByUserIdentifierAndProcInstIdAndIsDeleted(assignee, procInstId, false);
				if (wfUserAppNew == null) {
					wfUserAppNew = new WorkFlowUserApplication();
					wfUserAppNew.setProcessName(wfUserApp.getProcessName());
					wfUserAppNew.setProcInstId(wfUserApp.getProcInstId());
					wfUserAppNew.setUserIdentifier(assignee);
					wfUserAppNew.setWfApplication(wfUserApp.getWfApplication());
					workflowUserApplicationDao.save(wfUserAppNew);
				}
				runtimeService.setVariable(procInstId, newAssigneeKey, assignee);
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, WfImplConstants.ASSIGNEE_CHANGE_SUCCESS_MESSAGE);
			} else {
				return new InterruptedException();
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			throw new InterruptedException(e.getMessage());
		}
	}
	
	/**This process saves variables to source, parent and soft deletes WorkfloUserApp and its children
	 * 
	 * @param procInstId
	 * @param copyToSource
	 * @param copyToParent
	 * @param notClose
	 * 
	 * @return Object
	 */
	@LogMethodDetails(processInstanceId = "procInstId")
	public Object endProcess(String procInstId, Boolean copyToSource, Boolean copyToParent, Boolean notClose) 
			throws Exception{
		try {
			String currentAssignee = CommonHelperFunctions.getStringValue(runtimeService.getVariable(procInstId, Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			WorkFlowUserApplication wfUserApp = workflowUserApplicationDao.findFirstByUserIdentifierAndProcInstIdAndIsDeleted(
					currentAssignee, procInstId, false);
			if (wfUserApp != null) {
				if (copyToParent != null && copyToParent) {
					if (wfUserApp.getParentProcInstId() != null && !wfUserApp.getParentProcInstId().equals("")) {
						List<Map<String, Object>> childDataList = new ArrayList<>();
						String childDataString = CommonHelperFunctions.getStringValue(
								runtimeService.getVariable(
										wfUserApp.getParentProcInstId(),
										wfUserApp.getProcessName() + WfImplConstants.WORKFLOW_LIST_SUFFIX));
						if (childDataString != null && !childDataString.equals("")) {
							childDataList = objectMapper.readValue(childDataString, new TypeReference<List<Map<String, Object>>>() {});
						}
						childDataList.add(runtimeService.getVariables(procInstId));
						runtimeService.setVariable(wfUserApp.getParentProcInstId(),
								wfUserApp.getProcessName() + WfImplConstants.WORKFLOW_LIST_SUFFIX,
								objectMapper.writeValueAsString(childDataList));
					}
				}
				if (copyToSource != null && copyToSource) {
					WorkFlowSource wfSource = workflowSourceDao.findFirstByWfUserAppAndIsDeleted(wfUserApp, false);
					
					if (wfSource != null && wfSource.getSourceType().equals(SourceType.bpmn)) {
						List<Map<String, Object>> childDataList = new ArrayList<>();
						String childDataString = CommonHelperFunctions.getStringValue(
								runtimeService.getVariable(wfSource.getSourceId(),
										wfUserApp.getProcessName() + WfImplConstants.WORKFLOW_LIST_SUFFIX));
						if (!childDataString.isEmpty()) {
							childDataList = objectMapper.readValue(childDataString, new TypeReference<List<Map<String, Object>>>() {});
						}
						childDataList.add(runtimeService.getVariables(procInstId));
						runtimeService.setVariable(wfSource.getSourceId(),
								wfUserApp.getProcessName() + WfImplConstants.WORKFLOW_LIST_SUFFIX,
								objectMapper.writeValueAsString(childDataList));
					} else if (wfSource != null && wfSource.getSourceType().equals(SourceType.cmmn)){
						List<Map<String, Object>> childDataList = new ArrayList<>();
						String childDataString = CommonHelperFunctions.getStringValue( 
								cmmnRuntimeService.getVariable(wfSource.getSourceId(),
										wfUserApp.getProcessName() + WfImplConstants.WORKFLOW_LIST_SUFFIX));
						if (childDataString != null && !childDataString.equals("")) {
							childDataList = objectMapper.readValue(childDataString, new TypeReference<List<Map<String, Object>>>() {});
						}
						childDataList.add(runtimeService.getVariables(procInstId));
						cmmnRuntimeService.setVariable(wfSource.getSourceId(),
								wfUserApp.getProcessName() + WfImplConstants.WORKFLOW_LIST_SUFFIX,
								objectMapper.writeValueAsString(childDataList));
					}
					
				}
				if (notClose != null && !notClose) {
					List<WorkFlowUserApplication> wfUserAppChilds = workflowUserApplicationDao.
							findByUserIdentifierAndParentProcInstIdAndIsDeleted(currentAssignee, procInstId, false);
					for (WorkFlowUserApplication wfUserAppChild : wfUserAppChilds) {
						wfUserAppChild.setIsDeleted(true);
						workflowUserApplicationDao.save(wfUserAppChild);
					}
					wfUserApp.setIsDeleted(true);
					workflowUserApplicationDao.save(wfUserApp);
				}
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, WfImplConstants.END_PROCESS_SUCCESS_MESSAGE);
			} else {
				throw new InterruptedException(WfImplConstants.NO_WF_APP_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new InterruptedException(e.getMessage());
		}
	}
	

	/**This Service has to be the first service of called activity and makes entry into WorkflowUserApp model
	 * @param exec
	 * @return
	 */
	@LogMethodDetails
	public Object createChild(DelegateExecution exec) throws Exception {
		try {
			String parentProcInstId = CommonHelperFunctions.getStringValue(exec.getVariable(WfImplConstants.PARENT_PROC_INST_ID_KEY));
			if (parentProcInstId.isEmpty()) {
				parentProcInstId = exec.getRootProcessInstanceId();
			}
			if (!parentProcInstId.isEmpty()) {
				String rootAssignee = CommonHelperFunctions.getStringValue(runtimeService.getVariable(parentProcInstId, Constants.PROCESS_TASKS_ASSIGNEE_KEY));
				WorkFlowUserApplication rootWfUserApp = workflowUserApplicationDao.findFirstByUserIdentifierAndProcInstIdAndIsDeleted(
						rootAssignee, parentProcInstId, false);
				if (rootWfUserApp != null) {
					String currentAssignee = CommonHelperFunctions.getStringValue(
							exec.getVariable(Constants.PROCESS_TASKS_ASSIGNEE_KEY));
					WorkFlowUserApplication wfUserAppNew = new WorkFlowUserApplication();
					wfUserAppNew.setProcInstId(exec.getProcessInstanceId());
					wfUserAppNew.setProcessName(CommonHelperFunctions.getStringValue(exec.getVariable(WfImplConstants.WORKFLOW_TYPE)));
					wfUserAppNew.setUserIdentifier(currentAssignee);
					wfUserAppNew.setWfApplication(rootWfUserApp.getWfApplication());
					wfUserAppNew.setParentProcInstId(parentProcInstId);
					wfUserAppNew = workflowUserApplicationDao.save(wfUserAppNew);
					exec.setVariable(Constants.PROCESS_TASKS_ASSIGNEE_KEY, currentAssignee);				
					exec.setVariable(Constants.APPLICATION_ID_KEY, wfUserAppNew.getId());
					exec.setVariable(Constants.APPLICATION_NUMBER, rootWfUserApp.getWfApplication().getId());
					exec.setVariable(WfImplConstants.WORKFLOW_TYPE, wfUserAppNew.getProcessName());
					exec.setVariable(Constants.JOURNEY_TYPE, wfUserAppNew.getProcessName());
					exec.setVariable(WfImplConstants.PROC_INST_ID_KEY, exec.getProcessInstanceId());
					WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(currentAssignee, false);
					if (wfUser != null) {
						exec.setVariable(Constants.APPLICANT_MOBILE_NUMBER, wfUser.getMobileNumber());
						exec.setVariable(Constants.EMAIL_ID_KEY, wfUser.getEmail());
					}
					return exec;
				} else {
					throw new InterruptedException(WfImplConstants.NO_WF_APP_ERROR_MESSAGE);
				}
			} else {
				throw new InterruptedException(WfImplConstants.NO_ROOT_PROC_ID_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			throw new InterruptedException(e.getMessage());
		}
	}
	
	/**This service copies lead variables to main journey
	 * @param exec
	 * @return
	 * @throws Exception
	 */
	public Object copyLeadVariables(DelegateExecution exec) throws Exception {
		try {
			String assignee = CommonHelperFunctions.getStringValue(exec.getVariable(Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			if (!assignee.equals("")) {	
				List<WfUserLeads> userLeads = workflowUserLeadsDao.findByUniqueIdentifierAndExpired(assignee, false);
				if (userLeads.size() == 1) {
					String leadProcessId = userLeads.get(0).getLeadProcessInstanceId();
					Map<String, Object> leadVariables = runtimeService.getVariables(leadProcessId);
					Map<String, Object> leadSuffixVariables = leadVariables.keySet().stream()
					        .collect(Collectors.toMap(key -> key + "_" + WfImplConstants.LEAD_KEYWORD, key -> leadVariables.get(key)));
					exec.setVariables(leadSuffixVariables);
					return exec;
				} else {
					throw new InterruptedException(WfImplConstants.NO_UNIQUE_LEAD_ERROR_MESSAGE);
				}
			} else {
				throw new InterruptedException(WfImplConstants.NO_ASSIGNEE_KEY_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			throw new InterruptedException(e.getMessage());
		}
	}
	
	
	/**This service task converts list of objects to flat keys
	 * 
	 * @param exec
	 * @param listKey
	 * @param variables
	 * @param startIndex
	 * @param endIndex
	 * @return
	 * @throws Exception
	 */
	@LogMethodDetails
	public Object flatListObjectsToKeys(DelegateExecution exec, String listKey, String variables, Integer startIndex, Integer endIndex) throws Exception {
		try {
			List<Map<String, Object>> childDataList = new ArrayList<>();
			String childDataString = CommonHelperFunctions.getStringValue(exec.getVariable(listKey));
			String workflowName = listKey.split("\\_")[0];
			if (!childDataString.equals("")) {
				childDataList = objectMapper.readValue(childDataString, new TypeReference<List<Map<String, Object>>>() {});
				if (startIndex == null) {
					startIndex = 0;
				} else {
					startIndex -= 1;
				}
				if (endIndex == null) {
					endIndex = childDataList.size();
				}
				Integer i = startIndex + 1;
				childDataList = childDataList.subList(startIndex, endIndex);
				List<String> variablesList = Arrays.asList(variables.split("\\s*,\\s*"));
				for (Map<String, Object> childData : childDataList) {
					String inc = i.toString();
					Map<String, Object> modifiedChildData = variablesList.stream()
					        .collect(Collectors.toMap(key -> key + "_" + workflowName + "_" + inc, key -> childData.get(key)));
					exec.setVariables(modifiedChildData);
					i += 1;
				}
				return exec;
			} else {
				throw new InterruptedException(WfImplConstants.LIST_KEY_INVALID_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			throw new InterruptedException(e.getMessage());
		}
	}
	
	/**This service is used for back functionality and takes the execution to a previous state
	 * 
	 * @param username
	 * @param input
	 * @param result
	 * @return
	 */
	public ApiResponse backToCallActivity(String username, BackTaskInput input, BindingResult result) throws Exception {
		ApiResponse response;
		try {
			response = checkErrors(result);
			if (response == null) {
				Task task = taskService.createTaskQuery().taskId(input.getCurrentTaskId())
						.taskAssignee(username).active().singleResult();
				if (task == null) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
				} else {	
					runtimeService.createChangeActivityStateBuilder()
							.processInstanceId(task.getProcessInstanceId())
							.moveActivityIdTo(task.getTaskDefinitionKey(), input.getBackTaskDefinitionKey())
							.changeState();
					response = getTaskData(username, input.getProcessInstanceId(), HttpStatus.BAD_REQUEST,
							Constants.INVALID_TASK_ID_MESSAGE);
				}
			}
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			throw e;
		}
		return response;
	}

	/**This Service takes back the execution to call activity to make it run one at a time
	 * @param name
	 * @param input
	 * @param result
	 * @return
	 */
	@Transactional
	@LogMethodDetails(userId = "name")
	public ApiResponse addSubJourney(String name, AddSubJourney input, BindingResult result) throws Exception {
		BackTaskInput backInput = new BackTaskInput(input.getProcessInstanceId(), input.getCurrentTaskId(), input.getBackTaskDefinitionKey());
		runtimeService.setVariable(input.getProcessInstanceId(), input.getSubJourneyCounterKey() + "Counter", 1);
		ApiResponse changeStateResponse = backToCallActivity(name, backInput, result);
		if (changeStateResponse.getStatus() == 200) {
			Integer noOfSubJourneys = CommonHelperFunctions.getIntegerValue(
					runtimeService.getVariable(input.getProcessInstanceId(), input.getSubJourneyCounterKey()));
			runtimeService.setVariable(input.getProcessInstanceId(), input.getSubJourneyCounterKey(), noOfSubJourneys + 1);
		} else {
			throw new Exception(WfImplConstants.BACK_TO_CALL_ACTIVITY_ERROR_MESSAGE);
		}
		return changeStateResponse;
	}

	/***This Service deleted the sub journey from the list of objs and reduce the counter of the variable
	 * @param name
	 * @param input
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@LogMethodDetails(userId = "name")
	public ApiResponse deleteSubJourney(String name, DeleteSubJourney input) throws Exception {
		String subJourneyName = CommonHelperFunctions.getStringValue(runtimeService.getVariable(input.getProcessInstanceId(), input.getSubJourneyKey()));
		if (!subJourneyName.isEmpty()) {
			if (!CommonHelperFunctions.getStringValue(
					runtimeService.getVariable(
							input.getProcessInstanceId(), Constants.PROCESS_TASKS_ASSIGNEE_KEY)).equals(name)) {
				return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.SOMETHING_WRONG_MESSAGE, WfImplConstants.PROCESS_NOT_ASSINED_ERROR_MESSAGE);
			}
			String childDataString = CommonHelperFunctions.getStringValue(runtimeService.getVariable(input.getProcessInstanceId(),
				subJourneyName + WfImplConstants.WORKFLOW_LIST_SUFFIX));
			if (!childDataString.isEmpty()) {
				List<Map<String, Object>> childDataList = objectMapper.readValue(childDataString, new TypeReference<List<Map<String, Object>>>() {});
				childDataList.remove(input.getSubJourneyNumber() - 1);
				runtimeService.setVariable(input.getProcessInstanceId(),
						subJourneyName + WfImplConstants.WORKFLOW_LIST_SUFFIX,
						objectMapper.writeValueAsString(childDataList));
				Integer noOfSubJourneys = CommonHelperFunctions.getIntegerValue(
						runtimeService.getVariable(input.getProcessInstanceId(), input.getSubJourneyCounterKey()));
				runtimeService.setVariable(input.getProcessInstanceId(), input.getSubJourneyCounterKey(), noOfSubJourneys - 1);
				if (input.getDependentKeys() != null && !input.getDependentKeys().isEmpty()) {
					List<String> variablesList = Arrays.asList(input.getDependentKeys().split("\\s*,\\s*"));
					variablesList.replaceAll(x -> x + "_" + input.getSubJourneyNumber());
					runtimeService.removeVariables(input.getProcessInstanceId(), variablesList);
				}
				return getFormData(name, input.getProcessInstanceId());
			} else {
				throw new Exception(WfImplConstants.SUB_JOURNEY_LIST_ERROR_MESSAGE);
			}
		} else {
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.SOMETHING_WRONG_MESSAGE, WfImplConstants.PROCESS_NOT_ASSINED_ERROR_MESSAGE);
		}
	}
}

