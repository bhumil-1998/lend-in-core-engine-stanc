package com.kuliza.lending.wf_implementation.services;

import com.kuliza.lending.authorization.dao.WfUserDeviceRegDao;
import com.kuliza.lending.authorization.utils.AuthenticationConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.logging.annotations.LogMethodDetails;
import com.kuliza.lending.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.pojo.UserValidationinputForm;
import com.kuliza.lending.pojo.ValidationResponse;
import com.kuliza.lending.wf_implementation.base.BaseUserValidationService;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserValidationService extends BaseUserValidationService{
	
	@Autowired
	private WorkflowUserDao workflowUserDao;
	
	@Autowired
	private WfUserDeviceRegDao wfUserDeviceRegDao;

	/**
	 * @param userForm
	 * @return
	 * @throws Exception
	 * Service to generate password
	 */
	@Override
	@LogMethodDetails
	public ApiResponse generateUserPassword(HttpServletRequest request, UserPasswordGenerationForm userForm) throws Exception {
		return super.generateUserPassword(request, userForm);
	}

	@Override
	protected ApiResponse sendOTP(String mobileNumber, String otp) {
		return null;
	}

	/**
	 * @param userForm
	 * @return
	 * Utility to create and send password
	 */
	@LogMethodDetails
	public ApiResponse generateAndSendPassword(HttpServletRequest request, UserPasswordGenerationForm userForm) {
		return null;
		//  Here, you will need to write your own otp sending code
	}

	/**
	 * @param validationForm
	 * @return apiResponse
	 * @throws Exception
	 * Service to validate and register user
	 */
	@Override
	@LogMethodDetails
	public ApiResponse validateAndRegisterUser(HttpServletRequest request,
			UserValidationinputForm validationForm, Boolean isValidatedUser)
			throws Exception {
		return super.validateAndRegisterUser(request, validationForm, isValidatedUser);
	}

	/**
	 * @param validationForm
	 * @return
	 * Function to validate user
	 */
	public ValidationResponse validateUser(HttpServletRequest request, UserValidationinputForm validationForm) {
		// Note Write validation logic here
		return new ValidationResponse();
	}

	/**
	 * @param uniqueIdentifier
	 * @return
	 * Function to validate user
	 */
	public ApiResponse validateAndRegisterUserWithoutOTP(HttpServletRequest request, String uniqueIdentifier, String identifierType) throws Exception {
		UserValidationinputForm userValidationinputForm = new UserValidationinputForm();
		userValidationinputForm.setUsername(uniqueIdentifier);
		userValidationinputForm.setIdType(AuthenticationConstants.UserIdTypes.valueOf(identifierType));
		return validateAndRegisterUser(request, userValidationinputForm, true);
	}

}
