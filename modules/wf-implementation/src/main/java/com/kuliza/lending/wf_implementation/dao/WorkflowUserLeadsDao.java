package com.kuliza.lending.wf_implementation.dao;

import com.kuliza.lending.wf_implementation.models.WfUserLeads;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkflowUserLeadsDao extends CrudRepository<WfUserLeads, Long> {
  public WfUserLeads findById(long id);

  public WfUserLeads findByIdAndExpired(long id, boolean expired);

  public WfUserLeads findByLeadIdAndExpired(String leadId , boolean expired);
  
  public List<WfUserLeads> findByUniqueIdentifierAndExpired(String uniqueIdentifier , boolean expired);

}
