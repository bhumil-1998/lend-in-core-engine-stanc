package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.journey.pojo.AddSubJourney;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.BackToJourneyInput;
import com.kuliza.lending.journey.pojo.DeleteSubJourney;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.wf_implementation.pojo.WorkflowInitiateRequest;
import com.kuliza.lending.wf_implementation.services.WorkflowService;
import com.kuliza.lending.wf_implementation.utils.SwaggerConstants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(WfImplConstants.WORKFLOW_API_ENDPOINT)
public class WorkflowController {

	@Autowired
	private WorkflowService workflowService;

	@ApiOperation(value = SwaggerConstants.WC_INITIATE)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.WC_INITIATE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_INITIATE_API_ENDPOINT)
	public ResponseEntity<Object> initiateProcess(Principal principal,
			@RequestBody @Valid WorkflowInitiateRequest wfInitiateRequest) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.startOrResumeProcess(principal.getName(), wfInitiateRequest));
	}
	
	@ApiOperation(value = SwaggerConstants.WC_INITIATE_BY_ASSIGNEE)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.WC_INITIATE_BY_ASSIGNEE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_INITIATE_BY_ASSIGNEE_API_ENDPOINT)
	public ResponseEntity<Object> initiateProcessByAssignee(Principal principal,
			@RequestBody @Valid WorkflowInitiateRequest wfInitiateRequest) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.startOrResumeProcess(wfInitiateRequest.getAssignee(), wfInitiateRequest));
	}

	@ApiOperation(value = SwaggerConstants.WF_SUBMIT_FORM)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.WF_SUBMIT_FORM_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_SUBMIT_FORM_API_ENDPOINT)
	public ResponseEntity<Object> allSubmit(Principal principal, @Valid @RequestBody SubmitFormClass input) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.submitFormData(principal.getName(), input));
	}

	@ApiOperation(value = SwaggerConstants.WC_GET_SUBMIT_FORM)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.WC_GET_SUBMIT_FORM_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = WfImplConstants.WORKFLOW_SUBMIT_FORM_API_ENDPOINT)
	public ResponseEntity<Object> getPresentState(Principal principal, @RequestParam String processInstanceId) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.getFormData(principal.getName(), processInstanceId));
	}

	@ApiOperation(value = SwaggerConstants.WC_BACK)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.WC_BACK_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_BACK_API_ENDPOINT)
	public ResponseEntity<Object> moveBack(Principal principal, @Valid @RequestBody BackTaskInput input,
			BindingResult result) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.changeProcessState(principal.getName(), input, result));
	}

	@ApiOperation(value = SwaggerConstants.LC_BACK_TO_JOURNEY)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.LC_BACK_TO_JOURNEY_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_BACK_TO_JOURNEY_API_ENDPOINT)
	public ResponseEntity<Object> backToJourney(@RequestBody @Valid BackToJourneyInput backToJourneyInput) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.setUserData(backToJourneyInput));
	}
	
	@ApiOperation(value = SwaggerConstants.WC_SUB_JOURNEY)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.WC_SUB_JOURNEY_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_ADD_SUB_JOURNEY_API_ENDPOINT)
	public ResponseEntity<Object> addSubJourney(Principal principal, @Valid @RequestBody AddSubJourney input,
			BindingResult result) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(workflowService.addSubJourney(principal.getName(), input, result));
	}
	
	@ApiOperation(value = SwaggerConstants.WF_DELETE_SUBJOURNEY)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.WF_DELETE_SUBJOURNEY_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_DELETE_SUB_JOURNEY_API_ENDPOINT)
	public ResponseEntity<Object> deleteSubJourney(Principal principal, @Valid @RequestBody DeleteSubJourney input) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(workflowService.deleteSubJourney(principal.getName(), input));
	}
	
	
}
