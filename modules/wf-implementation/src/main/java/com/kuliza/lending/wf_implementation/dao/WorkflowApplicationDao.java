package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;

@Repository
public interface WorkflowApplicationDao extends CrudRepository<WorkFlowApplication, Long> {

	public WorkFlowApplication findById(long id);

	public WorkFlowApplication findByIdAndIsDeleted(long id, boolean isDeleted);

}
