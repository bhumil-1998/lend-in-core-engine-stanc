package com.kuliza.lending.wf_implementation.models;

import com.kuliza.lending.journey.model.AbstractWfUserLeads;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "wf_user_leads")
public class WfUserLeads extends AbstractWfUserLeads {

  public WfUserLeads(String leadId, String leadProcessInstanceId,
      String uniqueIdentifier) {
    super(leadId, leadProcessInstanceId, uniqueIdentifier);
  }
  
  public WfUserLeads() {
    super();
  }
}
