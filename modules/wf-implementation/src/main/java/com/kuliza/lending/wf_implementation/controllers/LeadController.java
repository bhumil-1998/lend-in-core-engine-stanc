package com.kuliza.lending.wf_implementation.controllers;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.wf_implementation.pojo.WorkflowInitiateRequest;
import com.kuliza.lending.wf_implementation.services.LeadService;
import com.kuliza.lending.wf_implementation.utils.SwaggerConstants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(WfImplConstants.LEAD_API_ENDPOINT)
public class LeadController {

	@Autowired
	private LeadService leadService;
	
	@ApiOperation(value = SwaggerConstants.LC_PRIMARY_FORM)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.LC_PRIMARY_FORM_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = WfImplConstants.WORKFLOW_PRIMARY_FORM_API_ENDPOINT)
	public ResponseEntity<Object> getLeadForm(@RequestParam String workflowName) {
		return CommonHelperFunctions.buildResponseEntity(leadService.getLeadForm(workflowName));
	}
	
	@ApiOperation(value = SwaggerConstants.LC_INITIATE)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.LC_INITIATE_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_INITIATE_API_ENDPOINT)
	public ResponseEntity<Object> initiateLead(@RequestBody @Valid WorkflowInitiateRequest wfInitiateRequest) {
		return CommonHelperFunctions.buildResponseEntity(leadService.initiateLead(wfInitiateRequest));
	}
	
	@ApiOperation(value = SwaggerConstants.LC_SUBMIT_FORM)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.LC_SUBMIT_FORM_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_SUBMIT_FORM_API_ENDPOINT)
	public ResponseEntity<Object> allSubmit(@Valid @RequestBody SubmitFormClass input) {
		return CommonHelperFunctions.buildResponseEntity(leadService.submitLeadForm(input));
	}
	
	@ApiOperation(value = SwaggerConstants.LC_GET_SUBMITTED_FORM)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.LC_GET_SUBMITTED_FORM_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = WfImplConstants.WORKFLOW_SUBMIT_FORM_API_ENDPOINT)
	public ResponseEntity<Object> getPresentState(@RequestParam String processInstanceId) {
		return CommonHelperFunctions.buildResponseEntity(leadService.getFormData(processInstanceId));
	}
	
	@ApiOperation(value = SwaggerConstants.LC_BACK)
	@ApiResponses(value = {@io.swagger.annotations.ApiResponse(message = SwaggerConstants.LC_BACK_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_BACK_API_ENDPOINT)
	public ResponseEntity<Object> moveBack(@Valid @RequestBody BackTaskInput input, BindingResult result) {
		return CommonHelperFunctions.buildResponseEntity(leadService.changeProcessState(input, result));
	}
	
}
