package com.kuliza.lending.wf_implementation.services;

import com.kuliza.lending.logging.annotations.LogMethodDetails;

import java.util.Map;

import org.flowable.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowSource;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.pojo.WorkflowInitiateRequest;

@Service
public class InitiateWorkflowScripts {
	
	private static final Logger logger = LoggerFactory.getLogger(InitiateWorkflowScripts.class);

	@LogMethodDetails(userId = "username")
	public static void preInitiateScript(String username,
			WorkflowInitiateRequest workflowInitiateRequest,
			WorkFlowApplication wfAppObj, WorkFlowUserApplication wfUserAppObj,
			Map<String, Object> processVariables, WorkFlowSource wfSourceObj) {
		logger.info("Initiate pre-processor started");
		// Any custom code before initiating the process should come her.
		// Example - Setting additional process variables, changes in wf app, wf user app objects.
		logger.info("Initiate pre-processor ended");
	}

	@LogMethodDetails(userId = "userId")
	public static void postInitiateScript(ProcessInstance processInstance, 
			WorkFlowApplication wfAppObj, WorkFlowUserApplication wfUserAppObj,
			WorkFlowSource wfSourceObj) {
		logger.info("Initiate post-processor started");
		// Any custom code after initiating the process should come her.
		logger.info("Initiate post-processor ended");
	}
}