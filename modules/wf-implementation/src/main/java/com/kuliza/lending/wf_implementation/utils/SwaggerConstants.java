package com.kuliza.lending.wf_implementation.utils;

public class SwaggerConstants {
	
	public static final String UVC_GENERATE_PASSWORD = "To generate password";
	public static final String UVC_GENERATE_PASSWORD_RESPONSE = "{\n" + 
			"    \"data\": null,\n" + 
			"    \"message\": \"OK\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String UVC_VALIDATE_USER = "";
	public static final String UVC_VALIDATE_USER_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"access_token\": \"eyJhbGciGpFVEtFRUROQ2JIm5iZiI6MCwiaWF0IjeQ\",\n" + 
			"        \"refresh_token\": \"eyJhbGciOiJSUzI1NVEiZiI6MCwiaWFmOWA\",\n" + 
			"        \"role\": [\n" + 
			"            \"user\"\n" + 
			"        ],\n" + 
			"        \"refresh_expires_in\": 1317600,\n" + 
			"        \"groups\": [\n" + 
			"            {\n" + 
			"                \"access\": {\n" + 
			"                    \"view\": true,\n" + 
			"                    \"manage\": true,\n" + 
			"                    \"manageMembership\": true\n" + 
			"                },\n" + 
			"                \"attributes\": {},\n" + 
			"                \"clientRoles\": {},\n" + 
			"                \"id\": \"7a3e5664-de81-472f-8d3a-4b58a6a3fedc\",\n" + 
			"                \"name\": \"employees\",\n" + 
			"                \"path\": \"/employees\",\n" + 
			"                \"realmRoles\": [],\n" + 
			"                \"subGroups\": []\n" + 
			"            }\n" + 
			"        ],\n" + 
			"        \"token_type\": \"bearer\",\n" + 
			"        \"expires_in\": 1317600,\n" + 
			"        \"username\": \"workflowuser@14.com\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String UVC_SET_MPIN = "To set MPIN";
	public static final String  UVC_SET_MPIN_RESPONSE = "{\n" + 
			"    \"data\": \"MPIN Set Successfully.\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String UVC_VALIDATE_MPIN = "To validate mpin";
	public static final String UVC_VALIDATE_MPIN_RESPONSE = "{\n" + 
			"    \"data\": \"MPIN Validation Successfull.\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String LC_PRIMARY_FORM = "This is exclusively for lead journey";
	public static final String LC_PRIMARY_FORM_RESPONSE = "";
	
	public static final String LC_INITIATE = "This is to initiate workflow";
	public static final String LC_INITIATE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"65e531fd-3836-11ea-a8e9-04ea56aaf7a8\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 13,\n" + 
			"            \"applicationId\": 37\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"test1\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"taskId\": \"65e88d6a-3836-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test1\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String LC_SUBMIT_FORM = "To submit the form";
	public static final String LC_SUBMIT_FORM_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"e61cad26-1fd7-11ea-8010-02429adedeed\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 57,\n" + 
			"            \"applicationId\": 87\n" + 
			"        },\n" + 
			"        \"test3-rebuild\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": \"Shrey Bhasin 1\",\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": \"2\",\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"taskId\": \"18844f70-3843-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test3-rebuild\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String LC_GET_SUBMITTED_FORM = "To get the submitted form";
	public static final String LC_GET_SUBMITTED_FORM_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"e61cad26-1fd7-11ea-8010-02429adedeed\",\n" + 
			"        \"test2\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": \"Shrey Bhasin 1\",\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": \"2\",\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 57,\n" + 
			"            \"applicationId\": 87\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"taskId\": \"a9f5b36d-1fd8-11ea-8010-02429adedeed\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test2\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String LC_BACK = "Move to the previous process";
	public static final String LC_BACK_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"e61cad26-1fd7-11ea-8010-02429adedeed\",\n" + 
			"        \"test2\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": \"Shrey Bhasin 1\",\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": \"2\",\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 57,\n" + 
			"            \"applicationId\": 87\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"taskId\": \"96af3314-3843-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test2\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	 
	public static final String WC_INITIATE = "To initiate the workflow";
	public static final String WC_INITIATE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"11a26b56-3844-11ea-a8e9-04ea56aaf7a8\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 14,\n" + 
			"            \"applicationId\": 38\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"test1\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"taskId\": \"11a29273-3844-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test1\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String WC_INITIATE_BY_ASSIGNEE = "To initiate the workflow by assignee";
	public static final String WC_INITIATE_BY_ASSIGNEE_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"b447ba97-3845-11ea-a8e9-04ea56aaf7a8\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 15,\n" + 
			"            \"applicationId\": 39\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"test1\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"taskId\": \"b447e1b4-3845-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test1\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String WC_GET_SUBMIT_FORM = "To get the submitted form details on the basis of process instance id";
	public static final String WC_GET_SUBMIT_FORM_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"b447ba97-3845-11ea-a8e9-04ea56aaf7a8\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 15,\n" + 
			"            \"applicationId\": 39\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"test1\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"taskId\": \"b447e1b4-3845-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test1\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String WF_SUBMIT_FORM = "To submit the form";
	public static final String WF_SUBMIT_FORM_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"65e531fd-3836-11ea-a8e9-04ea56aaf7a8\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 13,\n" + 
			"            \"applicationId\": 37\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"test1\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"taskId\": \"65e88d6a-3836-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test1\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String WC_BACK = "Go back";
	public static final String WC_BACK_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"65e531fd-3836-11ea-a8e9-04ea56aaf7a8\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 13,\n" + 
			"            \"applicationId\": 37\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"test1\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"taskId\": \"65e88d6a-3836-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test1\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String LC_BACK_TO_JOURNEY = "Go back to the journey";
	public static final String LC_BACK_TO_JOURNEY_RESPONSE = "";
	
	public static final String WC_SUB_JOURNEY = "Add sub journey";
	public static final String WC_SUB_JOURNEY_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"processInstanceId\": \"65e531fd-3836-11ea-a8e9-04ea56aaf7a8\",\n" + 
			"        \"data\": {\n" + 
			"            \"applicationNumber\": 13,\n" + 
			"            \"applicationId\": 37\n" + 
			"        },\n" + 
			"        \"stepperData\": null,\n" + 
			"        \"test1\": {\n" + 
			"            \"name\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"First Name\",\n" + 
			"                \"id\": \"name\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"text\",\n" + 
			"                        \"default\": \"\",\n" + 
			"                        \"order\": 10,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            },\n" + 
			"            \"status\": {\n" + 
			"                \"isRequired\": false,\n" + 
			"                \"name\": \"Status\",\n" + 
			"                \"id\": \"status\",\n" + 
			"                \"isWritable\": true,\n" + 
			"                \"type\": \"text\",\n" + 
			"                \"params\": {\n" + 
			"                    \"meta\": {\n" + 
			"                        \"type\": \"string\",\n" + 
			"                        \"form_type\": \"hidden\",\n" + 
			"                        \"default\": \"pending\",\n" + 
			"                        \"order\": 20,\n" + 
			"                        \"required\": true,\n" + 
			"                        \"disabled\": false,\n" + 
			"                        \"placeholder\": false,\n" + 
			"                        \"api\": \"\",\n" + 
			"                        \"validator\": \"[]\"\n" + 
			"                    }\n" + 
			"                },\n" + 
			"                \"value\": null,\n" + 
			"                \"enumValues\": null\n" + 
			"            }\n" + 
			"        },\n" + 
			"        \"taskId\": \"65e88d6a-3836-11ea-a8e9-04ea56aaf7a8\"\n" + 
			"    },\n" + 
			"    \"errorMessage\": \"\",\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"screenInfo\": \"test1\",\n" + 
			"    \"screenName\": \"Test 3\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String WF_DELETE_SUBJOURNEY = "To delete a sub-journey";
	public static final String WF_DELETE_SUBJOURNEY_RESPONSE = "{\n" + 
			"	\"data\":null,\n" + 
			"	\"message\":\"SUCCESS\",\n" + 
			"	\"status\":200\n" + 
			"}";
	
	public static final String DMS_UPLOAD = "Upload";
	public static final String DMS_UPLOAD_RESPONSE = "{\n" + 
			"    \"data\": {\n" + 
			"        \"documentId\": \"64\",\n" + 
			"        \"documentName\": \"sign.jpg\"\n" + 
			"    },\n" + 
			"    \"message\": \"SUCCESS!\",\n" + 
			"    \"status\": 200\n" + 
			"}";
	
	public static final String DMS_DOWNLOAD = "Upload";
	public static final String DMS_DOWNLOAD_RESPONSE = "{\n" + 
			"    \"bytes\": \"/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAgGBg ,\n" +   
					"Content-Type\": \"image/jpeg\"\n" + 
					"}";
	
	public static final String DMS_PORTAL_DOWNLOAD = "Download by portal api point";
	public static final String DMS_PORTAL_DOWNLOAD_RESPONSE = "{\n" + 
			"    \"bytes\": \"/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAgGBg ,\n" +   
			"Content-Type\": \"image/jpeg\"\n" + 
			"}";
}
