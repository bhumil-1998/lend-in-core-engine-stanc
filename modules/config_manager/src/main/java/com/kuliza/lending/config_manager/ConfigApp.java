package com.kuliza.lending.config_manager;

import static com.kuliza.lending.common.utils.Constants.JASYPT_ENCRYPTOR_PASSWORD;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kuliza" })
@EnableJpaRepositories(basePackages = "com.kuliza")
@EnableConfigurationProperties(LiquibaseProperties.class)
@EntityScan(basePackages = "com.kuliza")
@EnableEncryptableProperties
public class ConfigApp {

	@Autowired
	private LiquibaseProperties properties;
	
	@Autowired
	@Lazy
	private DataSource dataSource;
	
	public static void main(String[] args) {
		System.setProperty("jasypt.encryptor.password", JASYPT_ENCRYPTOR_PASSWORD);
		SpringApplication.run(ConfigApp.class);
	}

	@Bean
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog(this.properties.getChangeLog());
		liquibase.setContexts(this.properties.getContexts());
		liquibase.setDefaultSchema(this.properties.getDefaultSchema());
		liquibase.setDropFirst(this.properties.isDropFirst());
		liquibase.setShouldRun(this.properties.isEnabled());
		liquibase.setLabels(this.properties.getLabels());
		liquibase.setChangeLogParameters(this.properties.getParameters());
		liquibase.setRollbackFile(this.properties.getRollbackFile());
		liquibase.setDatabaseChangeLogLockTable("LOS_DATABASECHANGELOGLOCK");
		liquibase.setDatabaseChangeLogTable("LOS_DATABASECHANGELOG");
		return liquibase;

	}
}
