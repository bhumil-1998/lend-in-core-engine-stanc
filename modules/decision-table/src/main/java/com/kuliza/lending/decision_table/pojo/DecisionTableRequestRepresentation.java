package com.kuliza.lending.decision_table.pojo;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class DecisionTableRequestRepresentation implements Serializable {

    @NotNull
    private String decisionKey;
    private Integer version;


    public String getDecisionKey() {
        return decisionKey;
    }

    public void setDecisionKey(String decisionKey) {
        this.decisionKey = decisionKey;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
